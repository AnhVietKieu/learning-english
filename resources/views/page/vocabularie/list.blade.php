@extends('admin.master')

@section('content')

    <div class="col-md-12">
        <div class="grid simple ">
            <div class="grid-title no-border">
                <h2>{{lang_data('Vocabularie')}}</h2>
            </div>
            <div class="grid-body no-border">
                <div class="row">
                    <div class="pull-left" style="margin-left: 20px;" >
                        <form method="get" action="{{route('vocabularie.search')}}">
                            <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                            <input type="text" name="search" placeholder="Tìm kiếm theo từ">

                            <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i> {{lang_data('Search')}}</button>
                        </form>
                    </div>
                    <form method="post" action="{{route('user.deleteall')}}">
                        @csrf
                        <div class="pull-left" style="margin-top:25px; margin-left: 9px;">
                            @if(menu_permission('per_delete_vocabularie')==true)
                            <button id="delete-user" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i> {{lang_data('Delete all')}}</button>
                        @endif
                                @if(menu_permission('per_create_vocabularie')==true)
                            <a class="btn btn-success" href="{{route('vocabularie.create')}}"><i class="glyphicon glyphicon-plus"></i> {{lang_data('Add')}}</a>
                            @endif
                        </div>
                </div>

                    <table class="table no-more-tables">
                        <thead>
                        <tr>
                            <th>{{lang_data('Select')}}</th>
                            <th>{{lang_data('Vocabularie')}}</th>
                            <th>{{lang_data('Word format')}}</th>
                            <th>{{lang_data('Spelling')}}</th>
                            <th>{{lang_data('Define')}}</th>
                            <th style="height: 150px;width: 150px;">{{lang_data('Image')}}</th>
                            <th>{{lang_data('Description')}}</th>
                            <th style="text-align: center;">{{lang_data('Function')}} </th>

                        </tr>
                        </thead>
                        <tbody>
                            @foreach($vocabularies as $key)
                                <tr>
                                    <td></td>
                                    <td>{{$key->word}}</td>
                                    <td>{{$key->type}}</td>
                                    <td>{{$key->pronounce}}</td>
                                    <td>{{$key->meaning}}</td>
                                    <td>
                                        @if(!empty($key->image))
                                        <img height="150px" width="150px" src="{{asset('public/upload/vocabularie/'.$key->image)}}"
                                             id="preview" class="img-thumbnail">
                                            @endif
                                    </td>
                                    <td>
                                        <div style="overflow: auto;height:150px;width: 200px;">{!! $key->description !!}</div>
                                    </td>
                                    <td class="left">
                                        @if(menu_permission('per_edit_vocabularie')==true)
                                        <a href="{{route('vocabularie.edit',$key->id)}}">
                                            <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                        </a>
                                        @endif
                                            @if(menu_permission('per_delete_vocabularie')==true)
                                        <a href="{{route('vocabularie.delete',$key->id)}}">
                                            <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                        </a>
                                                @endif

                                    </td>
                                </tr>

                                @endforeach
                        </tbody>
                    </table>
                </form>
                <div class="pagination pull-right">{!! $vocabularies->links() !!}</div>
            </div>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#delete-user').css('display','none');
            $(".delete-user").on('change', function () {
                if ($("input[name='permission_id[]']:checked").length > 1) {
                    $('#delete-user').css('display', 'inline-block');
                } else {
                    $('#delete-user').css('display', 'none');
                }
            });
        });

    </script>
@endsection
