@extends('admin.master')
@section('content')
    <div class="row">
        <div class="grid simple ">
            <h2>{{lang_data('Config')}}</h2>
            <div style="padding: 15px;">
                <div class="row">
                    @if(menu_permission('per_create_config')==true)
                    <div class="pull-left" style="margin-left: 20px;" >
                        <a class="btn btn-success" href="{{route('config.create')}}"><i class="glyphicon glyphicon-plus"></i> {{lang_data('Add')}}</a>
                    </div>
                        @endif
                </div>
                <table class="table" id="example3">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>{{lang_data('Meta key')}}</th>
                        <th>{{lang_data('Meta value')}}</th>
                        <th>{{lang_data('Action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1;?>
                    @foreach($configs as $item)
                        <tr class="odd gradeX">
                            <td>{{$i++}}</td>
                            <td>{{$item->meta_key}}</td>
                            <td>
                               {!! $item->meta_value !!}
                            </td>
                            <td class="center">
                                @if(menu_permission('per_edit_config')==true)
                                <a href="config/edit/{{$item->id}}">
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                @endif
                                    @if(menu_permission('per_delete_config')==true)
                                <a href="config/delete/{{$item->id}}">
                                    <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>
                                        @endif

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
