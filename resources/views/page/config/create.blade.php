@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple form-grid">
                <div class="grid-title no-border">
                    <h4><b>{{lang_data('Create new')}}</b> </h4>
                    <div class="alert alert-success"  id="data"></div>
                </div>
                <div class="grid-body no-border">
                    <form  id="form_traditional_validation" name="form_traditional_validation" method="post" role="form" autocomplete="off" class="validate" novalidate="novalidate">
                        @csrf
                        <div class="form-group">
                            <label class="form-label">{{lang_data('Title')}} :</label>
                            <input type="text" name="meta_key"  class="form-control"  required aria-required="true">
                            <span id="error_validate_meta_key"></span>
                        </div>
                        <div class="form-group">
                            <label class="form-label">{{lang_data('Description')}}:</label>
                            <input class="form-control"  name="meta_value"   required="" aria-required="true">


                            <span id="error_validate_meta_value"></span>
                        </div>

                        <div class="form-actions">
                            <div class="pull-right">
                                <button class="btn btn-success btn-cons update-table-config" type="submit"><i class="fa fa-check-circle"></i>{{lang_data('Create new')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#data').css('display','none');



        // Thay thế <textarea id="post_content"> với CKEditor
    </script>
@endsection
