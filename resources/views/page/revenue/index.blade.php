@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h2>Doanh Thu</h2>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <h4>Đơn Hàng Có Doanh Thu Cao Nhất: <strong><a href="" style="color: red;" data-toggle="modal" data-target="#idRevenue">{{number_format($revenue[0]['total'])}} VNĐ</a></strong></h4>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <h4>Tổng Lợi Nhuận Cửa Hàng: <strong><a style="color: red;">{{number_format($profit)}} VNĐ</a></strong></h4>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <form  method="post" role="form"  action="">
                            @csrf
                            <div class="col-md-1 col-sm-1 col-xs-1 " style="padding: 30px;">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary ">Xem</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <p><strong>Loại</strong></p>
                                        <select name="type" id="typedt" class="form-control" required="">
                                            <option value="" selected>Chọn loại </option>
                                            <option value="month">Theo tháng</option>
                                            <option value="year">Theo năm</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="timedt" style="display: none">
                                    <div class="col-md-3 col-sm-3 col-xs-3 ">
                                        <div class="form-group">
                                            <p><strong>Nhập năm</strong>
                                            </p>
                                            <input style="line-height: 10px" class=" form-control"  id="end"  name="year1"  value="" type="text" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div id="monthtk" style="display: none">
                                    <div class="col-md-2 col-sm-2 col-xs-2 ">
                                        <div class="form-group">
                                            <p><strong>Nhập năm</strong>
                                            </p>
                                            <input style="line-height: 10px"  class=" form-control" id="end"  name="year2"  value="" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-2 ">
                                        <div class="form-group">
                                            <p><strong>Chọn tháng</strong>
                                            </p>
                                            <select name="month"  class="form-control" >
                                                @for ($i = 1; $i <=12 ; $i++)
                                                    <option value="{{$i}}">Tháng {{$i}}</option>
                                                @endfor

                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="idRevenue" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"><strong>Chi Tiết Đơn Hàng</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label for="">Tên Khách Hàng:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="" class="form-label">{{$revenue[0]['name']}}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label for="">Địa Chỉ:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="" class="form-label">{{$revenue[0]['address']}}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label for="">Số Điện Thoại:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="" class="form-label">{{$revenue[0]['phone']}}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label for="">Tên Sản Phẩm:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="" class="form-label">
                                            @for($i=0;$i<count($delivery_note_details);$i++)
                                                - {{ $delivery_note_details[$i]['title'] }} <br>
                                            @endfor
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label for="">Số Lượng:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="" class="form-label">
                                            @for($i=0;$i<count($delivery_note_details);$i++)
                                                - {{ $delivery_note_details[$i]['quantity'] }} Sản Phẩm <br>
                                            @endfor</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label for="">Đơn Giá:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="" class="form-label">
                                            @for($i=0;$i<count($delivery_note_details);$i++)
                                                - {{ number_format($delivery_note_details[$i]['price']) }} VNĐ <br>
                                            @endfor
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label for="">Tổng tiền:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="" class="form-label"><strong>{{number_format($revenue[0]['total'])}} VNĐ</strong></label>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded',function() {
            document.getElementById('typedt').onchange=changedata;},false);
        function changedata(event) {

            if(event.target.value =='year')
            {
                document.getElementById("timedt").style.display = "block";

                document.getElementById("monthtk").style.display = "none";

            }else if(event.target.value =='month'){

                document.getElementById("timedt").style.display = "none";
                document.getElementById("monthtk").style.display = "block";
            }

        }
    </script>
@endsection
