@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h3>Thống Kê Doanh Thu</h3>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="" >Tổng doanh thu đạt:  <strong>{{number_format($total)}} VNĐ</strong></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="" >Tổng doanh số nhập:  <strong>{{number_format($total_receipt)}} VNĐ</strong></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div> Lợi Nhuận: <strong style="color: red;">{{number_format($profit)}} VNĐ</strong> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

