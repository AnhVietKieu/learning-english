@extends('admin.master')
@section('content')
    <div class="row">
            <div class="grid simple ">
                <div class="grid-title">
                    <h2>{{lang_data('Role')}}</h2>

                </div>
                <div class="grid-body ">
                    <div class="horizontal">
                        @if(menu_permission('per_create_role')==true)
                        <div class="pull-left">
                            <a class="btn btn-success" href="{{route('role.create')}}"><i class="glyphicon glyphicon-plus"></i> Thêm</a>
                        </div>
                            @endif
                    </div>
                    <table class="table" id="example3">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>{{lang_data('Role')}}</th>
                            <th>{{lang_data('Name role')}}</th>
                            <th>{{lang_data('Number role')}}</th>
                            <th>{{lang_data('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $item)
                        <tr class="odd gradeX">
                            <td>#{{$item->id}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->display}}</td>
                            <td>{{$item->number_role}}</td>
                            <td class="center">
                                @if(menu_permission('per_edit_role')==true)
                                <a href="role/edit/{{$item->id}}">
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                @endif

                                    @if(menu_permission('per_delete_role')==true)
                               <a href="role/delete/{{$item->id}}">
                                   <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')"><i class="glyphicon glyphicon-trash"></i></button>
                               </a>
                                    @endif

                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

    </div>
@endsection
