@extends('admin.master')

@section('content')
    <form action="{{route('role.postcreate')}}" method="post" name="permission_create">
        @csrf

        <div class="row">

            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2>{{lang_data('Create new')}}</h2>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="reload"></a>
                        </div>
                    </div>
                    <div class="grid-body no-border" >
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Name role')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " name="name" placeholder="ví dụ:role_admin or role_member" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Name')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control" name="display" placeholder="ví dụ: Admin  " required>
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('display')){{$errors->first('display')}}@endif</span>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Number role')}} :</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="number" class="form-control " name="number_role" placeholder="số mức độ của quyền" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-12">
                                        <label class="form-label">{{lang_data('Permission')}} :</label>
                                    </div>

                                </div>


                                <div class="row">
                                        <div class="col col-12">

                                                <div class="row-fluid">
                                                    @foreach($permissions as $item)
                                                        @if(count(permission_parent($item->id))>=1)

                                                    <div class="col-md-4" style="min-height: 275px;">
                                                        <div class="grid simple " id="hover">
                                                            <table class="table table-bordered no-more-tables">
                                                                <thead class="cf">
                                                                <tr>

                                                                    <th class="text-center" colspan="2" >{{$item->display}}</th>


                                                                </tr>
                                                                </thead>
                                                                <tbody>

                                                                @foreach(permission_parent($item->id) as $item1)
                                                                    <tr>
                                                                        <td><input type="checkbox" name="permission_id[]" value="{{$item1['id']}}"></td>
                                                                        <td>{{$item1['display']}}</td>

                                                                    </tr>
                                                                @endforeach

                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                        @endif

                                                    @endforeach
                                                </div>
                                        </div>
                                </div>



                                <div class="col-md-12 m-t-10 m-b-10 ">
                                    <button type="submit" class="btn btn-success pull-right" ><i class="fa fa-check"></i> {{lang_data('Create new')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $(function() {
            $("form[name='permission_create']").validate({
                rules: {
                    name: {
                        required:true
                    },
                    number_role:{
                        required:true,
                        number:true
                    },
                    display: {
                        required: true
                    }
                },
                messages: {
                    name:{
                        required:"Tên quyền không được bỏ trống"
                    },
                    number_role:{
                        required:'Mức đọ quyền không được bỏ trống',
                        number:'Mức đọ quyền không đúng định dạng'
                    },
                    display: {
                        required: "Tên hiển thị  không được bỏ trống"
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
