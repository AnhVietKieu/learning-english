@extends('admin.master')

@section('content')
    <form action="{{route('permission.postcreate')}}" method="post" name="permission_create">
        @csrf

        <div class="row">

            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>{{lang_data('Create new')}}</h4>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="reload"></a>
                        </div>
                    </div>
                    <div class="grid-body no-border" >
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Rights prefix')}} :</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <i class=""></i>
                                        <select class="form-control select2" id="cardType" name="per" data-init-plugin="select2" required>
                                            <option value="0">{{lang_data('Default')}}
                                            </option>
                                            <option value="list">{{lang_data('List')}}
                                            </option>
                                            <option value="create">
                                                {{lang_data('Add')}}
                                            </option>
                                            <option value="edit">
                                                {{lang_data('Update')}}
                                            </option>
                                            <option value="delete">
                                                {{lang_data('Delete')}}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Permission')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " name="name" placeholder="Chức năng ví dụ:role or permission" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Name permission')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control" name="display" placeholder="Ví dụ: Danh sách quyền ">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('display')){{$errors->first('display')}}@endif</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                <div class="controls col-md-3">
                                    <label class="form-label">{{lang_data('Level permission')}} :</label>
                                </div>
                                <div class="form-group col-md-9">
                                    <i class=""></i>
                                    <select class="form-control select2" id="cardType" name="parent_id" data-init-plugin="select2">
                                        <option value="0"><span>Mặc định</span>
                                        </option>
                                        @foreach($permission_fathers as $item)
                                        <option value="{{$item->id}}">
                                                {{$item->display}}
                                        </option>
                                            @endforeach
                                    </select>
                                </div>
                                </div>

                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success pull-right" ><i class="fa fa-check"></i> {{lang_data('Create new')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<script>
    $(function() {
        $("form[name='permission_create']").validate({
            rules: {
                display: {
                    required: true
                }
            },
            messages: {
                display: {
                    required: "Tên hiển thị  không được bỏ trống",
                }

            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection
