@extends('admin.master')

@section('content')

    <form action="{{route('user.create')}}" name="user_create" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2>{{lang_data('Create new')}}</h2>
                    </div>
                    <div class="grid-body no-border" >
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Name')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " name="name">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('name')){{$errors->first('name')}}@endif</span>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Email')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="email" class="form-control" name="email">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('email')){{$errors->first('email')}}@endif</span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Password')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="password" class="form-control" id="password" name="password">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('password')){{$errors->first('password')}}@endif</span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Enter the password')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="password" class="form-control" name="password_same">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('password_same')){{$errors->first('password_same')}}@endif</span>
                                    </div>

                                </div>

                                <div >
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Image')}}:</label>
                                    </div>
                                    <div class="col-md-9" style="margin-bottom: 20px;">
                                        <div class="img col-sm-4" style="display: none;margin-top:50px; ">
                                            <img src="" id="preview" class="img-thumbnail">
                                        </div>
                                        <input type="file" class="form-control file-avatar" name="avatar">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('avatar')){{$errors->first('avatar')}}@endif</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Phone')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="number" class="form-control" name="phone">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('phone')){{$errors->first('phone')}}@endif</span>
                                    </div>

                                </div>

                                <div class="form-group">
                                        <div class="controls col-md-3">
                                            <label class="form-label">{{lang_data('Address')}}:</label>
                                        </div>
                                        <div class="form-group col-md-9">
                                            <input type="text" class="form-control" name="address">
                                            <span style="color: red; margin-bottom: 2px;">@if($errors->has('address')){{$errors->first('address')}}@endif</span>
                                        </div>

                                    </div>
                                <div class="form-group">
                                            <div class="controls col-md-3">
                                                <label class="form-label">{{lang_data('Gender')}}:</label>
                                            </div>
                                            <div class="form-group col-md-9">
                                                <input type="hidden" value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                                                <select class="form-control select2" id="cardType" name="gender" data-init-plugin="select2">
                                                    <option value="1">Nam</option>
                                                    <option value="0">Nữ</option>
                                                    <option value="2">Khác</option>
                                                </select>
                                            </div>

                                        </div>
                                <div class="form-group" >
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Birthday')}}:</label>
                                    </div>
                                        <div class="form-group col-md-2">
                                            <input type="hidden" value=""  id="e12" tabindex="-1" class="select2-offscreen">
                                            <select class="form-control select2" id="cardType" name="day" data-init-plugin="select2">
                                                @for($i=1;$i<=31;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                            </select>
                                        </div>
                                    <div class="form-group col-md-3">
                                        <input type="hidden" value="" id="e12" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="cardType" name="month" data-init-plugin="select2">
                                            @for($i=1;$i<=12;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor

                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="hidden" value=""  id="e12" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="cardType" name="year" data-init-plugin="select2">
                                            @for($i=1900;$i<2050;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                <div class="controls col-md-3">
                                    <label class="form-label">{{lang_data('Role')}} :</label>
                                </div>
                                <div class="form-group col-md-9">
                                    <i class=""></i>
                                    <select class="form-control select2" id="cardType" name="role_id" data-init-plugin="select2">
                                        </option>
                                        @foreach($roles as $item)
                                            <option value="{{$item->id}}">
                                                {{$item->display}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                        <button type="submit" class="btn btn-success pull-right" ><i class="fa fa-check"></i> {{lang_data('Add')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <script>
        $(function() {
            $("form[name='user_create']").validate({
                rules: {
                    name:{
                        required:true,
                        maxlength:20
                    },
                    password: {
                        required: true,
                        minlength:6
                    },
                    password_same:{
                        required:true,
                        equalTo:'#password'
                    },
                    phone:{
                        number:true,
                        maxlength:15,
                        minlength:10
                    },
                    email: {
                        required: true,
                        email: true
                    }

                },
                messages: {
                    name:{
                        required:'Tên hiển thị không được bỏ trống',
                        maxlength: 'Tên không quá 20 kí tự '
                    },
                    avatar:{
                        required:'Ảnh hiển thị không được bỏ trống'
                    },
                   phone:{

                        max: 'Số điện thoại không quá 15 số',
                       min:'Số điện thoại không đúng',
                       number:'Không phải số điện thoại'
                    },
                    email:{
                        required:"Email không được bỏ trống",
                        email:"Email không đúng định dạng"
                    },
                    password_same:{
                        required:'Không được bỏ trống nhập lại mật khẩu',
                        equalTo: 'Mật khẩu không trùng'
                    },
                    password: {
                        required: "Mật khẩu không được bỏ trống",
                        minlength:"Mật khẩu phải lớn hơn 6 kí tự"
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });

        $('input[type="file"]').change(function(e) {
            var fileName = e.target.files[0].name;
            $("#file").val(fileName);

            var reader = new FileReader();
            reader.onload = function(e) {
                // get loaded data and render thumbnail.
                $('.img').css('display','block');
                document.getElementById("preview").src = e.target.result;
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });
    </script>
    @endsection
