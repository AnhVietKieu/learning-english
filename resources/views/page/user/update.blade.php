@extends('admin.master')

@section('content')

    <form method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2>{{lang_data('Update new')}}</h2>
                    </div>
                    <div class="grid-body no-border" >
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div >
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Image')}}:</label>
                                    </div>
                                    <div class="col-md-9" style="margin-bottom: 20px;">
                                        @if(!empty($user->avatar))
                                            <div class="img col-sm-4" style="margin-bottom: 10px;margin-top:50px;">
                                                <img src="{{asset('public/upload/user/'.$user->avatar)}}" id="preview" class="img-thumbnail">
                                            </div>
                                            @else
                                            <div class="img col-sm-4" style="display: none;">
                                                <img src="" id="preview" class="img-thumbnail">
                                            </div>
                                        @endif
                                            <input type="file" class="form-control file-avatar" name="avatar">
                                            <span style="color: red; margin-bottom: 2px;">@if($errors->has('avatar')){{$errors->first('avatar')}}@endif</span>

                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('avatar')){{$errors->first('avatar')}}@endif</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Name')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " name="name" value="{{$user->name}}">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('name')){{$errors->first('name')}}@endif</span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Phone')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="number" class="form-control" name="phone" value="{{$user->phone}}">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('phone')){{$errors->first('phone')}}@endif</span>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Address')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control" name="address" value="{{$user->address}}">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('address')){{$errors->first('address')}}@endif</span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Gender')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="cardType" name="gender" data-init-plugin="select2">
                                            <option value="1" @if($user->gender==1){{'selected'}}@endif>Nam</option>
                                            <option value="0" @if($user->gender==0){{'selected'}}@endif>Nữ</option>
                                            <option value="2" @if($user->gender==2){{'selected'}}@endif>Khác</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Birthday')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control" name="birthday" value="{{date('Y/m/d',strtotime($user->birthday))}}">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('birthday')){{$errors->first('birthday')}}@endif</span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Status')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden"  value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="cardType" name="status" data-init-plugin="select2">
                                            <option value="1" @if($user->status==1){{'selected'}}@endif><b style="color: red;">Bị khoá</b></option>
                                            <option value="0" @if($user->status==0){{'selected'}}@endif><b style="color: green">Hoạt động</b></option>
                                            <option value="2" @if($user->status==2){{'selected'}}@endif><b style="color: yellow">Không hoạt đông</b></option>
                                        </select>
                                    </div>

                                </div>
                                <div class="clear" style="margin-top:150px; "></div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Role')}} :</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <i class=""></i>
                                        <select class="form-control select2" id="cardType" name="role_id" data-init-plugin="select2">
                                            </option>
                                            @foreach($roles as $item)
                                                <option value="{{$item->id}}" @if($user->role_id==$item->id){{'selected'}}@endif>
                                                    {{$item->display}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>{{lang_data('Update new')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $('input[type="file"]').change(function(e) {
            var fileName = e.target.files[0].name;
            $("#file").val(fileName);

            var reader = new FileReader();
            reader.onload = function(e) {
                // get loaded data and render thumbnail.
                $('.img').css('display','block');
                document.getElementById("preview").src = e.target.result;
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });
    </script>
@endsection
