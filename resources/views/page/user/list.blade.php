@extends('admin.master')

@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div  class="grid-title no-border">
                        <h2>{{lang_data('User')}}</h2>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="pull-left" style="margin-left: 20px;" >
                                <form method="get" action="{{route('user.search')}}">
                                    <div class="controls col-md-12">
                                        <div class="row-fluid">
                                            <div class="col-md-6" style="margin-right: -15px;">
                                                <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                                                <div class="checkbox check-success">
                                                    <input type="text" class="form-control" name="search" placeholder="Tìm kiếm theo tên">
                                                </div>
                                            </div>
                                            <div class="col-md-4" style="margin-right: -15px;margin-left: -5px;">
                                                <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Filter')}}:</b></label>
                                                <select name="filter" id="gendericonic" class="select2 form-control select2-offscreen" data-init-plugin="select2" tabindex="-1">
                                                    <option value="email">{{lang_data('Email')}}</option>
                                                    <option value="name">{{lang_data('Name')}}</option>
                                                    <option value="phone">{{lang_data('Phone')}}</option>
                                                </select>

                                            </div>
                                            <div class="col-md-1" style="margin-left:-4px;margin-top: 24px;">

                                                <button class="btn btn-success">
                                                    <i class="glyphicon glyphicon-search"></i> {{lang_data('Search')}}</button>

                                            </div>
                                        </div>
                                    </div></form>
                            </div>
                                <form method="post" action="{{route('user.deleteall')}}">
                                    @csrf
                                    <div class="pull-right" style="margin-top:25px; margin-right: 20px;">
                                        @if(menu_permission('per_delete_employess')==true)
                                        <button id="delete-user" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i> {{lang_data('Delete all')}}</button>
                                       @endif
                                            @if(menu_permission('per_create_employess')==true)
                                        <a class="btn btn-success" href="{{route('user.create')}}"><i class="glyphicon glyphicon-plus"></i> {{lang_data('Add')}}</a>
                                   @endif
                                    </div>
                            </div>
                        </div>
                        </div>

                        <div>
                            <table class="table table-striped dataTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>STT</th>
                                    <th>{{lang_data('Name')}}</th>
                                    <th>{{lang_data('Image')}}</th>
                                    <th style="width: 50px;">{{lang_data('Email')}}</th>
                                    <th>{{lang_data('Address')}}</th>
                                    <th>{{lang_data('Birthday')}}</th>
                                    <th>{{lang_data('Gender')}}</th>
                                    <th>{{lang_data('Role')}}</th>
                                    <th>{{lang_data('Status')}}</th>
                                    <th>{{lang_data('Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $item)
                                <tr>
                                    <td><input type="checkbox" name="user_id[]" value="{{$item->id}}" class='checkbox delete-user'></td>
                                    <td>#{{$item->id}}</td>
                                    <td><img src='{{asset('public/upload/user/'.$item->avatar)}}' width="50px" height="50px"></td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->address}}</td>
                                    <td>{{$item->birthday}}</td>
                                    <td>@if($item->gender == 1)
                                            <span>Nam</span>
                                        @else
                                            <span>Nữ</span>
                                        @endif
                                    </td>
                                    <td>{{$item['role']['display']}}</td>
                                    <td style="width: 200px; text-align: center">@if($item->status ==0)
                                            <p class="alert alert-success"><span>{{lang_data('Action')}} </span></p>
                                        @elseif($item->status ==1)
                                            <p class="alert alert-danger"><span>{{lang_data('Lock')}}</span></p>
                                        @elseif($item->status ==2)
                                            <p class="alert alert-warning"><span>{{lang_data('No action')}}</span></p>
                                        @endif
                                    </td>
                                    <td class="center">
                                        @if(menu_permission('per_edit_employess')==true)
                                        <a href="user/edit/{{$item->id}}">
                                            <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                        </a>
                                        @endif
                                            @if(menu_permission('per_delete_employess')==true)
                                        <a href="user/delete/{{$item->id}}">
                                            <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                        </a>
                                                @endif

                                    </td>
                                </tr>
                                @endforeach
                                </tbody>

                            </table>
                            </form>
                            <div class="pagination pull-right">{!! $users->links() !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function () {
        $('#delete-user').css('display','none');
        $(".delete-user").on('change', function () {
            if ($("input[name='user_id[]']:checked").length > 1) {
                $('#delete-user').css('display', 'inline-block');
            } else {
                $('#delete-user').css('display', 'none');
            }
        });
    });
</script>
@endsection
