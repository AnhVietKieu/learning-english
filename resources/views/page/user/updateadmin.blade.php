@extends('admin.master')

@section('content')
<div class="container-fluid" style="background-color:#FFFFFF; border-radius: 1%;">
    <div class="alert alert-warning" id="data"></div>
    <form class="form-no-horizontal-spacing" name="user_update_admin" id="form-condensed" method="post">
        @csrf
        <div class="row column-seperation">
            <div class="col-md-12">
                <h3 class="text-center"><b>{{lang_data('Change profile')}}</b></h3>

                <div class="row form-row">

                    <div class="form-group col-md-12">
                        <label class="form-label">Tên hiển thị :</label>
                        <input type="text" class="form-control" name="name" value="{{$user->name}}" required>
                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('name')){{$errors->first('name')}}@endif</span>
                        <span id="error_validate_name"></span>
                    </div>
                </div>

                <div class="row form-row">

                    <div class="form-group col-md-12">
                        <label class="form-label">{{lang_data('User name')}}:</label>
                        <input type="email" class="form-control" name="email" value="{{$user->email}}" required>
                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('email')){{$errors->first('email')}}@endif</span>
                        <span id="error_validate_email"></span>
                    </div>
                </div>

                <div class="row form-row">

                    <div class="form-group col-md-12">
                        <label class="form-label">{{lang_data('Phone')}}:</label>
                        <input type="number" class="form-control" name="phone" value="{{$user->phone}}">
                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('phone')){{$errors->first('phone')}}@endif</span>
                        <span id="error_validate_phone"></span>
                    </div>
                </div>

                <div class="row form-row">

                    <div class="form-group col-md-12">
                        <label class="form-label">{{lang_data('Address')}}:</label>
                        <input type="text" class="form-control" name="address" value="{{$user->address}}">
                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('address')){{$errors->first('address')}}@endif</span>
                    </div>

                </div>
                <div class="row form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">{{lang_data('Gender')}}:</label>
                        <input type="hidden" value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                        <select class="form-control select2" id="cardType" name="gender" data-init-plugin="select2">
                            <option value="1" @if($user->gender==1){{'selected'}}@endif>Nam</option>
                            <option value="0" @if($user->gender==0){{'selected'}}@endif>Nữ</option>
                            <option value="2" @if($user->gender==2){{'selected'}}@endif>Khác</option>
                        </select>
                    </div>

                </div>

                <div class="pull-right" style="margin-bottom: 10px;">
                    <button class="btn btn-success  changepassword " type="submit">{{lang_data('Change profile')}}</button>
                </div>
            </div>
            <div>
            </div>
        </div>
    </form>

    <script>
        $('#data').css('display', 'none');
        $(function() {
            $("form[name='user_update_admin']").validate({
                rules: {
                    name: {
                        required: true,
                        maxlength: 20
                    },
                    phone: {
                        number: true,
                        maxlength: 15,
                        minlength: 10
                    },
                    email: {
                        required: true,
                        email: true
                    }

                },
                messages: {
                    name: {
                        required: 'Tên hiển thị không được bỏ trống',
                        maxlength: 'Tên không quá 20 kí tự '
                    },
                    phone: {

                        maxlength: 'Số điện thoại không quá 15 số',
                        minlength: 'Số điện thoại không đúng',
                        number: 'Không phải số điện thoại'
                    },
                    email: {
                        required: "Email không được bỏ trống",
                        email: "Email không đúng định dạng"
                    }

                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });

        $('.changepassword').on('click', function () {
            var name = $("input[name='name']").val();
            var email = $("input[name='email']").val();
            var phone = $("input[name='phone']").val();
            var address = $("input[name='address']").val();


            var gender = $('#cardType').val();

            if(phone.length >12)

            if (name.length ==0) {
                $('#error_validate_name').html('<span style="color: red;">Name không được để trống</span>');
            }
            if(name.match(/^\w[@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\?]/)){
                $('#error_validate_name').html('<span style="color: red;">Name không được chứa kí tự đặc biệt</span>');
            }
            if (email.length == 0) {
                $('#error_validate_email').html('<span style="color: red;">Email mới không được để trống</span>');
            }
            if (email.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) <1){
                $('#error_validate_email').html('<span style="color: red;">Định dạng email không đúng</span>');
            }
            {{--if (name.length > 0 && email.length > 0 && !name.match(/^\w[@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\?]/) && email.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {--}}
            {{--    --}}{{--$.ajax({--}}
            {{--    --}}{{--    headers: {--}}
            {{--    --}}{{--        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
            {{--    --}}{{--    },--}}
            {{--    --}}{{--    type: 'POST',--}}

            {{--    --}}{{--    url: '{{route('user.postupdateadmin')}}',--}}

            {{--    --}}{{--    data: {--}}

            {{--    --}}{{--        name:name, email: email,address:address,phone:phone,gender:gender--}}
            {{--    --}}{{--    },--}}

            {{--    --}}{{--    success: function (data) {--}}
            {{--    --}}{{--        $('#data').css('display', 'block');--}}
            {{--    --}}{{--        document.getElementById('data').innerHTML = data--}}
            {{--    --}}{{--    }--}}
            {{--    --}}{{--});--}}
            {{--}--}}
        });
    </script>
    @endsection
