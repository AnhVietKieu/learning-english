@extends('admin.master')

@section('content')
<div class="container-fluid" style="background-color:#FFFFFF; border-radius: 1%;">
    <div class="alert alert-success" id="data"></div>
    <form class="form-no-horizontal-spacing" id="form-condensed" method="post">
        @csrf
        <div class="row column-seperation">
            <div class="col-md-12">
                <h3 class="text-center"><b>{{lang_data('Change password')}} </b></h3>

                <div class="row form-row">

                    <div class="col-md-12">
                        <label>{{lang_data('Password old')}} :</label>
                        <input name="password" type="password" class="form-control" required>
                        <span id="error_validate_password"></span>
                    </div>
                </div>
                <div class="row form-row">

                    <div class="col-md-12">
                        <label>{{lang_data('Password new')}} :</label>
                        <input name="password_new" type="password" class="form-control" required>
                        <span id="error_validate_password_new"></span>
                    </div>
                </div>


                <div class="row form-row">
                    <div class="col-md-12">
                        <label>{{lang_data('Enter the password')}} :</label>
                        <input name="same_password_new" type="password" class="form-control" value="" required>
                        <span id="error_validate_password_new_same"></span>
                    </div>
                </div>

                <div class="pull-left" style="margin-bottom: 10px;">
                    <button class="btn btn-success changepassword" type="button">{{lang_data('Change password')}}</button>
                </div>
            </div>
            <div>
            </div>
        </div>
    </form>

    <script>
        $('#data').css('display', 'none');

        $('.changepassword').on('click', function () {
            var password = $("input[name='password']").val();
            var password_new = $("input[name='password_new']").val();
            var password_same = $("input[name='same_password_new']").val();


            if (password.length < 1) {
                $('#error_validate_password').html('<span style="color: red;">Mật khẩu cũ không được để trống</span>');
            }
            if (password_new.length < 1) {
                $('#error_validate_password_new').html('<span style="color: red;">Mật khẩu mới không được để trống</span>');
            }
            if (password_same.length < 1) {
                $('#error_validate_password_new_same').html('<span style="color: red;">Xác nhận mật khẩu mới không được để trống</span>');
            } else {
                if (password_same !== password_new) {
                    $('#error_validate_password_new_same').html('<span style="color: red;">Xác nhận mật khẩu mới không đúng </span>');
                }
            }
            if (password.length > 0 && password_new.length > 0 && password_same.length > 0 && password_new === password_same) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',

                    url: '{{route('user.postchangepassword')}}',

                    data: {

                        password: password, password_new: password_new,
                    },

                    success: function (data) {
                        $('#data').css('display', 'block');
                        document.getElementById('data').innerHTML = data
                    }
                });
            }
        });
    </script>
    @endsection
