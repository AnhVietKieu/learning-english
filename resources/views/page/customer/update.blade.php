@extends('admin.master')

@section('content')

    <form  name="customer_update" method="post">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2>{{lang_data('Update new')}}</h2>
                    </div>
                    <div class="grid-body no-border" >
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Name')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " name="name" value="{{$customer->name}}">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('name')){{$errors->first('name')}}@endif</span>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Email')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="email" class="form-control" name="email" value="{{$customer->email}}" >
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('email')){{$errors->first('email')}}@endif</span>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Address')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control" name="address" value="{{$customer->address}}">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('address')){{$errors->first('address')}}@endif</span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Phone')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="number" class="form-control" name="phone" value="{{$customer->phone}}">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('phone')){{$errors->first('phone')}}@endif</span>
                                    </div>

                                </div>



                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Birthday')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <div class="form-group">
                                            <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
 <span class="input-group-addon" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                                         <span class="fa fa-calendar"></span></span>
                                                <input type="text" name="birthday" value="{{date('Y/m/d',strtotime($customer->birthday))}}" class="form-control datetimepicker-input" data-target="#datetimepicker1" >

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Gender')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="cardType" name="gender" data-init-plugin="select2">
                                            <option value="1" @if($customer->gender==1){{'selected'}}@endif>Nam</option>
                                            <option value="0" @if($customer->gender==0){{'selected'}}@endif>Nữ</option>
                                            <option value="2" @if($customer->gender==2){{'selected'}}@endif>Khác</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Status')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden"  value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="cardType" name="status" data-init-plugin="select2">
                                            <option value="1" @if($customer->status==1){{'selected'}}@endif><b style="color: red;">Bị khoá</b></option>
                                            <option value="0" @if($customer->status==0){{'selected'}}@endif><b style="color: green">Hoạt động</b></option>
                                            <option value="2" @if($customer->status==2){{'selected'}}@endif><b style="color: yellow">Không hoạt đông</b></option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i> {{lang_data('Update new')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $(function() {
            $("form[name='customer_update']").validate({
                rules: {
                    name:{
                        required:true,
                        maxlength:20
                    },
                    password: {
                        required: true,
                        minlength:6
                    },
                    password_same:{
                        required:true,
                        equalTo:'#password'
                    },
                    phone:{
                        number:true,
                        maxlength:15,
                        minlength:10
                    }

                },
                messages: {
                    name:{
                        required:'Tên hiển thị không được bỏ trống',
                        maxlength: 'Tên không quá 20 kí tự '
                    },
                    avatar:{
                        required:'Ảnh hiển thị không được bỏ trống'
                    },
                    phone:{

                        max: 'Số điện thoại không quá 15 số',
                        min:'Số điện thoại không đúng',
                        number:'Không phải số điện thoại'
                    },

                    password_same:{
                        required:'Không được bỏ trống nhập lại mật khẩu',
                        equalTo: 'Mật khẩu không trùng'
                    },
                    password: {
                        required: "Mật khẩu không được bỏ trống",
                        minlength:"Mật khẩu phải lớn hơn 6 kí tự"
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });

        $('input[type="file"]').change(function(e) {
            var fileName = e.target.files[0].name;
            $("#file").val(fileName);

            var reader = new FileReader();
            reader.onload = function(e) {
                // get loaded data and render thumbnail.
                $('.img').css('display','block');
                document.getElementById("preview").src = e.target.result;
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });
    </script>
@endsection
