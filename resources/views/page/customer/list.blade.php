@extends('admin.master')

@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div  class="grid-title no-border">
                        <h2>{{lang_data('List Customer')}}</h2>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="pull-left" style="margin-left: 20px;" >
                                <form method="get" action="{{route('customer.search')}}">
                                    <div class="controls col-md-12">
                                        <div class="row-fluid">
                                            <div class="col-md-6" style="margin-right: -15px;">
                                                <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                                                <div class="checkbox check-success">
                                                    <input type="text" class="form-control" name="search" placeholder="Tìm kiếm theo tên">
                                                </div>
                                            </div>
                                            <div class="col-md-4" style="margin-right: -15px;margin-left: -5px;">
                                                <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Filter')}}:</b></label>
                                                <select name="filter" id="gendericonic" class="select2 form-control select2-offscreen" data-init-plugin="select2" tabindex="-1">
                                                    <option value="email">{{lang_data('Email')}}</option>
                                                    <option value="name">{{lang_data('Name')}}</option>
                                                    <option value="phone">{{lang_data('Phone')}}</option>
                                                </select>

                                            </div>
                                            <div class="col-md-1" style="margin-left:-4px;margin-top: 24px;">

                                                <button class="btn btn-success">
                                                    <i class="glyphicon glyphicon-search"></i> {{lang_data('Search')}}</button>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <form method="post" action="{{route('customer.deleteall')}}">
                                @csrf
                                <div class="pull-right" style="margin-top:25px; margin-right: 20px;">
                                    @if(menu_permission('per_delete_customer')==true)
                                    <button id="delete-customer" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i> Xoá toàn bộ</button>
                                    @endif
                                        @if(menu_permission('per_create_customer')==true)
                                    <a class="btn btn-success " href="{{route('customer.create')}}"><i class="glyphicon glyphicon-plus"></i> Thêm</a>
                                        @endif
                                </div>
                        </div>
                    </div>
                </div>

                <div>
                    <table class="table table-striped dataTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>STT</th>
                            <th>{{lang_data('Name')}}</th>
                            <th>{{lang_data('Image')}}</th>
                            <th>{{lang_data('Email')}}</th>
                            <th>{{lang_data('Phone')}}</th>
                            <th>{{lang_data('Birthday')}}</th>
                            <th>{{lang_data('Gender')}}</th>
                            <th>{{lang_data('Level')}}</th>
                            <th>{{lang_data('Notification')}}</th>
                            <th>{{lang_data('Status')}}</th>
                            <th>{{lang_data('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $item)
                            <tr>
                                <td><input type="checkbox" name="customer_id[]" value="{{$item->id}}" class='checkbox delete-customer'></td>
                                <td style="text-decoration: underline;"><a href="customer/perview/{{$item->id}}">  #{{$item->id}}</a>
                                  </td>
                                <td>{{$item->username}}</td>
                                <td>@if(isset($item['avatar']))<img src='{{asset('public/upload/customer/'.$item->avatar)}}' width="50px" height="50px">@endif</td>
                                <td>{{$item->email}}</td>

                                <td>{{$item->phone}}</td>
                                <td>{{date('Y/m/d',strtotime($item->birthday))}}</td>
                                <td>@if($item->gender == 1)
                                        <span>Nam</span>
                                    @else
                                        <span>Nữ</span>
                                    @endif
                                </td>
                                <td style="width: 200px; text-align: center">
                                    @if(isset($item->levels['name']))
                                    <img src='{{asset('public/upload/level/'.$item->levels['image'])}}' width="50px" height="50px">
                                    @endif
                                </td>
                                <td>
                                    @if(menu_permission('per_edit_customer')==true)
                                        <label class="switch">
                                            <input type="checkbox" class="update-checkbox"  @if($item->status_notification==0){{'checked'}}@endif name="enable" data-customer-id="{{$item->id}}" >
                                            <span class="slider round"></span>
                                        </label>
                                    @endif
                                </td>

                                <td style="width: 20px; text-align: center">@if($item->status ==0)
                                        <p class="alert alert-success"><span>{{lang_data('Action')}} </span></p>
                                    @elseif($item->status ==1)
                                        <p class="alert alert-danger"><span>{{lang_data('Lock')}}</span></p>
                                    @elseif($item->status ==2)
                                        <p class="alert alert-warning"><span>{{lang_data('No action')}}</span></p>
                                    @endif
                                </td>
                                <td class="center">
                                    @if(menu_permission('per_edit_customer')==true)
                                    <a href="customer/edit/{{$item->id}}">
                                        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                    </a>
                                    @endif
                                    @if(menu_permission('per_delete_customer')==true)
                                    <a href="customer/delete/{{$item->id}}">
                                        <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                    </a>
                                        @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </form>
                    <div class="pagination pull-right">{!! $customers->links() !!}</div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#delete-customer').css('display','none');
            $(".delete-customer").on('change', function () {
                if ($("input[name='customer_id[]']:checked").length > 1) {
                    $('#delete-customer').css('display', 'inline-block');
                } else {
                    $('#delete-customer').css('display', 'none');
                }
            });

            $('.update-checkbox').on('click',function(){
                var customer_id = $(this).attr('data-customer-id');
                if(this.checked){
                    var enable=1;
                }else{
                    var enable=0;
                }
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',

                    url: '{{route('customer.status_notification')}}',

                    data: {

                        enable:enable,customer_id:customer_id,
                    },

                    success: function (data) {
                        Swal.fire({
                            title: data
                        })
                    }
                });
            });

        });
    </script>
@endsection
