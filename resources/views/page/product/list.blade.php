@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div  class="grid-title no-border">
                    <h2>Danh Sách Sản Phẩm</h2>
                </div>
                <div>
                    <div class="horizontal">
                        <div class="pull-left">
{{--                            <button id="delete-user" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i> Xoá toàn bộ</button>--}}
                            <a class="btn btn-success" href="{{route('product.create')}}"><i class="glyphicon glyphicon-plus"></i> Thêm</a>
                        </div>
                    </div>
                    <table class="table">
            <thead>
            <tr>
                <th>STT</th>
                <th>Sản Phẩm</th>
                <th>Danh Mục</th>
                <th>Giá gốc</th>
                <th>Màu sắc</th>
                <th width="150px;">Kích Thước</th>
                <th width="250px;">Số Lượng</th>
                <th>Người Tạo</th>
                <th>Giảm giá</th>
                <th>Trạng Thái</th>
                <th>Hành Động</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $item)
            <tr class="odd gradeX">
                <td>{{$item->id}}</td>
                <td>{{$item->title}}</td>
                <td>{{$item->category_name}}</td>
                <td>{{number_format($item->price,'0','.',',')}}</td>
                <td>
                    @if(isset($color[$item->id]))
                    @for($i=0;$i<count($color[$item->id]);$i++)
                        @if(isset($color[$item->id]))
                        <p style="background-color:{{$color[$item->id][$i]['color']}}; border-radius: 50%; width: 50px;height: 50px;">
                        </p>
                        @endif
                    @endfor
                        @endif
                </td>
                <td>
                    @if(!empty($color[$item->id]))
                    @for($i=0;$i<count($color[$item->id]);$i++)
                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                    [ -
                        @if(!empty($size[$item->id][$color_id]))
                       @for($j=0;$j<count($size[$item->id][$color_id]);$j++)
                           @if(isset($size[$item->id][$color_id][$j]))
                           {{$size[$item->id][$color_id][$j] }}-
                                    @endif
                           @endfor
                    ]&nbsp &nbsp
                       @endif
                    @endfor
                      @endif
                </td>
                <td>
                    @if(!empty($color[$item->id]))
                    @for($i=0;$i<count($color[$item->id]);$i++)
                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                        [#
                        @if(!empty($size[$item->id][$color_id]))

                            @for($j=0;$j<count($size[$item->id][$color_id]);$j++)
                                <?php $size_id =$size[$item->id][$color_id][$j] ?>
                                [ -
                                @if(!empty($quantity[$item->id][$color_id][$size_id]))
                                    @for($k=0;$k<count($quantity[$item->id][$color_id][$size_id]);$k++)
                                        @if(isset($quantity[$item->id][$color_id][$size_id][$k]))
                                        {{$quantity[$item->id][$color_id][$size_id][$k]}} -
                                            @endif
                                        @endfor
                                    ],
                                    @endif

                            @endfor

                            @endif
                         #]&nbsp &nbsp
                    @endfor
                    @endif
                </td>
                <td>{{$item->user_name}}</td>
                <td>{{$item->sale_off}}</td>
                <td>
                    @if($item->status ==0)
                        <p class="alert alert-success"><span>Hoạt động</span></p>
                    @elseif($item->status ==1)
                        <p class="alert alert-warning"><span>Khoá</span></p>
                    @endif</td>
                <div class="modal fade" id="detailProduct{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <strong><h3 class="modal-title" id="exampleModalLongTitle">Chi Tiết Sản Phẩm</h3></strong>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label for="">Tên Sản Phẩm:</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <label for="" class="form-label"><b>{{$item->title}}</b></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label for="">Danh Mục:</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <label for="" class="form-label"><b>{{$item->category_name}}</b></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label for="">Giá Bán Đã Giảm Giá:</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <label for="" class="form-label"><b>
                                                            {{number_format($item->price -($item->price*($item->sale_off/100)),'0','.',',')}} VNĐ
                                                        </b></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <label for="">Màu Sắc:</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <label for="" class="form-label">
                                                        @if(isset($color[$item->id]))
                                                        @for($i=0;$i<count($color[$item->id]);$i++)
                                                            @if(isset($color[$item->id]))
                                                            <p style="background-color:{{$color[$item->id][$i]['color']}}; border-radius: 50%; width: 50px;height: 50px;">
                                                            </p>
                                                            @endif
                                                        @endfor
                                                            @endif
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <label for="">Kích Thước:</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <label for="" class="form-label">
                                                        @if(!empty($color[$item->id]))
                                                            @for($i=0;$i<count($color[$item->id]);$i++)
                                                                <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                [ -
                                                                @if(!empty($size[$item->id][$color_id]))
                                                                    @for($j=0;$j<count($size[$item->id][$color_id]);$j++)
                                                                        @if(isset($size[$item->id][$color_id][$j]))
                                                                        <b>{{$size[$item->id][$color_id][$j] }}</b> -
                                                                            @endif
                                                                    @endfor
                                                                    ]&nbsp &nbsp
                                                                @endif
                                                            @endfor
                                                        @endif
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <label for="">Số Lượng Nhập:</label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="" class="form-label">
                                                        @if(!empty($color[$item->id]))
                                                            <?php $soluong=0;?>
                                                            @for($i=0;$i<count($color[$item->id]);$i++)
                                                                <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                [#
                                                                @if(!empty($size[$item->id][$color_id]))

                                                                    @for($j=0;$j<count($size[$item->id][$color_id]);$j++)
                                                                        <?php $size_id =$size[$item->id][$color_id][$j] ?>
                                                                        [ -
                                                                        @if(!empty($quantity[$item->id][$color_id][$size_id]))

                                                                            @for($k=0;$k<count($quantity[$item->id][$color_id][$size_id]);$k++)
                                                                                @if(isset($quantity[$item->id][$color_id][$size_id][$k]))
                                                                                <?php $soluong+= $quantity[$item->id][$color_id][$size_id][$k]; ?>
                                                                                        <b>{{$quantity[$item->id][$color_id][$size_id][$k]}}</b> -
                                                                                @endif
                                                                            @endfor
                                                                            ],
                                                                        @endif

                                                                    @endfor

                                                                @endif
                                                                #]&nbsp &nbsp
                                                            @endfor
                                                        @endif
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="">Tổng lượng sản phẩm:
                                                        @if(isset($soluong))
                                                            <b style="color: red;">{{$soluong}}</b>
                                                        @endif
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <label for="">Thông Tin Sản Phẩm:</label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="" class="form-label"><b>{!!  $item->description!!}</b></label>
                                                </div>
                                            </div>

{{--                                            <div class="form-group">--}}
{{--                                                <div class="col-md-9">--}}
{{--                                                    <h3>Hình Ảnh</h3>--}}
{{--                                                </div>--}}

{{--                                                    <div class="row" style="width: 100%;display: inline-block;border: 2px dashed #0087F7;">--}}
{{--                                                        @if(!empty($photo[$item->id]))--}}
{{--                                                            @foreach($photo[$item->id] as $key => $value)--}}
{{--                                                                @if(!empty($value))--}}
{{--                                                                    @for($i=0;$i<count($value);$i++)--}}
{{--                                                                        <img src="{{asset('public/upload/'.$photo[$item->id][$key][$i])}}" width="100px" height="100px">--}}
{{--                                                                    @endfor--}}
{{--                                                                @endif--}}
{{--                                                            @endforeach--}}
{{--                                                        @endif--}}
{{--                                                    </div>--}}

{{--                                            </div>--}}

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <td class="center">
                    <button class="btn btn-primary" title="Chi Tiết" data-toggle="modal" data-target="#detailProduct{{$item->id}}"><i class="glyphicon glyphicon-eye-open"></i></button>
                    <a href="product/edit/{{$item->id}}">
                        <button class="btn btn-success" title="Sửa"><i class="glyphicon glyphicon-edit"></i></button>
                    </a>
                    <a href="product/delete/{{$item->id}}">
                        <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')" title="Xóa"><i class="glyphicon glyphicon-trash"></i></button>
                    </a>
                </td>
            </tr>
            <?php $soluong=0;?>
            @endforeach
            </tbody>
        </table>

                    <div class="pagination-button pull-right">{!! $products->links() !!}</div>
    </div>
            </div>
        </div>
    </div>

    <!-- Modal -->


@endsection

