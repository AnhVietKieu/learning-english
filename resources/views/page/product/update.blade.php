@extends('admin.master')

@section('content')

    <form action="" method="post">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2>Sửa sản phẩm</h2>
                    </div>
                    <div class="grid-body no-border">
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Tên Sản Phẩm:</label>
                                    </div>
                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " value="{{$product->title}}" name="title">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('title')){{$errors->first('title')}}@endif</span>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Danh Mục Sản Phẩm:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <select class="form-control select2" id="cardType" name="category_id" data-init-plugin="select2">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}" @if($product->category_id == $category->id){{'selectd'}}@endif>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Giá:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="number" class="form-control" value="{{$product->price}}" name="price">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('price')){{$errors->first('price')}}@endif</span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Giá Khuyến Mãi:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="number" class="form-control" value="{{$product->sale_off}}" name="sale_off" max="100" min="0">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('sale_off')){{$errors->first('sale_off')}}@endif</span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Màu Sắc:</label>
                                    </div>
                                    <div class="controls col-md-9">
                                        <div class="row-fluid">

                                            @foreach($colors as $item1)

                                                <div class="col-md-6">
                                                    <div class="checkbox check-success">
                                                        <input id="checkbox{{$item1->id}}"
                                                               @if(isset($color))
                                                               @for($i=0;$i<count($color);$i++)
                                                               @foreach($color[$i] as $item2)

                                                               @if($item2 == $item1->id){{"checked"}} @endif

                                                               @endforeach
                                                               @endfor
                                                                   @endif
                                                               type="checkbox"  data-name-color="{{$item1->color}}" class="update-color" name="color_id[]" value="{{$item1->color}}">
                                                        <label for="checkbox{{$item1->id}}">{{$item1->color}}</label>
                                                        <p style="background:{{$item1->color}} ; width: 40px;height: 40px;border-radius:50%"></p>
                                                    </div>
                                                </div>

                                            @endforeach

                                        </div>
                                    </div>
                                </div>

                                <div class="data_color">
                                    <?php $i =0; $j=0;?>
                                    @if(isset($size))
                                        @foreach($size as $color_id => $color_name)


                                            <div class="{{$color_id}}">
                                                <div class="form-group">
                                                    <div class="controls col-md-3">
                                                        <label class="form-label">Kích Thước {{$color_id}}:</label>
                                                    </div>
                                                    <div class="controls col-md-9">
                                                        <div class="row-fluid">
                                                            @foreach($sizes as $item1)
                                                                <?php $i++?>
                                                                <div class="col-md-6">
                                                                    <div class="checkbox check-success">

                                                                        <input id="checkbox{{$color_id}}{{$item1->id}}" type="checkbox"
                                                                               @for($i=0;$i<count($color_name);$i++)
                                                                               @if($color_name[$i] == $item1->size)
                                                                               {{'checked'}}
                                                                               @endif
                                                                               @endfor
                                                                               class="update-size"  data-name-color="{{$color_id}}" name="size_id[{{$color_id}}][]" value="{{$item1->size}}">
                                                                        <label for="checkbox{{$color_id}}{{$item1->id}}">{{$item1->size}}</label>
                                                                    </div>
                                                                </div>

                                                            @endforeach
                                                                <div class="demsize"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @foreach($size_new[$color_id] as $item_size)
                                                    <div class="data-size{{$color_id}}{{$item_size}}">
                                                    </div>

                                                @endforeach

                                                    @if(isset($quantity[$color_id]))
                                                        @foreach($quantity[$color_id] as $key => $item)
                                                        <?php $j++?>
                                                            @foreach($item as $size =>$item1)


                                                                <div class="{{$color_id}}{{$size}}">


                                                                    <div class="demquantity"></div>
                                                                    <div class="form-group">
                                                                        <div class="controls col-md-3">
                                                                            <label class="form-label">Số lượng của {{$size}}:</label>
                                                                        </div>
                                                                        <div class="form-group col-md-9">
                                                                            <input type="number" class="form-control" value="{{$item1[0]}}" name="quantity[{{$color_id}}][{{$size}}]">

                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            @endforeach
                                                        @endforeach
                                                    @endif


                                            </div>

                                        @endforeach
                                        @endif

                                </div>

                                <div class="data-color1"></div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Mô tả:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <textarea class="form-control" name="description" id="editor1">
                                            {{$product->description}}
                                        </textarea>
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('description')){{$errors->first('description')}}@endif</span>
                                    </div>

                                </div>
                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Cập nhật
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>


        $(document).ready(function () {
            $("form[name='product_create']").validate({
                rules: {
                    title:{
                        required:true,
                        maxlength:20
                    },
                    price: {
                        required: true,
                        number:true,
                    },
                    sale_off:{
                        number:true,
                        minlength:0,
                        maxlength:100
                    },
                    description: {
                        required: true
                    }

                },
                messages: {
                    title:{
                        required:'Tên hiển thị không được bỏ trống',
                        maxlength: 'Tên không quá 20 kí tự '
                    },
                    price:{
                        required:'Ảnh hiển thị không được bỏ trống',
                        number:'Giá phải là số'
                    },
                    sale_off:{
                        max: 'Không quá 100',
                        number:'Giá giảm giá phải là số',
                        min:'Không được nhỏ hơn 0'
                    },
                    description: {
                        required: "Mô tả không được bỏ trống"
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
        $(".update-size").on('change', function () {

                var size_name = $(this).val();
                var color_name = $(this).attr('data-name-color');
                var demsize = document.getElementsByClassName('demsize').length;
                var demquantity = document.getElementsByClassName('demquantity').length;
                var demclasssize = document.getElementsByClassName(color_name+''+size_name).length;

                var $html1 = '<div class="'+color_name+''+size_name;
                $html1+='"><div class="demquantity"></div><div class="form-group"><div class="controls col-md-3"><label class="form-label">';
                $html1+='Số lượng của size :'+size_name;
                $html1+='</label>\n' +
                    '                                         </div>\n' +
                    '                                    <div class="form-group col-md-9">\n' +
                    '                                        <input type="number" class="form-control" name="quantity['+color_name+']['+size_name;
                $html1+=']">\n' +
                    '                                        <span style="color: red; margin-bottom: 2px;"></span>\n' +
                    '                                        </div>\n' +
                    '                                </div></div>';
             $('.data-size'+color_name+size_name).append($html1);

                if(demquantity%2 ==0)
                {
                    $('.data-size'+color_name+demsize+demquantity).append($html1);
                }else{
                    if(demsize%2 !=0)
                    {
                        $('.data-size'+color_name+demsize).append($html1);

                    }else{
                        $('.data-size'+color_name).append($html1);
                    }
                }


                if(demclasssize>0)
                {
                    $('.'+color_name+''+size_name).remove();
                }

        });



            $(".update-color").on('change', function () {
                var demsize = document.getElementsByClassName('demsize').length;
                var demquantity = document.getElementsByClassName('demquantity').length;

                var size = $(this).val();
                var size_name = $(this).attr('data-name-size');

                var color_name = $(this).attr('data-name-color');

                var demclass = document.getElementsByClassName(color_name).length;

                var $html = '<div class="'+color_name;
                $html += '"> <div class="form-group"><div class="controls col-md-3"><label class="form-label">';
                $html += 'Kích Thước: ' + color_name;
                $html += '</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="controls col-md-9"><div class="row-fluid">\n' +
                    '                                            @foreach($sizes as $size)\n' +
                    '                                               <div class="col-md-6">\n' +
                    '                                                   <div class="checkbox check-success">\n' +
                    '                                                       <input data-name-size="{{$size->size}}" id="checkbox{{$size->size.'+'.$size->id.'+'}}'+color_name;
                $html+='" class ="update-size'+color_name;
                $html+='" type="checkbox" value="{{$size->size}}" name="size_id['+color_name;
                $html+='][]" ><label for="checkbox{{$size->size.'+'.$size->id.'+'}}'+color_name;
                $html+='">{{$size->size}}</label>\n' +
                    '                                                   </div>\n' +
                    '                                               </div>\n' +
                    '                                            @endforeach\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </div><div class="demsize"></div><div class="data-size';


                if(demquantity%2 ==0)
                {
                    $html +=color_name+demsize+demquantity+'"></div>';
                }else{
                    if(demsize%2 != 0)
                    {
                        $html +=color_name+demsize+'"></div>';
                    }else{
                        $html +=color_name+'"></div></div>';
                    }
                }

                $('.data-color1').append($html);
                if(demclass>0)
                {
                    $('.'+color_name).remove();
                }

                $(".update-size"+color_name).on('change', function () {

                    if ($("input[name='size_id[]']:checked")) {

                        var size = $(this).val();
                        var size_name = $(this).attr('data-name-size');

                        var demclasssize = document.getElementsByClassName(color_name+''+size_name).length;

                        var $html1 = '<div class="'+color_name+''+size_name;
                        $html1+='"><div class="demquantity"></div><div class="form-group"><div class="controls col-md-3"><label class="form-label">';
                        $html1+='Số lượng của size :'+size_name;
                        $html1+='</label>\n' +
                            '                                         </div>\n' +
                            '                                    <div class="form-group col-md-9">\n' +
                            '                                        <input type="number" class="form-control" name="quantity['+color_name+']['+size_name;
                        $html1+=']">\n' +
                            '                                        <span style="color: red; margin-bottom: 2px;"></span>\n' +
                            '                                        </div>\n' +
                            '                                </div></div>';

                        if(demquantity%2 ==0)
                        {
                            $('.data-size'+color_name+demsize+demquantity).append($html1);
                        }else{
                            if(demsize%2 !=0)
                            {
                                $('.data-size'+color_name+demsize).append($html1);

                            }else{
                                $('.data-size'+color_name).append($html1);
                            }
                        }

                        if(demclasssize>0)
                        {
                            $('.'+color_name+''+size_name).remove();
                        }
                    }
                });

            });
    </script>
@endsection
