@extends('admin.master')
@section('content')
<div class="page-title">
    <h3>Photo - <span class="semi-bold">Gallery</span></h3>
</div>
<div >
{{--    {{asset('public/assets/plugins/jquery-superbox/img/superbox/superbox-thumb-1.jpg')}}--}}
    <div class="row">
        @foreach($photos as $item)
        <div class="col-md-3">
            <div class="thumbnail">
                <img src="{{asset('public/upload/'.$item->photo)}}"  alt="" >
            </div>
        </div>
        @endforeach
    </div>
    </div>
    <div class="paginate_button">{!! $photos->links() !!}</div>
@endsection
