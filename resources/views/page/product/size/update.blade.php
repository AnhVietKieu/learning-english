@extends('admin.master')

@section('content')
    <form action="" method="post" name="size_create">
        @csrf

        <div class="row">

            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>Sửa kích thước</h4>
                    </div>
                    <div class="grid-body no-border" >
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Tên quyền hạn:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="number" class="form-control " name="size" value="{{$size->size}}" placeholder="" >
                                    </div>
                                </div>
                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>Cập nhập</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $(function() {
            $("form[name='size_create']").validate({
                rules: {
                    size: {
                        required: true,
                        number:true
                    }
                },
                messages: {
                    size: {
                        required: "Kích thước không được bỏ trống",
                        number:"Không phải số"
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
