@extends('admin.master')
@section('content')
    <div class="row">
        <div class="grid simple ">
            <h2>Quản lý kích thước </h2>
            <div style="padding: 15px;">
                <div class="row">
                    <div class="pull-left" style="margin-left: 20px;" >
                        <form method="post" name="size_create" action="{{route('size.create')}}">
                            @csrf
                            <label><b style="text-decoration: underline;color: #0d0f12;">Kích thước:</b></label>
                            <input type="text" name="size" placeholder="Kích thước">
                            <span style="color: red; margin-bottom: 2px;">@if($errors->has('size')){{$errors->first('size')}}@endif</span>
                            <button class="btn btn-success"><i class="fa fa-check"></i> Thêm</button>
                        </form>
                    </div>
                </div>
                <table class="table" id="example3">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Kích thước</th>
                        <th style="text-align: center">Trạng thái</th>
                        <th>Chức năng </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sizes as $item)
                        <tr class="odd gradeX">
                            <td>#{{$item->id}}</td>
                            <td>{{$item->size}}</td>
                           <td style="width: 200px; text-align: center">@if($item->status ==0)
                                    <p class="alert alert-success"><span>Bình thường</span></p>
                                @elseif($item->status ==1)
                                    <p class="alert alert-warning"><span>Khoá</span></p>
                                @endif
                          </td>
                            <td class="center">
                                <a href="size/edit/{{$item->id}}">
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                <a href="size/delete/{{$item->id}}">
                                    <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <script>
        $(function() {
            $("form[name='size_create']").validate({
                rules: {
                    size: {
                        required: true,
                        number:true
                    }
                },
                messages: {
                    size: {
                        required: "Kích thước không được bỏ trống",
                        number:"Không phải số"
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
