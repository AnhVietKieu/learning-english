@extends('admin.master')
@section('content')
    <div class="col-md-12">
        <div class="grid simple ">
            <div class="grid-title no-border">
                <h2>Màu sắc</h2>
            </div>
            <div class="grid-body no-border">
                <div class="pull-left" style="margin-left: 20px;" >
                    <form method="post" name="color_create" action="{{route('color.create')}}">
                        @csrf
                        <label><b style="text-decoration: underline;color: #0d0f12;">Màu sắc:</b></label>
                        <input type="text" name="color" placeholder="Kích thước">
                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('color')){{$errors->first('color')}}@endif</span>
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Thêm</button>
                    </form>
                </div>
                    <table class="table no-more-tables">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên màu sắc</th>
                            <th>Màu sắc</th>
                            <th style="text-align: center">Trạng thái</th>
                            <th>Chức năng </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($colors as $item)
                            <tr class="odd gradeX">
                                <td>#{{$item->id}}</td>
                                <td>{{$item->color}}</td>
                                <td>
                                    <p style="background-color:{{$item->color}}; border-radius: 50%; width: 50px;height: 50px;">
                                    </p></td>
                                <td style="width: 200px; text-align: center">@if($item->status ==0)
                                        <p class="alert alert-success"><span>Bình thường</span></p>
                                    @elseif($item->status ==1)
                                        <p class="alert alert-warning"><span>Khoá</span></p>
                                    @endif
                                </td>
                                <td class="center">
                                    <a href="color/edit/{{$item->id}}">
                                        <button class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></button>
                                    </a>
                                    <a href="color/delete/{{$item->id}}">
                                        <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')"><i class="glyphicon glyphicon-trash"></i></button>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


            </div>
        </div>
    </div>
    </div>
    <script>
        $(function() {
            $("form[name='color_create']").validate({
                rules: {
                    color: {
                        required: true
                    }
                },
                messages: {
                    color: {
                        required: "Màu sắc không được bỏ trống"
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
