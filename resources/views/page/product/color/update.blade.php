@extends('admin.master')

@section('content')
    <form action="" method="post" >
        @csrf

        <div class="row">

            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>Sửa kích thước</h4>
                    </div>
                    <div class="grid-body no-border" >
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Màu sắc:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <p style="background-color:{{$color->color}}; border-radius: 50%; width: 50px;height: 50px;"></p>
                                        <input type="text" class="form-control " name="color" value="{{$color->color}}" placeholder="" >
                                    </div>
                                </div>
                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>Cập nhập</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
