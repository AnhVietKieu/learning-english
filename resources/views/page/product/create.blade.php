@extends('admin.master')

@section('content')

    <form action="" method="post" name="product_create" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2>Thêm sản phẩm</h2>
                    </div>
                    <div class="grid-body no-border">
                        <br>
                        <div class="row">
                            <di class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Tên Sản Phẩm:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " name="title">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('title')){{$errors->first('title')}}@endif</span>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Danh Mục Sản Phẩm:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <select class="form-control select2" id="cardType" name="category" data-init-plugin="select2">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Giá:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control" name="price">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('price')){{$errors->first('price')}}@endif</span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Giá Khuyến Mãi:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="number" class="form-control" name="sale_off" max="100" min="0">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('sale_off')){{$errors->first('sale_off')}}@endif</span>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Màu Sắc:</label>
                                    </div>
                                    <div class="controls col-md-9">
                                        <div class="row-fluid">
                                            @foreach($colors as $color)
                                            <div class="col-md-6">
                                                <div class="checkbox check-success">
                                                    <input id="checkbox{{$color->id}}"  type="checkbox"  data-name-color="{{$color->color}}" class="update-color" name="color_id[]" value="{{$color->id}}">
                                                    <label for="checkbox{{$color->id}}">{{$color->color}}</label>
                                                    <p style="background:{{$color->color}} ; width: 40px;height: 40px;border-radius:50%"></p>
                                                </div>
                                            </div>
                                                @endforeach

                                        </div>
                                    </div>
                                </div>
                                <div class="data-color"></div>
{{--                                <div class="form-group">--}}
{{--                                    <div class="controls col-md-3">--}}
{{--                                        <label class="form-label">Kích Thước:</label>--}}
{{--                                    </div>--}}
{{--                                    <div class="controls col-md-9"><div class="row-fluid">--}}
{{--                                            @foreach($sizes as $size)--}}
{{--                                               <div class="col-md-6">--}}
{{--                                                   <div class="checkbox check-success">--}}
{{--                                                       <input id="checkboxsize" type="checkbox" value="{{$size->id}}" name="size_id[]" >--}}
{{--                                                       <label for="checkboxsize">{{$size->size}}</label>--}}
{{--                                                   </div>--}}
{{--                                               </div>--}}
{{--                                            @endforeach--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <div class="controls col-md-3">--}}
{{--                                        <label class="form-label">Số lượng :</label>--}}
{{--                                         </div>--}}
{{--                                    <div class="form-group col-md-9">--}}
{{--                                        <input type="number" class="form-control" name="quantity">--}}
{{--                                        <span style="color: red; margin-bottom: 2px;"></span>--}}
{{--                                        </div>--}}
{{--                                </div>--}}

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Mô tả:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <textarea class="form-control" name="description" id="editor1">
                                        </textarea>
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('description')){{$errors->first('description')}}@endif</span>
                                    </div>

                                </div>
                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Thêm Mới
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>


        $(document).ready(function () {
            $("form[name='product_create']").validate({
                rules: {
                    title:{
                        required:true,
                        maxlength:20
                    },
                    price: {
                        required: true,
                        number:true,
                    },
                    sale_off:{
                        number:true,
                        max:100,
                        min:0
                    },
                    description: {
                        required: true
                    }

                },
                messages: {
                    title:{
                        required:'Tên hiển thị không được bỏ trống',
                        maxlength: 'Tên không quá 20 kí tự '
                    },
                    price:{
                        required:'Giá không được bỏ trống',
                        number:'Giá phải là số'
                    },
                    sale_off:{
                        max: 'Không quá 100',
                        number:'Giá giảm giá phải là số',
                        min:'Không được nhỏ hơn 0'
                    },
                    description: {
                        required: "Mô tả không được bỏ trống"
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });

            $(".update-color").on('change', function () {
                var demsize = document.getElementsByClassName('demsize').length;
                var demquantity = document.getElementsByClassName('demquantity').length;

                var color = $(this).val();
                var color_name = $(this).attr('data-name-color');

                var demclass = document.getElementsByClassName(color_name).length;



                    var $html = '<div class="'+color_name;
                    $html += '"> <div class="form-group"><div class="controls col-md-3"><label class="form-label">';
                    $html += 'Kích Thước: ' + color_name;
                    $html += '</label>\n' +
                        '                                    </div>\n' +
                        '                                    <div class="controls col-md-9"><div class="row-fluid">\n' +
                        '                                            @foreach($sizes as $size)\n' +
                        '                                               <div class="col-md-6">\n' +
                        '                                                   <div class="checkbox check-success">\n' +
                        '                                                       <input data-name-size="{{$size->size}}" id="checkbox{{$size->size.'+'.$size->id.'+'}}'+color_name;
                        $html+='" class ="update-size'+color_name;
                        $html+='" type="checkbox" value="{{$size->id}}" name="size_id['+color;
                        $html+='][]" ><label for="checkbox{{$size->size.'+'.$size->id.'+'}}'+color_name;
                        $html+='">{{$size->size}}</label>\n' +
                        '                                                   </div>\n' +
                        '                                               </div>\n' +
                        '                                            @endforeach\n' +
                        '                                        </div>\n' +
                        '                                    </div>\n' +
                        '                                </div><div class="demsize"></div><div class="data-size';
                            if(demquantity%2 ==0)
                            {
                                $html +=color_name+demsize+demquantity+'"></div>';
                            }else{
                                if(demsize%2 != 0)
                                {
                                    $html +=color_name+demsize+'"></div>';
                                }else{
                                    $html +=color_name+'"></div></div>';
                                }
                            }



                    $('.data-color').append($html);
                if(demclass>0)
                {
                    $('.'+color_name).remove();
                }
                    $(".update-size"+color_name).on('change', function () {
                        if ($("input[name='size_id[]']:checked")) {
                            var size = $(this).val();
                            var size_name = $(this).attr('data-name-size');

                            var demclasssize = document.getElementsByClassName(color_name+''+size_name).length;

                            var $html1 = '<div class="'+color_name+''+size_name;
                            $html1+='"><div class="demquantity"></div><div class="form-group"><div class="controls col-md-3"><label class="form-label">';
                            $html1+='Số lượng của size :'+size_name;
                        $html1+='</label>\n' +
                                '                                         </div>\n' +
                                '                                    <div class="form-group col-md-9">\n' +
                                '                                        <input type="number" class="form-control" name="quantity['+color+']['+size;
                        $html1+=']">\n' +
                                '                                        <span style="color: red; margin-bottom: 2px;"></span>\n' +
                                '                                        </div>\n' +
                                '                                </div></div>';

                            if(demquantity%2 ==0)
                            {
                                $('.data-size'+color_name+demsize+demquantity).append($html1);
                            }else{
                                if(demsize%2 !=0)
                                {
                                    $('.data-size'+color_name+demsize).append($html1);

                                }else{
                                    $('.data-size'+color_name).append($html1);
                                }
                            }

                            if(demclasssize>0)
                            {
                               $('.'+color_name+''+size_name).remove();
                            }
                        }
                    });

            });
        });
    </script>
@endsection
