@extends('admin.master')
@section('content')
    <div class="col-md-12">
        <div class="grid simple ">
            <div class="grid-title no-border">
                <h2>{{lang_data('Listen')}}</h2>
            </div>
            <div class="grid-body no-border">
                <div class="row">
                    <div class="pull-left" style="margin-left: 20px;" >
                        <form method="get" action="{{route('listen.search')}}">
                            <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                            <input type="text" name="search" placeholder="Tìm kiếm theo câu hỏi">
                            <button class="btn btn-success"><i class="glyphicon glyphicon-search"></i> {{lang_data('Search')}}</button>
                        </form>
                    </div>
                    <form method="post" action="{{route('user.deleteall')}}">
                        @csrf
                        <div class="pull-left" style="margin-top:25px; margin-left: 9px;">
                            @if(menu_permission('per_delete_listen')==true)
                            <button id="delete-user" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i> {{lang_data('Delete all')}}</button>
                            @endif
                                @if(menu_permission('per_create_listen')==true)
                            <a class="btn btn-success" href="{{route('listen.create')}}"><i class="glyphicon glyphicon-plus"></i> {{lang_data('Add')}}</a>
                            @endif
                        </div>
                </div>
                <table class="table no-more-tables">
                    <thead>
                    <tr>
                        <th>{{lang_data('Select')}}</th>
                        <th>{{lang_data('Question')}}</th>
                        <th>{{lang_data('Audio')}}</th>
                        <th>{{lang_data('Answer')}} A</th>
                        <th>{{lang_data('Answer')}} B</th>
                        <th>{{lang_data('Answer')}} C</th>
                        <th>{{lang_data('Answer')}} D</th>
                        <th>{{lang_data('Answer true')}}</th>
                        <th>{{lang_data('Lesson')}} </th>
                        <th>{{lang_data('Score')}}</th>
                        <th>{{lang_data('User create')}} </th>
                        <th>{{lang_data('Status')}}</th>
                        <th>{{lang_data('Function')}}</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listens as $key)
                        <tr>

                            <td class='v-align-middle'>
                                <input type='checkbox' name='lesson_id[]' value='{{$key->id}}' class='checkbox delete-user'>
                            </td>
                            <td>
                                <div style="overflow: auto;height:150px;width: 200px;">{!! $key->question !!}</div>
                            </td>
                            <td>
                                @if($key->audio != '')
                                <audio controls>
                                    <source src="{{asset('public/upload/audio/'.$key->audio)}}" type="audio/mp4">
                                </audio>
                                @endif
                            </td>
                            <td>{{$key->answer_a}}</td>
                            <td>{{$key->answer_b}}</td>
                            <td>{{$key->answer_c}}</td>
                            <td>{{$key->answer_d}}</td>
                            <td>{{$key->answer_true}}</td>
                            <td>{{$key['lessons']['title']}}</td>
                            <td>{{$key->score}}</td>
                            <td>{{ \App\User::find_user($key->created_by)}}</td>
                            <td style="width: 200px; text-align: center">@if($key->status ==0)
                                    <p class="alert alert-success"><span>{{lang_data('Action')}} </span></p>
                                @elseif($key->status ==1)
                                    <p class="alert alert-danger"><span>{{lang_data('Lock')}}</span></p>
                                @elseif($key->status ==2)
                                    <p class="alert alert-warning"><span>{{lang_data('No action')}}</span></p>
                                @endif
                            </td>
                            <td class="center">
                                @if(menu_permission('per_edit_listen')==true)
                                <a href="listen/edit/{{$key->id}}">
                                    <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                @endif
                                    @if(menu_permission('per_delete_listen')==true)
                                <a href="listen/delete/{{$key->id}}">
                                    <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>
                                        @endif

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </form>
                <div class="pagination pull-right">{!! $listens->links() !!}</div>
            </div>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function () {

            var video = document.getElementById('video');
            var supposedCurrentTime = 0;
            video.addEventListener('timeupdate', function() {
                if (!video.seeking) {
                    supposedCurrentTime = video.currentTime;
                }
            });
// prevent user from seeking
            video.addEventListener('seeking', function() {
                // guard agains infinite recursion:
                // user seeks, seeking is fired, currentTime is modified, seeking is fired, current time is modified, ....
                var delta = video.currentTime - supposedCurrentTime;
                if (Math.abs(delta) > 0.01) {
                    console.log("Seeking is disabled");
                    video.currentTime = supposedCurrentTime;
                }
            });
// delete the following event handler if rewind is not required
            video.addEventListener('ended', function() {
                // reset state in order to allow for rewind
                supposedCurrentTime = 0;
            });

            $('#delete-user').css('display','none');
            $(".delete-user").on('change', function () {
                if ($("input[name='lesson_id[]']:checked").length > 1) {
                    $('#delete-user').css('display', 'inline-block');
                } else {
                    $('#delete-user').css('display', 'none');
                }
            });
        });

    </script>
@endsection
