@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple form-grid">
                <div class="grid-title no-border">
                    <h4><b>{{lang_data('Create new')}}</b> </h4>
                    <div class="alert alert-success"  id="data"></div>
                </div>
                <div class="grid-body no-border">
                    <form  id="form_traditional_validation" action="{{route('listen.create')}}" name="config_notification.create" method="post" enctype="multipart/form-data" role="form" autocomplete="off" class="validate" novalidate="novalidate">
                        @csrf

                        <div >
                            <div class="controls col-md-3">
                                <label class="form-label">{{lang_data('Audio')}}:</label>
                            </div>
                            <div class="col-md-9" style="margin-bottom: 20px;">
                                <div class="img col-sm-4" style="display: none;margin-top:50px; ">
                                    <img src="" id="preview" class="img-thumbnail">
                                </div>
                                <input type="file" class="form-control file-avatar" name="audio">
                                <span style="color: red; margin-bottom: 2px;">@if($errors->has('avatar')){{$errors->first('avatar')}}@endif</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label">{{lang_data('Question')}}:</label>
                            <textarea class="form-control"  name="question"  id="editor1"  required="" aria-required="true">

                                </textarea>

                            <span id="error_validate_meta_value"></span>
                        </div>

                        <div class="grid-body no-border">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{lang_data('Answer')}} A:</label>
                                        <span class="help"><input type="radio" value="answer_a" name="answer_true"></span>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="answer_a" id="phone">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">{{lang_data('Answer')}} C:</label>
                                        <span class="help"><input type="radio"   value="answer_c" name="answer_true"></span>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="answer_c" id="tin">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{lang_data('Answer')}} B:</label>
                                        <span class="help"><input type="radio" value="answer_b" name="answer_true"></span>
                                        <div class="controls">
                                            <input type="text" class="form-control auto" name="answer_b" data-a-sign="$ ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">{{lang_data('Answer')}} D:</label>
                                        <span class="help"><input type="radio"  value="answer_d" name="answer_true"></span>
                                        <div class="controls">
                                            <input type="text" class="form-control auto" name="answer_d" data-v-max="9999" data-v-min="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label class="form-label"> {{lang_data('Note')}}:</label>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control auto" name="note">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label class="form-label"> {{lang_data('Lesson')}}:</label>
                            </div>
                            <div class="form-group">
                                <i class=""></i>
                                <select class="form-control select2" id="cardType" name="lesson_id" data-init-plugin="select2">
                                    </option>
                                    @foreach($lessons as $key)
                                        <option value="{{$key->id}}">{{$key->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label class="form-label"> {{lang_data('Score')}}:</label>
                            </div>
                            <div class="form-group">
                                <input type="text"  class="form-control auto" name="score">
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="pull-right">
                                <button class="btn btn-success btn-cons update-table-config" type="submit"><i class="fa fa-check-circle"></i>{{lang_data('Add new')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#data').css('display','none');
        $(function() {
            $("form[name='config_notification.create']").validate({
                rules: {
                    title: {
                        required: true
                    },
                    description:{
                        required:true
                    }
                },
                messages: {
                    title: {
                        required: "Tên hiển thị  không được bỏ trống",
                    },
                    description: {
                        required: "Mô tả  không được bỏ trống",
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });



        // Thay thế <textarea id="post_content"> với CKEditor
        CKEDITOR.replace( 'editor1' ,
            {
                filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            });
        // tham số là biến name của textarea

    </script>
@endsection

