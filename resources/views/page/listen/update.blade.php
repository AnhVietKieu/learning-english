@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple form-grid">
                <div class="grid-title no-border">
                    <h4><b>{{lang_data('Update new')}}</b> </h4>
                    <div class="alert alert-success"  id="data"></div>
                </div>
                <div class="grid-body no-border">
                    <form  id="form_traditional_validation" name="config_notification.create"  enctype="multipart/form-data" method="post" role="form" autocomplete="off" class="validate" novalidate="novalidate">
                        @csrf

                        <div >
                            <div class="controls col-md-3">
                                <label class="form-label">{{lang_data('Audio')}}:</label>
                            </div>
                            <div class="col-md-9" style="margin-bottom: 20px;">
                                <div class="img col-sm-4" style="display: none;margin-top:50px; ">
                                    <img src="" id="preview" class="img-thumbnail">
                                </div>
                                <input type="file" class="form-control file-avatar" name="audio">
                                <span style="color: red; margin-bottom: 2px;">@if($errors->has('avatar')){{$errors->first('avatar')}}@endif</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="form-label">{{lang_data('Question')}}:</label>
                            <textarea class="form-control"  name="question"  id="editor1"  required="" aria-required="true">
                                    {{$listen->question}}
                                </textarea>

                            <span id="error_validate_meta_value"></span>
                        </div>

                        <div class="grid-body no-border">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{lang_data('Answer')}} A:</label>
                                        <span class="help"><input type="radio" value="answer_a" name="answer_true" @if($listen->answer_true === 'answer_a'){{"checked"}}@endif></span>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="answer_a" id="phone" value="{{$listen->answer_a}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">{{lang_data('Answer')}} C:</label>
                                        <span class="help"><input type="radio" value="answer_c" name="answer_true" @if($listen->answer_true === 'answer_c'){{"checked"}}@endif></span>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="answer_c" id="tin" value="{{$listen->answer_c}}">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{lang_data('Answer')}} B:</label>
                                        <span class="help"><input type="radio" value="answer_b" name="answer_true" @if($listen->answer_true === 'answer_b'){{"checked"}}@endif></span>
                                        <div class="controls">
                                            <input type="text" class="form-control auto" name="answer_b" data-a-sign="$ " value="{{$listen->answer_b}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">{{lang_data('Answer')}} D:</label>
                                        <span class="help"><input type="radio" value="answer_d" name="answer_true" @if($listen->answer_true === 'answer_d'){{"checked"}}@endif></span>
                                        <div class="controls">
                                            <input type="text" class="form-control auto" name="answer_d" data-v-max="9999" data-v-min="0" value="{{$listen->answer_d}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label class="form-label"> {{lang_data('Note')}}:</label>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control auto" name="note" value="{{$listen->note}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label class="form-label"> {{lang_data('Lesson')}}:</label>
                            </div>
                            <div class="form-group">
                                <i class=""></i>
                                <select class="form-control select2" id="cardType" name="lesson_id" data-init-plugin="select2">
                                    </option>
                                    @foreach($lessons as $key)
                                        <option value="{{$key->id}}" @if($listen->lesson_id == $key->id){{"selected"}}@endif>{{$key->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <label class="form-label">{{lang_data('Status')}}:</label>
                            </div>
                            <div class="form-group">
                                <input type="hidden"  value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                                <select class="form-control select2" id="cardType" name="status" data-init-plugin="select2">

                                    <option value="0" @if($listen->status==0){{'selected'}}@endif><b style="color: green">{{lang_data('Action')}}</b></option>
                                    <option value="1" @if($listen->status==1){{'selected'}}@endif><b style="color: yellow">{{lang_data('No action')}}</b></option>
                                </select>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label class="form-label"> {{lang_data('Score')}}:</label>
                            </div>
                            <div class="form-group">
                                <input type="text" value="{{$listen->score}}"  class="form-control auto" name="score">
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="pull-right">
                                <button class="btn btn-success btn-cons update-table-config" type="submit"><i class="fa fa-check-circle"></i>{{lang_data('Add new')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#data').css('display','none');
        $(function() {
            $("form[name='config_notification.create']").validate({
                rules: {
                    title: {
                        required: true
                    },
                    description:{
                        required:true
                    }
                },
                messages: {
                    title: {
                        required: "Tên hiển thị  không được bỏ trống",
                    },
                    description: {
                        required: "Mô tả  không được bỏ trống",
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });



        // Thay thế <textarea id="post_content"> với CKEditor
        CKEDITOR.replace( 'editor1' ,
            {
                filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            });
        // tham số là biến name của textarea

    </script>
@endsection

