@extends('admin.master')

@section('content')
    <form action="" method="post" name="create_create" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>{{lang_data('Update new')}}</h4>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="reload"></a>
                        </div>
                    </div>
                    <div class="grid-body no-border" >
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">

                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Name')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " name="name" value="{{$level->name}}" placeholder="" >
                                    </div>
                                </div>
                                <div >
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Image')}}:</label>
                                    </div>
                                    <div class="col-md-9" style="margin-bottom: 20px;">
                                        @if(!empty($level->image))
                                            <div class="img col-sm-4" style="margin-bottom: 10px;margin-top:50px;">
                                                <img src="{{asset('public/upload/level/'.$level->image)}}" id="preview" class="img-thumbnail">
                                            </div>
                                        @else
                                            <div class="img col-sm-4" style="margin-bottom: 10px;display: none;margin-top:50px;">
                                                <img src="" id="preview" class="img-thumbnail">
                                            </div>
                                        @endif
                                        <input type="file" class="form-control file-avatar" name="image">
                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('avatar')){{$errors->first('avatar')}}@endif</span>

                                        <span style="color: red; margin-bottom: 2px;">@if($errors->has('avatar')){{$errors->first('avatar')}}@endif</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Score')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " name="score" value="{{$level->score}}" placeholder="" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Status')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden"  value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="cardType" name="status" data-init-plugin="select2">

                                            <option value="0" @if($level->status==0){{'selected'}}@endif><b style="color: green">{{lang_data('Action')}}</b></option>
                                            <option value="1" @if($level->status==1){{'selected'}}@endif><b style="color: yellow">{{lang_data('No action')}}</b></option>
                                        </select>
                                    </div>

                                </div>

                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>{{lang_data('Update new')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $(function() {
            $("form[name='create_create']").validate({
                rules: {
                    display: {
                        required: true
                    }
                },
                messages: {
                    display: {
                        required: "Tên hiển thị  không được bỏ trống",
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });

            $('input[type="file"]').change(function(e) {
                var fileName = e.target.files[0].name;
                $("#file").val(fileName);

                var reader = new FileReader();
                reader.onload = function(e) {
                    // get loaded data and render thumbnail.
                    $('.img').css('display','block');
                    document.getElementById("preview").src = e.target.result;
                };
                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            });

        });
    </script>
@endsection
