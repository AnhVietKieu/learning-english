@extends('admin.master')

@section('content')

    <form action="" name="receipt_update" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2>Sửa Hóa Đơn</h2>
                    </div>
                    <div class="grid-body no-border" >
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Tên Nhà Cung Cấp:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="supplier_id" data-init-plugin="select2">
                                            @foreach($receipts as $data)
                                                <option value="{{$data->spId}}">{{$data->spName}}</option>
                                                @endforeach
                                            @foreach($suppliers as $list)
                                                    <option value="{{$list->id}}">{{$list->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                @for($i=0;$i<count($receipt_details);$i++)
                                    <div class="form-group">
                                        <div class="controls col-md-3">
                                            <label class="form-label">Sản Phẩm {{$i + 1}}:</label>
                                        </div>

                                        <div class="col-md-9">
                                            <div class="grid simple">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                                        <select class="form-control select2" id="product" name="product_id[]" data-init-plugin="select2">
                                                            <option value="{{$receipt_details[$i]['product_id']}}">{{$receipt_details[$i]['title']}}</option>
                                                            @foreach($products as $list)
                                                                <option value="{{$list->id}}">{{$list->title}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="controls col-md-3">
                                            <label class="form-label">Số Lượng:</label>
                                        </div>
                                        <div class="form-group col-md-9">
                                            <input type="number" class="form-control" value="{{$receipt_details[$i]['quantity']}}"  name="quantity[]">
                                            <span style="color: red; margin-bottom: 2px;"></span>
                                        </div>
                                        <div class="controls col-md-3">
                                            <label class="form-label">Màu Sắc:</label>
                                        </div>
                                        <div class="form-group col-md-9">
                                            <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                            <select class="form-control select2" id="" name="color_id[]" data-init-plugin="select2">
                                                <option value="{{$receipt_details[$i]['color_id']}}">{{$receipt_details[$i]['color']}}</option>
                                                @foreach($colors as $list)
                                                    <option value="{{$list->id}}">{{$list->color}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="controls col-md-3">
                                            <label class="form-label">Kích Thước:</label>
                                        </div>
                                        <div class="form-group col-md-9">
                                            <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                            <select class="form-control select2" id="" name="size_id[]" data-init-plugin="select2">
                                                <option value="{{$receipt_details[$i]['size_id']}}">{{$receipt_details[$i]['size']}}</option>
                                                @foreach($sizes as $list)
                                                    <option value="{{$list->id}}">{{$list->size}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="controls col-md-3">
                                            <label class="form-label">Giá Bán:</label>
                                        </div>
                                        <div class="col-md-9" style="margin-bottom: 20px;">
                                            <input type="number" id="delivery_price" value="{{$receipt_details[$i]['price']}}" name="price[]" class="form-control">
                                            <span style="color: red; margin-bottom: 2px;"></span>
                                        </div>
                                    </div>
                                @endfor
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Thanh Toán:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="status" data-init-plugin="select2">
                                            <option value="{{$receipts[0]['status']}}">
                                                @if($receipts[0]['status'] == 1)
                                                    Chưa Thanh Toán
                                                    @else
                                                Thanh Toán
                                                    @endif
                                            </option>
                                            <option value="1">Chưa Thanh Toán</option>
                                            <option value="0">Đã Thanh Toán</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Người Bán:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px"  tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="user_id" data-init-plugin="select2">
                                            <option value="{{session('user_id')}}">{{session('username')}}</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group include"></div>
                                <div style="margin-right: 15px;">
                                    <button class="btn btn-danger pull-left push-input"  type="button"><i class="glyphicon glyphicon-plus"></i><span> Thêm Sản Phẩm</span></button>
                                </div>
                                <div class="col-md-12 m-t-10 m-b-10 text-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Thêm Mới</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
    <script>
        $(document).ready(function () {
            $('.push-input').click(function () {
                var $html = '<div class="form-group">\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Sản Phẩm:</label>\n' +
                    '                                    </div>\n' +
                    '\n' +
                    '                                    <div class="col-md-9">\n' +
                    '                                        <div class="grid simple">\n' +
                    '                                            <div class="row">\n' +
                    '                                                <div class="col-md-12">\n' +
                    '                                                    <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">\n' +
                    '                                                    <select class="form-control select2" id="product" name="product_id[]" data-init-plugin="select2">\n' +
                    '                                                        @foreach($products as $item)\n' +
                    '                                                            <option value="{{$item->id}}">{{$item->title}}</option>\n' +
                    '                                                        @endforeach\n' +
                    '                                                    </select>\n' +
                    '                                                </div>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Số Lượng:</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-9">\n' +
                    '                                        <input type="number" class="form-control" name="quantity[]">\n' +
                    '                                        <span style="color: red; margin-bottom: 2px;"></span>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Màu Sắc:</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-9">\n' +
                    '                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">\n' +
                    '                                        <select class="form-control select2" id="color" name="color_id[]" data-init-plugin="select2">\n' +
                    '                                            @foreach($colors as $item)\n' +
                    '                                                <option value="{{$item->id}}">{{$item->color}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Kích Thước:</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-9">\n' +
                    '                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">\n' +
                    '                                        <select class="form-control select2" id="size" name="size_id[]" data-init-plugin="select2">\n' +
                    '                                            @foreach($sizes as $item)\n' +
                    '                                                <option value="{{$item->id}}">{{$item->size}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '\n' +
                    '                                </div>\n' +
                    '                                <div class="form-group">\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Giá Bán:</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-md-9" style="margin-bottom: 20px;">\n' +
                    '                                        <input type="number" name="price[]" id="" class="form-control">\n' +
                    '                                        <span style="color: red; margin-bottom: 2px;"></span>\n' +
                    '                                    </div>\n' +
                    '                                </div>';
                $('.include').append($html);
            });
        });

    </script>

@endsection
