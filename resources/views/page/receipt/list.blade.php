@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div  class="grid-title no-border">
                    <h2>Danh Sách Hoá Đơn</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="pull-left" style="margin-left: 20px;" >
                            <form method="get" action="{{route('receipt.search')}}">
                                <label><b style="text-decoration: underline;color: #0d0f12;">Tìm kiếm:</b></label>
                                <input type="text" name="search" placeholder="Tìm kiếm theo tên email">
                                <button class="btn btn-success"><i class="glyphicon glyphicon-search"></i> Tìm kiếm</button>
                            </form>
                        </div>
                        <form method="post" action="">
                            @csrf
                            <div class="pull-left" style="margin-top:25px; margin-left: 9px;">
                                <a class="btn btn-success" href="{{route('receipt.create')}}"><i class="glyphicon glyphicon-plus"></i> Thêm</a>
                            </div>
                    </div>
                </div>
            </div>

            <div>
                <table class="table table-striped dataTable">
                    <thead>
                    <tr>
                        <th style="text-align: center">Mã Hóa Đơn</th>
                        <th>Ngày Tạo</th>
                        <th style="text-align: center">Tổng Sản Phẩm</th>
                        <th>Trạng Thái</th>
                        <th>Hành Động</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                    @foreach($receipts as $item)
                        <tr>
                            <td style="text-align: center">{{$item->id}}</td>
                            <td>{{$item->date_of_receipt}}</td>
                            <td style="text-align: center">{{number_format($item->total)}} VNĐ</td>
                            <td>@if($item->status == 1)
                                    <span>Chưa Thanh Toán</span>
                                @else
                                    <span>Thanh Toán</span>
                                @endif
                            </td>
                            <td class="center">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#detailReceipt{{$item->id}}" title="Chi Tiết Hóa Đơn" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                <a href="receipt/edit/{{$item->id}}">
                                    <button class="btn btn-success" title="Sửa" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                <a href="receipt/delete/{{$item->id}}">
                                    <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')" title="Xóa" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>
                                @if($item->status == 1)
                                <a href="receipt/check/{{$item->id}}">
                                    <button class="btn" style="background: #53aa78;" title="Thanh Toán" type="button"><i class="glyphicon glyphicon-check"></i></button>
                                </a>
                                    @endif
                                @if($item->status == 0)
                                    <button class="btn" disabled style="background: #53aa78;" title="Thanh Toán" type="button"><i class="glyphicon glyphicon-check"></i></button>
                                @endif
                            </td>
                        </tr>
                        <div class="modal fade" id="detailReceipt{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <strong><h3 class="modal-title" id="exampleModalLongTitle">Chi Tiết Hóa Đơn Nhập Hàng</h3></strong>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Tên Nhà Cung Cấp:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">{{$item->name}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Địa Chỉ:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">{{$item->address}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Số Điện Thoại:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">{{$item->phone}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Tên Sản Phẩm:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                @if(!empty($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                        [ -
                                                                        @if(!empty($title[$item->id][$color_id]))
                                                                            @for($j=0;$j<count($title[$item->id][$color_id]);$j++)
                                                                                @if(isset($title[$item->id][$color_id][$j]))
                                                                                    <b>{{$title[$item->id][$color_id][$j] }}</b> -
                                                                                @endif
                                                                            @endfor
                                                                            ]&nbsp &nbsp
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Màu Sắc:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                @if(isset($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        @if(isset($color[$item->id]))
                                                                            <p style="background-color:{{$color[$item->id][$i]['color']}}; border-radius: 50%; width: 50px;height: 50px;">
                                                                            </p>
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Kích Thước:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                @if(!empty($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                        [ -
                                                                        @if(!empty($size[$item->id][$color_id]))
                                                                            @for($j=0;$j<count($size[$item->id][$color_id]);$j++)
                                                                                @if(isset($size[$item->id][$color_id][$j]))
                                                                                    <b>{{$size[$item->id][$color_id][$j] }}</b> -
                                                                                @endif
                                                                            @endfor
                                                                            ]&nbsp &nbsp
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Số Lượng Nhập:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                @if(!empty($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                        [ -
                                                                        @if(!empty($quantity[$item->id][$color_id]))
                                                                            @for($j=0;$j<count($quantity[$item->id][$color_id]);$j++)
                                                                                @if(isset($quantity[$item->id][$color_id][$j]))
                                                                                    <b>{{$quantity[$item->id][$color_id][$j] }}</b> -
                                                                                @endif
                                                                            @endfor
                                                                            ]&nbsp &nbsp
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Đơn Giá:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="">
                                                                @if(!empty($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                        [ -
                                                                        @if(!empty($price[$item->id][$color_id]))
                                                                            @for($j=0;$j<count($price[$item->id][$color_id]);$j++)
                                                                                @if(isset($price[$item->id][$color_id][$j]))
                                                                                    <b>{{$price[$item->id][$color_id][$j] }}</b> -
                                                                                @endif
                                                                            @endfor
                                                                            ]&nbsp &nbsp
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Tổng Tiền:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                <b>{{number_format($item->total)}} VNĐ</b>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Nhân Viên Bán Hàng:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                {{$item->username}}
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="receipt/invoice/{{$item->id}}"><button type="button" class="btn btn-secondary" ><i class="glyphicon glyphicon-export"></i>Xuất Hóa Đơn</button></a>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </tbody>

                </table>
                </form>
                <div class="pagination-button pull-right"> {{ $receipts->links() }} </div>
            </div>
        </div>
    </div>

@endsection
