@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div  class="grid-title no-border">
                    <h2>{{lang_data('History')}}</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="pull-left" style="margin-left: 20px;" >
                            <form method="get" action="{{route('history.search')}}">

                                <div class="controls col-md-12">
                                    <div class="row-fluid">
                                        <div class="col-md-6" style="margin-right: -15px;">
                                            <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                                            <div class="checkbox check-success">
                                                <input type="text" class="form-control" name="search" placeholder="Tìm kiếm theo tên">
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="margin-right: -15px;margin-left: -5px;">
                                            <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Filter')}}:</b></label>
                                            <select name="filter" id="gendericonic" class="select2 form-control select2-offscreen" data-init-plugin="select2" tabindex="-1">
                                                <option value="2">Mặc định</option>
                                                <option value="0">Chưa chấm</option>
                                                <option value="1">Đã chấm</option>
                                            </select>

                                        </div>
                                        <div class="col-md-2" style="margin-left:-4px;margin-top: 24px;">

                                            <button class="btn btn-success">
                                                <i class="glyphicon glyphicon-search"></i> {{lang_data('Search')}}</button>

                                        </div>


                                    </div>
                                </div>

                            </form>
                        </div>
                        <form method="post" action="">
                            @csrf

                    </div>
                </div>
            </div>

            <div>
                <table class="table table-striped dataTable">
                    <thead>
                    <tr>
                        <th style="text-align: center">{{lang_data('Customer')}}</th>
                        <th>{{lang_data('Coure')}}</th>
                        <th>{{lang_data('Lesson')}}</th>
                        <th>{{lang_data('Answer true/Total')}}</th>
                        <th style="text-align: center">{{lang_data('Score')}}</th>
                        <th>{{lang_data('User')}}</th>
                        <th>{{lang_data('Created at')}}</th>
                        <th>{{lang_data('Updated at')}}</th>
                        <th>{{lang_data('Status')}}</th>
                        <th>{{lang_data('Action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($history as $item)
                        <tr>
                            <td>{{$item['customer']['username']}}</td>
                            <td>{{$item['lesson']['coure']['name']}}</td>
                            <td>{{$item['lesson']['title']}}</td>
                            <td>{{$item['answer_true']}}</td>
                            <td>{{$item['score']}}</td>
                            <td>{{$item['user']['username']}}</td>
                            <td>{{$item['created_at']}}</td>
                            <td>{{$item['updated_at']}}</td>
                            <td style="width: 20px; text-align: center">@if($item->status ==0)
                                    <p class="alert alert-success"><span>Chưa chấm </span></p>
                                @elseif($item->status ==1)
                                    <p class="alert alert-danger"><span>Đã chấm </span></p>
                                @elseif($item->status ==2)
                                    <p class="alert alert-warning"><span>Không hoạt đông</span></p>
                                @endif
                            </td>

                            <td class="center">
                                @if(menu_permission('per_edit_history')==true)
                                <a href="{{route('history.update',$item->id)}}">
                                    <button class="btn btn-success" title="Sửa" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                @endif
                                <button class="btn btn-primary" data-toggle="modal" data-target="#detailDelivery{{$item->id}}" title="Chi Tiết Hóa Đơn" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                    @if(menu_permission('per_delete_history')==true)
                                    <a href="{{route('history.delete',$item->id)}}">
                                    <button class="btn btn-danger" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}" title="Xóa" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>
                                        @endif
                            </td>

                        </tr>

                        <div class="modal fade" id="detailDelivery{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <strong><h3 class="modal-title" id="exampleModalLongTitle">Chi Tiết Lịch Sử</h3></strong>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Tên Khách Hàng:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label"></label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#"><button type="button" class="btn btn-secondary" ><i class="glyphicon glyphicon-export"></i>Xuất</button></a>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @endforeach

                    </tbody>

                </table>
                <div class="pull-right">
                    <div class="paginate">{!! $history->links() !!}</div>
                </div>
                </form>

            </div>
        </div>
    </div>


@endsection
