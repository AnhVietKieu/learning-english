@extends('admin.master')
@section('content')
    <form action="" method="post" name="create_create" >
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2><u>{{lang_data('History')}}</u></h2>
                        <div class="tiles white col-md-12  no-padding" style="box-shadow:2px 4px 4px 4px gainsboro;">
                            <div class="tiles-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="friend-list">
                                            <div class="friend-profile-pic">
                                                <div class="user-profile-pic-normal">
                                                    <img width="35" height="35" src="assets/img/profiles/d2x.jpg" data-src="assets/img/profiles/d.jpg" data-src-retina="assets/img/profiles/d2x.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="friend-details-wrapper">
                                                <div class="friend-name">
                                                    {{lang_data('Name')}}: {{$test['customer']['username']}}
                                                </div>
                                                <div class="friend-description">
                                                    {{lang_data('Email')}}: {{$test['customer']['email']}}
                                                </div>
                                                <div class="friend-name">
                                                    {{lang_data('Lesson')}}: {{$test['lesson']['title']}}
                                                </div>
                                                <div class="friend-name">
                                                    {{lang_data('Total score')}}: {{$test['lesson']['score']}}
                                                </div>
                                                <div class="friend-name">
                                                    {{lang_data('Day test')}}: {{$test['created_at']}}
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 200px;"></div>

                    </div>



                    <form method="post" action="" >
                        @csrf

                        @if(isset($test['lesson']->reads))

                            @foreach($test['lesson']->reads as $key)
                                <div class="pull-right">

                                        <span>{{lang_data('False')}}</span>
                                        <label class="switch">
                                            <input type="checkbox" onclick="return false;" onkeydown="return false;" value="true"
                                                   @foreach($test['history_test'] as $item)
                                                   @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                   @if($item1==$key->id && $item['type']=='read' && $key1== $key['answer_true'])
                                                   {{'checked'}}
                                                   @endif
                                                   @endforeach
                                                   @endforeach
                                                   name="answer_user_read[{{$key->id}}][status]">
                                            <span class="slider round green"></span>
                                        </label>
                                        <span>{{lang_data('true')}}</span>

                                </div>


                                <label class="form-label"><b>{{lang_data('Question')}} {{$i++}}<span> ({{$key['score']}} điểm)</span>:{!! $key->question !!}</b></label>


                                <div class="grid-body no-border">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">{{lang_data('Answer')}} A:</label>
                                                <span class="help"><input type="radio" class="check-radio"
                                                                          @foreach($test['history_test'] as $item)
                                                                          @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                                          @if($item1==$key->id && $item['type']=='read' && $key1=='answer_a')
                                                                          {{'checked'}}
                                                                          @endif
                                                                          @endforeach
                                                                          @endforeach
                                                                          value="answer_a" name="answer_read[{{$key->id}}]"></span>
                                                <div class="controls">
                                                    <input type="text" class="form-control"  value="{{$key->answer_a}}" disabled name="answer_a" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">{{lang_data('Answer')}} C:</label>
                                                <span class="help"><input type="radio" @foreach($test['history_test'] as $item)
                                                    @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                    @if($item1==$key->id && $item['type']=='read' && $key1=='answer_c')
                                                    {{'checked'}}
                                                    @endif
                                                    @endforeach
                                                    @endforeach class="check-radio"  value="answer_c" name="answer_read[{{$key->id}}]"></span>
                                                <div class="controls">
                                                    <input type="text" class="form-control" value="{{$key->answer_c}}" disabled name="answer_c">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">{{lang_data('Answer')}} B:</label>
                                                <span class="help"><input type="radio"
                                                                          @foreach($test['history_test'] as $item)
                                                                          @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                                          @if($item1==$key->id && $item['type']=='read' && $key1=='answer_b')
                                                                          {{'checked'}}
                                                                          @endif
                                                                          @endforeach
                                                                          @endforeach
                                                                          class="check-radio" value="answer_b" name="answer_read[{{$key->id}}]"></span>
                                                <div class="controls">
                                                    <input type="text" class="form-control"  value="{{$key->answer_b}}" disabled name="answer_b">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">{{lang_data('Answer')}} D:</label>
                                                <span class="help"><input type="radio"  @foreach($test['history_test'] as $item)
                                                    @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                    @if($item1==$key->id && $item['type']=='read' && $key1=='answer_d')
                                                    {{'checked'}}
                                                    @endif
                                                    @endforeach
                                                    @endforeach  class="check-radio" value="answer_d" name="answer_read[{{$key->id}}]"></span>
                                                <div class="controls">
                                                    <input type="text" class="form-control" value="{{$key->answer_d}}" disabled name="answer_d" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <br>
                            <div>
                                <label>{{lang_data('Score')}} :</label>
                                <input type="number"
                                       @foreach($test['history_test'] as $item)
                                       @foreach($item['answer_customer'] as $item1 =>$key1 )
                                       @if($item1==$key->id && $item['type']=='read' && $key1== $key['answer_true'])
                                     value="{{$key->score}}"
                                       @endif
                                       @endforeach
                                       @endforeach
                                       class="form-control " readonly name="answer_user_read[{{$key->id}}][score]">
                            </div>
                                <div class="hero-inner">
                                    <h4><u>{{lang_data('Note')}}:</u></h4>
                                    <p class="lead">{{$key->note}}</p>

                                </div>
                                <hr>



                            @endforeach
                        @endif
                        @if(isset($test['lesson']->listens))

                            @foreach($test['lesson']->listens as $key)
                                <div class="pull-right">

                                        <span>{{lang_data('False')}}</span>
                                        <label class="switch">
                                            <input type="checkbox" onclick="return false;" onkeydown="return false;"  value="true" @foreach($test['history_test'] as $item)
                                            @foreach($item['answer_customer'] as $item1 =>$key1 )
                                            @if($item1==$key->id && $item['type']=='listen' && $key1== $key['answer_true'])
                                            {{'checked'}}
                                            @endif
                                            @endforeach
                                            @endforeach name="answer_user_listen[{{$key->id}}][status]">
                                            <span class="slider round green"></span>
                                        </label>
                                        <span>{{lang_data('True')}}</span>

                                </div>
                                <label class="form-label"><b>Câu hỏi {{$i++}}: <span> ({{$key['score']}} điểm)</span>{!! $key->question !!}</b></label>
                                @if($key->audio != '')
                                    <div><audio controls>
                                            <source src="{{asset('public/upload/audio/'.$key->audio)}}" type="audio/mp4">
                                        </audio></div>
                                @endif

                                <div class="grid-body no-border">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">{{lang_data('Answer')}} A:</label>
                                                <span class="help"><input type="radio"
                                                                          @foreach($test['history_test'] as $item)
                                                                          @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                                          @if($item1==$key->id && $item['type']=='listen' && $key1=='answer_a')
                                                                          {{'checked'}}
                                                                          @endif
                                                                          @endforeach
                                                                          @endforeach class="check-radio" value="answer_a" name="answer_listen[{{$key->id}}]"></span>
                                                <div class="controls">
                                                    <input type="text" class="form-control"  value="{{$key->answer_a}}" disabled name="answer_a" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">{{lang_data('Answer')}} C:</label>
                                                <span class="help"><input type="radio"  @foreach($test['history_test'] as $item)
                                                    @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                    @if($item1==$key->id && $item['type']=='listen' && $key1=='answer_c')
                                                    {{'checked'}}
                                                    @endif
                                                    @endforeach
                                                    @endforeach  class="check-radio"  value="answer_c" name="answer_listen[{{$key->id}}]"></span>
                                                <div class="controls">
                                                    <input type="text" class="form-control" value="{{$key->answer_c}}" disabled name="answer_c">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">{{lang_data('Answer')}} B:</label>
                                                <span class="help"><input type="radio"  @foreach($test['history_test'] as $item)
                                                    @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                    @if($item1==$key->id && $item['type']=='listen' && $key1=='answer_b')
                                                    {{'checked'}}
                                                    @endif
                                                    @endforeach
                                                    @endforeach class="check-radio" value="answer_b" name="answer_listen[{{$key->id}}]"></span>
                                                <div class="controls">
                                                    <input type="text" class="form-control"  value="{{$key->answer_b}}" disabled name="answer_b">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">{{lang_data('Answer')}} D:</label>
                                                <span class="help"><input type="radio"  @foreach($test['history_test'] as $item)
                                                    @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                    @if($item1==$key->id && $item['type']=='listen' && $key1=='answer_d')
                                                    {{'checked'}}
                                                    @endif
                                                    @endforeach
                                                    @endforeach
                                                    class="check-radio" value="answer_d" name="answer_listen[{{$key->id}}]"></span>
                                                <div class="controls">
                                                    <input type="text" class="form-control" value="{{$key->answer_d}}" disabled name="answer_d" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <label>{{lang_data('Score')}} :</label>
                                <input type="number"
                                       @foreach($test['history_test'] as $item)
                                       @foreach($item['answer_customer'] as $item1 =>$key1 )
                                       @if($item1==$key->id && $item['type']=='listen' && $key1== $key['answer_true'])
                                       value="{{$key->score}}"
                                       @endif
                                       @endforeach
                                       @endforeach
                                       class="form-control " readonly  name="answer_user_listen[{{$key->id}}][score]">

                                <div class="hero-inner">
                                    <h4><u>{{lang_data('Note')}}:</u></h4>
                                    <p class="lead">{{$key->note}}</p>

                                </div>
                                <hr>


                            @endforeach
                        @endif
                        @if(isset($test['lesson']->writes))
                            @foreach($test['lesson']->writes as $key)
                                <div class="pull-right">

                                    <span>{{lang_data('False')}}</span>
                                    <label class="switch">
                                        <input type="checkbox" onclick="return false;" onkeydown="return false;"  value="true" @foreach($test['history_test'] as $item)
                                        @foreach($item['answer_customer'] as $item1 =>$key1 )
                                        @if($item1==$key->id && $item['type']=='write' && !empty($key1))
                                        {{'checked'}}
                                        @endif
                                        @endforeach
                                        @endforeach  name="answer_user_write[{{$key->id}}][status]">
                                        <span class="slider round green"></span>
                                    </label>
                                    <span>{{lang_data('True')}}</span>

                                </div>
                                <label class="form-label"><b>Câu hỏi {{$i++}}:<span> ({{$key['score']}} điểm)</span></b></label>
                               <b>{{$key->name}}</b>
                                <div class="grid-body no-border">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!! $key->question !!}
                                        </div>
                                        <div class="col-md-12">
                                            <p style="text-decoration: underline;"><b>{{lang_data('Answer')}}:</b></p>
                                            <textarea disabled name="answer_write[{{$key->id}}]" style="height: 150px;" class="col-lg-11" value="">
                                                @foreach($test['history_test'] as $item)
                                                    @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                        @if($item1==$key->id && $item['type']=='write')
                                                            {{$key1}}
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </textarea>
                                        </div>
                                    </div>

                                </div>
                                <label>{{lang_data('Score')}} :</label>
                                <input type="number" @foreach($test['history_test'] as $item)
                                @foreach($item['answer_user'] as $item1 =>$key1 )
                                @if($item1==$key->id && $item['type']=='write')
                                value="{{$key1['score']}}"
                                @endif
                                @endforeach
                                @endforeach  class="form-control" min="0"
                                       max="{{$key->score}}"
                                       name="answer_user_write[{{$key->id}}][score]">


                                <div class="hero-inner">
                                    <h4><u>{{lang_data('Note')}}:</u></h4>
                                    <input type="text" @foreach($test['history_test'] as $item)@foreach($item['answer_user'] as $item1 =>$key1 )@if($item1==$key->id && $item['type']=='write')value="{{$key1['note']}}"
                                    @endif
                                    @endforeach
                                    @endforeach class="form-control "  name="answer_user_write[{{$key->id}}][note]">

                                </div>
                            <hr>
                            @endforeach
                        @endif

                        @if(isset($test['lesson']->speaks))
                            @foreach($test['lesson']->speaks as $key)
                                <div class="pull-right">

                                    <span>{{lang_data('False')}}</span>
                                    <label class="switch">
                                        <input type="checkbox" onclick="return false;" onkeydown="return false;"  value="true" @foreach($test['history_test'] as $item)
                                        @foreach($item['answer_customer'] as $item1 =>$key1 )
                                        @if($item1==$key->id && $item['type']=='speak' && !empty($key1))
                                        {{'checked'}}
                                        @endif
                                        @endforeach
                                        @endforeach name="answer_user_speak[{{$key->id}}][status]">
                                        <span class="slider round green"></span>
                                    </label>
                                    <span>{{lang_data('True')}}</span>

                                </div>
                                <label class="form-label"><b>{{lang_data('Question')}} {{$i++}}:<span> ({{$key['score']}} điểm)</span></b></label>
                                <b>{{$key->name}}</b>
                                <div class="grid-body no-border">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!! $key->question !!}
                                        </div>
                                        <div class="col-md-12">
                                            <div><audio controls>
                                                    @foreach($test['history_test'] as $item)
                                                        @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                            @if($item1==$key->id && $item['type']=='speak')
                                                                <source src="{{asset('public/upload/audio/customer/'.$key1)}}" type="audio/mp4">
                                                            @endif
                                                        @endforeach
                                                    @endforeach

                                                </audio></div>

                                        </div>
                                    </div>

                                </div>
                                <label>{{lang_data('Score')}} :</label>
                                <input type="number" @foreach($test['history_test'] as $item)@foreach($item['answer_user'] as $item1 =>$key1 )@if($item1==$key->id && $item['type']=='speak')value="{{$key1['score']}}"
                                @endif
                                @endforeach
                                @endforeach class="form-control "
                                       min="0"
                                      max="{{$key->score}}"

                                       name="answer_user_speak[{{$key->id}}][score]">
                                <div class="hero-inner">
                                    <h4><u>{{lang_data('Note')}}:</u></h4>
                                    <input type="text" @foreach($test['history_test'] as $item)@foreach($item['answer_user'] as $item1 =>$key1 )@if($item1==$key->id && $item['type']=='speak')value="{{$key1['note']}}"
                                    @endif
                                    @endforeach
                                    @endforeach class="form-control " name="answer_user_speak[{{$key->id}}][note]">
                                </div>
                                <hr>

                            @endforeach
                        @endif


                        <div class="col-md-12 m-t-10 m-b-10 align-center">
                            <button class="btn btn-success pull-right" type="submit">{{lang_data('Mark test')}}</button>
                        </div>


                    </form>


                </div>
            </div>
        </div>
    </form>
    @endsection
