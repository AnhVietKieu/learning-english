@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div  class="grid-title no-border">
                    <h2>Nhà Kho</h2>
                </div>
                <div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Sản Phẩm</th>
                            <th>Danh Mục</th>
                            <th>Giá gốc</th>
                            <th>Màu sắc</th>
                            <th width="150px;">Kích Thước</th>
                            <th width="250px;">Số Lượng</th>
                            <th>Người Tạo</th>
                            <th>Tổng số lượng </th>
                            <th>Tổng Giá</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $tong=0;
                            $tongsanpham =0;
                        ?>
                        @foreach($products as $item)
                            <tr class="odd gradeX">
                                <td>{{$item->id}}</td>
                                <td>{{$item->title}}t</td>
                                <td>{{$item->category_name}}</td>
                                <td>{{number_format($item->price,'0','.',',')}}</td>
                                <td>
                                    @if(isset($color[$item->id]))
                                        @for($i=0;$i<count($color[$item->id]);$i++)
                                                <p style="background-color:{{$color[$item->id][$i]['color']}}; border-radius: 50%; width: 50px;height: 50px;">
                                                </p>
                                        @endfor
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($color[$item->id]))
                                        @for($i=0;$i<count($color[$item->id]);$i++)
                                            <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                            [ -
                                            @if(!empty($size[$item->id][$color_id]))

                                                @for($j=0;$j<count($size[$item->id][$color_id]);$j++)
                                                    @if(isset($size[$item->id][$color_id][$j]['size']))
                                                        {{$size[$item->id][$color_id][$j]['size'] }}-
                                                    @endif
                                                @endfor
                                                ]&nbsp &nbsp
                                            @endif
                                        @endfor
                                    @endif
                                </td>
                                <td>
                                    <?php $soluong=0;?>
                                        @if(!empty($color[$item->id]))

                                            @for($i=0;$i<count($color[$item->id]);$i++)
                                                <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                [#
                                                @if(!empty($size[$item->id][$color_id]))

                                                    @for($j=0;$j<count($size[$item->id][$color_id]);$j++)
                                                        <?php $size_id =$size[$item->id][$color_id][$j]['size_id'] ?>
                                                        [ -
                                                        @if(!empty($quantity[$item->id][$color_id][$size_id]))
                                                            @for($k=0;$k<count($quantity[$item->id][$color_id][$size_id]);$k++)
                                                                @if(isset($quantity[$item->id][$color_id][$size_id][$k]))
                                                                    <?php $soluong += $quantity[$item->id][$color_id][$size_id][$k];?>
                                                                    {{$quantity[$item->id][$color_id][$size_id][$k]}} -
                                                                @endif
                                                            @endfor
                                                            ],
                                                        @endif

                                                    @endfor

                                                @endif
                                                #]&nbsp &nbsp
                                            @endfor
                                        @endif

                                </td>
                                <td>{{$item->user_name}}</td>
                                <td>{{$soluong}}</td>
                                <td>
                                    <?php
                                    $tongsanpham+=$soluong;
                                    $tong+= $item->price*$soluong;
                                    ?>
                                    {{number_format($item->price*$soluong,'0','.',',')}}
                                </td>
                            </tr>
<!--                            --><?php //$soluong=0;?>
                        @endforeach
                        </tbody>
                        <div>
                            <h4>TỔNG SỐ SẢN PHẨM : <b>{{$tongsanpham}}</b></h4>
                            <h4>TỔNG GIÁ: <b>{{number_format($tong,'0','.',',')}}</b></h4>
                        </div>
                    </table>

                    <div class="pagination pull-right">{!! $products->links() !!}</div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->


@endsection

