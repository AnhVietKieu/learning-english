@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div  class="grid-title no-border">
                    <h2>Thanh Lọc Dữ Liệu</h2>
                </div>
                <div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Bảng dữ liệu</th>
                            <th>Tổng số</th>
                            <th>Hành Động</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Hoá đơn</td>
                            <td>{{$recepit}}</td>
                            <td><a href="clean/recepit"><button class="btn btn-success"  @if( $recepit == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        <tr>
                            <td>Đơn Hàng</td>
                            <td>{{$delivery}}</td>
                            <td><a href="clean/delivery"><button class="btn btn-success" @if( $delivery == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        <tr>
                            <td>Sản Phẩm</td>
                            <td>{{$product}}</td>
                            <td><a href="clean/product"><button class="btn btn-success"  @if( $product == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        <tr>
                            <td>Danh Mục</td>
                            <td>{{$category}}</td>
                            <td><a href="clean/category"><button class="btn btn-success"  @if( $category == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        <tr>
                            <td>Màu Sắc</td>
                            <td>{{$color}}</td>
                            <td><a href="clean/color"><button class="btn btn-success"  @if( $color == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        <tr>
                            <td>Kích Thước</td>
                            <td>{{$size}}</td>
                            <td><a href="clean/size"><button class="btn btn-success"  @if( $size == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        <tr>
                            <td>Hình Ảnh</td>
                            <td>{{$photo}}</td>
                            <td><a href="clean/photo"><button class="btn btn-success"  @if( $photo == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        <tr>
                            <td>Nhân Viên</td>
                            <td>{{$user}}</td>
                            <td><a href="clean/user"><button class="btn btn-success"  @if( $user == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        <tr>
                            <td>Khách Hàng</td>
                            <td>{{$customer}}</td>
                            <td><a href="clean/customer"><button class="btn btn-success"  @if( $customer == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        <tr>
                            <td>Nhà Cung Cấp</td>
                            <td>{{$supplier}}</td>
                            <td><a href="clean/supplier"><button class="btn btn-success"  @if( $supplier == 0){{'disabled'}}@endif>Thanh Lọc</button></a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

