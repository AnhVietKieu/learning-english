@extends('admin.master')
@section('content')
    <div class="row">
        <div class="grid simple ">
            <h2>Quản lý thông báo</h2>
            <div style="padding: 15px;">
                <div class="row">
                    <div class="pull-left" style="margin-left: 20px;" >
                        <a class="btn btn-success" href="{{route('config_notification.create')}}"><i class="glyphicon glyphicon-plus"></i> Thêm</a>
                    </div>
                </div>
                <table class="table" id="example3">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên khoá</th>
                        <th>Hiển thị</th>
                        <th>Thời gian </th>
                        <th>Chức năng </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1;?>
                    @foreach($config_notifications as $item)
                        <tr class="odd gradeX">
                            <td>{{$i++}}</td>
                            <td>{{$item->title}}</td>
                            <td>
                               {!! $item->description !!}
                            </td>
                            <td>{{$item->time}}</td>
                            <td class="center">

                                <a href="config_notification_customer/send/{{$item->id}}">
                                    <button class="btn btn-success"  style="width: 84px; height: 36px; margin-bottom: 5px;" @if($item->status == 1){{'disabled'}}@endif><i class="glyphicon glyphicon-send"></i>@if($item->status == 1){{" Đã gửi"}}@else{{' Gửi'}}@endif</button>
                                </a>

                                <a href="config_notification/edit/{{$item->id}}">
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                <a href="config_notification/delete/{{$item->id}}">
                                    <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
