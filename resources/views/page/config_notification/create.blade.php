@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple form-grid">
                <div class="grid-title no-border">
                    <h4><b>Thông báo</b> </h4>
                    <div class="alert alert-success"  id="data"></div>
                </div>
                <div class="grid-body no-border">
                    <form  id="form_traditional_validation" name="config_notification.create" method="post" role="form" autocomplete="off" class="validate" novalidate="novalidate">
                        @csrf
                        <div class="form-group">
                            <label class="form-label">Tên tiêu đề :</label>
                            <input type="text" name="title"  class="form-control"  required aria-required="true">
                            <span id="error_validate_meta_key"></span>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Nội dung:</label>
                            <textarea class="form-control"  name="description"  id="editor1"  required="" aria-required="true">

                            </textarea>

                            <span id="error_validate_meta_value"></span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Đặt lịch gửi:</label>
                                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">

                                        <input type="text" name="time" class="form-control datetimepicker-input" data-target="#datetimepicker1" >
                                        <span class="input-group-addon" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <span class="fa fa-calendar"></span>
                                    </span>
                                    </div>
                                </div>
                                @if($errors->has('date_send'))
                                    <div class=" alert alert-warning">
                                        <strong>{{ $errors->first('date_send')}}</strong>
                                    </div>
                                @endif
                                <span id="error_validate_date_send"></span>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="pull-right">
                                <button class="btn btn-success btn-cons update-table-config" type="submit"><i class="fa fa-check-circle"></i>Thêm </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#data').css('display','none');
        $(function() {
            $("form[name='config_notification.create']").validate({
                rules: {
                    title: {
                        required: true
                    },
                    description:{
                        required:true
                    }
                },
                messages: {
                   title: {
                        required: "Tên hiển thị  không được bỏ trống",
                    },
                    description: {
                        required: "Mô tả  không được bỏ trống",
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });



        // Thay thế <textarea id="post_content"> với CKEditor
        CKEDITOR.replace( 'editor1' ,
            {
                filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            });
        // tham số là biến name của textarea

    </script>
@endsection
