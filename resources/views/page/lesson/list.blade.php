@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div  class="grid-title no-border">
                    <h2>{{lang_data('List lesson')}}</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="pull-left" style="margin-left: 20px;" >
                            <form method="get" action="{{route('lesson.search')}}">
                                <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                                <input type="text" name="search" placeholder="Tìm kiếm theo tên ">
                                <button class="btn btn-success"><i class="glyphicon glyphicon-search"></i> {{lang_data('Search')}}</button>
                            </form>
                        </div>
                        <form method="post" action="{{route('user.deleteall')}}">
                            @csrf
                            <div class="pull-left" style="margin-top:25px; margin-left: 9px;">
                                @if(menu_permission('per_delete_lesson')==true)
                                <button id="delete-user" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i>{{lang_data('Delete All')}}</button>
                               @endif
                                @if(menu_permission('per_create_lesson')==true)
                                <a class="btn btn-success" href="{{route('lesson.create')}}"><i class="glyphicon glyphicon-plus"></i> {{lang_data('Add')}}</a>
                                    @endif
                            </div>
                    </div>
                </div>
            </div>

            <div>
                <table class="table table-striped dataTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>STT</th>
                        <th>{{lang_data('Name')}}</th>
                        <th>{{lang_data('Description')}}</th>
                        <th>{{lang_data('Image')}}</th>
                        <th>{{lang_data('Title video')}}</th>
                        <th>{{lang_data('Video')}}</th>
                        <th>{{lang_data('Coure')}}</th>
                        <th>{{lang_data('Level')}}</th>
                        <th>{{lang_data('Time')}}</th>
                        <th>{{lang_data('Total score')}}</th>
                        <th>{{lang_data('User create')}}</th>
                        <th>{{lang_data('User update')}}</th>
                        <th>{{lang_data('Status')}}</th>
                        <th>{{lang_data('Action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lessons as $item)
                        <tr>
                            <td><input type="checkbox" name="user_id[]" value="{{$item->id}}" class='checkbox delete-user'></td>
                            <td>#{{$item->id}}</td>
                            <td>{{$item->title}}</td>
                            <td style="overflow: auto;height:150px;width: 200px;">{!! $item->description!!}</td>
                            <td><img src='{{asset('public/upload/lesson/'.$item->image)}}' width="50px" height="50px"></td>
                            <td>{{$item->title_video}}</td>
                            <td>{{$item->video}}</td>
                            <td>{{$item['coure']['name']}}</td>
                            <td><img src='{{asset('public/upload/level/'.$item['level']['image'])}}' width="50px" height="50px"></td>
                            <td>{{$item->time}}</td>
                            <td>{{$item->score}}</td>
                            <td>{{ \App\User::find_user($item->created_by)}}</td>
                            <td>{{ \App\User::find_user($item->updated_by)}}</td>
                            <td style="width: 200px; text-align: center">@if($item->status ==0)
                                    <p class="alert alert-success"><span>Hoạt động </span></p>
                                @elseif($item->status ==1)
                                    <p class="alert alert-danger"><span>Bị khoá</span></p>
                                @elseif($item->status ==2)
                                    <p class="alert alert-warning"><span>Không hoạt đông</span></p>
                                @endif
                            </td>
                            <td class="center">
                                @if(menu_permission('per_edit_lesson')==true)
                                <a href="{{route('lesson.update',$item->id)}}">
                                    <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                @endif
                                    @if(menu_permission('per_delete_lesson')==true)
                                <a href="{{route('lesson.delete',$item->id)}}">
                                    <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>
                                        @endif

                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                </form>
                <div class="pagination pull-right">{!! $lessons->links() !!}</div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#delete-user').css('display','none');
            $(".delete-user").on('change', function () {
                if ($("input[name='user_id[]']:checked").length > 1) {
                    $('#delete-user').css('display', 'inline-block');
                } else {
                    $('#delete-user').css('display', 'none');
                }
            });
        });
    </script>
@endsection
