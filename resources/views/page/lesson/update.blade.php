@extends('admin.master')

@section('content')
    <form  method="post" name="create_create" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>{{lang_data('Update new')}}</h4>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="reload"></a>
                        </div>
                    </div>
                    <div class="grid-body no-border" >
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">

                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Name lesson')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " value="{{$lesson->title}}" name="title" placeholder="" >
                                    </div>
                                </div>
                                <div >
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Image')}}:</label>
                                    </div>
                                    <div class="col-md-9" style="margin-bottom: 20px;">
                                        @if(!empty($lesson->image))
                                            <div class="img col-sm-4" style="margin-bottom: 10px;margin-top:50px;">
                                                <img src="{{asset('public/upload/lesson/'.$lesson->image)}}" id="preview" class="img-thumbnail">
                                            </div>
                                        @else
                                            <div class="img col-sm-4" style="margin-bottom: 10px;display: none;margin-top:50px;">
                                                <img src="" id="preview" class="img-thumbnail">
                                            </div>
                                        @endif
                                        <input type="file" class="form-control file-avatar" name="image">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Description')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <textarea class="form-control"  name="description"  id="editor1"  required="" aria-required="true">
                                            {{$lesson->description}}
                                         </textarea>

                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Title video')}} :</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " value="{{$lesson->title_video}}" name="title_video" placeholder="" >
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Url video')}} :</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " value="{{$lesson->video}}" name="video" placeholder="" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Coure')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <i class=""></i>
                                        <select class="form-control select2" id="cardType" name="coure_id" data-init-plugin="select2">

                                            @foreach($coures as $key)
                                                <option value="{{$key->id}}" @if($lesson->coure_id == $key->id){{'selected'}}@endif>{{$key->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Level')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <i class=""></i>
                                        <select class="form-control select2" id="cardType" name="level_id" data-init-plugin="select2">
                                            @foreach($levels as $key)
                                                <option value="{{$key->id}}" @if($lesson->level_id == $key->id){{'selected'}}@endif>{{$key->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Time test')}}:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " value="{{$lesson->time}}" name="time" placeholder="" >
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="controls col-md-3">
                                        <label class="form-label">{{lang_data('Score')}} :</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control "  value="{{$lesson->score}}" name="score" placeholder="" >
                                    </div>
                                </div>


                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>{{lang_data('Update new')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $(function() {
            $("form[name='create_create']").validate({
                rules: {
                    display: {
                        required: true
                    }
                },
                messages: {
                    display: {
                        required: "Tên hiển thị  không được bỏ trống",
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });

            $('input[type="file"]').change(function(e) {
                var fileName = e.target.files[0].name;
                $("#file").val(fileName);

                var reader = new FileReader();
                reader.onload = function(e) {
                    // get loaded data and render thumbnail.
                    $('.img').css('display','block');
                    document.getElementById("preview").src = e.target.result;
                };
                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            });

            CKEDITOR.replace( 'editor1' ,
                {
                    filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
                    filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
                    filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                    filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                });
        });
    </script>
@endsection
