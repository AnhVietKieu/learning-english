@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div  class="grid-title no-border">
                    <h2>Danh Sách Hoá Đơn</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="pull-left" style="margin-left: 20px;" >
                            <form method="get" action="{{route('delivery.search')}}">
                                <label><b style="text-decoration: underline;color: #0d0f12;">Tìm kiếm:</b></label>
                                <input type="text" name="search" placeholder="Tìm kiếm theo mã hóa đơn">
                                <button class="btn btn-success"><i class="glyphicon glyphicon-search"></i> Tìm kiếm</button>
                            </form>
                        </div>
                        <form method="post" action="">
                            @csrf
                            <div class="pull-left" style="margin-top:25px; margin-left: 9px;">
                                <a class="btn btn-success" href="{{route('delivery.create')}}"><i class="glyphicon glyphicon-plus"></i> Thêm</a>
                            </div>
                    </div>
                </div>
            </div>

            <div>
                <table class="table table-striped dataTable">
                    <thead>
                    <tr>
                        <th style="text-align: center">Mã Hóa Đơn</th>
                        <th>Ngày Tạo</th>
                        <th>Tổng Tiền</th>
                        <th>Trạng Thái</th>
                        <th>Hành Động</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                    @foreach($delivery_notes as $item)
                        <tr>
                            <td style="text-align: center">{{$item->id}}</td>
                            <td>{{$item->date_of_delivery_note}}</td>
                            <td>{{number_format($item->total)}} VNĐ</td>
                            <td>@if($item->status_pay == 1)
                                    <span>Chưa Thanh Toán</span>
                                @else
                                    <span>Thanh Toán</span>
                                @endif
                            </td>
                            <td class="center">
                                <a href="delivery/edit/{{$item->id}}">
                                    <button class="btn btn-success" title="Sửa" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#detailDelivery{{$item->id}}" title="Chi Tiết Hóa Đơn" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                <a href="delivery/delete/{{$item->id}}">
                                    <button class="btn btn-danger" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}" title="Xóa" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>
                                @if($item->status_pay == 1)
                                    <a href="delivery/check/{{$item->id}}">
                                        <button class="btn" style="background: #53aa78;" title="Thanh Toán" type="button"><i class="glyphicon glyphicon-check"></i></button>
                                    </a>
                                @endif
                                @if($item->status_pay == 0)
                                    <button class="btn" disabled style="background: #53aa78;" title="Thanh Toán" type="button"><i class="glyphicon glyphicon-check"></i></button>
                                @endif
                            </td>

                        </tr>
                        <div class="modal fade" id="detailDelivery{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <strong><h3 class="modal-title" id="exampleModalLongTitle">Chi Tiết Đơn Hàng</h3></strong>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Tên Khách Hàng:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">{{ $item->name }}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Địa Chỉ:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">{{ $item->address }}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Số Điện Thoại:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">{{ $item->phone }}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Tên Sản Phẩm:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                @if(!empty($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                        [ -
                                                                        @if(!empty($title[$item->id][$color_id]))
                                                                            @for($j=0;$j<count($title[$item->id][$color_id]);$j++)
                                                                                @if(isset($title[$item->id][$color_id][$j]))
                                                                                    <b>{{$title[$item->id][$color_id][$j] }}</b> -
                                                                                @endif
                                                                            @endfor
                                                                            ]&nbsp &nbsp
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Màu Sắc:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                @if(isset($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        @if(isset($color[$item->id]))
                                                                            <p style="background-color:{{$color[$item->id][$i]['color']}}; border-radius: 50%; width: 50px;height: 50px;">
                                                                            </p>
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Kích Thước:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                @if(!empty($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                        [ -
                                                                        @if(!empty($size[$item->id][$color_id]))
                                                                            @for($j=0;$j<count($size[$item->id][$color_id]);$j++)
                                                                                @if(isset($size[$item->id][$color_id][$j]))
                                                                                    <b>{{$size[$item->id][$color_id][$j] }}</b> -
                                                                                @endif
                                                                            @endfor
                                                                            ]&nbsp &nbsp
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Số Lượng Bán:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                @if(!empty($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                        [ -
                                                                        @if(!empty($quantity[$item->id][$color_id]))
                                                                            @for($j=0;$j<count($quantity[$item->id][$color_id]);$j++)
                                                                                @if(isset($quantity[$item->id][$color_id][$j]))
                                                                                    <b>{{$quantity[$item->id][$color_id][$j] }}</b> -
                                                                                @endif
                                                                            @endfor
                                                                            ]&nbsp &nbsp
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Đơn Giá:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="">
                                                                @if(!empty($color[$item->id]))
                                                                    @for($i=0;$i<count($color[$item->id]);$i++)
                                                                        <?php $color_id = $color[$item->id][$i]['color_id'] ?>
                                                                        [ -
                                                                        @if(!empty($price[$item->id][$color_id]))
                                                                            @for($j=0;$j<count($price[$item->id][$color_id]);$j++)
                                                                                @if(isset($price[$item->id][$color_id][$j]))
                                                                                    <b>{{$price[$item->id][$color_id][$j] }}</b> -
                                                                                @endif
                                                                            @endfor
                                                                            ]&nbsp &nbsp
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Tổng Tiền:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                <b>{{ number_format($item->total) }} VNĐ</b>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Nhân Viên Bán Hàng:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                {{ $item->username }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for="">Trạng Thái:</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <label for="" class="form-label">
                                                                @if($item->status_pay == 1)
                                                                    Chưa Thanh Toán
                                                                    @else
                                                                    Đã Thanh Toán
                                                                    @endif
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="delivery/invoice/{{$item->id}}"><button type="button" class="btn btn-secondary" ><i class="glyphicon glyphicon-export"></i>Xuất Hóa Đơn</button></a>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </tbody>

                </table>
                </form>
                <div class="pagination-button pull-right">{{ $delivery_notes->links() }}</div>
            </div>
        </div>
    </div>
@endsection
