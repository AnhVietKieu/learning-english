@extends('admin.master')

@section('content')

    <form action="{{route('delivery.create')}}" name="delivery_create" method="post" enctype="multipart/form-data">
        @csrf


        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2>Thêm Hóa Đơn</h2>
                    </div>
                    <div class="grid-body no-border" >
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Tên Khách Hàng:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="customer_id" data-init-plugin="select2">
                                            <option value="" disabled selected hidden></option>
                                            @foreach($customers as $items)
                                            <option value="{{$items->id}}">{{$items->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label" >Sản Phẩm:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="grid simple">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                                    <select class="form-control select2 update-product"  name="product_id[]" data-init-plugin="select2">
                                                        <option value=""></option>
                                                        @foreach($products as $item)
                                                            <option value="{{$item->id}}" id="{{$item->id}}">{{$item->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="data-color"></div>
                                    <div id="data-size"></div>
                                    <div id="data-quantity"></div>
                                    <div class="form-group include"></div>

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Giao Hàng:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="status" data-init-plugin="select2">
                                            <option value="0">Chưa Giao Hàng</option>
                                            <option value="1">Đã Giao Hàng</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Thanh Toán:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="status" data-init-plugin="select2">
                                            <option value="0">Chưa Thanh Toán</option>
                                            <option value="1">Đã Thanh Toán</option>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group" >
                                    <div class="controls col-md-3">
                                        <label class="form-label">Ngày Giao Hàng:</label>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="hidden" value=""  tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="day" data-init-plugin="select2">
                                            @for($i=1;$i<=31;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="hidden" value="" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="month" data-init-plugin="select2">
                                            @for($i=1;$i<=12;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor

                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="hidden" value="" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="year" data-init-plugin="select2">
                                            @for($i=2018;$i<2050;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Người Bán:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <select name="user_id" class="form-control" id="">
                                            <option value="{{session('user_id')}}">{{session('username')}}</option>
                                        </select>
                                    </div>

                                </div>

                                <div style="margin-right: 15px;">
                                    <button class="btn btn-danger pull-left push-input"  type="button"><i class="glyphicon glyphicon-plus"></i><span> Thêm Sản Phẩm</span></button>
                                </div>
                                <div class="col-md-12 m-t-10 m-b-10 text-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Thêm Mới</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
    <script>
        $(document).ready(function () {
            $(function() {
            $("form[name='delivery_create']").validate({
                rules: {
                    product_id:{
                        required:true,
                        maxlength:20
                    },
                    customer_id:{
                        required:true,
                        maxlength:20
                    },
                },
                messages: {
                    product_id:{
                        required:'Tên hiển thị không được bỏ trống',
                        maxlength: 'Tên không quá 20 kí tự '
                    },
                    customer_id:{
                        required:'Tên khách hàng không được bỏ trống',
                        maxlength: 'Tên không quá 20 kí tự '
                    },
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
            $('.push-input').click(function () {
                var dem = document.getElementsByClassName('count').length;

                var $html = '<div class="form-group count">\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Sản Phẩm:'+dem;
                    $html+='</label>\n' +
                    '                                    </div>\n' +
                    '\n' +
                    '                                    <div class="col-md-9">\n' +
                    '                                        <div class="grid simple">\n' +
                    '                                            <div class="row">\n' +
                    '                                                <div class="col-md-12">\n' +
                    '                                                    <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">\n' +
                    '                                                    <select class="form-control select3 update-product-test" id="select3" name="product_id[]" data-init-plugin="select3"><option></option>\n' +
                    '                                                        @foreach($products as $item)\n' +
                    '                                                            <option value="{{$item->id}}">{{$item->title}}</option>\n' +
                    '                                                        @endforeach\n' +
                    '                                                    </select>\n' +
                    '                                                </div>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                    </div><div id="data-color'+dem;
                $html+='"></div><div id="data-size'+dem;
                $html+='"></div><div id="data-quantity'+dem;
                $html+='"></div>';
                    {{--'                                    <div class="controls col-md-3">\n' +--}}
                    {{--'                                        <label class="form-label">Số Lượng:</label>\n' +--}}
                    {{--'                                    </div>\n' +--}}
                    {{--'                                    <div class="form-group col-md-9">\n' +--}}
                    {{--'                                        <input type="text" class="form-control" name="quantity[]">\n' +--}}
                    {{--'                                    </div>\n' +--}}
                    {{--'                                    <div class="controls col-md-3">\n' +--}}
                    {{--'                                        <label class="form-label">Màu Sắc:</label>\n' +--}}
                    {{--'                                    </div>\n' +--}}
                    {{--'                                    <div class="form-group col-md-9">\n' +--}}
                    {{--'                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">\n' +--}}
                    {{--'                                        <select class="form-control select3" id="color" name="color_id[]" data-init-plugin="select3">\n' +--}}
                    {{--'                                            @foreach($colors as $item)\n' +--}}
                    {{--'                                                <option value="{{$item->id}}">{{$item->color}}</option>\n' +--}}
                    {{--'                                            @endforeach\n' +--}}
                    {{--'                                        </select>\n' +--}}
                    {{--'                                    </div>\n' +--}}
                    {{--'                                    <div class="controls col-md-3">\n' +--}}
                    {{--'                                        <label class="form-label">Kích Thước:</label>\n' +--}}
                    {{--'                                    </div>\n' +--}}
                    {{--'                                    <div class="form-group col-md-9">\n' +--}}
                    {{--'                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">\n' +--}}
                    {{--'                                        <select class="form-control select2" id="size" name="size_id[]" data-init-plugin="select2">\n' +--}}
                    {{--'                                            @foreach($sizes as $item)\n' +--}}
                    {{--'                                                <option value="{{$item->id}}">{{$item->size}}</option>\n' +--}}
                    {{--'                                            @endforeach\n' +--}}
                    {{--'                                        </select>\n' +--}}
                    {{--'                                    </div>\n' +--}}
                    {{--'\n' +--}}
                    {{--'                                </div>\n' +--}}
                    {{--'                                <div class="form-group">\n' +--}}
                    {{--'                                    <div class="controls col-md-3">\n' +--}}
                    {{--'                                        <label class="form-label">Giá Bán:</label>\n' +--}}
                    {{--'                                    </div>\n' +--}}
                    {{--'                                    <div class="col-md-9" style="margin-bottom: 20px;">\n' +--}}
                    {{--'                                        <input type="text" name="price[]" id="" class="form-control">\n' +--}}
                    {{--'                                    </div>\n' +--}}
                    {{--'                                </div>';--}}
                $('.include').append($html);
                $(".update-product-test").on('change',function () {

                    var product_id = $(this).val();

                    var dem = document.getElementsByClassName('count').length;


                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',

                        url: '{{route('delivery.dataproduct')}}',

                        data: {

                            product_id:product_id,
                        },

                        success: function (data) {

                            $('#data-color0').html(data);

                            $('.update-color-product').on('change',function () {
                                var color_id = $(this).val();
                                var product_id =$(this).attr('data-product');


                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: 'POST',

                                    url: '{{route('delivery.datasize')}}',

                                    data: {

                                        product_id:product_id,color_id:color_id
                                    },

                                    success: function (data) {
                                        $('#data-size0').html(data);



                                        $('.update-size-product').on('change',function () {
                                            var size_id = $(this).val();
                                            var product_id =$(this).attr('data-product');


                                            $.ajax({
                                                headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                },
                                                type: 'POST',

                                                url: '{{route('delivery.dataquantity')}}',

                                                data: {

                                                    product_id:product_id,color_id:color_id,size_id:size_id
                                                },

                                                success: function (data) {
                                                    $('#data-quantity0').html(data);

                                                }

                                            });


                                        });


                                    }

                                });


                            });

                        }
                    });
                });
            });


            $(".update-product").on('change',function () {

               var product_id = $(this).val();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',

                    url: '{{route('delivery.dataproduct')}}',

                    data: {

                        product_id:product_id,
                    },

                    success: function (data) {
                             // $('#data').css('display', 'block');

                            document.getElementById('data-color').innerHTML = data;

                        $('.update-color-product').on('change',function () {
                            var color_id = $(this).val();
                            var product_id =$(this).attr('data-product');


                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type: 'POST',

                                url: '{{route('delivery.datasize')}}',

                                data: {

                                    product_id:product_id,color_id:color_id
                                },

                                success: function (data) {
                                    document.getElementById('data-size').innerHTML= data


                                    $('.update-size-product').on('change',function () {
                                        var size_id = $(this).val();
                                        var product_id =$(this).attr('data-product');


                                        $.ajax({
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                            type: 'POST',

                                            url: '{{route('delivery.dataquantity')}}',

                                            data: {

                                                product_id:product_id,color_id:color_id,size_id:size_id
                                            },

                                            success: function (data) {
                                                document.getElementById('data-quantity').innerHTML= data



                                            }

                                        });


                                    });


                                }

                                });


                        });

                    }
                });
            });



        });

    </script>
@endsection
