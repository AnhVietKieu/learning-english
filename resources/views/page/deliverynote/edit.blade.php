@extends('admin.master')

@section('content')

    <form action="" name="delivery_update" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h2>Sửa Hóa Đơn</h2>
                    </div>
                    <div class="grid-body no-border" >
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Tên Khách Hàng:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="customer_id" data-init-plugin="select2">
                                            @foreach($delivery_notes as $data)
                                            <option value="{{$data->customer_id}}">{{$data->name}}</option>
                                            @endforeach
                                            @foreach($customers as $items)
                                                <option value="{{$items->id}}">{{$items->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                @for($i=0;$i<count($delivery_note_details);$i++)
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Sản Phẩm {{$i+1}}:</label>
                                    </div>

                                    <div class="col-md-9">
                                        <div class="grid simple">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                                    <select class="form-control select2" id="product" name="product_id[]" data-init-plugin="select2">
                                                        <option value="{{ $delivery_note_details[$i]['product_id'] }}">{{ $delivery_note_details[$i]['title'] }}</option>
                                                        @foreach($products as $item)
                                                            <option value="{{$item->id}}">{{$item->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="controls col-md-3">
                                        <label class="form-label">Số Lượng:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="number" class="form-control" value="{{ $delivery_note_details[$i]['quantity'] }}"  name="quantity[]">
                                        <span style="color: red; margin-bottom: 2px;"></span>
                                    </div>
                                    <div class="controls col-md-3">
                                        <label class="form-label">Màu Sắc:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="color_id[]" data-init-plugin="select2">
                                            <option value="{{ $delivery_note_details[$i]['color_id'] }}">{{ $delivery_note_details[$i]['color'] }}</option>
                                            @foreach($colors as $item)
                                                <option value="{{$item->id}}">{{$item->color}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="controls col-md-3">
                                        <label class="form-label">Kích Thước:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="size_id[]" data-init-plugin="select2">
                                            <option value="{{ $delivery_note_details[$i]['size_id'] }}">{{ $delivery_note_details[$i]['size'] }}</option>
                                            @foreach($sizes as $item)
                                                <option value="{{$item->id}}">{{$item->size}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="controls col-md-3">
                                        <label class="form-label">Giá Bán:</label>
                                    </div>
                                    <div class="col-md-9" style="margin-bottom: 20px;">
                                        <input type="number" id="delivery_price" value="{{ $delivery_note_details[$i]['price'] }}" name="price[]" class="form-control">
                                        <span style="color: red; margin-bottom: 2px;"></span>
                                    </div>
                                </div>
                                @endfor
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Giao Hàng:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="status" data-init-plugin="select2">
                                            <option value="{{$delivery_notes[0]['status']}}">
                                                @if($delivery_notes[0]['status'] == 1)
                                                    Chưa Giao Hàng
                                                @else
                                                    Đã Giao Hàng
                                                @endif
                                            </option>
                                            <option value="1">Chưa Giao Hàng</option>
                                            <option value="0">Đã Giao Hàng</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Thanh Toán:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="status_pay" data-init-plugin="select2">
                                            <option value="{{$delivery_notes[0]['status_pay']}}">
                                                @if($delivery_notes[0]['status_pay'] == 1)
                                                    Chưa Thanh Toán
                                                @else
                                                    Đã Thanh Toán
                                                @endif
                                            </option>
                                            <option value="1">Chưa Thanh Toán</option>
                                            <option value="0">Đã Thanh Toán</option>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group" >
                                    <div class="controls col-md-3">
                                        <label class="form-label">Ngày Giao Hàng:</label>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <input type="hidden" value=""  tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="day" data-init-plugin="select2">
                                            @for($i=1;$i<=31;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="hidden" value="" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="month" data-init-plugin="select2">
                                            @for($i=1;$i<=12;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor

                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="hidden" value="" tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="year" data-init-plugin="select2">
                                            @for($i=2018;$i<2050;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Người Bán:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="hidden" value="" style="width:300px"  tabindex="-1" class="select2-offscreen">
                                        <select class="form-control select2" id="" name="user_id" data-init-plugin="select2">
                                            <option value="{{session('user_id')}}">{{session('username')}}</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group include"></div>
                                <div style="margin-right: 15px;">
                                    <button class="btn btn-danger pull-left push-input"  type="button"><i class="glyphicon glyphicon-plus"></i><span> Thêm Sản Phẩm</span></button>
                                </div>
                                <div class="col-md-12 m-t-10 m-b-10 text-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Thêm Mới</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
    <script>
        $(document).ready(function () {
            $('.push-input').click(function () {
                var $html = '<div class="form-group">\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Sản Phẩm:</label>\n' +
                    '                                    </div>\n' +
                    '\n' +
                    '                                    <div class="col-md-9">\n' +
                    '                                        <div class="grid simple">\n' +
                    '                                            <div class="row">\n' +
                    '                                                <div class="col-md-12">\n' +
                    '                                                    <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">\n' +
                    '                                                    <select class="form-control select2" id="product" name="product_id[]" data-init-plugin="select2">\n' +
                    '                                                        @foreach($products as $item)\n' +
                    '                                                            <option value="{{$item->id}}">{{$item->title}}</option>\n' +
                    '                                                        @endforeach\n' +
                    '                                                    </select>\n' +
                    '                                                </div>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Số Lượng:</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-9">\n' +
                    '                                        <input type="number" class="form-control" name="quantity[]">\n' +
                    '                                        <span style="color: red; margin-bottom: 2px;"></span>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Màu Sắc:</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-9">\n' +
                    '                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">\n' +
                    '                                        <select class="form-control select2" id="color" name="color_id[]" data-init-plugin="select2">\n' +
                    '                                            @foreach($colors as $item)\n' +
                    '                                                <option value="{{$item->id}}">{{$item->color}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Kích Thước:</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-9">\n' +
                    '                                        <input type="hidden" value="" style="width:300px" tabindex="-1" class="select2-offscreen">\n' +
                    '                                        <select class="form-control select2" id="size" name="size_id[]" data-init-plugin="select2">\n' +
                    '                                            @foreach($sizes as $item)\n' +
                    '                                                <option value="{{$item->id}}">{{$item->size}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '\n' +
                    '                                </div>\n' +
                    '                                <div class="form-group">\n' +
                    '                                    <div class="controls col-md-3">\n' +
                    '                                        <label class="form-label">Giá Bán:</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-md-9" style="margin-bottom: 20px;">\n' +
                    '                                        <input type="number" name="price[]" id="" class="form-control">\n' +
                    '                                        <span style="color: red; margin-bottom: 2px;"></span>\n' +
                    '                                    </div>\n' +
                    '                                </div>';
                $('.include').append($html);
            });
        });

    </script>

@endsection
