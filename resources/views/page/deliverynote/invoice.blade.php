@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-md-11">
            <div class="grid simple">
                <div class="grid-body no-border invoice-body">
                    <br>
                    <div class="pull-left"><img src="{{asset('public/assets/img/invoicelogo.png')}}"
                                                data-src="{{asset('public/assets/img/invoicelogo.png')}}"
                                                data-src-retina="{{asset('public/assets/img/invoicelogo2x.png')}}" width="222"
                                                height="31" class="invoice-logo" alt="">
                        <address>
                            <strong>Twitter, Inc.</strong><br>
                            795 Folsom Ave, Suite 600<br>
                            San Francisco, CA 94107<br>
                            <abbr title="Phone">P:</abbr> (123) 456-7890
                        </address>
                    </div>
                    <div class="pull-right">
                        <h2>HÓA ĐƠN</h2>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        @foreach($delivery_notes as $data)
                        <div class="col-md-9">
                            <h4 class="semi-bold">{{$data->name}}</h4>
                            <address>
                                <strong>{{$data->address}}</strong><br>
                                <abbr title="Phone">P:</abbr> {{$data->phone}}
                            </address>
                        </div>
                        <div class="col-md-3">
                            <br>
                            <div>
                                <div class="pull-left"> Mã Hóa Đơn :</div>
                                <div class="pull-left"> {{$data->id}}</div>
                                <div class="clearfix"></div>
                            </div>
                            <div>
                                <div class="pull-left"> Ngày Tạo :</div>
                                <div class="pull-left"> {{date_format($data->date_of_delivery_note,"d.m.Y")}}</div>
                                <div class="clearfix"></div>
                            </div>
                            <br>
                            <div class="">
                                <div class="pull-left"> Nhân Viên:</div>
                                <div style="text-decoration: underline; font-size: 15px;"><strong style="padding: 12px">{{$data->username}}</strong></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                            @endforeach
                    </div>

                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width:100px" class="unseen text-center">Số lượng</th>
                            <th style="width: 150px" class="unseen text-center">Mã Sản Phẩm</th>
                            <th class="text-left">Thông Tin Chi Tiết</th>
                            <th style="width:140px" class="text-right">Đơn Giá</th>
                            <th style="width:120px" class="text-right">Thành Tiền</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($delivery_note_details as $list)
                        <tr>
                            <td class="unseen text-center">{{$list->quantity}}</td>
                            <td class="unseen text-center">{{$list->id}}</td>
                            <td>
                                Tên Sản Phẩm: {{$list->title}}, Kích Thước: {{$list->size}}, Màu Sắc: {{$list->color}}
                            </td>
                            <td class="text-right">{{$list->price}} VNĐ</td>
                            <td class="text-right">{{ $list->quantity * $list->price }} VNĐ</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" rowspan="4"></td>
                            <td class="text-right"><strong style="font-size: 16px">Tổng Tiền</strong></td>
                            <td class="text-right"><strong style="font-size: 16px">{{$list->total}} VNĐ</strong></td>
                        </tr>
                        </tbody>
                    </table>

                    <br>
                    <br>
                    <h5 class="text-right text-black">Ký Tên:</h5>
                    <h5 class="text-right semi-bold text-black"><p style="text-decoration: underline; font-size: 15px">{{$delivery_notes[0]['name']}}</p></h5>
                    <br>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <div class="invoice-button-action-set">
                <p>
                    <button class="btn btn-primary" onclick="window.print();" type="button"><i class="fa fa-print"></i></button>
                </p>
            </div>
        </div>
    </div>
@endsection
