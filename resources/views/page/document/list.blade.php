@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div  class="grid-title no-border">
                    <h2>{{lang_data('Document')}}</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="pull-left" style="margin-left: 20px;" >
                            <form method="get" action="{{route('user.search')}}">
                                <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                                <input type="text" name="search" placeholder="Tìm kiếm theo tên email">
                                <button class="btn btn-success"><i class="glyphicon glyphicon-search"></i> {{lang_data('Search')}}</button>
                            </form>
                        </div>
                        <form method="post" action="{{route('user.deleteall')}}">
                            @csrf
                            <div class="pull-left" style="margin-top:25px; margin-left: 9px;">
                                @if(menu_permission('per_delete_document')==true)
                                <button id="delete-user" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i> {{lang_data('Delete all')}}</button>
                               @endif
                                    @if(menu_permission('per_create_document')==true)
                                <a class="btn btn-success" href="{{route('document.create')}}"><i class="glyphicon glyphicon-plus"></i> {{lang_data('Add')}}</a>
                           @endif
                            </div>
                    </div>
                </div>
            </div>

            <div>
                <table class="table table-striped dataTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>STT</th>
                        <th>{{lang_data('Name')}}</th>
                        <th>{{lang_data('File')}}</th>
                        <th>{{lang_data('Status')}}</th>
                        <th>{{lang_data('Action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($documents as $item)
                        <tr>
                            <td><input type="checkbox" name="user_id[]" value="{{$item->id}}" class='checkbox delete-user'></td>
                            <td>#{{$item->id}}</td>
                            <td>{{$item->title}}</td>
                            <td>{{$item->file}}</td>
                            <td style="width: 200px; text-align: center">@if($item->status ==0)
                                    <p class="alert alert-success"><span>{{lang_data('Action')}} </span></p>
                                @elseif($item->status ==1)
                                    <p class="alert alert-danger"><span>{{lang_data('Lock')}}</span></p>
                                @elseif($item->status ==2)
                                    <p class="alert alert-warning"><span>{{lang_data('No Action')}}</span></p>
                                @endif
                            </td>
                            <td class="center">
                                @if(menu_permission('per_edit_document')==true)
                                <a href="document/edit/{{$item->id}}">
                                    <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                @endif
                                    @if(menu_permission('per_delete_document')==true)
                                <a href="document/delete/{{$item->id}}">
                                    <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>
                                        @endif

                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                </form>

            </div>
        </div>
    </div>
    </div>
    </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#delete-user').css('display','none');
            $(".delete-user").on('change', function () {
                if ($("input[name='user_id[]']:checked").length > 1) {
                    $('#delete-user').css('display', 'inline-block');
                } else {
                    $('#delete-user').css('display', 'none');
                }
            });
        });
    </script>
@endsection
