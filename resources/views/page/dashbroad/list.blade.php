@extends('admin.master')

@section('content')
    <div class="page-title">
        <h3>{{lang_data('Dashbroad')}}</h3>
    </div>
    <div id="container">
        <div class="row 2col">
            <div class="col-md-3 col-sm-6 spacing-bottom-sm spacing-bottom">
                <a href="{{route('coure.list')}}"> <div class="tiles blue added-margin">
                        <div class="tiles-body">
                            <div class="controller">
                            </div>
                            <div class="tiles-title"  style="font-size: 25px;">{{lang_data('Coure')}}</div>
                            <div class="heading"> <span class="animate-number" data-value="{{$coure}}" data-animation-duration="1200">0</span> </div>
                            <div class="progress transparent progress-small no-radius">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%"></div>
                            </div>
                        </div>
                    </div></a>

            </div>
            <div class="col-md-3 col-sm-6 spacing-bottom-sm spacing-bottom">
                <a href="{{route('lesson.list')}}">
                    <div class="tiles green added-margin">
                        <div class="tiles-body">
                            <div class="controller">
                            </div>
                            <div class="tiles-title"  style="font-size: 25px;">{{lang_data('Lesson')}}</div>
                            <div class="heading"> <span class="animate-number" data-value="{{$lesson}}" data-animation-duration="1000">0</span> </div>
                            <div class="progress transparent progress-small no-radius">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="79%"></div>
                            </div>

                        </div>
                    </div>
                </a>

            </div>
            <div class="col-md-3 col-sm-6 spacing-bottom">
                <a href="{{route('document.list')}}">
                    <div class="tiles red added-margin">
                        <div class="tiles-body">
                            <div class="controller">
                            </div>
                            <div class="tiles-title"  style="font-size: 25px;">{{lang_data('Number document')}}</div>
                            <div class="heading"><span class="animate-number" data-value="{{$document}}" data-animation-duration="1200">0</span> </div>
                            <div class="progress transparent progress-white progress-small no-radius">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="45%"></div>
                            </div>

                        </div>
                    </div>
                </a>

            </div>
            <div class="col-md-3 col-sm-6">
                <a href="{{route('vocabularie.list')}}">
                    <div class="tiles purple added-margin">
                        <div class="tiles-body">
                            <div class="controller">
                            </div>
                            <div class="tiles-title"  style="font-size: 25px;">{{lang_data('Number vocabularie')}}</div>
                            <div class="row-fluid">
                                <div class="heading"><span class="animate-number" data-value="{{$vocu}}" data-animation-duration="700">0</span> </div>
                                <div class="progress transparent progress-white progress-small no-radius">
                                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="12%"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </a>

            </div>
        </div>

        <div class="row 2col">
            <div class="col-md-3 col-sm-6 spacing-bottom-sm spacing-bottom">
                <a href="{{route('read.list')}}">
                    <div class="tiles blue added-margin">
                        <div class="tiles-body">
                            <div class="controller">
                            </div>
                            <div class="tiles-title"  style="font-size: 25px;">{{lang_data('Read')}}</div>
                            <div class="heading"> <span class="animate-number" data-value="{{$read}}" data-animation-duration="1200">0</span></div>
                            <div class="progress transparent progress-small no-radius">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%"></div>
                            </div>

                        </div>
                    </div>
                </a>

            </div>
            <div class="col-md-3 col-sm-6 spacing-bottom-sm spacing-bottom">
                <a href="{{route('listen.list')}}">
                    <div class="tiles green added-margin">
                        <div class="tiles-body">
                            <div class="controller">
                            </div>
                            <div class="tiles-title"  style="font-size: 25px;">{{lang_data('Listen')}}</div>
                            <div class="heading"> <span class="animate-number" data-value="{{$listen}}" data-animation-duration="1000">0</span> </div>
                            <div class="progress transparent progress-small no-radius">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="79%"></div>
                            </div>

                        </div>
                    </div>
                </a>

            </div>
            <div class="col-md-3 col-sm-6 spacing-bottom">
                <a href="{{route('write.list')}}">
                    <div class="tiles red added-margin">
                        <div class="tiles-body">
                            <div class="controller">
                            </div>
                            <div class="tiles-title"  style="font-size: 25px;">{{lang_data('Write')}}</div>
                            <div class="heading"> <span class="animate-number" data-value="{{$write}}" data-animation-duration="1200">0</span> </div>
                            <div class="progress transparent progress-white progress-small no-radius">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="45%"></div>
                            </div>

                        </div>
                    </div>
                </a>

            </div>
            <div class="col-md-3 col-sm-6">
                <a href="{{route('speak.list')}}">
                    <div class="tiles purple added-margin">
                        <div class="tiles-body">
                            <div class="controller">
                            </div>
                            <div class="tiles-title" style="font-size: 25px;">{{lang_data('Speak')}}</div>
                            <div class="row-fluid">
                                <div class="heading"> <span class="animate-number" data-value="{{$speak}}" data-animation-duration="700">0</span> </div>
                                <div class="progress transparent progress-white progress-small no-radius">
                                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="12%"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </a>

            </div>
        </div>



        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="grid simple ">
                            <div class="grid-body no-border" style="overflow: auto;">
                                <h3>  <span class="semi-bold">{{lang_data('Customer')}}</span></h3>

                                <br>
                                <table class="table table-bordered no-more-tables" >
                                    <thead class="cf">
                                        <tr>

                                            <th class="text-center">{{lang_data('Name')}}</th>
                                            <th class="text-center">{{lang_data('Email')}}</th>
                                            <th class="text-center">{{lang_data('Gender')}}</th>
                                            <th class="text-center">{{lang_data('Create Day')}}</th>
                                            <th class="text-center">{{lang_data('Level')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($customer as $item)
                                        <tr>
                                            <td>{{$item->username}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>@if($item->gender==0){{'Nữ'}}@else{{'Nam'}}@endif</td>
                                            <td>{{date('Y/m/d',strtotime($item['created_at']))}}</td>
                                            <td>@if(isset($item['levels']['image']))<img src='{{asset('public/upload/level/'.$item['levels']['image'])}}' width="50px" height="50px">@endif</td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="grid simple ">

                            <div class="grid-body no-border" style="overflow: auto;">
                                <h3><span class="semi-bold">{{lang_data('Teacher')}}</span></h3>
                                <br>
                                <table class="table table-bordered no-more-tables">
                                    <thead>
                                    <tr>

                                        <th class="text-center">{{lang_data('Name')}}</th>
                                        <th class="text-center">{{lang_data('Email')}}</th>
                                        <th class="text-center">{{lang_data('Create Day')}}</th>
                                        <th class="text-center">{{lang_data('Gender')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($teacher as $item)
                                    <tr>
                                        <td class="text-center">{{$item->name}}</td>
                                        <td class="text-center">{{$item->email}}</td>
                                        <td>{{date('Y/m/d',strtotime($item['created_at']))}}</td>
                                        <td class="text-center">@if($item->gender==0){{'Nữ'}}@else{{'Nam'}}@endif</td>

                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
    <!-- END PAGE -->
    @endsection
