@extends('admin.master')
@section('content')
        <div class="col-md-12">
            <div class="grid simple ">
                <div class="grid-title no-border">
                    <h2>{{lang_data('Coure')}}</h2>
                </div>
                <div class="grid-body no-border">
                    <form method="post" action="{{route('permision.deleteall')}}">
                        @csrf
                        <div class="horizontal">
                            <div class="pull-left">
                                @if(menu_permission('per_delete_coure')==true)
                                <button id="delete-user" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i>{{lang_data('Delete all')}}</button>
                                @endif
                                @if(menu_permission('per_create_coure')==true)
                                <a class="btn btn-success" href="{{route('coure.create')}}"><i class="glyphicon glyphicon-plus"></i> {{lang_data('Add')}}</a>
                                    @endif
                            </div>
                        </div>

                        <table class="table no-more-tables">
                            <thead>
                            <tr>
                                <th>{{lang_data('Select')}}</th>
                                <th>{{lang_data('Name')}}</th>
                                <th>{{lang_data('Image')}}</th>
                                <th>{{lang_data('Description')}}</th>
                                <th>{{lang_data('Status')}}</th>
                                <th>{{lang_data('Level')}}</th>
                                <th>{{lang_data('User create')}}</th>
                                <th>{{lang_data('User update')}}</th>
                                <th>{{lang_data('Function')}}</th>

                            </tr>
                            </thead>
                            <tbody>
                            {!! $permissions !!}
                            </tbody>
                        </table>
                    </form>

                </div>
            </div>
        </div>
    </div>
        <script>
            $(document).ready(function () {
                $('#delete-user').css('display','none');
                $(".delete-user").on('change', function () {
                    if ($("input[name='permission_id[]']:checked").length > 1) {
                        $('#delete-user').css('display', 'inline-block');
                    } else {
                        $('#delete-user').css('display', 'none');
                    }
                });
            });

        </script>
@endsection
