@extends('admin.master')
@section('content')
    <div class="col-md-12">
        <div class="grid simple ">
            <div class="grid-title no-border">
                <h2>Danh mục</h2>
            </div>
            <div class="grid-body no-border">
                <form method="post" action="{{route('category.deleteall')}}">
                    @csrf
                    <div class="horizontal">
                        <div class="pull-left">
                            <button id="delete-category" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i> Xoá toàn bộ</button>
                            <a class="btn btn-success" href="{{route('category.create')}}"><i class="glyphicon glyphicon-plus"></i> Thêm</a>
                        </div>
                    </div>

                    <table class="table no-more-tables">
                        <thead>
                        <tr>
                            <th>Chọn</th>
                            <th>Mã ID</th>
                            <th>Tên danh mục</th>
                            <th style="text-align: center">Trạng thái</th>
                            <th>Chức năng </th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php echo $categories;?>
                        </tbody>
                    </table>
                </form>

            </div>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#delete-category').css('display','none');
            $(".delete-category").on('change', function () {
                if ($("input[name='category_id[]']:checked").length > 1) {
                    $('#delete-category').css('display', 'inline-block');
                } else {
                    $('#delete-category').css('display', 'none');
                }
            });
        });

    </script>
@endsection
