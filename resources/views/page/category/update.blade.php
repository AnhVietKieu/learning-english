@extends('admin.master')

@section('content')
    <form  method="post" name="category_create">
        @csrf

        <div class="row">

            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>Sửa danh mục</h4>

                    </div>
                    <div class="grid-body no-border" >
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <div class="controls col-md-3">
                                        <label class="form-label">Tên danh mục:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <input type="text" class="form-control " name="name" value="{{$category->name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls col-md-3">
                                        <label class="form-label">Phân cấp:</label>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <i class=""></i>
                                        <select class="form-control select2" id="cardType" name="parent_id" data-init-plugin="select2">
                                            <option value="0"><span>Mặc định</span>
                                            </option>
                                            @foreach($category_fathers as $item)
                                                @if($item->id!== $category->id)
                                                <option value="{{$item->id}}" @if($category->parent_id==$item->id){{'selected'}}@endif>
                                                    {{$item->name}}
                                                </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12 m-t-10 m-b-10 align-center">
                                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Cập nhập</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $(function() {
            $("form[name='category_create']").validate({
                rules: {
                   name: {
                        required: true
                    }
                },
                messages: {
                   name: {
                        required: "Tên hiển thị  không được bỏ trống",
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
