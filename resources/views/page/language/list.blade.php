@extends('admin.master')
@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple ">
                        <div class="grid-title no-border">
                            <h4><span class="semi-bold">{{lang_data('Language')}}</span></h4>

                        </div>
                        <div class="grid-body no-border">

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form action="{{route('language.create')}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <div class="controls col-md-12">
                                                <label class="form-label">{{lang_data('Language')}}:</label>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <input type="hidden" value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                                                <select class="form-control select2" id="cardType" name="language" data-init-plugin="select2">
                                                    <option value="af_Afrikaans">Afrikaans</option>
                                                    <option value="sq_Albanian">Albanian</option>
                                                    <option value="am_Amharic">Amharic</option>
                                                    <option value="ar_Arabic">Arabic</option>
                                                    <option value="hy_Armenian">Armenian</option>
                                                    <option value="az_Azerbaijan">Azerbaijan</option>
                                                    <option value="bn_Bengali">Bengali</option>
                                                    <option value="eu_Basque">Basque</option>
                                                    <option value="be_Belarusian">Belarusian</option>
                                                    <option value="bg_Bulgarian">Bulgarian</option>
                                                    <option value="ca_Catalan">Catalan</option>
                                                    <option value="zh_Chinese">Chinese</option>
                                                    <option value="hr_Croatian">Croatian</option>
                                                    <option value="cs_Czech">Czech</option>
                                                    <option value="da_Danish">Danish</option>
                                                    <option value="nl_Dutch">Dutch</option>
                                                    <option value="en_English">English</option>
                                                    <option value="et_Estonian">Estonian</option>
                                                    <option value="fi_Finnish">Finnish</option>
                                                    <option value="fr_French">French</option>
                                                    <option value="gl_Galician">Galician</option>
                                                    <option value="ka_Georgian">Georgian</option>
                                                    <option value="de_German">German</option>
                                                    <option value="el_Greek">Greek</option>
                                                    <option value="gu_Gujarati">Gujarati</option>
                                                    <option value="he_Hebrew">Hebrew</option>
                                                    <option value="hi_Hindi">Hindi</option>
                                                    <option value="hu_Hungarian">Hungarian</option>
                                                    <option value="is_Icelandic">Icelandic</option>
                                                    <option value="id_Indonesian">Indonesian</option>
                                                    <option value="ga_Irish">Irish</option>
                                                    <option value="it_Italian">Italian</option>
                                                    <option value="ja_Japanese">Japanese</option>
                                                    <option value="kk_Kazakh">Kazakh</option>
                                                    <option value="ko_Korean">Korean</option>
                                                    <option value="lv_Latvian">Latvian</option>
                                                    <option value="lt_Lithuanian">Lithuanian</option>
                                                    <option value="mk_Macedonian">Macedonian</option>
                                                    <option value="ms_Malay">Malay</option>
                                                    <option value="mn_Mongolian">Mongolian</option>
                                                    <option value="ne_Nepali">Nepali</option>
                                                    <option value="nb_Norwegian-Bokmal">Norwegian-Bokmal</option>
                                                    <option value="nn_Norwegian-Nynorsk">Norwegian-Nynorsk</option>
                                                    <option value="fa_Persian">Persian</option>
                                                    <option value="pl_Polish">Polish</option>
                                                    <option value="pt_Portuguese">Portuguese</option>
                                                    <option value="ro_Romanian">Romanian</option>
                                                    <option value="ru_Russian">Russian</option>
                                                    <option value="sr_Serbian">Serbian</option>
                                                    <option value="si_Sinhala">Sinhala</option>
                                                    <option value="sk_Slovak">Slovak</option>
                                                    <option value="sl_Slovenian">Slovenian</option>
                                                    <option value="es_Spanish">Spanish</option>
                                                    <option value="sw_Swahili">Swahili</option>
                                                    <option value="sv_Swedish">Swedish</option>
                                                    <option value="ta_Tamil">Tamil</option>
                                                    <option value="te_Telugu">Telugu</option>
                                                    <option value="th_Thai">Thai</option>
                                                    <option value="tr_Turkish">Turkish</option>
                                                    <option value="uk_Ukrainian">Ukrainian</option>
                                                    <option value="ur_Urdu">Urdu</option>
                                                    <option value="uz_Uzbek">Uzbek</option>
                                                    <option value="vi_Vietnamese">Vietnamese</option>
                                                    <option value="cy_Welsh">Welsh</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="controls col-md-12">
                                                <label class="form-label">{{lang_data('Status')}}:</label>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <input type="hidden" value="" style="width:300px" id="e12" tabindex="-1" class="select2-offscreen">
                                                <select class="form-control select2" id="cardType" name="status" data-init-plugin="select2">
                                                    <option value="1">{{lang_data('Action')}}</option>
                                                    <option value="0">{{lang_data('No action')}}</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div >
                                            <div class="controls col-md-12">
                                                <label class="form-label">{{lang_data('Flag')}}:</label>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 20px;">
                                                <div class="img col-sm-4" style="display: none;margin-top:50px; ">
                                                    <img src="" id="preview" class="img-thumbnail">
                                                </div>
                                                <input type="file" class="form-control file-avatar" name="icon">
                                                <span style="color: red; margin-bottom: 2px;">@if($errors->has('avatar')){{$errors->first('avatar')}}@endif</span>
                                            </div>
                                        </div>

                                        @if(menu_permission('per_create_language')==true)
                                        <div class="col-md-12 m-t-10 m-b-10 align-center">
                                            <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i> {{lang_data('Create new')}}</button>
                                        </div>
                                            @endif
                                    </form>


                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple ">
                        <div class="grid-title no-border">
                            <h4>{{lang_data('List language')}}</h4>
                            <div class="card-body">
                                <div class="row">
                                    <div class="pull-left" style="margin-left: 20px;" >
                                        <form method="get" action="{{route('user.search')}}">
                                            <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                                            <input type="text" name="search" placeholder="Tìm kiếm theo tên email">
                                            <button class="btn btn-success"><i class="glyphicon glyphicon-search"></i> {{lang_data('Search')}}</button>
                                            @if(menu_permission('per_create_default_language')==true)
                                            <a class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title=""
                                               href="{{route('language.create_lan')}}"
                                               data-original-title="Thêm"><i class="fa fa-credit-card"></i></a>
                                                @endif
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="grid-body no-border">
                            <table class="table table-bordered no-more-tables">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width:12%">{{lang_data('Flag')}}</th>
                                    <th class="text-center" style="width:22%">{{lang_data('Language')}}</th>
                                    <th class="text-center" style="width:22%">{{lang_data('Status')}}</th>
                                    <th class="text-center" style="width:22%">{{lang_data('Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($language as $item)
                                    <tr>
                                    <td><img src='{{asset('public/upload/language/'.$item['icon'])}}' width="50px" height="50px"></td>
                                    <td class="text-center">{{$item['language']}}</td>
                                        <td class="text-center">
                                            @if($item->status ==1)
                                                <p class="alert alert-success"><span>{{lang_data('Action')}} </span></p>
                                            @elseif($item->status ==0)
                                                <p class="alert alert-danger"><span>{{lang_data('Lock')}}</span></p>
                                            @endif
                                        </td>
                                        <td>
                                            @if(menu_permission('per_create_translat_language')==true)
                                        <a class="btn btn-complete btn-xs" data-toggle="tooltip" data-placement="top" title=""
                                           href="{{route('language.setting_lan',$item->id)}}"
                                           data-original-title="Dịch"><i class="fa fa-language"></i></a>
                                            @endif
                                            @if(menu_permission('per_edit_language')==true)
                                        <a class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title=""
                                           href="{{route('language.update',$item->id)}}" data-original-title="Sửa"><i class="fa fa-edit"></i></a>
                                            @endif
                                                @if(menu_permission('per_delete_language'))
                                        <a href="{{route('language.delete',$item->id)}}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-danger btn-xs cdelete" id="2" data-original-title="Xóa">
                                            <i class="fa fa-trash"></i></a>
                                                    @endif

                                    </td>
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination pull-right">{!! $language->links() !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    </div>
    </div>
    </div>


        <script>
        $(function() {

            $(document).ready(function () {
                $('#delete-user').css('display','none');
                $(".delete-user").on('change', function () {
                    if ($("input[name='user_id[]']:checked").length > 1) {
                        $('#delete-user').css('display', 'inline-block');
                    } else {
                        $('#delete-user').css('display', 'none');
                    }
                });
            });
            $("form[name='create_create']").validate({
                rules: {
                    display: {
                        required: true
                    }
                },
                messages: {
                    display: {
                        required: "Tên hiển thị  không được bỏ trống",
                    }

                },
                submitHandler: function(form) {
                    form.submit();
                }
            });

            $('input[type="file"]').change(function(e) {
                var fileName = e.target.files[0].name;
                $("#file").val(fileName);

                var reader = new FileReader();
                reader.onload = function(e) {
                    // get loaded data and render thumbnail.
                    $('.img').css('display','block');
                    document.getElementById("preview").src = e.target.result;
                };
                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            });

        });
    </script>
    @endsection
