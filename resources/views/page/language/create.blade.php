@extends('admin.master')
@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                    <div class="grid-title no-border">
                        <h4>Danh sách </h4>
                        <div class="card-body">
                            <div class="row">
                                <div class="pull-left" style="margin-left: 20px;" >
                                    <form method="post" action="{{route('language.lan_data')}}">
                                        @csrf
                                        <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Name')}}:</b></label>
                                        <input type="text" name="lan_data" placeholder="">
                                        <button class="btn btn-success">{{lang_data('Add')}}</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="grid-body no-border">
                        <table class="table table-bordered no-more-tables">
                            <thead>
                            <tr>
                                <th class="text-center" style="width:12%">{{lang_data('Word')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($language_data as $item)
                                <tr>
                                    <td class="text-center">{{$item->lan_data}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination pull-right">{!! $language_data->links() !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
