@extends('admin.master')
@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                    <div class="grid-title no-border">
                        <h4>{{lang_data('List language')}} {{$lan['language']}} </h4>
                        <div class="card-body">
                            <div class="row">
                                <div class="pull-left" style="margin-left: 20px;" >
                                    <form method="get" action="{{route('language.search')}}">
                                        <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                                        <input type="hidden" name="lan_id" value="{{$lan['id']}}">
                                        <input type="text" name="lan_data" placeholder="">
                                        <button class="btn btn-success">{{lang_data('Search')}}</button>
                                    </form>
                                </div>
                                <form method="post" action="">
                                    @csrf
                                    <button class="btn btn-success pull-right" style="margin-top: 25px;"><i class="fa fa-save"></i>{{lang_data('Save')}}</button>
                                    <input type="hidden" name="lan_id" value="{{$lan['id']}}">

                            </div>
                        </div>
                    </div>
                    <div class="grid-body no-border">
                        <table class="table table-bordered no-more-tables">
                            <thead>
                            <tr>
                                <th class="text-center" style="width:12%">{{lang_data('Word default')}}</th>
                                <th class="text-center" style="width:12%">{{lang_data('Word')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($language_data as $item)

                                <tr>
                                    <td class="text-center">{{$item->lan_data}}</td>
                                    <td class="text-center">
                                        <input type="text" class="form-control" name="lan_value[{{$item['lan_data']}}]" value="{{meta_value_language($item['lan_data'],$lan['id'])}}">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </form>
                        <div class="pagination pull-right">{!! $language_data->links() !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
