@extends('admin.master')
@section('content')
    <div class="col-md-12">
        <div class="grid simple ">
            <div class="grid-title no-border">
                <h2>{{lang_data('Write')}}</h2>
            </div>
            <div class="grid-body no-border">
                <div class="row">
                    <div class="pull-left" style="margin-left: 20px;" >
                        <form method="get" action="{{route('write.search')}}">
                            <label><b style="text-decoration: underline;color: #0d0f12;">{{lang_data('Search')}}:</b></label>
                            <input type="text" name="search" placeholder="Tìm kiếm theo tên câu hỏi">
                            <button class="btn btn-success"><i class="glyphicon glyphicon-search"></i> {{lang_data('Search')}}</button>
                        </form>
                    </div>
                    <form method="post" action="{{route('user.deleteall')}}">
                        @csrf
                        <div class="pull-left" style="margin-top:25px; margin-left: 9px;">
                            @if(menu_permission('per_delete_write')==true)
                            <button id="delete-user" class="btn btn-success" type="submit" onclick="if(!confirm('Bạn có chăc chắn muốn xoá')){return false}"><i class="glyphicon glyphicon-trash"></i> {{lang_data('Delete All')}}</button>
                           @endif
                            @if(menu_permission('per_create_write')==true)
                            <a class="btn btn-success" href="{{route('write.create')}}"><i class="glyphicon glyphicon-plus"></i> {{lang_data('Add')}}</a>
                                @endif
                        </div>
                </div>
                <table class="table no-more-tables">
                    <thead>
                    <tr>
                        <th>{{lang_data('Select')}}</th>
                        <th>{{lang_data('Question')}}</th>
                        <th>{{lang_data('Description question')}}</th>
                        <th>{{lang_data('Answer true')}}</th>
                        <th>{{lang_data('Note')}} </th>
                        <th>{{lang_data('Lesson')}} </th>
                        <th>{{lang_data('Score')}}</th>
                        <th>{{lang_data('User create')}} </th>
                        <th>{{lang_data('Status')}}</th>
                        <th>{{lang_data('Function')}} </th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($writes as $key)
                        <tr>

                            <td class='v-align-middle'>
                                <input type='checkbox' name='lesson_id[]' value='{{$key->id}}' class='checkbox delete-user'>
                            </td>
                            <td>
                                {{$key->name}}
                            </td>
                            <td>
                                <div style="overflow: auto;height:150px;width: 200px;">{!! $key->question !!}</div>
                            </td>
                            <td>{{$key->answer_true}}</td>
                            <td>{{$key->note}}</td>
                            <td>{{$key['lessons']['title']}}</td>
                            <td>{{$key->score}}</td>
                            <td>{{ \App\User::find_user($key->created_by)}}</td>
                            <td style="width: 200px; text-align: center">@if($key->status ==0)
                                    <p class="alert alert-success"><span>Hoạt động </span></p>
                                @elseif($key->status ==1)
                                    <p class="alert alert-danger"><span>Bị khoá</span></p>
                                @elseif($key->status ==2)
                                    <p class="alert alert-warning"><span>Không hoạt đông</span></p>
                                @endif
                            </td>
                            <td class="center">
                                @if(menu_permission('per_edit_write')==true)
                                <a href="write/edit/{{$key->id}}">
                                    <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                </a>
                                @endif
                                    @if(menu_permission('per_delete_write')==true)
                                <a href="write/delete/{{$key->id}}">
                                    <button class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa??')" type="button"><i class="glyphicon glyphicon-trash"></i></button>
                                </a>
                                        @endif

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </form>
                <div class="pagination pull-right">{!! $writes->links() !!}</div>
            </div>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#delete-user').css('display','none');
            $(".delete-user").on('change', function () {
                if ($("input[name='lesson_id[]']:checked").length > 1) {
                    $('#delete-user').css('display', 'inline-block');
                } else {
                    $('#delete-user').css('display', 'none');
                }
            });
        });

    </script>
@endsection
