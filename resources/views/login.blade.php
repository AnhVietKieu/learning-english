<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>BE LOGIN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="{{asset('public/webarch/css/loading.css')}}" rel="stylesheet">

<style>
    body{
        padding-top:7.2rem;
        padding-bottom:4.2rem;
        font-family: "Times New Roman";
        background-image: linear-gradient(white,#D6F2FC,white);;
    }
    a{
        text-decoration:none !important;
    }
    /*h1,h2,h3{*/
    /*    font-family: 'Kaushan Script', cursive;*/
    /*}*/
    .myform{
        position: relative;
        display: -ms-flexbox;
        display: flex;
        padding: 1rem;
        -ms-flex-direction: column;
        flex-direction: column;
        width: 100%;
        pointer-events: auto;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid rgba(0,0,0,.2);
        border-radius: 1.1rem;
        outline: 0;
        max-width: 500px;
    }
    .tx-tfm{
        text-transform:uppercase;
    }
    .mybtn{
        border-radius:50px;
    }

    .login-or {
        position: relative;
        color: #aaa;
        margin-top: 10px;
        margin-bottom: 10px;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .span-or {
        display: block;
        position: absolute;
        left: 50%;
        top: -2px;
        margin-left: -25px;
        background-color: #fff;
        width: 50px;
        text-align: center;
    }
    .hr-or {
        height: 1px;
        margin-top: 0px !important;
        margin-bottom: 0px !important;
    }
    .google {
        color:#666;
        width:100%;
        height:40px;
        text-align:center;
        outline:none;
        border: 1px solid lightgrey;
    }
    form .error {
        color: #ff0000;
    }
    #second{display:none;}
</style>
</head>
<body>
<div class="container">
    <div class="loader">
        <img src="{{asset('public/assets/img/loading.gif')}}" alt="Loading..." />
    </div>
    <div class="row">
        <div class="col-md-5 mx-auto">
            <div id="first">
                <div class="myform form ">
                    <div class="logo mb-3">
                        <div class="col-md-12 text-center">
                            <h1>Đăng nhập</h1>
                        </div>
                        @if(session('error'))
                            <div class="alert alert-danger">{{session('error')}}</div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">{{session('success')}}</div>
                        @endif
                    </div>
                    <form action="" method="post" name="login">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email:</label>
                            <input type="email" name="email" value="kieuvietanh8597@gmail.com" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Nhập email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mật khẩu:</label>
                            <input type="password" name="password" id="password" value="123456" class="form-control" aria-describedby="emailHelp" placeholder="Nhập mật khẩu">
                        </div>
                        <div class="form-group">
                            <div class="g-recaptcha" style="margin-bottom: 10px;"data-sitekey="6LdooMoUAAAAAFG4EcCJYNEAUTP5X3sT3srra398" aria-required="true"></div>
                            <span style="color: red; margin-bottom: 2px;">@if($errors->has('g-recaptcha-response')){{$errors->first('g-recaptcha-response')}}@endif</span>
                        </div>
                       <div class="col-md-12 text-center ">
                            <button type="submit" class=" btn btn-block mybtn btn-success tx-tfm">Đăng nhập</button>
                        </div>
                        <div class="form-group" style="margin-top:10px;">
                            <p class="text-left"><a href="{{route('user.resetpassword')}}">Quên mật khẩu ?</a></p>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
<script>

    $(function() {
        $("form[name='login']").validate({
            rules: {

                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength:6
                }
            },
            messages: {
                email:{
                    required:"Email không được bỏ trống",
                    email:"Email không đúng định dạng"
                },

                password: {
                    required: "Mật khẩu không được bỏ trống",
                    minlength:"Mật khẩu phải lớn hơn 6 kí tự"
                }

            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });

    window.addEventListener("load", function () {
        const loader = document.querySelector(".loader");
        loader.className += " hidden"; // class "loader hidden"
    });

</script>
</body>
</html>
