@extends('frontend.layout.master.master_display')
@section('content')
    <section class="banner-top-basic">
        <div class="img-banner">
            <img class="w-100" src="{{asset('public/4words/img/basic/background.png')}}" alt="">
        </div>
        <div class="title-top w-100 h-100">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text">
                            <h1>Tiếng anh nền tảng</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="content">
        <section class="introduce" style="thumb-color: #fff">
            <div class="container text-center">

                <div class="row">
                    <div class="col-md-12">

                        <section class="section">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Tài liệu tham khảo</h4>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group">
                                        @foreach($document as $item)
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            {{$i++}}: {{$item['title']}}
                                            <span >
                                                <a href="{{asset('public/upload/document/'.$item['file'])}}" download="">
                                                    <span class="btn btn-success" style="color: white;">Tải về</span>
                                                </a>
                                            </span>
                                        </li>
                                            @endforeach
                                    </ul>
                                </div>
                            </div>

                        </section>
                    </div>

                </div>

            </div>

    </div>


@endsection
