@extends('frontend.layout.master.master_display')
@section('content')
    <section class="banner-top-achievements">
        <div class="img-banner">
            <img class="w-100" src="{{asset('public/4words/img/achievements-student/banner/banner.jpg')}}" alt="">
        </div>
        <div class="title-top w-100 h-100">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text">
                            <h1>Thành tích <span>Học sinh</span>
                            </h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="apply-achiements">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="widget-apply">

                        <div class="list-apply">
                            <div class="row text-center">
                                @foreach($student as $item)
                                <div class="col-lg-3 col-md-4">
                                    <div class="scholarship">
                                        @if(empty($item['avatar']))
                                            <div style="border-radius: 50%;">
                                                <img width="150px" height="150px"  src="{{asset('public/upload/download.png')}}" alt="">
                                            </div>
                                            @endif

                                        <div style="border-radius: 50%;">
                                            <img width="150px" height="171px"  src="{{asset('public/upload/customer/'.$item['avatar'])}}" alt="">
                                        </div>
                                        <div class="info">
                                            <h5 class="title">{{$item['username']}}</h5>
                                            <p class="content">Ngày sinh:{{date('m/d/Y',strtotime($item['birthday']))}}</p>
                                            <div class="ashford">

                                                <div class="ashford-content">
                                                    <p>Level:<img style="width: 50px;height: 50px; border-radius: 50%;" src="{{asset('public/upload/level/'.$item['levels']['image'])}}" alt="">
                                                    </p>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="paginate">{!! $student->links() !!}</div>
                            </div>


                        </div>
                    </div>
{{--                    <div class="widget-apply">--}}
{{--                        <div class="title-apply">--}}
{{--                            <h2>Du học Mỹ</h2>--}}
{{--                        </div>--}}
{{--                        <div class="list-apply">--}}
{{--                            <div class="row text-center">--}}
{{--                                <div class="col-lg-3 col-md-4">--}}
{{--                                    <div class="scholarship">--}}
{{--                                        <div class="thumb">--}}
{{--                                            <img style="left: -118px;" src="img/home/achievement/1.jpg" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="info">--}}
{{--                                            <h5 class="title">Đặng Thúy Ngân</h5>--}}
{{--                                            <p class="content">Đại học Asford</p>--}}
{{--                                            <div class="ashford">--}}
{{--                                                <div class="img">--}}
{{--                                                    <img src="img/home/achievement/icon.png" alt="">--}}
{{--                                                </div>--}}
{{--                                                <div class="ashford-content">--}}
{{--                                                    <p>Thứ hạng</p>--}}
{{--                                                    <p class="top">TOP 10</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="totallity">--}}
{{--                                                <p class="content">Học bổng</p>--}}
{{--                                                <p class="hocbong">toàn phần trong 4 năm</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3 col-md-4">--}}
{{--                                    <div class="scholarship">--}}
{{--                                        <div class="thumb">--}}
{{--                                            <img style="left: -118px;" src="img/home/achievement/1.jpg" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="info">--}}
{{--                                            <h5 class="title">Đặng Thúy Ngân</h5>--}}
{{--                                            <p class="content">Đại học Asford</p>--}}
{{--                                            <div class="ashford">--}}
{{--                                                <div class="img">--}}
{{--                                                    <img src="img/home/achievement/icon.png" alt="">--}}
{{--                                                </div>--}}
{{--                                                <div class="ashford-content">--}}
{{--                                                    <p>Thứ hạng</p>--}}
{{--                                                    <p class="top">TOP 10</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="totallity">--}}
{{--                                                <p class="content">Học bổng</p>--}}
{{--                                                <p class="hocbong">toàn phần trong 4 năm</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3 col-md-4">--}}
{{--                                    <div class="scholarship">--}}
{{--                                        <div class="thumb">--}}
{{--                                            <img style="left: -118px;" src="img/home/achievement/1.jpg" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="info">--}}
{{--                                            <h5 class="title">Đặng Thúy Ngân</h5>--}}
{{--                                            <p class="content">Đại học Asford</p>--}}
{{--                                            <div class="ashford">--}}
{{--                                                <div class="img">--}}
{{--                                                    <img src="img/home/achievement/icon.png" alt="">--}}
{{--                                                </div>--}}
{{--                                                <div class="ashford-content">--}}
{{--                                                    <p>Thứ hạng</p>--}}
{{--                                                    <p class="top">TOP 10</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="totallity">--}}
{{--                                                <p class="content">Học bổng</p>--}}
{{--                                                <p class="hocbong">toàn phần trong 4 năm</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3 col-md-4">--}}
{{--                                    <div class="scholarship">--}}
{{--                                        <div class="thumb">--}}
{{--                                            <img style="left: -118px;" src="img/home/achievement/1.jpg" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="info">--}}
{{--                                            <h5 class="title">Đặng Thúy Ngân</h5>--}}
{{--                                            <p class="content">Đại học Asford</p>--}}
{{--                                            <div class="ashford">--}}
{{--                                                <div class="img">--}}
{{--                                                    <img src="img/home/achievement/icon.png" alt="">--}}
{{--                                                </div>--}}
{{--                                                <div class="ashford-content">--}}
{{--                                                    <p>Thứ hạng</p>--}}
{{--                                                    <p class="top">TOP 10</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="totallity">--}}
{{--                                                <p class="content">Học bổng</p>--}}
{{--                                                <p class="hocbong">toàn phần trong 4 năm</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3 col-md-4">--}}
{{--                                    <div class="scholarship">--}}
{{--                                        <div class="thumb">--}}
{{--                                            <img style="left: -118px;" src="img/home/achievement/1.jpg" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="info">--}}
{{--                                            <h5 class="title">Đặng Thúy Ngân</h5>--}}
{{--                                            <p class="content">Đại học Asford</p>--}}
{{--                                            <div class="ashford">--}}
{{--                                                <div class="img">--}}
{{--                                                    <img src="img/home/achievement/icon.png" alt="">--}}
{{--                                                </div>--}}
{{--                                                <div class="ashford-content">--}}
{{--                                                    <p>Thứ hạng</p>--}}
{{--                                                    <p class="top">TOP 10</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="totallity">--}}
{{--                                                <p class="content">Học bổng</p>--}}
{{--                                                <p class="hocbong">toàn phần trong 4 năm</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3 col-md-4">--}}
{{--                                    <div class="scholarship">--}}
{{--                                        <div class="thumb">--}}
{{--                                            <img style="left: -118px;" src="img/home/achievement/1.jpg" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="info">--}}
{{--                                            <h5 class="title">Đặng Thúy Ngân</h5>--}}
{{--                                            <p class="content">Đại học Asford</p>--}}
{{--                                            <div class="ashford">--}}
{{--                                                <div class="img">--}}
{{--                                                    <img src="img/home/achievement/icon.png" alt="">--}}
{{--                                                </div>--}}
{{--                                                <div class="ashford-content">--}}
{{--                                                    <p>Thứ hạng</p>--}}
{{--                                                    <p class="top">TOP 10</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="totallity">--}}
{{--                                                <p class="content">Học bổng</p>--}}
{{--                                                <p class="hocbong">toàn phần trong 4 năm</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3 col-md-4">--}}
{{--                                    <div class="scholarship">--}}
{{--                                        <div class="thumb">--}}
{{--                                            <img style="left: -118px;" src="img/home/achievement/1.jpg" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="info">--}}
{{--                                            <h5 class="title">Đặng Thúy Ngân</h5>--}}
{{--                                            <p class="content">Đại học Asford</p>--}}
{{--                                            <div class="ashford">--}}
{{--                                                <div class="img">--}}
{{--                                                    <img src="img/home/achievement/icon.png" alt="">--}}
{{--                                                </div>--}}
{{--                                                <div class="ashford-content">--}}
{{--                                                    <p>Thứ hạng</p>--}}
{{--                                                    <p class="top">TOP 10</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="totallity">--}}
{{--                                                <p class="content">Học bổng</p>--}}
{{--                                                <p class="hocbong">toàn phần trong 4 năm</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <div class="col-md-12">
                    <div class="contact-apply text-center">
                        <a href="">
                            <img src="{{asset('public/4words/img/home/course/tongdai.png')}}" alt="">
                            <span>Liên hệ cấp quản lý - Xây dựng lộ trình học tập và du học</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- apply-achiements -->
{{--    <section class="list-schooll">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-12">--}}
{{--                    <h3 class="title text-center">--}}
{{--                        Các trường <span>có học viên theo học</span>--}}
{{--                    </h3>--}}
{{--                    <div class="widget-list-schooll">--}}
{{--                        <div class="box">--}}
{{--                            <a href="">--}}
{{--                                <img src="img/achievements-student/school/school-1.png" alt="">--}}
{{--                            </a>--}}
{{--                            <span>Top 10</span>--}}
{{--                        </div>--}}
{{--                        <div class="box">--}}
{{--                            <a href="">--}}
{{--                                <img src="img/achievements-student/school/school-2.png" alt="">--}}
{{--                            </a>--}}
{{--                            <span>Top 10</span>--}}
{{--                        </div>--}}
{{--                        <div class="box">--}}
{{--                            <a href="">--}}
{{--                                <img src="img/achievements-student/school/school-1.png" alt="">--}}
{{--                            </a>--}}
{{--                            <span>Top 10</span>--}}
{{--                        </div>--}}
{{--                        <div class="box">--}}
{{--                            <a href="">--}}
{{--                                <img src="img/achievements-student/school/school-1.png" alt="">--}}
{{--                            </a>--}}
{{--                            <span>Top 10</span>--}}
{{--                        </div>--}}
{{--                        <div class="box">--}}
{{--                            <a href="">--}}
{{--                                <img src="img/achievements-student/school/school-1.png" alt="">--}}
{{--                            </a>--}}
{{--                            <span>Top 10</span>--}}
{{--                        </div>--}}
{{--                        <div class="box">--}}
{{--                            <a href="">--}}
{{--                                <img src="img/achievements-student/school/school-1.png" alt="">--}}
{{--                            </a>--}}
{{--                            <span>Top 10</span>--}}
{{--                        </div>--}}
{{--                        <div class="box">--}}
{{--                            <a href="">--}}
{{--                                <img src="img/achievements-student/school/school-1.png" alt="">--}}
{{--                            </a>--}}
{{--                            <span>Top 10</span>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
@endsection
