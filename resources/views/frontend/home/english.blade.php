@extends('frontend.layout.master.master_display')
@section('content')
    <section class="banner-top-basic">
        <div class="img-banner">
            <img class="w-100" src="img/basic/background.png" alt="">
        </div>
        <div class="title-top w-100 h-100">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text">
                            <span>Chương trình</span>
                            <h1>Tiếng anh nền tảng</h1>
                            <p>Chương trình Tiếng Anh Nền Tảng (Academic English) chú trọng phát triển khả năng ngôn ngữ và kỹ năng học thuật tạo nền tảng giúp học sinh:</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="basic-skill">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-icon">
                        <div class="col-md-3 p-custom">
                            <div class="box-icon">
                                <div class="icon-img">
                                    <img src="img/basic/icon/icon-1.png" alt="">
                                </div>
                                <p>
                                    lĩnh hội đủ kiến thức và kỹ năng để tiếp cận tốt với các bài thi chuẩn hoá cấp cao như
                                    IELTS, TOEFL, SAT, SSAT, ACT…
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3 p-custom">
                            <div class="box-icon">
                                <div class="icon-img">
                                    <img src="img/basic/icon/icon-2.png" alt="">
                                </div>
                                <p>
                                    nâng cao khả năng học tập khi tiếp cận với các chương trình giảng dạy quốc tế ở các bậc học
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3 p-custom">
                            <div class="box-icon">
                                <div class="icon-img">
                                    <img src="img/basic/icon/icon-1.png" alt="">
                                </div>
                                <p>
                                    phát triển năng lực bản thân để tự tin trong môi trường sống, học tập và làm việc quốc tế
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3 p-custom">
                            <div class="box-icon">
                                <div class="icon-img">
                                    <img src="img/basic/icon/icon-1.png" alt="">
                                </div>
                                <p>
                                    nuôi dưỡng niềm ham thích với bộ môn tiếng Anh và quyết tâm “làm chủ”, dứt điểm ngoại ngữ này tại mức xuất sắc
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <! basic-skill -->
    <section class="basic-content p-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title text-center">
                        <h3>Nội dung</h3>
                        <p>
                            Chương trình Tiếng Anh Nền Tảng tại 4WORDS được phân chia gọn gàng thành 3 cấp độ,
                            tương đương với 3 khoá học với chuẩn điểm đầu vào và mục tiêu điểm đầu ra tương ứng.
                            Quá trình đào tạo tập trung:
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">1</span>
                        <p class="content">
                            Củng cố kiến thức cơ bản và nâng tầm ngữ pháp cấp cao cũng như khả năng ứng dụng vào bài Nói – Viết
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">2</span>
                        <p class="content">
                            Bồi dưỡng toàn diện khả năng NGHE – NÓI – ĐỌC – VIẾT
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">3</span>
                        <p class="content">
                            Xây dựng vốn từ vựng phong phú về tất cả các chủ điểm khoa học tự nhiên và xã hội. Tối thiểu 300 từ mới/khoá học.
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">4</span>
                        <p class="content">
                            Hoàn thiện cách viết câu, tổ chức dàn ý chặt chẽ, hướng tới phát triển hoàn thiện các tổ hợp câu phức tạp hơn, đoạn văn – bài luận
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">5</span>
                        <p class="content">
                            Xây dựng vốn từ vựng phong phú về tất cả các chủ điểm khoa học tự nhiên và xã hội. Tối thiểu 300 từ mới/khoá học.
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">6</span>
                        <p class="content">
                            Nắn chỉnh phát âm đúng, luyện nói có ngữ điệu và ngôn ngữ cơ thể, rèn khả năng giao tiếp tự nhiên
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- basic-content -->
    <!--<section class="basic-teacher p-70">-->
    <!--<div class="container">-->
    <!--<div class="row">-->
    <!--<div class="col-md-6">-->
    <!--<div class="teacher-background">-->
    <!--<h3 class="title">-->
    <!--Đội ngũ-->
    <!--<span>Giáo viên</span>-->
    <!--</h3>-->
    <!--</div>-->
    <!---->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</section>-->
    <section class="basic-table">
        <table class="table table-customize table-responsive">
            <thead>
            <tr>
                <th>Khóa học</th>
                <th>Cấp độ</th>
                <th>Điểm đầu vào</th>
                <th>Điểm đầu ra</th>
                <th>Tổng thời gian (giờ)</th>
                <th>Số giờ/buổi</th>
                <th>Số buổi</th>
                <th>Tổng chi phí (VNĐ)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td data-title="Khóa học" rowspan="3">Academic English</td>
                <td data-title="Cấp độ">Level 1</td>
                <td data-title="Điểm đầu vào">30/100</td>
                <td data-title="Điểm đầu ra">50+/100</td>
                <td data-title="Tổng thời gian (giờ)">60</td>
                <td data-title="Số giờ/buổi">2</td>
                <td data-title="Số buổi">30</td>
                <td data-title="Tổng chi phí (VNĐ)">6.000.000</td>
            </tr>
            <tr>
                <td data-title="Cấp độ">Level 2</td>
                <td data-title="Điểm đầu vào">50/100</td>
                <td data-title="Điểm đầu ra">70+/100</td>
                <td data-title="Tổng thời gian (giờ)">60</td>
                <td data-title="Số giờ/buổi">2</td>
                <td data-title="Số buổi">30</td>
                <td data-title="Tổng chi phí (VNĐ)">9.600.000</td>
            </tr>
            <tr>
                <td data-title="Cấp độ">Level 3</td>
                <td data-title="Điểm đầu vào">70/100</td>
                <td data-title="Điểm đầu ra">90+/100</td>
                <td data-title="Tổng thời gian (giờ)">60</td>
                <td data-title="Số giờ/buổi">2</td>
                <td data-title="Số buổi">30</td>
                <td data-title="Tổng chi phí (VNĐ)">9.600.000</td>
            </tr>
            </tbody>
        </table>
    </section>
    <section class="no-fear p-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title text-center">
                        <h3 class="title pb-5">
                            "Không còn lo sợ - <span>không còn trì hoãn</span> "
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4 p-custom">
                    <div class="box-student">
                        <div class="avatar">
                            <a href="" title="">
                                <img src="http://4words.edu.vn/wp-content/uploads/2019/11/Hoàng-Sơn-là-bạn-nam-nhé.jpg" alt="Hoàng Sơn">
                            </a>
                        </div>
                        <div class="title">
                            <h3>Hoàng Sơn</h3>
                            <p>THPT Việt Đức</p>
                            <ul>
                                <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="content">
                            <div class="point">
                                <p>Thành tích</p>
                                <h3>IELTS <span>7.0</span></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 p-custom">
                    <div class="box-student">
                        <div class="avatar">
                            <a href="" title="">
                                <img src="http://4words.edu.vn/wp-content/uploads/2019/11/Hoàng-Sơn-là-bạn-nam-nhé.jpg" alt="Hoàng Sơn">
                            </a>
                        </div>
                        <div class="title">
                            <h3>Hoàng Sơn</h3>
                            <p>THPT Việt Đức</p>
                            <ul>
                                <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="content">
                            <div class="point">
                                <p>Thành tích</p>
                                <h3>IELTS <span>7.0</span></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 p-custom">
                    <div class="box-student">
                        <div class="avatar">
                            <a href="" title="">
                                <img src="http://4words.edu.vn/wp-content/uploads/2019/11/Hoàng-Sơn-là-bạn-nam-nhé.jpg" alt="Hoàng Sơn">
                            </a>
                        </div>
                        <div class="title">
                            <h3>Hoàng Sơn</h3>
                            <p>THPT Việt Đức</p>
                            <ul>
                                <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="content">
                            <div class="point">
                                <p>Thành tích</p>
                                <h3>IELTS <span>7.0</span></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 p-custom">
                    <div class="box-student">
                        <div class="avatar">
                            <a href="" title="">
                                <img src="http://4words.edu.vn/wp-content/uploads/2019/11/Hoàng-Sơn-là-bạn-nam-nhé.jpg" alt="Hoàng Sơn">
                            </a>
                        </div>
                        <div class="title">
                            <h3>Hoàng Sơn</h3>
                            <p>THPT Việt Đức</p>
                            <ul>
                                <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="content">
                            <div class="point">
                                <p>Thành tích</p>
                                <h3>IELTS <span>7.0</span></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <! --footer -->
@endsection
