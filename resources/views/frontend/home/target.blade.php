@extends('frontend.layout.master.master_display')
@section('content')
    <section class="banner-top-target background">
        <div class="img-banner">
            <img class="w-100"  src="{{asset('public/4words/img/home/banner.jpg')}}" alt="">
        </div>
        <div class="title-top w-100 h-100">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text">
                            <h1>Giới thiệu chung <span> English</span></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="target-action p-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="content">
                        English là trung tâm đào tạo tiếng Anh phục vụ cho những mục
                        tiêu đa dạng của người dùng.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-action">
                        <h3 class="title text-left pt-3">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            Mục tiêu <span>hoạt động</span>
                        </h3>

                        <ul class="list-title">
                            <li>
                                <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                <span>Chứng chỉ chuẩn hoá quốc tế IELTS, TOEFL, SAT I, SAT II: phục vụ cho mục đích du học, săn học bổng và xét tuyển thẳng vào đại học trong nước.</span>
                            </li>
                            <li>
                                <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                <span>Tiếng anh học thuật: củng cố kiến thức tiếng anh cơ bản, phát triển toàn diện 4 kĩ năng Nghe – Nói – Đọc – Viết, tạo nền tảng vững vàng để học tốt các chứng chỉ cấp cao hoặc thích ứng tốt với chương trình chính khoá liên kết quốc tế .</span>
                            </li>
                        </ul>
                        <p>
                            Tiếng anh dành riêng cho doanh nghiệp và các cá nhân: cung cấp các chương trình thiết kế riêng theo nhu cầu sử dụng của doanh nghiệp và các cá nhân là doanh nhân, người đi làm, người yêu thích du lịch, học sinh sinh viên tham dự các cuộc thi quốc gia, quốc tế.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
