@extends('frontend.layout.master.master_display')
@section('content')
    <section class="slider">
        <div id="slider-index" class="owl-carousel owl-theme owl-loaded owl-drag">
            <div class="owl-stage-outer">
                <div class="owl-stage">
                    <div class="owl-item">
                        <div class="img" style="background-image: url({{asset('public/4words/img/home/banner.jpg')}}); thumb-size: cover; thumb-repeat: no-repeat; thumb-position: center">
                            <!--<img src="img/home/banner.jpg" class="d-block w-100" alt="">-->
                            <div class="container">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="title">
                                            <h3>Thay đổi cách học cùng <span style="color: #f39316">English</span></h3>
                                            <p class="title-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                            <button class="more btn-common btn-size">Tìm hiểu thêm</button>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="people">
                                            <img src="{{asset('public/4words/img/home/people.png')}}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="owl-item">
                        <div class="img" style="background-image: url({{asset('public/4words/img/home/banner.jpg')}}); thumb-size: cover; thumb-repeat: no-repeat; thumb-position: center">
                            <!--<img src="img/home/banner.jpg" class="d-block w-100" alt="">-->
                            <div class="container">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="title">
                                            <h3>Thay đổi cách học cùng <span style="color: #f39316">English</span></h3>
                                            <p class="title-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                            <button class="more btn-common btn-size">Tìm hiểu thêm</button>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="people">
                                            <img src="{{asset('public/4words/img/home/people.png')}}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end slider -->


    <section class="introduce" style="thumb-color: #fff">
        <div class="container text-center">

            <div class="row">
                <div class="col-md-4">
                    <div class="icon">
                        <img class="img-fluid" src="{{asset('public/4words/img/home/hat.png')}}" alt="">
                    </div>
                    <p class="content">
                        ENGLISH là trung tâm đào tạo tiếng Anh phục vụ những mục tiêu đa dạng của người dùng:
                        từ việc du học/săn học bổng tới xét tuyển thẳng đại học trong nước và phát triển sự nghiệp,
                        nâng cao chất lượng cuộc sống.
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="icon">
                        <img src="{{asset('public/4words/img/home/earth.png')}}" alt="">
                    </div>
                    <p class="content">
                        Song hành cùng đào tạo, ENGLISH cung cấp dịch vụ tư vấn du học Mỹ, Canada, Úc, hỗ trợ
                        định hướng ngành nghề, kĩ năng cần thiết cho việc chuyển tiếp lên bậc đại học trong hay
                        ngoài nước.
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="icon">
                        <img src="{{asset('public/4words/img/home/green.png')}}" alt="">
                    </div>
                    <p class="content">
                        Lý tưởng đóng góp cho giáo dục và mang lại giá trị thực cho người học chưa bao giờ tách rời khỏi yêu cầu phát triển một start-up vững mạnh mà ngược lại, trở thành tôn chỉ và quy ước cho mọi hoạt động của ENGLISH…
                    </p>
                </div>
            </div>

        </div>

        <div class="container-fluid">
            <div class="row p-0">
                <div class="col-4 p-0">
                    <div class="tower-left" style="height: 25px;">

{{--                        <img src="{{asset('public/4words/img/home/tower-left.png')}}" alt="">--}}
                    </div>
                </div>
                <div class="col-4">

                </div>
                <div class="col-4 p-0">
                    <div class="tower-right" style="height: 25px;">

{{--                        <img src=" {{asset('public/4words/img/home/tower-right.png')}}" alt="">--}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="course p-70">
        <div class="container text-center">
            <h3 class="title pb-40">Khoá học<span class="square">xuất sắc</span> </h3>
            <div class="row">

            </div>
            <div class="row">
                @foreach($coure as $item)
                <div class="col-sm-6 col-lg-3">
                    <div class="widget text-center" style="overflow: hidden;">
                        <div class="thumb">
                            <img src="{{asset('public/upload/coure/'.$item['image'])}}" alt="">
                        </div>
                        <div class="content" >

                            <h4><a>{{$item['name']}}</a></h4>
                            <div class="content" style="text-align: left;">
                               {!! $item->description !!}
                            </div>
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <section class="basic-content p-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title text-center">
                        <h3>Tại sao nên chọn English </h3>
                        <p>
                            Chương trình Tiếng Anh Nền Tảng tại ENGLISH được phân chia gọn gàng thành 3 cấp độ,
                            tương đương với 3 khoá học với chuẩn điểm đầu vào và mục tiêu điểm đầu ra tương ứng.
                            Quá trình đào tạo tập trung:
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">1</span>
                        <p class="content">
                            Củng cố kiến thức cơ bản và nâng tầm ngữ pháp cấp cao cũng như khả năng ứng dụng vào bài Nói – Viết
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">2</span>
                        <p class="content">
                            Bồi dưỡng toàn diện khả năng NGHE – NÓI – ĐỌC – VIẾT
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">3</span>
                        <p class="content">
                            Xây dựng vốn từ vựng phong phú về tất cả các chủ điểm khoa học tự nhiên và xã hội. Tối thiểu 300 từ mới/khoá học.
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">4</span>
                        <p class="content">
                            Hoàn thiện cách viết câu, tổ chức dàn ý chặt chẽ, hướng tới phát triển hoàn thiện các tổ hợp câu phức tạp hơn, đoạn văn – bài luận
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">5</span>
                        <p class="content">
                            Xây dựng vốn từ vựng phong phú về tất cả các chủ điểm khoa học tự nhiên và xã hội. Tối thiểu 300 từ mới/khoá học.
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="number-build">
                        <span class="number">6</span>
                        <p class="content">
                            Nắn chỉnh phát âm đúng, luyện nói có ngữ điệu và ngôn ngữ cơ thể, rèn khả năng giao tiếp tự nhiên
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- course -->
{{--    <section class="achievements" style="background: url('img/home/background.jpg') no-repeat center; background-size: cover ">--}}
{{--        <div class="container text-center">--}}
{{--            <h3 class="title">Thành tích <span class="square">học viên</span> </h3>--}}

{{--            <div class="row">--}}
{{--                @foreach($customer as $item)--}}

{{--                <div class="col-sm-12 col-md-4 col-lg-3">--}}
{{--                    <div class="widget">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="{{asset('public/upload/level/'.$item['levels']['image'])}}" alt="">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                    @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- achievements -->

    <!--achievement -->
    <section class="opinion p-70">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <h3 class="title">
                        <span style="color: #f39316">Ý Kiến</span>
                        Học viên, phụ huynh
                    </h3>
                    <p class="content">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqused
                    </p>

                </div>
                <div class="col-sm-12 col-lg-8">
                    <div id="opinion-widget" class="owl-carousel owl-theme owl-loaded owl-drag">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <div class="widget-parent">
                                        <div class="thumb">
                                            <img src="{{asset('public/4words/img/home/opinion-1.jpg')}}" alt="">
                                        </div>
                                        <div class="name">
                                            <div class="widget-name">
                                                <h4>Trần Quang Huy</h4>
                                                <p>Phụ huynh học viên</p>
                                            </div>

                                        </div>
                                        <div class="content">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidataqvmagnam
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="widget-parent">
                                        <div class="thumb">
                                            <img src="{{asset('public/4words/img/home/opinion-2.jpg')}}" alt="">
                                        </div>
                                        <div class="name">
                                            <div class="widget-name">
                                                <h4>Trần Quang Huy</h4>
                                                <p>Phụ huynh học viên</p>
                                            </div>

                                        </div>
                                        <div class="content">
                                            <p>
                                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidataqvmagnam
                                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="widget-parent">
                                        <div class="thumb">
                                            <img src="{{asset('public/4words/img/home/opinion-2.jpg')}}" alt="">
                                        </div>
                                        <div class="name">
                                            <div class="widget-name">
                                                <h4>Trần Quang Huy</h4>
                                                <p>Phụ huynh học viên</p>
                                            </div>

                                        </div>
                                        <div class="content">
                                            <p>
                                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidataqvmagnam
                                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                                            </p>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
