@extends('frontend.layout.master.master_display')
@section('content')
    <section class="detail-news p-70">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="detail">
                        <h3 class="title-news">
                            <a href="">
                                Lorem ipsum dolor  sit amet, consectetur
                                adipisicing elit.
                            </a>
                        </h3>
                        <div class="icon">
                            <p class="content">Chia sẻ</p>
                            <div class="logo">
                                <a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-twitter-square" style="color: #3c97ff" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-google-plus-square" style="color: #d54931" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-envelope" style="color: #f1ae42" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <p class="italic">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                        </p>
                        <div class="img-detail">
                            <img class="img-fluid" src="img/news/detail.jpg" alt="">
                        </div>
                        <p class="content-news">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                            officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit
                            voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                            voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                            voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                            consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore
                            magnam aliquam quaerat voluptatem.
                        </p>
                        <p class="content-news">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                            officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit
                            voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                            voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                            voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                            consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore
                            magnam aliquam quaerat voluptatem.
                        </p>
                        <div class="icon">
                            <p class="content">Chia sẻ</p>
                            <div class="logo">
                                <a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-twitter-square" style="color: #3c97ff" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-google-plus-square" style="color: #d54931" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-envelope" style="color: #f1ae42" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="related-news">
                        <h3 class="title-news">
                            Tin liên quan
                        </h3>
                        <div class="related-content">
                            <h5 class="title-news">
                                <a href="">
                                    Lorem ipsum dolor  sit amet, consectetur adipisicing elit
                                </a>
                            </h5>
                            <p class="content-news">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                            </p>
                            <p class="date">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                22/12/2018
                            </p>
                        </div>
                        <div class="related-content">
                            <h5 class="title-news">
                                <a href="">
                                    Lorem ipsum dolor  sit amet, consectetur adipisicing elit
                                </a>
                            </h5>
                            <p class="content-news">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                            </p>
                            <p class="date">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                22/12/2018
                            </p>
                        </div>
                        <div class="related-content">
                            <h5 class="title-news">
                                <a href="">
                                    Lorem ipsum dolor  sit amet, consectetur adipisicing elit
                                </a>
                            </h5>
                            <p class="content-news">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                            </p>
                            <p class="date">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                22/12/2018
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 offset-md-1">
                    <h3 class="title-news">
                        nổi bật
                    </h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="img-laster ">
                                <a href=""><img class="img-fluid" src="img/news/laster-1.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="description">
                                <h5 class="title-news">
                                    <a href="">Lorem ipsum dolor  sit amet, consectetur adipisicing elit</a>

                                </h5>
                                <p class="date">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                    22/12/2018
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-5">
                        <div class="col-md-12">
                            <div class="img-laster ">
                                <a href=""><img class="img-fluid" src="img/news/laster-1.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="description">
                                <h5 class="title-news">
                                    <a href="">Lorem ipsum dolor  sit amet, consectetur adipisicing elit</a>

                                </h5>
                                <p class="date">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                    22/12/2018
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-5">
                        <div class="col-md-12">
                            <div class="img-laster ">
                                <a href=""><img class="img-fluid" src="img/news/laster-1.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="description">
                                <h5 class="title-news">
                                    <a href="">Lorem ipsum dolor  sit amet, consectetur adipisicing elit</a>

                                </h5>
                                <p class="date">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                    22/12/2018
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contact-right-pc position-fixed shadow">
        <div class="content">
            <ul>
                <li ><button class="icon-fb btn border-0"><i class="fa fa-facebook" aria-hidden="true"></i></button></li>
                <li ><button class="icon-fb btn border-0"><i class="fa fa-facebook" aria-hidden="true"></i></button></li>
                <li ><button class="icon-fb btn border-0"><i class="fa fa-facebook" aria-hidden="true"></i></button></li>
            </ul>
        </div>
    </section>
    <section class="scroll-top position-fixed">
        <button class="btn border-0 button-scroll text-white shadow" onclick="scrollToTop()"><i class="fa fa-angle-double-up" aria-hidden="true"></i></button>
    </section>
@endsection
