@extends('frontend.layout.master.master_display')
@section('content')
    <section class="banner-top-target background">
        <div class="img-banner">
            <img class="w-100" src="{{asset('public/4words/img/home/banner.jpg')}}" alt="">
        </div>
        <div class="title-top w-100 h-100">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text">
                            <h1>Chương trình <span> English</span></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-top: 10px;">
        <section class="section">

            {!! $coure !!}
        </section>
    </div>

@endsection
