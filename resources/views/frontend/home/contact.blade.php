@extends('frontend.layout.master.master_display')
@section('content')
    <section class="contact" style="margin-top: 25px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-contact">
                        <form action="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Họ tên bạn *</label>
                                        <input type="email" class="form-control input-form-contact" id="" placeholder="Điền đầy đủ họ tên có dấu">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="d-block text-left">Bạn là? </label>
                                    <div class="select-object p-1 ">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="m-0 fle position-relative w-100 border-0">
                                                    <span class="btn w-100 border-0 checkmark ">Học sinh</span>
                                                    <input type="radio" name="object" value="0" checked class="position-absolute" onclick="switchButton(this)">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label class="m-0  position-relative w-100 border-0">
                                                    <span class="btn w-100 border-0 checkmark">Phụ huynh</span>
                                                    <input type="radio" name="object" value="1" class="position-absolute" onclick="switchButton(this)">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Điện thoại *</label>
                                                <input type="email" class="form-control input-form-contact" id="exampleFormControlInput1" placeholder="Điền đầy đủ số điện thoại">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Email *</label>
                                                <input type="email" class="form-control input-form-contact" id="exampleFormControlInput1" placeholder="Điền đầy đủ email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn-contact">Liên hệ</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
