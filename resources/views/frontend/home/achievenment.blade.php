@extends('frontend.layout.master.master_display')
@section('content')
    <section class="banner-top-achievements">
        <div class="img-banner">
            <img class="w-100" src="{{asset('public/4words/img/relic/banner/banner-1.jpg')}}" alt="">
        </div>
        <div class="title-top w-100 h-100">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text">
                            <h1>Thông tin <span>giáo viên</span>
                            </h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <section class="student">
        <div class="container">
            <div class="row p-0">
                @foreach($teacher as $item)
                    @if(!empty($item['role']))
                     <div class="col-md-6" >
                    <div class="widget" style="background-color: #006744;">
                        <div class="avatar">
                            @if(isset($item['avatar']))
                            <div class="img-info">
                                <img src="{{asset('public/upload/user/'.$item['avatar'])}}" alt="">
                            </div>
                            @endif
                            <h3>{{$item['name']}}</h3>

                        </div>
                        <div class="content">
                            <div class="certificate">
                                <ul class="list">
                                    <li>
                                        <span class="text">Giới tính</span>
                                        <span class="number">@if($item['gender']==1){{'Nam'}}@else{{"Nữ"}}@endif</span>
                                    </li>
                                    <li>
                                        <span class="text">Ngày sinh</span>
                                        <span class="number">8.9</span>
                                    </li>

                                </ul>
                            </div>
                            <div class="exam text-center">
                                <p>lần thi đầu tiên</p>
                            </div>
                            <div class="ashford">
                                <div class="img">
                                    <img src="{{asset('public/4words/img/home/achievement/icon.png')}}" alt="">
                                </div>
                                <div class="ashford-content">
                                    <p>Đại học</p>
                                    <p class="top">Ngoại thương</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection
