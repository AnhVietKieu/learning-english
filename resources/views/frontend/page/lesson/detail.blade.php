@extends('frontend.layout.master.master')
@section('content')

    <div class="main-content">
        <section class="section">
            <div class="row">
                <div class="col-lg-3 col-sm-12">
                    <div class="card " >


                        <div class="card-body" style="overflow: auto; height: 530px;">
                            <ul class="list-unstyled list-unstyled-border">
                                @if(isset($lessons))
                                    @foreach($lessons as $key)
                                        <li class="media active">
                                            <div class="row">
                                                @if(action_history($key->id)==true)
                                                    <i class="far fa-check-circle"></i>
                                                @else
                                                    <span style="padding: 6px;"></span>
                                                        @endif
                                                <span class="media-title"  data-key="{{$key->id}}">
                                                    <a href="{{route('FE_lesson_detail_list',array('coure_id'=>$title->id,'lesson_id'=>$key->id))}}">{{$key->title}}</a>
                                                </span>
                                            </div>

                                        </li>
                                    @endforeach
                                @endif

                            </ul>

                        </div>

                    </div>
                </div>

                <div class="col-lg-9 col-sm-12" >
                    <div class="card">

                        <div class="card-body" style="height: 530px;overflow: auto; position: relative;" id="result" >

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Video </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Bài kiểm tra</a>
                                    <input type="hidden" name="time" id="input-name1" value="@if(isset($lesson->time)){{$lesson->time}}@endif">
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Mô tả</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div id="panel-9-11-0-0" class="so-panel widget widget_sow-editor panel-first-child" data-index="27" >
                                        @if(isset($lesson->url))
                                            <iframe width="680" height="400" src="https://www.youtube.com/embed/{{$lesson->url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" style="position:relative;overflow: auto;" aria-labelledby="profile-tab">
                                    @if(isset($lesson))
                                        <div class="col-12 mb-12 display-submit">
                                            <div class="hero align-items-center text-black">
                                                <div class="2 hero-inner text-center">
                                                    <h4>Đề kiểm tra</h4>
                                                    <i>
                                                        (Bài làm sẽ tự động được nộp sau khi hết thời gian<br>
                                                        Hệ thống không tự động chấm các câu tự luận)
                                                    </i>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        Số câu:<b>{{$count}} câu</b>
                                                    </div>
                                                    <div class="col-md-6">
                                                        Thời gian: <b>@if(@isset($lesson->time)){{$lesson->time}}@endif phút </b>
                                                    </div>
                                                </div>
                                                <div class="mt-4">
                                                    <button  class="btn btn-success testing"><i class="fas fa-sign-in-alt"></i> Làm bài</button>
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                    <div class="display-testing">

                                        <div id="test"></div>
                                        <form method="post" id="my_form"  name="form-lesson" class="form-lesson" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="lesson_id" value="@if(isset($lesson->id)){{$lesson->id}}@endif">
                                            @if(isset($test->reads))

                                                @foreach($test->reads as $key)
                                                    <label class="form-label">Câu hỏi {{$i++}}:{!! $key->question !!}</label>

                                                    <div class="grid-body no-border">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="form-label">Đáp án A:</label>
                                                                    <span class="help"><input type="radio" class="check-radio" value="answer_a" name="answer_read[{{$key->id}}]"></span>
                                                                    <div class="controls">
                                                                        <input type="text" class="form-control"  value="{{$key->answer_a}}" disabled name="answer_a" >
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="form-label">Đáp án C:</label>
                                                                    <span class="help"><input type="radio"   class="check-radio"  value="answer_c" name="answer_read[{{$key->id}}]"></span>
                                                                    <div class="controls">
                                                                        <input type="text" class="form-control" value="{{$key->answer_c}}" disabled name="answer_c">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="form-label">Đáp án B:</label>
                                                                    <span class="help"><input type="radio"  class="check-radio " value="answer_b" name="answer_read[{{$key->id}}]"></span>
                                                                    <div class="controls">
                                                                        <input type="text" class="form-control"  value="{{$key->answer_b}}" disabled name="answer_b">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="form-label">Đáp án D:</label>
                                                                    <span class="help"><input type="radio"  class="check-radio" value="answer_d" name="answer_read[{{$key->id}}]"></span>
                                                                    <div class="controls">
                                                                        <input type="text" class="form-control" value="{{$key->answer_d}}" disabled name="answer_d" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>


                                                @endforeach
                                            @endif
                                            @if(isset($test->listens))

                                                @foreach($test->listens as $key)
                                                    <label class="form-label">Câu hỏi {{$i++}}:{!! $key->question !!}</label>
                                                    @if($key->audio != '')
                                                        <div><audio controls>
                                                                <source src="{{asset('public/upload/audio/'.$key->audio)}}" type="audio/mp4">
                                                            </audio></div>
                                                    @endif

                                                    <div class="grid-body no-border">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="form-label">Đáp án A:</label>
                                                                    <span class="help"><input type="radio" class="check-radio" value="answer_a" name="answer_listen[{{$key->id}}]"></span>
                                                                    <div class="controls">
                                                                        <input type="text" class="form-control"  value="{{$key->answer_a}}" disabled name="answer_a" >
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="form-label">Đáp án C:</label>
                                                                    <span class="help"><input type="radio"   class="check-radio"  value="answer_c" name="answer_listen[{{$key->id}}]"></span>
                                                                    <div class="controls">
                                                                        <input type="text" class="form-control" value="{{$key->answer_c}}" disabled name="answer_c">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="form-label">Đáp án B:</label>
                                                                    <span class="help"><input type="radio"  class="check-radio" value="answer_b" name="answer_listen[{{$key->id}}]"></span>
                                                                    <div class="controls">
                                                                        <input type="text" class="form-control"  value="{{$key->answer_b}}" disabled name="answer_b">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="form-label">Đáp án D:</label>
                                                                    <span class="help"><input type="radio"  class="check-radio" value="answer_d" name="answer_listen[{{$key->id}}]"></span>
                                                                    <div class="controls">
                                                                        <input type="text" class="form-control" value="{{$key->answer_d}}" disabled name="answer_d" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>


                                                @endforeach
                                            @endif
                                            @if(isset($test->writes))
                                                @foreach($test->writes as $key)
                                                    <label class="form-label">Câu hỏi {{$i++}}:</label>
                                                    <p>{{$key->name}}</p>
                                                    <div class="grid-body no-border">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                {!! $key->question !!}
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p style="text-decoration: underline;"><b>Trả lời:</b></p>
                                                                <textarea name="answer_write[{{$key->id}}]" style="height: 150px;" class="col-lg-11" value=""></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif

                                            @if(isset($test->speaks))
                                                @foreach($test->speaks as $key)
                                                    <label class="form-label">Câu hỏi {{$i++}}:</label>
                                                    <p>{{$key->name}}</p>
                                                    <div class="grid-body no-border">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                {!! $key->question !!}
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="file" id="my_images" class="form-control file-avatar"  name="answer_speak_file[{{$key->id}}]">
                                                            </div>
                                                        </div>
                                                    </div>

                                                @endforeach
                                            @endif


                                            <div class="hero align-items-center text-black" style=" background:lawngreen;margin-top: 15px;padding: 0px;">
                                                <div class="row">

                                                    <div class="demo" style="margin-right: 10px;"></div>

                                                    <button type="button" class="btn btn-success tst">Kết thúc</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    @if(isset($lesson->description))
                                        {!!$lesson->description!!}
                                    @endif
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </section>
    </div>
    <script>
        $(document).ready(function () {

            $('.display-testing').css('display', 'none');

            $('.testing').on('click', function () {

                var dem = 1;
                $('.display-testing').css('display', 'block');
                $('.display-submit').css('display', 'none');


                $('.tst').on('click', function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        url: '{{route('FE_lesson_mark')}}',
                        data: new FormData($('#my_form')[0]),
                        processData: false,
                        contentType: false,

                        success: function (data) {
                            document.getElementById('result').innerHTML = data
                        }

                    });
                })


                var millis = (document.getElementById('input-name1').value) * 60000;

                $('.demo').css('display', 'none');

                if (millis != '') {
                    function displaytimer() {
                        //Thank you MaxArt.
                        var hours = Math.floor(millis / 36e5),
                            mins = Math.floor((millis % 36e5) / 6e4),
                            secs = Math.floor((millis % 6e4) / 1000);
                        $('.demo').css('display', 'block');
                        $('.demo').html(hours + ':' + mins + ':' + secs);
                    }

                    setInterval(function () {
                        if (millis > 1) {
                            millis -= 1000;
                            displaytimer();
                        } else {
                            if (dem == 1) {
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: 'POST',
                                    url: '{{route('FE_lesson_mark')}}',
                                    data: new FormData($('#my_form')[0]),
                                    processData: false,
                                    contentType: false,

                                    success: function (data) {
                                        document.getElementById('result').innerHTML = data
                                    }

                                });
                                dem += 1;
                            }

                        }

                    }, 1000);
                }


            });
        })
    </script>
    @endsection
