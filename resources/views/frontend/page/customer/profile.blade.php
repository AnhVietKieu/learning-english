@extends('frontend.layout.master.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h4>Thông tin cá nhân</h4>
                </div>
                <form action="{{route('FE_postprofile')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-12 col-sm-12 col-lg-12">
                        <div class="card profile-widget">
                            <div class="profile-widget-header">
                                @if(isset($customer['avatar']))
                                    <img alt="image" src="{{asset('public/upload/customer/'.$customer['avatar'])}}" class="rounded-circle profile-widget-picture">
                                @else
                                    <img alt="image" src="{{asset('public/frontend/assets/img/avatar/avatar-1.png')}}" class="rounded-circle profile-widget-picture">
                                @endif
                                <div class="profile-widget-items">
                                    <div class="profile-widget-item">
                                        <div class="profile-widget-item-label">Thứ hạng</div>
                                        <div class="profile-widget-item-value">
                                            @if(isset($customer['levels']['image']))
                                                <img src='{{asset('public/upload/level/'.$customer['levels']['image'])}}' width="50px" height="50px">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="profile-widget-item">
                                        <div class="profile-widget-item-label text-center">Thông báo</div>
                                        <div class="profile-widget-item-value">
                                            <label class="custom-switch mt-2 ">
                                                <input type="checkbox" name="status_notification" value="0" @if($customer['status_notification']==0){{'checked'}}@endif class="custom-switch-input">
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="profile-widget-description pb-0">
                                <div class="col-12 col-md-12 col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label>Tên hiển thị</label>
                                                <input type="text" name="username" value="{{$customer->username}}" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Điện thoại</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fas fa-phone"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" name="phone" value="{{$customer->phone}}" class="form-control phone-number">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fas fa-lock"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" name="email" value="{{$customer->email}}" class="form-control pwstrength" data-indicator="pwindicator">
                                                </div>
                                                <div id="pwindicator" class="pwindicator">
                                                    <div class="bar"></div>
                                                    <div class="label"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label >Giới Tính:</label>
                                                </div>
                                                <div class="form-group ">
                                                    <input type="hidden" value=""  id="e12" tabindex="-1" class="select2-offscreen">
                                                    <select class="form-control select2" id="cardType" name="gender" data-init-plugin="select2">
                                                        <option @if($customer['gender']==1){{'selected'}}@endif value="1">Nam</option>
                                                        <option @if($customer['gender']==0){{'selected'}}@endif value="0">Nữ</option>
                                                        <option @if($customer['gender']==2){{'selected'}}@endif value="2">Khác</option>
                                                    </select>
                                                </div>

                                            </div>




                                                <div class="row">
                                                    <div class='col-sm-12'>
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label >Ngày sinh:</label>
                                                            </div>
                                                            <div class='input-group date' id='datetimepicker1'>
                                                                <input type='text' name="birthday" value="{{date('Y/m/d',strtotime($customer['birthday']))}}" class="form-control" />
                                                                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div id="msg">Avatar</div>
                                                    <form method="post" id="image-form">
                                                        <input type="file" name="avatar" class="file" accept="image/*">
                                                        <div class="input-group my-3">
                                                            <input type="text" name="avatar" class="form-control" disabled placeholder="Upload File" id="file">
                                                            <div class="input-group-append">
                                                                <button type="button" class="browse btn btn-primary">Browse...</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="ml-2 col-sm-6">
                                                    <img src="https://placehold.it/80x80" id="preview" class="img-thumbnail">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right pt-0">
                                <button type="submit" class="btn btn-success">Lưu thông tin</button>
                            </div>
                        </div>
                    </div></form>

</div>

            <script>
                $('#datetimepicker1').datetimepicker();
                $("#preview").css('display','none');
                $(document).on("click", ".browse", function() {
                    var file = $(this).parents().find(".file");
                    file.trigger("click");

                });
                $('input[type="file"]').change(function(e) {
                    var fileName = e.target.files[0].name;

                    $("#file").val(fileName);

                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $("#preview").css('display','block');

                        // get loaded data and render thumbnail.
                        document.getElementById("preview").src = e.target.result;
                    };
                    // read the image file as a data URL.
                    reader.readAsDataURL(this.files[0]);
                });
            </script>


    @endsection
