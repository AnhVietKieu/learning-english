@extends('frontend.layout.master.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h4>Thay đổi mật khẩu</h4>
                </div>
                <form action="{{route('FE_postpassword')}}" method="post" enctype="multipart/form-data">
                    <div class="alert alert-success" id="data"></div>
                    @csrf
                    <div class="col-12 col-sm-12 col-lg-12">
                            <div class="profile-widget-description pb-0">
                                <div class="col-12 col-md-12 col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label>Mật khẩu cũ :</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fas fa-lock"></i>
                                                        </div>
                                                    </div>
                                                    <input type="password" name="password"  class="form-control pwstrength" data-indicator="pwindicator">

                                                </div>

                                                <div id="pwindicator" class="pwindicator">
                                                    <div class="bar"></div>
                                                    <div class="label"></div>
                                                </div>
                                                <span id="error_validate_password"></span>
                                            </div>

                                            <div class="form-group">
                                                <label>Mật khẩu mới:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fas fa-lock"></i>
                                                        </div>
                                                    </div>
                                                    <input type="password" name="password_new"  class="form-control pwstrength" data-indicator="pwindicator">

                                                </div>
                                                <div id="pwindicator" class="pwindicator">
                                                    <div class="bar"></div>
                                                    <div class="label"></div>
                                                </div>
                                                <span id="error_validate_password_new"></span>
                                            </div>

                                            <div class="form-group">
                                                <label>Nhập lại mật khẩu</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fas fa-lock"></i>
                                                        </div>
                                                    </div>
                                                    <input type="password" name="same_password_new"  class="form-control pwstrength" data-indicator="pwindicator">

                                                </div>
                                                <div id="pwindicator" class="pwindicator">
                                                    <div class="bar"></div>
                                                    <div class="label"></div>
                                                </div>
                                                <span id="error_validate_password_new_same"></span>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right pt-0">
                                <button type="button" class="btn btn-success changepassword">Lưu thông tin</button>
                            </div>
                        </div>
                    </div></form>

            </div>

    <script>
        $('#data').css('display', 'none');

        $('.changepassword').on('click', function () {
            var password = $("input[name='password']").val();
            var password_new = $("input[name='password_new']").val();
            var password_same = $("input[name='same_password_new']").val();


            if (password.length < 1) {
                $('#error_validate_password').html('<span style="color: red;">Mật khẩu cũ không được để trống</span>');
            }
            if (password_new.length < 1) {
                $('#error_validate_password_new').html('<span style="color: red;">Mật khẩu mới không được để trống</span>');
            }
            if (password_same.length < 1) {
                $('#error_validate_password_new_same').html('<span style="color: red;">Xác nhận mật khẩu mới không được để trống</span>');
            } else {
                if (password_same !== password_new) {
                    $('#error_validate_password_new_same').html('<span style="color: red;">Xác nhận mật khẩu mới không đúng </span>');
                }
            }
            if (password.length > 0 && password_new.length > 0 && password_same.length > 0 && password_new === password_same) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',

                    url: '{{route('FE_postpassword')}}',

                    data: {

                        password: password, password_new: password_new,
                    },

                    success: function (data) {
                        $('#data').css('display', 'block');
                        document.getElementById('data').innerHTML = data
                    }
                });
            }
        });
    </script>




@endsection
