@extends('frontend.layout.master.master')
@section('content')
    <div class="main-content">
<section class="section">
    <div class="section-header" style="border: 1px solid #DFDFDF">
        <div class="item">
            <div class="if-left-his">

                <h4 class="title-c">
                    <a href="/course/content?id=57">Khoá học:{{$test->coure['name']}}</a>
                </h4>
                <a class="course-c-1" href="/course/content?id=57&amp;lesson_id=615&amp;active=2&amp;activeExercise=4864">Bài học:{{$test['title']}}</a><br>

            </div>
            <div class="if-right-his">
                <div class="n-correct">Câu đúng: <span class="color-1">{{$test['history'][0]['answer_true']}}</span><span>/{{$test['history'][0]['total_question']}}</span>
                </div>
                <div class="tme-test">Thời gian nộp bài:
                    <span>{{$test['history'][0]['created_at']}}</span></div>
                <div class="n-point">Điểm: <span class="color-1">{{$test['history'][0]['score']}}</span><span>/{{$test['score']}}</span>
                </div>
            </div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">


            @if(isset($test->reads))

                @foreach($test->reads as $key)
                    <div class="col-12 mb-4">
                        <div class="hero
                            @foreach($test['history'][0]['history_test'] as $item)
                                @foreach($item['answer_customer'] as $item1 =>$key1)
                                    @if($item1==$key->id && $item['type']=='read' && $key1==$key['answer_true'])
                                    {{'bg-success'}}
                                    @elseif($item1==$key->id && $item['type']=='read' && $key1!=$key['answer_true'])
                                    {{'bg-danger'}}
                                    @endif
                                @endforeach
                        @endforeach  text-white" style="background: #D9534F;">
                    <label class="form-label">Câu hỏi {{$i++}}:@foreach($test['history'][0]['history_test'] as $item)
                            @foreach($item['answer_user'] as $item1 =>$key1 )
                                @if($item1==$key->id && $item['type']=='read')
                                    <b style=" color: #34395E;">@if($key1['score']=='null')({{'0 điểm'}})@else{{$key1['score']}} điểm @endif</b>
                                @endif
                            @endforeach
                        @endforeach{!! $key->question !!}</label>

                    <div class="grid-body no-border">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Đáp án A:</label>
                                    <span class="help"><input type="radio" @foreach($test['history'][0]['history_test'] as $item)
                                        @foreach($item['answer_customer'] as $item1 =>$key1 )
                                        @if($item1==$key->id && $item['type']=='read' && $key1=='answer_a')
                                        {{'checked'}}
                                        @endif
                                        @endforeach
                                        @endforeach class="check-radio" value="answer_a" name="answer_read[{{$key->id}}]"></span>
                                    <div class="controls">
                                        <input type="text" class="form-control"  value="{{$key->answer_a}}" disabled name="answer_a" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Đáp án C:</label>
                                    <span class="help"><input type="radio"@foreach($test['history'][0]['history_test'] as $item)
                                        @foreach($item['answer_customer'] as $item1 =>$key1 )
                                        @if($item1==$key->id && $item['type']=='read' && $key1=='answer_c')
                                        {{'checked'}}
                                        @endif
                                        @endforeach
                                        @endforeach  class="check-radio"  value="answer_c" name="answer_read[{{$key->id}}]"></span>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{$key->answer_c}}" disabled name="answer_c">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Đáp án B:</label>
                                    <span class="help"><input type="radio" @foreach($test['history'][0]['history_test'] as $item)
                                        @foreach($item['answer_customer'] as $item1 =>$key1 )
                                        @if($item1==$key->id && $item['type']=='read' && $key1=='answer_b')
                                        {{'checked'}}
                                        @endif
                                        @endforeach
                                        @endforeach class="check-radio" value="answer_b" name="answer_read[{{$key->id}}]"></span>
                                    <div class="controls">
                                        <input type="text" class="form-control"  value="{{$key->answer_b}}" disabled name="answer_b">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Đáp án D:</label>
                                    <span class="help"><input type="radio" @foreach($test['history'][0]['history_test'] as $item)
                                        @foreach($item['answer_customer'] as $item1 =>$key1 )
                                        @if($item1==$key->id && $item['type']=='read' && $key1=='answer_d')
                                        {{'checked'}}
                                        @endif
                                        @endforeach
                                        @endforeach class="check-radio" value="answer_d" name="answer_read[{{$key->id}}]"></span>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{$key->answer_d}}" disabled name="answer_d" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>

                            <div class="hero-inner">
                                <h2>Đáp án đúng :</h2>
                                <p class="lead">{{$key[$key->answer_true]}}</p>
                                <h2>Ghi chú</h2>
                                <p class="lead">{{$key->note}}</p>
                            </div>

                        </div>
                    </div>
                @endforeach
            @endif

                @if(isset($test->listens))

                    @foreach($test->listens as $key)
                        <div class="col-12 mb-4">
                            <div class="hero  @foreach($test['history'][0]['history_test'] as $item)
                            @foreach($item['answer_customer'] as $item1 =>$key1 )
                            @if($item1==$key->id && $item['type']=='listen' && $key1==$key['answer_true'])
                            {{'bg-success'}}
                            @elseif($item1==$key->id && $item['type']=='listen' && $key1!=$key['answer_true'])
                            {{'bg-danger'}}

                            @endif
                            @endforeach
                            @endforeach text-white"  style="background: #D9534F;">
                                <label class="form-label">Câu hỏi {{$i++}}:@foreach($test['history'][0]['history_test'] as $item)
                                        @foreach($item['answer_user'] as $item1 =>$key1 )
                                            @if($item1==$key->id && $item['type']=='listen')
                                                <b style=" color: #34395E;">@if($key1['score']=='null')({{'0 điểm'}})@else{{$key1['score']}} điểm @endif</b>
                                            @endif
                                        @endforeach
                                    @endforeach{!! $key->question !!}</label>

                            @if($key->audio != '')
                            <div><audio controls>
                                    <source src="{{asset('public/upload/audio/'.$key->audio)}}" type="audio/mp4">
                                </audio></div>
                        @endif

                        <div class="grid-body no-border">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Đáp án A:</label>
                                        <span class="help"><input type="radio" @foreach($test['history'][0]['history_test'] as $item)
                                            @foreach($item['answer_customer'] as $item1 =>$key1 )
                                            @if($item1==$key->id && $item['type']=='listen' && $key1=='answer_a')
                                            {{'checked'}}
                                            @endif
                                            @endforeach
                                            @endforeach class="check-radio" value="answer_a" name="answer_listen[{{$key->id}}]"></span>
                                        <div class="controls">
                                            <input type="text" class="form-control"  value="{{$key->answer_a}}" disabled name="answer_a" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Đáp án C:</label>
                                        <span class="help"><input type="radio" @foreach($test['history'][0]['history_test'] as $item)
                                            @foreach($item['answer_customer'] as $item1 =>$key1 )
                                            @if($item1==$key->id && $item['type']=='listen' && $key1=='answer_c')
                                            {{'checked'}}
                                            @endif
                                            @endforeach
                                            @endforeach class="check-radio"  value="answer_c" name="answer_listen[{{$key->id}}]"></span>
                                        <div class="controls">
                                            <input type="text" class="form-control" value="{{$key->answer_c}}" disabled name="answer_c">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Đáp án B:</label>
                                        <span class="help"><input type="radio"@foreach($test['history'][0]['history_test'] as $item)
                                            @foreach($item['answer_customer'] as $item1 =>$key1 )
                                            @if($item1==$key->id && $item['type']=='listen' && $key1=='answer_b')
                                            {{'checked'}}
                                            @endif
                                            @endforeach
                                            @endforeach  class="check-radio" value="answer_b" name="answer_listen[{{$key->id}}]"></span>
                                        <div class="controls">
                                            <input type="text" class="form-control"  value="{{$key->answer_b}}" disabled name="answer_b">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Đáp án D:</label>
                                        <span class="help"><input type="radio" @foreach($test['history'][0]['history_test'] as $item)
                                            @foreach($item['answer_customer'] as $item1 =>$key1 )
                                            @if($item1==$key->id && $item['type']=='listen' && $key1=='answer_d')
                                            {{'checked'}}
                                            @endif
                                            @endforeach
                                            @endforeach class="check-radio" value="answer_d" name="answer_listen[{{$key->id}}]"></span>
                                        <div class="controls">
                                            <input type="text" class="form-control" value="{{$key->answer_d}}" disabled name="answer_d" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                                <div class="hero-inner">
                                    <h2>Đáp án đúng :</h2>
                                    <p class="lead">{{$key[$key->answer_true]}}</p>
                                    <h2>Ghi chú</h2>
                                    <p class="lead">{{$key->note}}</p>
                                </div>
                            </div>
                        </div>


                    @endforeach
                @endif

                @if(isset($test->writes))
                    @foreach($test->writes as $key)
                        <div class="col-12 mb-4">
                            <div class="hero bg-info text-white">
                        <label class="form-label">Câu hỏi {{$i++}}: @foreach($test['history'][0]['history_test'] as $item)
                                @foreach($item['answer_user'] as $item1 =>$key1 )
                                    @if($item1==$key->id && $item['type']=='write')
                                        <b style=" color: #34395E;">({{$key1['score']}} điểm)</b>
                                    @endif
                                @endforeach
                            @endforeach</label>
                        <p>{{$key->name}}</p>
                        <div class="grid-body no-border">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! $key->question !!}
                                </div>
                                <div class="col-md-12">
                                    <p style="text-decoration: underline;"><b>Trả lời:</b></p>
                                    <textarea disabled name="answer_write[{{$key->id}}]" style="height: 150px;" class="col-lg-11" value="">
                                         @foreach($test['history'][0]['history_test'] as $item)
                                            @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                @if($item1==$key->id && $item['type']=='write')
                                                    {{$key1}}
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </textarea>
                                </div>
                            </div>
                        </div>
                                <div class="hero-inner">
                                    <h2>Congratulations</h2>
                                    @foreach($test['history'][0]['history_test'] as $item)
                                        @foreach($item['answer_user'] as $item1 =>$key1 )
                                            @if($item1==$key->id && $item['type']=='write')
                                                <p class="lead"> {{$key1['note']}}</p>
                                    @endif
                                    @endforeach
                                    @endforeach


                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

                @if(isset($test->speaks))
                    @foreach($test->speaks as $key)
                        <div class="col-12 mb-4">
                            <div class="hero bg-info text-white">
                        <label >Câu hỏi {{$i++}}:@foreach($test['history'][0]['history_test'] as $item)
                                @foreach($item['answer_user'] as $item1 =>$key1 )
                                    @if($item1==$key->id && $item['type']=='speak')
                                        <b style=" color: #34395E;">({{$key1['score']}} điểm)</b>
                                    @endif
                                @endforeach
                            @endforeach</label>
                        <p>{{$key->name}}</p>
                        <div class="grid-body no-border">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! $key->question !!}
                                </div>
                                <div><audio controls>
                                        @foreach($test['history'][0]['history_test'] as $item)
                                            @foreach($item['answer_customer'] as $item1 =>$key1 )
                                                @if($item1==$key->id && $item['type']=='speak')
                                                    <source src="{{asset('public/upload/audio/customer/'.$key1)}}" type="audio/mp4">
                                                @endif
                                            @endforeach
                                        @endforeach

                                    </audio></div>

                            </div>
                        </div>
                                <div class="hero-inner">
                                    <h2>Congratulations</h2>
                                    @foreach($test['history'][0]['history_test'] as $item)
                                        @foreach($item['answer_user'] as $item1 =>$key1 )
                                            @if($item1==$key->id && $item['type']=='speak')
                                                <p class="lead"> {{$key1['note']}}</p>
                                            @endif
                                        @endforeach
                                    @endforeach

                                </div>
                            </div>
                        </div>

                    @endforeach
                @endif
        </div>
    </div>
</section>
    </div>

@endsection
