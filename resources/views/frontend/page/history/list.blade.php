@extends('frontend.layout.master.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header" style="border: 1px solid #DFDFDF">
                <h1>Lich su</h1>
            </div>
            <div class="row">
                @foreach($history as $item)
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card ">
                        <div class="card-header">
                            <h4>{{$item['lesson']['coure']['name']}}</h4>
                            <div class="card-header-action">
                                <div class="dropdown">
                                    <a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle">Lịch sử</a>
                                    <div class="dropdown-menu">
                                        <a href="{{route('FE_history_detail',$item->id)}}" class="dropdown-item has-icon"><i class="fas fa-eye"></i> View</a>
                                        <a href="#" class="dropdown-item has-icon"><i class="far fa-edit"></i> Báo cáo</a>
                                    </div>
                                </div>
                                <a href="#" class="btn @if($item['status']==0){{'btn-success'}}@else{{'btn-danger'}}@endif">@if($item['status']==0){{'Chưa chấm'}}@else{{'Đã chấm'}}@endif</a>

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="if-right-his">
                                <div class="n-correct">Bài học:{{$item['lesson']['title']}} </div>
                                <?php $var = $item['answer_false']+$item['answer_true'] ;?>
                                <div class="n-correct">Câu đúng:<span class="color-1">{{$item['answer_true']}}</span><span>@if(isset($var)&&$var!=0)/{{$var}}@endif</span>
                                </div>
                                <div class="tme-test">Nộp bài:
                                    <span>{{$item['created_at']}}</span>
                                </div>
                                <div class="n-point">Điểm:<span class="color-1">{{$item['score']}}</span><span>/{{$item['lesson']['score']}}</span>
                                </div>
                                <div class="n-correct">Người chấm:{{$item['user']['name']}} </div>
                            </div>

                        </div>
                    </div>
                </div>
                    @endforeach

                    <div class="pagination pull-right">{!! $history->links() !!}</div>

            </div>

        </section>
    </div>

@endsection
