@extends('frontend.layout.master.master')
@section('content')
    <div class="main-content">
        <section class="section">
    <div class="card">
    <div class="card-header">
        <h4>Tài liệu tham khảo</h4>
    </div>
    <div class="card-body">
        <ul class="list-group">
            @foreach($documents as $item)
            <li class="list-group-item d-flex justify-content-between align-items-center">
                {{$i++}}:
                {{$item->title}}
                <span class="badge badge-success badge-pill"><a href="{{asset('public/upload/document/'.$item->file)}}" download=""><span style="color: black;">Dowload</span></a></span>
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endsection
