@extends('frontend.layout.master.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Từ vựng </h1>

            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <nav class="navbar navbar-expand-lg main-navbar">
                            <form class="form-inline mr-auto">

                                <div class="search-element">
                                        <input class="form-control search-volu" name="search" type="search" placeholder="Search" aria-label="Search" data-width="250">
                                        <button class="btn" type="submit" ><i class="fas fa-search"></i></button>
                                     <div class="search-backdrop"></div>

                                    <div class="search-result" id="result">

                                        <div class="search-header">
                                            Không có thông tin
                                        </div>



                                    </div>
                                </div>
                            </form>

                        </nav>
                        @if(!empty($vocubal))
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Từ vựng : {{$vocubal->word}}</h4>
                                </div>
                                <div class="card-body">
                                    <div class="list-group">
                                        <div  class="list-group-item list-group-item-action flex-column align-items-start ">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 ><u>Dạng:</u> {{$vocubal->type}}</h5>
                                            </div>
                                            <p class="mb-1">@if(isset($vocubal->image)) <img height="150px" width="150px" src="{{asset('public/upload/vocabularie/'.$vocubal->image)}}"
                                                                                                       id="preview" class="img-thumbnail">@endif</p>
                                            <p class="mb-1"><u>Phiên âm:</u> {{$vocubal->pronounce}}</p>
                                            <p class="mb-1"><u>Nghĩa của từ :</u>{{$vocubal->meaning}}</p>
                                            <p class="mb-1"><u>Mô tả :</u> {!! $vocubal->description !!}</p>
                                        </div>

                                    </div>

                                </div>
                                <div class="card-footer">

                                </div>
                            </div>
                        </div>
                            @endif
                    </div>
                </div>
            </div>

        </section>
    </div>

    <script>
        $(document).ready(function () {
            $(".search-volu").on('keyup', function () {
                var search = this.value;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',

                    url: '{{route('FE_post_vocabularie')}}',

                    data: {

                        search:search
                    },

                    success: function (data) {
                        document.getElementById('result').innerHTML = data
                    }

                });
            });
        });
    </script>
    @endsection
