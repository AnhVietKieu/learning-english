@include('frontend.layout.display.head')
@include('frontend.layout.display.header')
@yield('content')
@include('frontend.layout.display.footer')
