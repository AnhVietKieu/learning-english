@include('frontend.layout.dashbroad.head')

@include('frontend.layout.dashbroad.header')

@yield('content')

@include('frontend.layout.dashbroad.footer')
