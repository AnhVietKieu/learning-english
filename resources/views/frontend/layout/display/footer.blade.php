<footer>
    <section class="footer-container p-70" style="background-image: url({{asset('public/4words/img/basic/bg-footer.png')}}); margin-top: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-bottom">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="logo-footer">
                                    <h2>ENGLISH </h2>
                                    <div class="word-time">
                                        <div class="icon-word"> <i class="fa fa-calendar-o" aria-hidden="true"></i></div>
                                        <div class="text-word-time">
                                            <p>Giờ làm việc</p>
                                            <ul>
                                                <li><strong>8h30 – 20h30 </strong> từ Thứ 2 đến Thứ 7</li>
                                                <li><strong>8h30 – 19h</strong> Chủ nhật | Thời gian nghỉ trưa: 12h00 – 13:30</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="list-footer-container">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="list-footer">
                                                <ul>
                                                    <li>
                                                        <a href=""> <span class="icon"><i class="fa fa-phone" aria-hidden="true"></i></span> <span class="text">0123456789</span> </a>
                                                    </li>
                                                    <li>
                                                        <a href=""> <span class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span> <span class="text">info@english_vn.edu.vn</span> </a>
                                                    </li>
                                                    <li>
                                                        <a href=""> <span class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span> <span class="text">406 Hồ Tùng Mậu, Cầu Giấy, Hà Nội</span> </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="list-footer icon-footer">
                                                <ul>
                                                    <li>
                                                        <a href=""> <span class="icon"><i class="fa fa-comments-o" aria-hidden="true"></i></span> <span class="text">Đóng góp ý kiến</span> </a>
                                                    </li>
                                                    <li>
                                                        <a href=""> <span class="icon"><i class="fa fa-users" aria-hidden="true"></i></span> <span class="text">Tuyển dụng</span> </a>
                                                    </li>
                                                    <li>
                                                        <a href=""> <span class="icon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span> <span class="text">Học bổng</span> </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>
<script src="{{asset('public/4words/js/jquery-3.4.1.js')}}"></script>
<script src="{{asset('public/4words/js/popper.min.js')}}"></script>
<script src="{{asset('public/4words/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/4words/js/float-panel.js')}}"></script>
<script src="{{asset('public/4words/js/wow.min.js')}}"></script>
<script src="{{asset('public/4words/js/custom.js')}}"></script>
<script src="{{asset('public/4words/js/owl.carousel.js')}}"></script>
<script src="{{asset('public/4words/js/setup.js')}}"></script>
</body>
</html>
