<body>
<header>
    <div class="container">
        <div class="row top-menu">
            <div class="logo">
{{--                <a href="{{route('FE.home')}}"><img src="{{asset('public/4words/img/home/logo.png')}}" alt=""></a>--}}
            </div>
            <div class="nav">
                <ul>
                    <li>
                        <a href="{{route('FE.home')}}">Trang chủ</a>
                    </li>
                    <li><a href="{{route('FE.target')}}">Giới thiệu chung</a></li>
                    <li><a href="{{route('FE.achievenment_student')}}">Thành tích học tập</a></li>
                    <li><a href="{{route('FE.new_home')}}">Khoá học</a></li>
                    <li><a href="{{route('FE.achievenment')}}">Giáo viên</a></li>
                    <li><a href="{{route('FE.share')}}">Tài liệu tham khảo</a></li>
                    <li><a href="{{route('FE.contact')}}">Liên lạc</a></li>
                    @if(Session::has('username_cus'))
                        <li style="color: red;text-transform: capitalize;"><i class="fa fa-user"></i> {{session('username_cus')}}</li>
                        <li><a href="{{route('FE_logout')}}">Đăng suất</a></li>
                        @else
                        <li><a href="{{route('FE_register')}}" >Đăng ký </a></li>
                        <li><a href="{{route('FE_login')}}">Đăng nhập </a></li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</header>

<!-- advantages -->

