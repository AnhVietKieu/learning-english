<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <nav class="navbar navbar-expand-lg main-navbar">
            <form class="form-inline mr-auto">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                </ul>

            </form>

            <ul class="navbar-nav navbar-right">

                <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg @if(isset($notification)&&count($notification)>=1) {{'beep'}}@endif "><i class="far fa-bell"></i></a>
                    <div class="dropdown-menu dropdown-list dropdown-menu-right">
                        <div class="dropdown-header">Thông báo
                            <div class="float-right">
                                <a href="#"></a>
                            </div>
                        </div>
                        <div class="dropdown-list-content dropdown-list-icons" id="notification">

                            @foreach($notification as $item)
                            <a href="{{route('FE_history_detail',$item->id)}}" class="dropdown-item dropdown-item-unread">
                                <div class="dropdown-item-icon bg-primary text-white">
                                    <i class="fas fa-code"></i>
                                </div>
                                <div class="dropdown-item-desc">
                                   {{$item['lesson']['title']}} đã được chấm !
                                    <div class="time text-primary">Link</div>
                                </div>
                            </a>
                            @endforeach
                        </div>
                        <div class="dropdown-footer text-center">
                            <a href="#"></a>
                        </div>
                    </div>
                </li>
                <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                        <img alt="image"  src="{{asset('public/upload/customer/'.\Illuminate\Support\Facades\Auth::guard('customers')->user()->avatar)}}"  class="rounded-circle mr-1">
                        <div class="d-sm-none d-lg-inline-block">Hi,@if(Session::has('username_cus')){{session('username_cus')}}@endif</div></a>
                    <div class="dropdown-menu dropdown-menu-right">

                        <a href="{{route('FE_profile')}}" class="dropdown-item has-icon">
                            <i class="far fa-user"></i> Thông tin cá nhân
                        </a>
                        <a href="{{route('FE_password')}}" class="dropdown-item has-icon">
                            <i class="fas fa-cog"></i> Thay đổi mật khẩu
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{route('FE_logout')}}" class="dropdown-item has-icon text-danger">
                            <i class="fas fa-sign-out-alt"></i> Logout
                        </a>
                    </div>
                </li>
            </ul>

        </nav>

        <div class="main-sidebar">
            <aside id="sidebar-wrapper">
                <div class="sidebar-brand">
                    <a href="index.html">FE English</a>
                </div>
                <div class="sidebar-brand sidebar-brand-sm">
                    <a href="index.html">St</a>
                </div>
                <ul class="sidebar-menu">
                    <li class="nav-item dropdown @if(Request::path()== 'coure'){{'active'}}@endif">
                        <a href="{{route('FE_coure')}}" class="nav-link "><i class="fas fa-fire"></i><span>Khoá học</span></a>
                    </li>
                    <li @if(Request::path()== 'vocabularie')class="active" @endif><a class="nav-link" href="{{route('FE_vocabularie')}}"><i class="far fa-file-alt"></i><span>Từ vựng</span></a></li>

                    <li @if(Request::path()== 'history')class="active" @endif><a class="nav-link" href="{{route('FE_history')}}"><i class="fas fa-history"></i><span>Lịch sử</span></a></li>

                    <li class="menu-header">Tài liệu</li>

                    <li @if(Request::path()== 'document')class="active" @endif><a class="nav-link" href="{{route('FE_document.list')}}"><i class="fas fa-th"></i><span>Tài liệu tham khảo</span></a></li>
                </ul>


            </aside>
        </div>

        @if(Session::has('success'))
            <div class="alert alert-success right">
                <p> {{session('success')}} </p>
            </div>
        @endif

        @if(Session::has('error'))
            <div class="alert alert-danger right" >
                <p> {{session('error')}} </p>
            </div>
        @endif










