<body class="content" >
<div class="loader">
    <img src="{{asset('public/assets/img/loading.gif')}}" alt="Loading..." />
</div>
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse ">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">

        <div class="header-seperation">
            <ul class="nav pull-left notifcation-center visible-xs visible-sm">
                <li class="dropdown">
                    <a href="#main-menu" data-webarch="toggle-left-side">
                        <i class="material-icons">menu</i>
                    </a>
                </li>
            </ul>



            <!-- BEGIN LOGO -->
            <a href="{{route('admin')}}">
{{--                 <img src="{{asset('public/assets/img/aaaa.png')}}" class="logo" alt="" data-src="{{asset('public/assets/img/aaaa.png')}}" data-src-retina="{{asset('public/assets/img/aaaa.png')}}" width="125" height="40" /> --}}
            </a>



            <!-- END LOGO -->
        </div>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <div class="header-quick-nav">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="pull-left">
                <ul class="nav quick-section">
                    <li class="quicklinks">
                        <a  class="" id="layout-condensed-toggle">
                            <i class="material-icons">menu</i>
                        </a>
                    </li>
                </ul>
            </div>




            <!-- END TOP NAVIGATION MENU -->
            <!-- BEGIN CHAT TOGGLER -->
            <div class="pull-right">
                <div class="chat-toggler sm">
                    <div class="profile-pic">
                    </div>
                </div>
                <ul class="nav quick-section ">
                    <li class="quicklinks">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            @if(!empty($user_config))
                                <img src="{{asset('public/upload/language/'.$user_config[0])}}" alt="Language">
                            @endif
                        </a>
                        <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                            @foreach($language as $item)
                                <li>
                                    <a href="{{route('language.setting',$item['id'])}}">
                                        <img class="user-thumb" width="25px" height="25px" src="{{asset('public/upload/language/'.$item['icon'])}}" >
                                        <span style="color: red;">{{$item->language}}</span>
                                    </a>
                                </li>

                            @endforeach
                        </ul>
                    </li>
                </ul>
                <ul class="nav quick-section ">
                    <li class="quicklinks">
                        <a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options">
                            <i class="material-icons">tune</i>
                        </a>
                        <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                            <li>
                                <a><i class="glyphicon glyphicon-user"></i><span style="color: red;"> @if(session('username')){{ session('username')}}@endif </span></a>
                            </li>
                            <li>
                                <a href="{{route('user.changepassword')}}"><i class="glyphicon glyphicon-lock"></i><span> {{lang_data('Change password')}}</span></a>
                            </li>
                            <li>
                                <a href="{{route('user.updateadmin')}}"><i class="glyphicon glyphicon-edit"></i><span> {{lang_data('Change profile')}}</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{route('logout')}}"><i class="material-icons">power_settings_new</i> {{lang_data('Log Out')}}</a>
                            </li>
                        </ul>
                    </li>
                </ul>




            </div>

            <!-- END CHAT TOGGLER -->
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar " id="main-menu">
        <!-- BEGIN MINI-PROFILE -->
        <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
            <div class="user-info-wrapper sm">

            </div>
            <!-- END MINI-PROFILE -->
            <!-- BEGIN SIDEBAR MENU -->

            <ul>
                @if(menu_permission('per_list_dashbroad')==true)

                <li @if(Request::path()== 'admin/dashbroad')class="open active" @endif>
                    <a href="{{route('dashbroad.list')}}"><i class="glyphicon glyphicon-stats"></i><span class="title">{{lang_data('Dashbroad')}}</span> <span class=" badge badge-disable pull-right "></span>
                    </a>
                </li>
                @endif

                @if(menu_permission('per_list_coure')==true)

                 <li @if(Request::path()== 'admin/coure')class="open active" @endif>
                    <a href="{{route('coure.list')}}"><i class="glyphicon glyphicon-education"></i><span class="title">{{lang_data('Coure')}}</span> <span class=" badge badge-disable pull-right "></span>
                    </a>
                </li>
                    @endif

                    @if(menu_permission('per_list_lesson')==true||menu_permission('per_list_read')==true||
                    menu_permission('per_list_listen')==true||menu_permission('per_list_write')==true||
                    menu_permission('per_list_speak')==true)
                  <li>
                    <a href="#"><i class="glyphicon glyphicon-asterisk"></i> <span class="title">{{lang_data('Lesson')}}</span> <span class="selected"></span> <span class="arrow "></span> </a>
                    <ul class="sub-menu">
                        @if(menu_permission('per_list_lesson')==true)
                        <li> <a href="{{route('lesson.list')}}">{{lang_data('List lesson')}}</a> </li>
                        @endif

                        @if(menu_permission('per_list_read')==true)

                        <li> <a href="{{route('read.list')}}">{{lang_data('Read')}}</a> </li>
                            @endif

                            @if(menu_permission('per_list_write')==true)

                        <li> <a href="{{route('write.list')}}">{{lang_data('Write')}}</a> </li>
                            @endif
                            @if(menu_permission('per_list_listen')==true)
                         <li> <a href="{{route('listen.list')}}">{{lang_data('Listen')}} </a> </li>
                            @endif
                            @if(menu_permission('per_list_speak')==true)
                          <li> <a href="{{route('speak.list')}}"> {{lang_data('Speak')}}</a> </li>
                                @endif

                    </ul>
                </li>
                    @endif

{{--                 <li>--}}
{{--                    <a href="{{route('lesson.list')}}"><i class="glyphicon glyphicon-asterisk"></i><span class="title">Bài học</span> <span class=" badge badge-disable pull-right "></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                    @if(menu_permission('per_list_document')==true||menu_permission('per_list_vocabularie')==true)

                <li> <a href=""><i class="glyphicon glyphicon-floppy-disk"></i><span class="title">{{lang_data('List document')}}</span> <span class="selected"></span> <span class="arrow "></span> </a>
                    <ul class="sub-menu">
                        @if(menu_permission('per_list_vocabularie')==true)
                        <li><a href="{{route('vocabularie.list')}}"><span class="title">{{lang_data('Vocabularie')}}</span> <span class=" badge badge-disable pull-right "></span>
                            </a> </li>
                        @endif
                        @if(menu_permission('per_list_document')==true)
                        <li> <a href="{{route('document.list')}}"><span class="title">{{lang_data('Document')}}</span> <span class=" badge badge-disable pull-right "></span>
                            </a> </li>
                            @endif

                    </ul>
                </li>
                    @endif

                    @if(menu_permission('per_list_history')==true)
                <li @if(Request::path()== 'admin/history')class="open active" @endif>
                    <a href="{{route('history_customer.list')}}"><i class="fa fa-history"></i> <span class="title">{{lang_data('History')}}</span> <span class=" badge badge-disable pull-right "></span> </a>
                </li>
                    @endif

                    @if(menu_permission('per_list_customer')==true||menu_permission('per_list_level')==true)

                <li>
                    <a href="#"><i class="glyphicon glyphicon-education"></i> <span class="title">{{lang_data('Customer')}}</span> <span class="selected"></span> <span class="arrow "></span> </a>
                    <ul class="sub-menu">
                        @if(menu_permission('per_list_customer')==true)
                        <li>  <a href="{{route('customer.list')}}"><span class="title">{{lang_data('Customer')}}</span> <span class=" badge badge-disable pull-right "></span>
                            </a>
                        </li>
                        @endif
                        @if(menu_permission('per_list_level')==true)

                        <li><a href="{{route('level.list')}}"><span class="title">{{lang_data('Level')}}</span> <span class=" badge badge-disable pull-right "></span>
                            </a>
                        </li>
                            @endif

                    </ul>
                </li>
                    @endif

                    @if(menu_permission('per_list_employess')==true)
                <li @if(Request::path()== 'admin/listuser')class="open active" @endif>
                    <a href="{{route('user.list')}}"><i class="glyphicon glyphicon-user"></i><span class="title">{{lang_data('User')}}</span> <span class=" badge badge-disable pull-right "></span>
                    </a>
                </li>
                    @endif

                    @if(menu_permission('per_list_language')==true)
                <li @if(Request::path()== 'admin/language')class="open active" @endif>
                    <a href="{{route('language.list')}}"><i class="glyphicon glyphicon-lamp"></i><span class="title">{{lang_data('Language')}}</span> <span class=" badge badge-disable pull-right "></span>
                    </a>
                </li>
                    @endif
                    @if(menu_permission('per_list_role')==true||menu_permission('per_list_permission')==true)
                <li>
                    <a href="#"><i class="glyphicon glyphicon-home"></i> <span class="title">{{lang_data('Permission user')}}</span> <span class="selected"></span> <span class="arrow "></span> </a>
                    <ul class="sub-menu">
                        @if(menu_permission('per_list_role')==true)
                        <li> <a href="{{route('role.list')}}">{{lang_data('Role')}}</a> </li>
                        @endif
                        @if(menu_permission('per_list_permission')==true)
                        <li> <a href="{{route('permission.list')}}">{{lang_data('Permission')}}</a> </li>
                        @endif
                    </ul>
                </li>
                    @endif

                @if(menu_permission('per_list_config')==true||menu_permission('per_list_config_notification')==true)
                <li  > <a href=""><i class="glyphicon glyphicon-cog"></i> <span class="title">{{lang_data('Config')}}</span> <span class="selected"></span> <span class="arrow "></span> </a>
                    <ul class="sub-menu">
                        @if(menu_permission('per_list_config')==true)
                        <li> <a href="{{route('config.list')}}">{{lang_data('Config')}}</a> </li>
                        @endif

                        @if(menu_permission('per_list_config_notification')==true)
                        <li><a href="{{route('config_notification_customer.list')}}">{{lang_data('Notification customer')}}</a></li>
                        @endif
                    </ul>
                </li>
                    @endif

            </ul>

            <div class="clearfix"></div>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <a href="#" class="scrollup">Scroll</a>
    <div class="footer-widget">
        <div class="progress transparent progress-small no-radius no-margin">
            <div class="progress-bar progress-bar-success animate-progress-bar" data-percentage="79%" style="width: 79%;"></div>
        </div>
        <div class="pull-right">
        </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <div class="clearfix"></div>

        <div class="content">
            <div class="container-fluid" style="background-color:#FFFFFF; border-radius: 1%;overflow: scroll;">
                @if(Session::has('success'))
                    <div class="pull-right" style="margin-right: 280px; margin-top: 10px;" >
                        <div class="alert alert-block alert-success" style="width: 300px;height: 60px; position: absolute;z-index: 99">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <p> {{session('success')}} </p>
                        </div>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="pull-right" style="margin-right: 280px; margin-top: 10px;" >
                        <div class="alert alert-block alert-danger" style="width: 300px;height: 60px;position: absolute;z-index: 99">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <p> {{session('error')}} </p>
                        </div>
                    </div>
            @endif

