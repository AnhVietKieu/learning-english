</div>
</div>

</div>
<!-- END PAGE -->
<!-- BEGIN CHAT -->

<!-- END CHAT -->
</div>
<!-- END CONTAINER -->
<script>
    window.addEventListener("load", function () {
        const loader = document.querySelector(".loader");
        loader.className += " hidden"; // class "loader hidden"
    });
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
    CKEDITOR.replace( 'editor1' ,
        {
            filebrowserBrowseUrl : '{{asset('public/ckfinder/ckfinder.html')}}',
            filebrowserImageBrowseUrl : '{{asset('public/ckfinder/ckfinder.html?type=Images')}}',
            filebrowserFlashBrowseUrl : '{{asset('public/ckfinder/ckfinder.html?type=Flash')}}',
            filebrowserUploadUrl : '{{asset('public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')}}',
            filebrowserImageUploadUrl : '{{asset('public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')}}',
            filebrowserFlashUploadUrl : '{{asset('public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')}}'
        });
</script>

<script src="{{asset('public/assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<!-- BEGIN JS DEPENDECENCIES-->
<script src="{{asset('public/assets/plugins/jquery/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/bootstrapv3/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/jquery-block-ui/jqueryblockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/bootstrap-select2/select2.min.js')}}" type="text/javascript"></script>

<!-- BEGIN JS DEPENDECENCIES-->
<!-- END CORE JS DEPENDECENCIES-->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{asset('public/webarch/js/webarch.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/js/chat.js')}}" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="{{asset('public/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/jquery-inputmask/jquery.inputmask.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/jquery-autonumeric/autoNumeric.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/ios-switch/ios7-switch.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/bootstrap-select2/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/boostrap-clockpicker/bootstrap-clockpicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>

<!-- END PAGE LEVEL JS INIT -->
<script src="{{asset('public/assets/js/form_elements.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/plugins/jquery-superbox/js/superbox.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script>
    $(function()
    {
        $('.superbox').SuperBox();
    });
</script>
</body>

</html>
