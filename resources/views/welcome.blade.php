<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>CKEditor 5 Integration Example</title>
</head>
<style>
    body{
        width: 200px;
        height: 100px;
        margin: auto;
    }

</style>
<body>
<div   id="editor"></div>

<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
<script src="{{asset('public/ckfinder/ckfinder.js')}}" type="text/javascript"></script>
<script type="text/javascript">

    ClassicEditor
        .create( document.querySelector( '#editor' ), {
            ckfinder: {
                uploadUrl:'{{asset('public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json')}}'
            },
            toolbar: [ 'ckfinder', 'imageUpload', 'undo', 'redo' ]
        } )
        .catch( function( error ) {
            console.error( error );
        } );


</script>
</body>
</html>
