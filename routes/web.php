<?php

Route::get('403',function(){
    return view('error.403');
});
Route::get('404',function(){
    return view('error.404');
});
Route::get('ck',function(){
    return view('welcome');
});
Route::get('BE','HomeController@login')->name('login')->middleware('checklogin');
Route::post('BE','HomeController@postlogin');

Route::get('resetpassword','HomeController@resetpassword')->name('user.resetpassword');

Route::post('resetpassword','HomeController@postresetpassword')->name('user.postresetpassword');

Route::get('password_new','HomeController@passwordnew')->name('user.passwordnew');

Route::post('password_new','HomeController@postpasswordnew')->name('user.postpasswordnew');


Route::get('otp','HomeController@sendotp')->name('user.sendotp');

Route::middleware(['auth'])->group(function() {

    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', 'HomeController@index')->name('admin');

        Route::group(['prefix' => 'permission'], function () {
            Route::get('/', 'PermissionController@show')->name('permission.list')->middleware('checkrole:per_list_permission');

            Route::get('create','PermissionController@store')->name('permission.create')->middleware('checkrole:per_create_permission');
            Route::post('create','PermissionController@poststore')->name('permission.postcreate')->middleware('checkrole:per_create_permission');

            Route::post('deleteall','PermissionController@destroyall')->name('permision.deleteall')->middleware('checkrole:per_delete_permission');

            Route::get('delete/{id}','PermissionController@destroy')->middleware('checkrole:per_delete_permission');

            Route::get('edit/{id}','PermissionController@edit')->middleware('checkrole:per_edit_permission');
            Route::post('edit/{id}','PermissionController@postedit')->middleware('checkrole:per_edit_permission');
        });

        Route::group(['prefix' => 'role'], function () {
            Route::get('/', 'RoleController@show')->name('role.list')->middleware('checkrole:per_list_role');

            Route::get('create','RoleController@store')->name('role.create')->middleware('checkrole:per_create_role');
            Route::post('create','RoleController@poststore')->name('role.postcreate')->middleware('checkrole:per_create_role');

            Route::get('delete/{id}','RoleController@destroy')->middleware('checkrole:per_delete_role');

            Route::get('edit/{id}','RoleController@edit')->middleware('checkrole:per_edit_role');
            Route::post('edit/{id}','RoleController@postedit')->middleware('checkrole:per_edit_role');

            Route::post('deleteall','UserController@destroyall')->name('user.deleteall')->middleware('checkrole:per_delete_role');
        });
        Route::get('listuser','UserController@show')->name('user.list')->middleware('checkrole:per_list_employess');

        Route::group(['prefix' =>'user'],function (){

            Route::get('create','UserController@create')->name('user.create')->middleware('checkrole:per_create_employess');
            Route::post('create','UserController@postcreate')->middleware('checkrole:per_create_employess');

            Route::get('delete/{id}','UserController@destroy')->middleware('checkrole:per_delete_employess');

            Route::get('edit/{id}','UserController@edit')->middleware('checkrole:per_edit_employess');
            Route::post('edit/{id}','UserController@postedit')->middleware('checkrole:per_edit_employess');

            Route::get('','UserController@search')->name('user.search');

            Route::get('changepassword','UserController@changepassword')->name('user.changepassword');

            Route::post('changepassword','UserController@postchangepassword')->name('user.postchangepassword');

            Route::get('updateadmin','UserController@updateadmin')->name('user.updateadmin');
            Route::post('updateadmin','UserController@postupdateadmin')->name('user.postupdateadmin');
        });

        Route::get('listcustomer','CustomerController@show')->name('customer.list')->middleware('checkrole:per_list_customer');

        Route::group(['prefix' =>'customer'],function (){

            Route::get('create','CustomerController@create')->name('customer.create')->middleware('checkrole:per_create_customer');
            Route::post('create','CustomerController@postcreate')->middleware('checkrole:per_create_customer');

            Route::get('delete/{id}','CustomerController@destroy')->middleware('checkrole:per_delete_customer');

            Route::get('edit/{id}','CustomerController@edit')->middleware('checkrole:per_edit_customer');
            Route::post('edit/{id}','CustomerController@postedit')->middleware('checkrole:per_edit_customer');

            Route::post('deleteall','CustomerController@destroyall')->name('customer.deleteall')->middleware('checkrole:per_delete_customer');

            Route::get('perview/{id}','CustomerController@perview');

            Route::get('','CustomerController@search')->name('customer.search');

            Route::post('listcustomer','CustomerController@status_notification')->name('customer.status_notification')->middleware('checkrole:per_edit_customer');
        });
        Route::group(['prefix' => 'config'], function () {
            Route::get('/', 'ConfigController@show')->name('config.list')->middleware('checkrole:per_list_config');

            Route::get('create','ConfigController@create')->name('config.create')->middleware('checkrole:per_create_config');
            Route::post('create','ConfigController@postcreate')->name('config.postcreate')->middleware('checkrole:per_create_config');

            Route::get('delete/{id}','ConfigController@destroy')->middleware('checkrole:per_delete_config');

            Route::get('edit/{id}','ConfigController@edit')->middleware('checkrole:per_edit_config');
            Route::post('edit/{id}','ConfigController@postedit')->middleware('checkrole:per_edit_config');
        });

        Route::group(['prefix' => 'config_notification_customer'], function () {
            Route::get('/', 'Config_Notification_CustomerController@show')->name('config_notification_customer.list')->middleware('checkrole:per_list_config_notification');

            Route::get('create','Config_Notification_CustomerController@create')->name('config_notification_customer.create')->middleware('checkrole:per_create_config_notification');
            Route::post('create','Config_Notification_CustomerController@postcreate')->middleware('checkrole:per_create_config_notification');

            Route::get('delete/{id}','Config_Notification_CustomerController@destroy')->middleware('checkrole:per_delete_config_notification');

            Route::get('edit/{id}','Config_Notification_CustomerController@edit')->middleware('checkrole:per_edit_config_notification');
            Route::post('edit/{id}','Config_Notification_CustomerController@postedit')->middleware('checkrole:per_edit_config_notification');

            Route::get('send/{id}','Config_Notification_CustomerController@send')->middleware('checkrole:per_list_config_notification_send');
        });

        Route::group(['prefix'=>'coure'],function(){
            Route::get('','CoureController@show')->name('coure.list')->middleware('checkrole:per_list_coure');

            Route::get('create','CoureController@create')->name('coure.create')->middleware('checkrole:per_create_coure');
            Route::post('create','CoureController@postcreate')->middleware('checkrole:per_create_coure');

            Route::get('edit/{id}','CoureController@edit')->middleware('checkrole:per_edit_coure');
            Route::post('edit/{id}','CoureController@postedit')->middleware('checkrole:per_edit_coure');

            Route::get('delete/{id}','CoureController@destroy')->middleware('checkrole:per_delete_coure');

        });

        Route::group(['prefix'=>'level'],function(){
            Route::get('','LevelController@show')->name('level.list')->middleware('checkrole:per_list_level');

            Route::get('create','LevelController@create')->name('level.create')->middleware('checkrole:per_create_level');
            Route::post('create','LevelController@postcreate')->middleware('checkrole:per_create_level');

            Route::get('edit/{id}','LevelController@edit')->middleware('checkrole:per_edit_level');
            Route::post('edit/{id}','LevelController@postedit')->middleware('checkrole:per_edit_level');

            Route::get('delete/{id}','LevelController@destroy')->middleware('checkrole:per_delete_level');


        });

        Route::group(['prefix'=>'lesson'],function(){
            Route::get('','LessonController@show')->name('lesson.list')->middleware('checkrole:per_list_lesson');

            Route::get('create','LessonController@create')->name('lesson.create')->middleware('checkrole:per_create_lesson');
            Route::post('create','LessonController@postcreate')->middleware('checkrole:per_create_lesson');

            Route::get('edit/{id}','LessonController@edit')->name('lesson.update')->middleware('checkrole:per_edit_lesson');
            Route::post('edit/{id}','LessonController@postedit')->middleware('checkrole:per_edit_lesson');

            Route::get('delete/{id}','LessonController@destroy')->name('lesson.delete')->middleware('checkrole:per_delete_lesson');

            Route::get('search','LessonController@search')->name('lesson.search')->middleware('checkrole:per_list_lesson');

            Route::group(['prefix'=>'read'],function(){
                Route::get('','ReadController@show')->name('read.list')->middleware('checkrole:per_list_read');

                Route::get('create','ReadController@create')->name('read.create')->middleware('checkrole:per_create_read');
                Route::post('create','ReadController@postcreate')->middleware('checkrole:per_create_read');

                Route::get('edit/{id}','ReadController@edit')->middleware('checkrole:per_edit_read');
                Route::post('edit/{id}','ReadController@postedit')->middleware('checkrole:per_edit_read');

                Route::get('delete/{id}','ReadController@destroy')->middleware('checkrole:per_delete_read');

                Route::get('search','ReadController@search')->name('read.search')->middleware('checkrole:per_list_read');
            });

            Route::group(['prefix'=>'listen'],function(){
                Route::get('','ListenController@show')->name('listen.list')->middleware('checkrole:per_list_listen');

                Route::get('create','ListenController@create')->name('listen.create')->middleware('checkrole:per_create_listen');
                Route::post('create','ListenController@postcreate')->middleware('checkrole:per_create_listen');

                Route::get('edit/{id}','ListenController@edit')->middleware('checkrole:per_edit_listen');
                Route::post('edit/{id}','ListenController@postedit')->middleware('checkrole:per_edit_listen');

                Route::get('delete/{id}','ListenController@destroy')->middleware('checkrole:per_delete_listen');

                Route::get('search','ListenController@search')->name('listen.search')->middleware('checkrole:per_list_listen');


            });

            Route::group(['prefix'=>'speak'],function(){
                Route::get('','SpeakController@show')->name('speak.list')->middleware('checkrole:per_list_speak');

                Route::get('create','SpeakController@create')->name('speak.create')->middleware('checkrole:per_create_speak');
                Route::post('create','SpeakController@postcreate')->middleware('checkrole:per_create_speak');

                Route::get('edit/{id}','SpeakController@edit')->middleware('checkrole:per_edit_speak');
                Route::post('edit/{id}','SpeakController@postedit')->middleware('checkrole:per_edit_speak');

                Route::get('delete/{id}','SpeakController@destroy')->middleware('checkrole:per_delete_speak');

                Route::get('search','SpeakController@search')->name('speak.search')->middleware('checkrole:per_list_speak');


            });

            Route::group(['prefix'=>'write'],function(){
                Route::get('','WriteController@show')->name('write.list')->middleware('checkrole:per_list_write');

                Route::get('create','WriteController@create')->name('write.create')->middleware('checkrole:per_create_write');
                Route::post('create','WriteController@postcreate')->middleware('checkrole:per_create_write');

                Route::get('edit/{id}','WriteController@edit')->middleware('checkrole:per_edit_write');
                Route::post('edit/{id}','WriteController@postedit')->middleware('checkrole:per_edit_write');

                Route::get('delete/{id}','WriteController@destroy')->middleware('checkrole:per_delete_write');

                Route::get('search','WriteController@search')->name('write.search')->middleware('checkrole:per_list_write');


            });

        });

        Route::group(['prefix'=>'history'],function(){
            Route::get('','HistoryController@index')->name('history_customer.list')->middleware('checkrole:per_list_history');

            Route::get('edit/{id}','HistoryController@show')->name('history.update')->middleware('checkrole:per_edit_history');
            Route::post('edit/{id}','HistoryController@markTest')->middleware('checkrole:per_edit_history');

            Route::get('delete/{id}','HistoryController@destroy')->name('history.delete')->middleware('checkrole:per_delete_history');

            Route::get('search','HistoryController@search')->name('history.search');

        });

        Route::group(['prefix'=>'dashbroad'],function(){
            Route::get('','DashbroadController@index')->name('dashbroad.list')->middleware('checkrole:per_list_dashbroad');

        });

        Route::group(['prefix'=>'vocabularie'],function(){
            Route::get('','VocabularieController@index')->name('vocabularie.list')->middleware('checkrole:per_list_vocabularie');

            Route::get('create','VocabularieController@create')->name('vocabularie.create')->middleware('checkrole:per_create_vocabularie');
            Route::post('create','VocabularieController@postcreate')->middleware('checkrole:per_create_vocabularie');

            Route::get('edit/{id}','VocabularieController@edit')->name('vocabularie.edit')->middleware('checkrole:per_edit_vocabularie');
            Route::post('edit/{id}','VocabularieController@postedit')->middleware('checkrole:per_edit_vocabularie');

            Route::get('delete/{id}','VocabularieController@destroy')->name('vocabularie.delete')->middleware('checkrole:per_delete_vocabularie');

            Route::get('search','VocabularieController@search')->name('vocabularie.search')->middleware('checkrole:per_list_vocabularie');
        });

        Route::group(['prefix'=>'document'],function(){
            Route::get('','DocumentController@index')->name('document.list')->middleware('checkrole:per_list_document');

            Route::get('create','DocumentController@create')->name('document.create')->middleware('checkrole:per_create_document');
            Route::post('create','DocumentController@postcreate')->middleware('checkrole:per_create_document');

            Route::get('edit/{id}','DocumentController@edit')->middleware('checkrole:per_edit_document');
            Route::post('edit/{id}','DocumentControllere@postedit')->middleware('checkrole:per_edit_document');

            Route::get('delete/{id}','DocumentController@destroy')->middleware('checkrole:per_delete_document');

        });

        Route::group(['prefix'=>'language'],function(){
            Route::get('','LanguageController@index')->name('language.list')->middleware('checkrole:per_list_language');

            Route::get('create','LanguageController@create')->name('language.create')->middleware('checkrole:per_create_language');
            Route::post('create','LanguageController@postcreate')->middleware('checkrole:per_create_language');

            Route::get('edit/{id}','LanguageController@edit')->name('language.update')->middleware('checkrole:per_edit_language');
            Route::post('edit/{id}','LanguageController@postedit')->middleware('checkrole:per_edit_language');

            Route::get('delete/{id}','LanguageController@destroy')->name('language.delete')->middleware('checkrole:per_delete_language');

            // change language
            Route::get('setting/{id}','LanguageController@setting')->name('language.setting');



        });
        // create language default
        Route::get('setting/create_lan','LanguageController@create_lan')->name('language.create_lan')->middleware('checkrole:per_create_default_language');

        Route::post('setting/lan_data','LanguageController@lan_data')->name('language.lan_data')->middleware('checkrole:per_create_default_language');


        //translat language
        Route::get('setting_lan/{id}','LanguageController@setting_lan')->name('language.setting_lan')->middleware('checkrole:per_create_translat_language');

        Route::post('setting_lan/{id}','LanguageController@postsetting_lan')->name('language.postsetting_lan')->middleware('checkrole:per_create_translat_language');

        Route::get('lan_search','LanguageController@lan_search')->name('language.search');

    });



});

Route::group(['namespace' => 'FE'], function() {

   Route::get('FE','HomeController@index');

    Route::get('FE_404',function(){
        return view('frontend.page.error.404');
    });

   Route::get('','HomeController@login')->name('FE_login')->middleware('checkloginclient');

   Route::post('','HomeController@postlogin')->name('FE_postlogin');

    Route::get('FE/register','HomeController@register')->name('FE_register');

    Route::post('FE/regsiter','HomeController@postregister')->name('FE_postregister');

    Route::get('FE/logout','HomeController@logout')->name('FE_logout');


    Route::group(['prefix'=>'FE'],function(){
        Route::get('','DisplayController@frontend')->name('FE.home');

        Route::get('achievenment','DisplayController@achievenment')->name('FE.achievenment');

        Route::get('achievenment_student','DisplayController@achievenment_student')->name('FE.achievenment_student');

        Route::get('contact','DisplayController@contact')->name('FE.contact');

        Route::get('detail','DisplayController@detail')->name('FE.detail');

        Route::get('english','DisplayController@english')->name('FE.english');

        Route::get('coure','DisplayController@new_home')->name('FE.new_home');

        Route::get('share','DisplayController@share')->name('FE.share');

        Route::get('target','DisplayController@target')->name('FE.target');

        Route::get('why','DisplayController@why')->name('FE.why');



    });


    Route::get('testlevel','CustomerController@level');

    Route::middleware(['guest:customers'])->group(function() {

        Route::post('notification','HomeController@notification')->name('FE_notification');

        Route::group(['prefix'=>'coure'],function(){
            Route::get('','HomeController@show')->name('FE_coure');

            Route::get('{id}/detail','LessonController@show')->name('FE_lesson');

            Route::get('detail_lessson/{coure_id}/{lesson_id}','HomeController@showdetail')->name('FE_lesson_detail_list');

            Route::post('detail','LessonController@detail')->name('FE_lesson_detail');
        });

        Route::group(['prefix'=>'customer'],function(){
            Route::get('','CustomerController@profile')->name('FE_profile');

            Route::post('','CustomerController@postprofile')->name('FE_postprofile');

            Route::get('password','CustomerController@password')->name('FE_password');

            Route::post('password','CustomerController@postpassword')->name('FE_postpassword');
        });

        Route::group(['prefix'=>'lesson'],function (){

            Route::post('mark','LessonController@mark')->name('FE_lesson_mark');
        });


        Route::group(['prefix'=>'vocabularie'],function(){
            Route::get('','HomeController@vocabularie')->name('FE_vocabularie');

            Route::post('','HomeController@post_vocabularie')->name('FE_post_vocabularie');

            Route::get('vocabularie?search={search}','HomeController@display_vocabularie')->name('FE_display_vocabularie');
        });

        Route::group(['prefix'=>'document'],function (){
            Route::get('','DocumentController@index')->name('FE_document.list');
        });

        Route::group(['prefix'=>'history'],function(){
            Route::get('','HistoryController@index')->name('FE_history');

            Route::get('detail/{id}','HistoryController@show')->name('FE_history_detail');
        });



    });


});


Route::get('logout','HomeController@logout')->name('logout');

