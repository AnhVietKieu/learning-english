<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    protected $table= 'customers';

    protected $hidden = ['password'];
    protected $fillable = [
        'id',
        'name',
        'avatar',
        'username',
        'password',
        'email',
        'address',
        'phone',
        'birthday',
        'level_id',
        'gender',
        'score',
        'status_notification',
        'status'
    ];

    protected $casts = [
        'birthday' => 'date',
        'create_at' => 'datetime',
        'update_at' => 'datetime'
    ];

    public function levels()
    {
        return $this->belongsTo(Levels::class,'level_id','id');
    }
    public function history()
    {
        return $this->hasMany(History::class,'customer_id','id');
    }

    static function login($arr)
    {

        $customer =Customer::where('email',$arr['email'])->first();

        if($customer)
        {
            if(Hash::make($arr['password'])==$customer->password )
            {
                return $customer->password;
            }else{
                dd(1);
            }
        }else{
          return false;
        }


    }

    static function register($arr)
    {
        Customer::create([
            'email'=>$arr['email'],
            'password'=>bcrypt($arr['password'])]
        );
    }

    static function search_not($request)
    {
        $sql = Customer::with('levels');

        switch ($request->filter){
            case 'phone':
                $user = $sql->where('phone',trim($request->search))->paginate(Config::value('pagination'));
                break;
            case 'email':
                $search=str_replace("%40",'@',$request->search);
                $user = $sql->where('email','like','%'.$search.'%')->paginate(Config::value('pagination'));
                break;
            case 'name':
                $user = $sql->where('name','like','%'.trim($request->search).'%')
                    ->paginate(Config::value('pagination'));
            default:
                $user = $sql->paginate(Config::value('pagination'));
                break;
        }

        return $user;


    }

}
