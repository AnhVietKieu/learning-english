<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vocabularies extends Model
{
    protected $fillable = [
        'id',
        'word',
        'type',
        'pronounce',
        'meaning',
        'image',
        'is_active',
        'description'
    ];
}
