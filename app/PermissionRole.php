<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PermissionRole extends Model
{
    use Notifiable;
    protected $hidden = [];
    protected $fillable = [
        'permission_id',
        'role_id'
    ];
    protected $casts = [
        'created_at' => 'date',
        'update_at' => 'date'
    ];

    static function permission_role ()
    {
        $permission = PermissionRole::select(
            'permissions.id',
            'permissions.name',
            'permissions.display',
            'permissions.parent_id')
            ->join('permissions','permissions.id','=','permission_roles.permission_id')
            ->where('role_id',session('role_id'))->get();

        return $permission;
    }
}
