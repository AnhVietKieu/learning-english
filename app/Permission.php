<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Permission extends Model
{
    use Notifiable;
    public static $html = '';
    protected $hidden = [];
    protected $fillable = [
        'id',
        'name',
        'display',
        'parent_id',
        '_token'
    ];
    protected $casts = [
        'created_at' => 'date',
        'update_at' => 'date'
    ];

    static function menuParent($data, $parent, $str)
    {
        foreach ($data as $val) {
            if ($val->parent_id == $parent) {
                static::$html .= "
                    <tr>
                       <td class='v-align-middle'>
                        <input type='checkbox' name='permission_id[]' value='$val->id' class='checkbox delete-user'>
                    </td>
                    
                     <td>" . $str . $val->display . "</td>
                    <td >" . $val->name . "</td>
                    <td class='center'>";
                if(menu_permission('per_edit_permission')==true){
                    static::$html.=" <a  href='permission/edit/" . $val->id . "'>
                    <button class='btn btn-success' type='button'><i class='glyphicon glyphicon-edit'></i></button>
                    </a>";
                }
                if(menu_permission('per_delete_permission')){
                    static::$html.=" <a  href='permission/delete/" . $val->id . "'>
                       <button class='btn btn-danger' onclick=\"return confirm('Bạn có chắc chắn muốn xóa??')\" type='button'><i class='glyphicon glyphicon-trash'></i></button>
                    </a>";
                }
                static::$html.="  
                     </td>
                    </tr>";
                Permission::menuParent($data, $val->id, '---- ');
            }
        }
        return static::$html;
    }

    static function valuepermission($per)
    {
        $result = str_replace('per_', '', $per);
        if (strstr($result, 'list')) {
            return substr($result, '0', '4');
        } else {
            return substr($result, '0', '6');
        }
    }
}
