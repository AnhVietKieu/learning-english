<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $hidden = [];
    protected $fillable = [
        'id',
        'title',
        'file',
        'created_by',
        'status',
        'updated_by'
    ];
    protected $casts = [
        'created_at' => 'date',
        'update_at' => 'date'
    ];
}
