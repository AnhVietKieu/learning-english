<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class History_User extends Model
{
    protected $table='history_user';
    protected $fillable = [
        'id',
        'customer_id',
        'lesson_id' ,
        'status',
        'created_at',
        'updated_at'
        ];

    static function history_action($lesson_id)
    {
        $history_user= History_User::where('customer_id',Auth::guard('customers')->user()->id)
            ->where('lesson_id',$lesson_id)->first();

        if($history_user)
        {
            $history_user->update(['lesson_id'=>$lesson_id]);
        }else{
            History_User::create(['customer_id'=>Auth::guard('customers')->user()->id,
                'lesson_id'=>$lesson_id]);
        }

        return true;
    }
}
