<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Levels extends Model
{
    protected $fillable = [
        'name',
        'image',
        'score',
        'status',
        'created_by',
        'updated_by'
    ];
}
