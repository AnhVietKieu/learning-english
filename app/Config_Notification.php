<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config_Notification extends Model
{
    protected $table ='config_notifications';
    protected $hidden = [];
    protected $fillable = [
        'title',
        'description',
        '_token',
        'time'
    ];
    protected $casts = [
        'created_at' => 'date',
        'update_at' => 'date'
    ];
}
