<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Config extends Model
{
    protected $table= 'user_config';

    protected $fillable = [
        'id',
        'meta_key',
        'meta_value',
        'user_id',
    ];
}
