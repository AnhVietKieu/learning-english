<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lessons extends Model
{

    protected $fillable = [
        'id',
        'title',
        'image',
        'description',
        'video',
        'url',
        'title_video',
        'time',
        'sort',
        'level_id',
        'coure_id',
        'score',
        'status',
        'created_by',
        'updated_by'
    ];

    public function reads()
    {
        return $this->hasMany(Reads::class,'lesson_id','id');
    }
    public function writes()
    {
        return $this->hasMany(Writes::class,'lesson_id','id');
    }
    public function listens()
    {
        return $this->hasMany(Listens::class,'lesson_id','id');
    }
    public function speaks()
    {
        return $this->hasMany(Speaks::class,'lesson_id','id');
    }
    public function coure()
    {
        return $this->belongsTo(Coures::class,'coure_id','id');
    }

    public function history()
    {
        return $this->hasMany(History::class,'lesson_id','id');
    }

    public function level()
    {
        return $this->belongsTo(Levels::class,'level_id','id');
    }


    static function count_lesson($lesson_id,$type)
    {
        switch ($type){
            case 'Read':
                $count= Reads::where('lesson_id',$lesson_id)->count();
                break;
            case 'Listen';
                $count= Listens::where('lesson_id',$lesson_id)->count();
                break;
            case 'Write':
                $count= Writes::where('lesson_id',$lesson_id)->count();
                break;
            case 'Speak';
                $count= Speaks::where('lesson_id',$lesson_id)->count();
                break;
            case 'Total':
                $count= Reads::where('lesson_id',$lesson_id)->count()+Listens::where('lesson_id',$lesson_id)->count()+Writes::where('lesson_id',$lesson_id)->count()+Speaks::where('lesson_id',$lesson_id)->count();
        }

        return $count;

    }

    static function get_url($url)
    {
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i', $url, $match)) {
           return $match[1];
        }
    }

    static function delete_lesson($id)
    {
        $check_read = Reads::where('lesson_id',$id)->get();

        if(!empty($check_read))
        {
            foreach ($check_read as $key)
            {
                Reads::find($key->id)->delete();
            }

            Lessons::find($id)->delete();
        }else{
            Lessons::find($id)->delete();
        }

        return true;
    }
    static function check_delete($id)
    {
        $check_read = Reads::where('lesson_id',$id)->get();
        $check_write = Writes::where('lesson_id',$id)->get();
        $check_listen = Listens::where('lesson_id',$id)->get();
        $check_speak = Speaks::where('lesson_id',$id)->get();

        if(!empty($check_read))
        {
            foreach ($check_read as $key){
                Reads::destroy($key->id);
            }
        }
        if(!empty($check_write))
        {
            foreach ($check_write as $key){
                Writes::destroy($key->id);
            }
        }
        if(!empty($check_speak))
        {
            foreach ($check_speak as $key){
                Speaks::destroy($key->id);
            }
        }
        if(!empty($check_listen))
        {
            foreach ($check_listen as $key){
                Listens::destroy($key->id);
            }
        }

        return true;
    }

    static function lesson_view($url,$time)
    {
        $html ='<ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Video</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Bài kiểm tra</a>
                                    <input type="hidden" name="time" id="input-name" value="'.$time.'">
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Mô tả</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent" >
                                <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div id="panel-9-11-0-0" class="so-panel widget widget_sow-editor panel-first-child" data-index="27" >';
        if(!empty($url)){
            $html.='<iframe width="680" height="400" src="https://www.youtube.com/embed/'.$url.'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        }

       $html.='</div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">';
        return $html;
    }

    static function markReadListen($history_id,$lesson_id,$type)
    {
        $history_test=  History_Test::where('history_id',$history_id);



        $read = Reads::where('lesson_id',$lesson_id)->get();

        $listen = Listens::where('lesson_id',$lesson_id)->get();



        switch ($type)
        {
            case 'read':
                $test =$history_test->where('type','read')->first();


                foreach($read as $item)
                {

                    $answer_customer= json_decode($test['answer_customer'],true);

                    if(isset( $answer_customer)&&count( $answer_customer)>=1)
                    {
                        foreach ($answer_customer as $key => $value)
                        {
                            if($key==$item['id'] && $value==$item['answer_true'])
                            {
                                $arr[$item['id']] = [
                                    'status' =>'true',
                                    'score' =>$item['score']
                                ];
                            }elseif($key==$item['id'] && $value!= $item['answer_true'])
                            {
                                $arr[$item['id']] = [
                                    'score' =>'null'
                                ];
                            }
                        }
                    }else{
                        $arr[$item['id']] = [
                            'score' =>'null'
                        ];
                    }


                }
                return $arr ;
                break;
            case 'listen';

                $test =$history_test->where('type','listen')->first();

                foreach($listen as $item)
                {
                    $answer_customer= json_decode($test['answer_customer'],true);

                    if(isset( $answer_customer)&&count( $answer_customer)>=1)
                    {
                        foreach ($answer_customer as $key => $value)
                        {
                            if($key==$item['id'] && $value==$item['answer_true'])
                            {
                                $arr[$item['id']] = [
                                    'status' =>'true',
                                    'score' =>$item['score']
                                ];
                            }elseif($key==$item['id'] && $value!= $item['answer_true'])
                            {
                                $arr[$item['id']] = [
                                    'score' =>'null'
                                ];
                            }
                        }
                    }else{
                        $arr[$item['id']] = [
                            'score' =>'null'
                        ];
                    }
                }

                return $arr;
                break;
        }




    }

}
