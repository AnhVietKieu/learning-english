<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\File\File;

class Listens extends Model
{
    protected $fillable = [
        'id',
        'name',
        'audio',
        'question',
        'answer_a',
        'answer_b',
        'answer_c',
        'answer_d',
        'answer_true',
        'lesson_id',
        'status',
        'note',
        'score',
        'created_by',
        'updated_by'
    ];
    public function lessons()
    {
        return $this->belongsTo(Lessons::class,'lesson_id','id');
    }

    static function  reponsitory($request,$type,$file_name)
    {
        if($type == 'create'){
            $arr=[
                'audio' => ($file_name!='')?$file_name:'null',
                'question' =>$request->question,
                'answer_a' =>$request->answer_a,
                'answer_b' =>$request->answer_b,
                'answer_c' =>$request->answer_c,
                'answer_d' =>$request->answer_d,
                'score' => $request->score,
                'answer_true' =>$request->answer_true,
                'note' =>$request->note,
                'lesson_id' =>$request->lesson_id,
                'created_by'=>Auth::user()->id
            ];
        }else{
            $arr=[
                'audio' =>$file_name,
                'question' =>$request->question,
                'answer_a' =>$request->answer_a,
                'answer_b' =>$request->answer_b,
                'answer_c' =>$request->answer_c,
                'answer_d' =>$request->answer_d,
                'answer_true' =>$request->answer_true,
                'note' =>$request->note,
                'score' => $request->score,
                'lesson_id' =>$request->lesson_id,
                'updated_by'=>Auth::user()->id
            ];
        }

        return $arr;


    }

    static function lesson_listen($listens,$i)
    {
        $html='';
        foreach ($listens as $item){

            $html.='<label class="form-label">Câu hỏi '.$i++.': '.$item->question.'</label>';
                                  if($item->audio != ''){
                                    $html.='<div><audio controls>
                                    <source src="'.asset('public/upload/audio/'.$item->audio).'" type="audio/mp4">
                                     </audio></div>';
                                  }
                                  $html.='<div class="grid-body no-border">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Đáp án A:</label>
                                                    <span class="help"><input type="radio" value="answer_a" name="answer_listen['.$item->id.']"></span>
                                                    <div class="controls">
                                                        <input type="text" value="'.$item->answer_a.'" disabled class="form-control" name="answer_a" id="phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Đáp án C:</label>
                                                    <span class="help"><input type="radio" value="answer_c" name="answer_listen['.$item->id.']"></span>
                                                    <div class="controls">
                                                        <input type="text" value="'.$item->answer_c.'" disabled class="form-control" name="answer_c" id="tin">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Đáp án B:</label>
                                                    <span class="help"><input type="radio" value="answer_b" name="answer_listen['.$item->id.']"></span>
                                                    <div class="controls">
                                                        <input type="text" value="'.$item->answer_b.'" disabled class="form-control auto" name="answer_b" data-a-sign="$ ">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Đáp án D:</label>
                                                    <span class="help"><input type="radio"  value="answer_d" name="answer_listen['.$item->id.']"></span>
                                                    <div class="controls">
                                                        <input type="text" value="'.$item->answer_d.'" disabled class="form-control auto" name="answer_d" data-v-max="9999" data-v-min="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>';

        }

        return $html;
    }
}
