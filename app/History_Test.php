<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class History_Test extends Model
{
    protected $table='history_test';

    protected $fillable = [
        'id',
        'answer_customer',
        'answer_user',
        'type',
        'history_id',
        'status',
        'created_at',
        'updated_at',
        ];

    protected $casts = [
        'created_at' => 'date',
        'update_at' => 'date'
    ];

    public function history()
    {
        return $this->belongsTo(History::class,'history_id','id');
    }

    static function history_test($arr,$type,$history_id)
    {
        switch ($type){
            case 'read':
               $array = [
                   'answer_customer'=>json_encode($arr),
                   'type' =>'read',
                   'history_id' =>$history_id,
                   'answer_user' =>'{}'
               ];

                $his_test =History_Test::create($array);
                if($his_test)
                {
                    return $his_test;
                }else{
                    return false;
                }
                break;
            case 'listen':
                $array = [
                    'answer_customer'=>json_encode($arr),
                    'type' =>'listen',
                    'history_id' =>$history_id,
                    'answer_user' =>'{}'
                ];

                $his_test =History_Test::create($array);
                if($his_test)
                {
                    return $his_test;
                }else{
                    return false;
                }
                break;
            case 'write':
                $array = [
                    'answer_customer'=>json_encode($arr),
                    'type' =>'write',
                    'history_id' =>$history_id,
                    'answer_user' =>'{}'
                ];

                $his_test =History_Test::create($array);
                if($his_test)
                {
                    return $his_test;
                }else{
                    return false;
                }
                break;

            case 'speak':

                $date = new Carbon();

                if(isset($arr)){
                    foreach ($arr as $item =>$file)
                    {
                        if($file->getClientOriginalExtension()=='mp3'
                            || $file->getClientOriginalExtension()=='mp4'
                            || $file->getClientOriginalExtension()=='m4a'){

                            $file_name = $item.'_'.Auth::guard('customers')->user()->id.'_'.str_replace(array(' ',':','-'),'_',$date->toDateTimeString()).'_'.$file->getClientOriginalName();

                            $file->move('public/upload/audio/customer', $file_name);
                        }

                        $arr[$item] =  $file_name;

                    }

                }

                $array = [
                    'answer_customer'=>json_encode($arr),
                    'type' =>'speak',
                    'history_id' =>$history_id,
                    'answer_user' =>'{}'
                ];

                $his_test =History_Test::create($array);
                if($his_test)
                {
                    return $his_test;
                }else{
                    return false;
                }

                break;
        }
    }

    static function history_test_update($arr,$type,$id)
    {
        $total = 0;
        $total_answer =0;
        switch ($type){
            case 'read':
                $array = [
                    'type' =>'read',
                    'answer_user' =>json_encode($arr),
                    'status' =>1
                ];



                foreach ($arr as $item)
                {

                    if(isset($item['status']))
                    {
                        $total_answer +=1;
                    }


                    $total+= $item['score']=='null'?0:$item['score'];

                }

                $result = [
                    'score_total' => $total,
                    'total_question' => count(isset($item)?$item:[]),
                    'total_answer' =>$total_answer
                ];

                $his_test =History_Test::where('history_id',$id)->where('type','read')->update($array);

                if($his_test)
                {
                    return $result;
                }else{
                    return false;
                }
                break;
            case 'listen':
                $array = [
                    'type' =>'listen',
                    'answer_user' =>json_encode($arr),
                    'status' =>1
                ];

                foreach ($arr as $item)
                {
                    if(isset($item['status']))
                    {
                        $total_answer +=1;
                    }
                    $total+= $item['score']=='null'?0:$item['score'];

                }

                $result = [
                    'score_total' => $total,
                    'total_question' => count(isset($item)?$item:[]),
                    'total_answer' =>$total_answer
                ];


                $his_test =History_Test::where('history_id',$id)->where('type','listen')->update($array);
                if($his_test)
                {
                    return $result;
                }else{
                    return false;
                }
                break;
            case 'write':
                $array = [
                    'type' =>'write',
                    'answer_user' =>json_encode($arr),
                    'status' =>1
                ];

                foreach ($arr as $item)
                {
                    if(isset($item['status']))
                    {
                        $total_answer +=1;
                    }
                    $total+= $item['score']=='null'?0:$item['score'];

                }

                $result = [
                    'score_total' => $total,
                    'total_question' => count(isset($item)?$item:[]),
                    'total_answer' =>$total_answer
                ];


                $his_test =History_Test::where('history_id',$id)->where('type','write')->update($array);
                if($his_test)
                {
                    return $result;
                }else{
                    return false;
                }
                break;

            case 'speak':

                $array = [
                    'type' =>'speak',
                    'status' =>1,
                    'answer_user' => json_encode($arr)
                ];

                foreach ($arr as $item)
                {
                    if(isset($item['status']))
                    {
                        $total_answer +=1;
                    }
                    $total+= $item['score']=='null'?0:$item['score'];
                }

                $result = [
                    'score_total' => $total,
                    'total_question' => count(isset($item)?$item:[]),
                    'total_answer' =>$total_answer
                ];

                $his_test =History_Test::where('history_id',$id)->where('type','speak')->update($array);
                if($his_test)
                {
                    return $result;
                }else{
                    return false;
                }

                break;
        }
    }

}
