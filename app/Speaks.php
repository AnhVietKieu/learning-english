<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Speaks extends Model
{
    protected $fillable = [
        'id',
        'name',
        'question',
        'answer_true',
        'lesson_id',
        'status',
        'note',
        'score',
        'created_by',
        'updated_by'
    ];
    public function lessons()
    {
        return $this->belongsTo(Lessons::class,'lesson_id','id');
    }

    static function reponsitory( $request,$type)
    {
        if($type =='create'){
            $arr =[
                'name' =>empty($request->name)??'null',
                'question' => $request->question,
                'answer_true' =>$request->answer_true,
                'lesson_id' =>$request->lesson_id,
                'score' => $request->score,
                'status' =>$request->status,
                'note' =>$request->note,
                'created_by' => Auth::User()->id
            ];
        }else{
            $arr =[
                'name' =>empty($request->name)??'null',
                'question' => $request->question,
                'answer_true' =>$request->answer_true,
                'lesson_id' =>$request->lesson_id,
                'score' => $request->score,
                'status' =>$request->status,
                'note' =>$request->note,
                'updated_by' => Auth::User()->id
            ];
        }

        return $arr;


    }

    static function lesson_speaks($speak,$i)
    {
        $html='';

        foreach ($speak as $item)
        {

            $html.='<label class="form-label">Câu hỏi '.$i++.': '.$item->name.'</label>';

            $html.='<div class="grid-body no-border">
                                        <div class="row">
                                            <div class="col-md-12">
                                                '.$item->question.'
                                            </div>
                                            <div class="col-md-12">
                                          
                                                 <input type="file" class="form-control file-avatar"name="answer_speak_file['.$item->id.']">
                                            </div>
                                        </div>
                                    </div>';

        }

        return $html;
    }
}
