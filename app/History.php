<?php

namespace App;

use App\Http\Controllers\FE\LessonController;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table ='history';
    protected $fillable = [
        'id',
        'customer_id',
        'lesson_id',
        'score',
        'answer_false',
        'answer_true',
        'score',
        'total_question',
        'user_id',
        'status',
        'status_notification',
        'note',
        'created_at',
        'updated_at'
    ];

    public function history_test()
    {
        return $this->hasMany(History_Test::class,'history_id','id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id');
    }

    public function lesson()
    {
        return $this->belongsTo(Lessons::class,'lesson_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    static function total_score($read,$write,$listen,$speak,$type)
    {
        switch ($type)
        {
            case 'score':
                return $score = $read+$write+$listen+$speak;
                break;
            case 'answer_true';
                return  $answer_true = $read+$write+$listen+$speak;
                break;
        }

    }
    static function search_not($request){
        $sql =  History::with('customer')->with('history_test')->with('user')->with(['lesson'=>function($query){
            $query->with('coure');
        }])->orderBy('id','desc');
        switch ($request->filter){
            case '2':
                $history = $sql->paginate(Config::value('pagination'));
                break;
            case '0':
                // chưa chấm
                $history =$sql->where('status','0')->paginate(Config::value('pagination'));
                break;
            case '1':
                // đã chấm
                $history = $sql->where('status','1')->paginate(Config::value('pagination'));
                break;
            default:
                $history = $sql->paginate(Config::value('pagination'));
                break;
        }

        return $history;
    }
    static function search_se($request)
    {

        $sql = History::join('customers','customers.id','=','history.customer_id')->with('user')->with(['lesson'=>function($query){
                $query->with('coure');
            }])->orderBy('history.id','desc')->where('customers.username','like','%'.$request->search.'%');

        switch ($request->filter){
            case '2':

                $history = $sql->paginate(Config::value('pagination'));
                break;
            case '0':
                dd($request->search);
                // chưa chấm
                $history =$sql->where('history.status','0')->paginate(Config::value('pagination'));
                break;
            case '1':
                // đã chấm
                $history = $sql->where('history.status','1')->paginate(Config::value('pagination'));

                break;
            default:
                $history = $sql->paginate(Config::value('pagination'));
                break;
        }

        return $history;
    }
}
