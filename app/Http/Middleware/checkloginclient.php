<?php

namespace App\Http\Middleware;

use Closure;

class checkloginclient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('username_cus'))
        {
            return redirect('coure');

        }else
        {
            return $next($request);
        }
    }
}
