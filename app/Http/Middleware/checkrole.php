<?php

namespace App\Http\Middleware;

use App\Permission;
use App\Role;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class checkrole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        $ListRoleOfUser =User::where('id',Auth::User()->id)->select('role_id')->first();

        $Role_id =Role::select('permission_roles.permission_id')
            ->join('permission_roles','roles.id','=','permission_roles.role_id')
            ->join('permissions','permissions.id','=','permission_roles.permission_id')
            ->where('roles.id',$ListRoleOfUser->role_id)->pluck('permission_id')->unique();

        $checkPermission = Permission::where('name',$role)->value('id');

        if($Role_id->contains($checkPermission))
        {
            return $next($request);

        }else{
            return  redirect('403');
        }
    }
}
