<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'price' => 'required|numeric',
            'description' =>'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Không được để chống tên sản phẩm',
            'price.required' => 'Không được để chống giá bán',
            'price.numeric' =>'Giá không phải là số',
            'description.required' => 'Không được để chống mô tả',
        ];
    }
}
