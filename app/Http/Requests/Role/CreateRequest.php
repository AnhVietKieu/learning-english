<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'number_role' => 'required',
            'display' =>'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Không được để chống tên quyền',
            'number_role.required' => 'Không được để chống mức quyền hạn',
            'display.required' => 'Không được để chống tên hiển thị',
        ];
    }
}
