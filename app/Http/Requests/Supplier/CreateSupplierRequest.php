<?php

namespace App\Http\Requests\Supplier;

use Illuminate\Foundation\Http\FormRequest;

class CreateSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' =>'required',
            'address' =>'required',
            'country' =>'required|max:200',
            'address' =>'required|max:200',
            'phone' =>'required|max:20|min:10'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Không được để chống tên đăng nhập',
            'email.email'=>'Định dạng không phải email',
            'country.required' => 'Không được để chống quốc gia',
            'country.max' => 'Quốc gia không lớn hơn 200 kí tự',
            'phone.required' => 'Điện thoại không được bỏ trống',
            'phone.max' =>'Điện thoại không đúng',
            'phone.number' =>'Điện thoại không phải số',
            'phone.min' =>'Điện thoại không đúng',
            'address.required' =>'Địa chỉ không được bỏ trống ',
            'address.max' =>'Địa chỉ quá lớn',
        ];
    }
}
