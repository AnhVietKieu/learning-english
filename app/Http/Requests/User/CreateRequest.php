<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:6',
            'name' =>'required',
            'address' =>'required',
            'gender' =>'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Không được để chống tên đăng nhập',
            'email.email'=>'Định dạng không phải email',
            'password.required' => 'Không được để chống mật khẩu',
            'password.min' => 'Mật khẩu không được ít hơn 6 ký tự',
            'name.required' => 'Tên hiển thị không được bỏ trống',
            'address.required' =>'Địa chỉ không được bỏ trống ',
            'gender.required' =>'Giới tính không được bỏ trống',
        ];
    }
}
