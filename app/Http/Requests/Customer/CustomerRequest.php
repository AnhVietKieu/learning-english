<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' =>'required',
            'address' =>'required',
            'gender' =>'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Không được để chống tên đăng nhập',
            'email.email'=>'Định dạng không phải email',
            'name.required' => 'Tên hiển thị không được bỏ trống',
            'address.required' =>'Địa chỉ không được bỏ trống ',
            'gender.required' =>'Giới tính không được bỏ trống',
        ];
    }
}
