<?php

namespace App\Http\Controllers;

use App\Coures;
use App\Customer;
use App\Document;
use App\Language;
use App\Lessons;
use App\Levels;
use App\Listens;
use App\Reads;
use App\Speaks;
use App\User;
use App\Vocabularies;
use App\Writes;
use Illuminate\Http\Request;

class DashbroadController extends Controller
{
   public function index(Request $request)
   {
    $user_agent = $request->header('User-Agent');
    $bname = 'Unknown';
    $platform = 'Unknown';

    //First get the platform?
    if (preg_match('/linux/i', $user_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $user_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $user_agent)) {
        $platform = 'windows';
    }


    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$user_agent) && !preg_match('/Opera/i',$user_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$user_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$user_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$user_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$user_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$user_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

        dd($bname,$platform);
        dd(1);

       $data['teacher'] = User::with('role')->where('role_id',1)->orderBy('id','desc')->paginate(10);

       $data['customer'] = Customer::with('levels')->orderBy('id','desc')
           ->paginate(5);

       $data['coure'] = Coures::count();
       $data['lesson'] = Lessons::count();
       $data['read'] = Reads::count();
       $data['listen'] = Listens::count();
       $data['speak'] = Speaks::count();
       $data['write'] = Writes::count();
       $data['document'] = Document::count();
       $data['vocu'] = Vocabularies::count();

       return view('page.dashbroad.list',$data);
   }
}
