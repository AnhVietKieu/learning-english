<?php

namespace App\Http\Controllers;

use App\Levels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LevelController extends Controller
{
   public function show()
   {
       $data['levels']=Levels::get();

       return view('page.level.list',$data);
   }
   public function create()
   {
       return view('page.level.create');
   }

   public function postcreate(Request $request)
   {
       if(isset($request->image)){
           $file = $request->image;
           if($file->getClientOriginalExtension()=='gif'
               || $file->getClientOriginalExtension()=='png'
               || $file->getClientOriginalExtension()=='jpg' || $file->getClientOriginalExtension()=='jpeg'){

               if($file->getSize() >'2271841'){
                   return redirect()->back()->with('error',lang_data('Image too large'));
               }
               if($file->move('public/upload/level/', $file->getClientOriginalName())==false)
               {
                   return redirect()->back()->with('error',lang_data('Download image error'));
               }
           }

           $file_name = $file->getClientOriginalName();

       }

       $arr = [
           'name' =>$request->name,
           'image' =>isset($file_name)?$file_name:'null',
           'score' =>$request->score
       ];

       if(Levels::create($arr))
       {
           return redirect()->back()->with('success',lang_data('Create new success'));
       }else{
           return redirect()->back()->with('error', lang_data('Create new error')
           );
       }
   }

    public function edit($id)
    {
        $data['level'] = Levels::where('id',$id)->first();
        if(empty($data['level'])){
            return redirect('404');
        }else{
            return view('page.level.update',$data);
        }
    }
    public function postedit($id,Request $request)
    {
        $check = Levels::where('levels.id',$id)->first();

        if(isset($request->image)){
            $file = $request->image;

            if($file->getClientOriginalExtension()=='gif'
                || $file->getClientOriginalExtension()=='png'
                || $file->getClientOriginalExtension()=='jpg'|| $file->getClientOriginalExtension()=='jpeg'){
                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error',lang_data('Image too large'));
                }
                    $file->move('public/upload/level/', $file->getClientOriginalName());
            }else{
                return redirect()->back()->with('error',lang_data('Image not true'));
            }
            $file_name = $file->getClientOriginalName();
        }


        $arr = [
            'name' =>$request->name,
            'image' =>isset($file_name)?$file_name:$check->image,
            'score' =>$request->score
        ];


        if(Levels::where('id',$id)->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else
        {
            return redirect()->back()->with('error',lang_data('Update new error'));
        }

    }

    public function destroy($id)
    {
        $level= Levels::where('id',$id)->first();

        if (empty($level)) {
            return view('error.404');
        }else{
            if (Levels::where('id', $id)->delete()) {
                return redirect()->back()->with('success',lang_data('Delete success'));
            } else {
                return redirect()->back()->with('error',lang_data('Delete error'));
            }
        }

    }
}
