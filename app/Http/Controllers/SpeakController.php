<?php

namespace App\Http\Controllers;

use App\Config;
use App\Lessons;
use App\Reads;
use App\Speaks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SpeakController extends Controller
{
    public function show()
    {
        $data['speaks'] = Speaks::with('lessons') ->orderBy('id','desc')->paginate(Config::value('pagination'));



        return view('page.speak.list',$data);
    }

    public function create()
    {
        $data['lessons'] = Lessons::all();

        return view('page.speak.create',$data);
    }

    public function postcreate(Request $request)
    {
        $arr = Speaks::reponsitory($request,'create');

        if(Speaks::create($arr))
        {
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Create new error'));
        }
    }

    public function edit($id)
    {
        $data['speak'] = Speaks::find($id);

        $data['lessons'] = Lessons::all();

        if(empty($data['speak'])){
            return redirect('404');
        }

        return view('page.speak.update',$data);


    }

    public function postedit($id,Request $request)
    {

        $check = Speaks::find($id);

        if(empty($check))
        {
            return redirect('404');
        }

        $arr = Speaks::reponsitory($request,'update');

        if($check->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Update new error'));
        }


    }

    public function destroy($id)
    {
        $speak= Speaks::where('id',$id)->first();

        if (empty($speak)) {
            return view('error.404');
        }else{
            if (Speaks::where('id', $id)->delete()) {
                return redirect()->back()->with('success',lang_data('Delete success'));
            } else {
                return redirect()->back()->with('error',lang_data('Delete error'));
            }
        }

    }

    public function search(Request $request){

        $data['speaks'] = Speaks::with('lessons')
            ->where('question','like','%'.$request->search.'%')
            ->orderBy('id','desc')->paginate(Config::value('pagination'));

        return view('page.speak.list',$data);

    }
}
