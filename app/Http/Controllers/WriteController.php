<?php

namespace App\Http\Controllers;

use App\Config;
use App\Lessons;
use App\Reads;
use App\Writes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WriteController extends Controller
{
    public function show()
    {

        $data['writes'] = Writes::with('lessons')->orderBy('id','desc')->paginate(Config::value('pagination'));

        return view('page.write.list',$data);
    }

    public function create()
    {
        $data['lessons'] = Lessons::all();

        return view('page.write.create',$data);
    }

    public function postcreate(Request $request)
    {
        $arr = Writes::reponsitory($request,'create');

        if(Writes::create($arr))
        {
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Create new error'));
        }
    }

    public function edit($id)
    {
        $data['write'] = Writes::find($id);

        $data['lessons'] = Lessons::all();

        if(empty($data['write'])){
            return redirect('404');
        }

        return view('page.write.update',$data);


    }

    public function postedit($id,Request $request)
    {

        $check = Writes::find($id);

        if(empty($check))
        {
            return redirect('404');
        }

        $arr = Writes::reponsitory($request,'update');


        if($check->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Update new error'));
        }


    }

    public function destroy($id)
    {
        $write= Writes::where('id',$id)->first();

        if (empty($write)) {
            return view('error.404');
        }else{
            if (Writes::where('id', $id)->delete()) {
                return redirect()->back()->with('success',lang_data('Delete success'));
            } else {
                return redirect()->back()->with('error',lang_data('Delete error'));
            }
        }

    }

    public function search(Request $request){

        $data['writes'] = Writes::with('lessons')
            ->where('question','like','%'.$request->search.'%')
            ->orderBy('id','desc')->paginate(Config::value('pagination'));

        return view('page.write.list',$data);

    }
}
