<?php

namespace App\Http\Controllers;

use App\Config;
use App\Lessons;
use App\Listens;
use App\Reads;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListenController extends Controller
{
    public function show()
    {
        $data['listens'] = Listens::with('lessons')->orderBy('id','desc')->paginate(Config::value('pagination'));

        return view('page.listen.list',$data);
    }

    public function create()
    {
        $data['lessons'] = Lessons::all();

        return view('page.listen.create',$data);
    }

    public function postcreate(Request $request)
    {

        if(isset($request->audio)){
            $file = $request->audio;

            if($file->getClientOriginalExtension()=='mp3'
                || $file->getClientOriginalExtension()=='mp4'
                || $file->getClientOriginalExtension()=='m4a'){

                if($file->move('public/upload/audio', $file->getClientOriginalName())==false)
                {
                    return redirect()->back()->with('error',lang_data('Download file error'));
                }
            }else{
                return redirect()->back()->with('error',lan_id('File not true'));
            }

            $file_name = $file->getClientOriginalName();

        }

        $arr = Listens::reponsitory($request,'create',$file_name);


        if(Listens::create($arr))
        {
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Create new error'));
        }
    }

    public function edit($id)
    {
        $data['listen'] = Listens::find($id);

        $data['lessons'] = Lessons::all();

        if(empty($data['listen'])){
            return redirect('404');
        }

        return view('page.listen.update',$data);


    }

    public function postedit($id,Request $request)
    {
        $check = Listens::find($id);

        if(empty($check))
        {
            return redirect('404');
        }

        if(isset($request->audio)){
            $file = $request->audio;

            if($file->getClientOriginalExtension()=='mp3'
                || $file->getClientOriginalExtension()=='mp4'
                || $file->getClientOriginalExtension()=='m4a'){

                if($file->move('public/upload/audio', $file->getClientOriginalName())==false)
                {
                    return redirect()->back()->with('error',lang_data('Download file error'));
                }
            }else{
                return redirect()->back()->with('error',lan_id('File not true'));
            }

            $file_name = $file->getClientOriginalName();

        }

        $arr = Listens::reponsitory($request,'update',isset($file_name)?$file_name:$check->audio);


        if($check->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Update new error'));
        }


    }

    public function destroy($id)
    {
        $listen= Listens::where('id',$id)->first();

        if (empty($listen)) {
            return view('error.404');
        }else{
            if (Listens::where('id', $id)->delete()) {
                return redirect()->back()->with('success',lang_data('Delete success'));
            } else {
                return redirect()->back()->with('error',lang_data('Delete error'));
            }
        }

    }

    public function search(Request $request){

        $data['listens'] = Listens::with('lessons')
            ->where('question','like','%'.$request->search.'%')
            ->orderBy('id','desc')->paginate(Config::value('pagination'));

        return view('page.listen.list',$data);

    }
}
