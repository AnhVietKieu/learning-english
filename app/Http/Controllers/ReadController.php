<?php

namespace App\Http\Controllers;

use App\Config;
use App\Lessons;
use App\Reads;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReadController extends Controller
{
    public function show()
    {

        $data['reads'] = Reads::with('lessons')
            ->orderBy('id','desc')->paginate(Config::value('pagination'));


        return view('page.read.list',$data);
    }

    public function create()
    {
        $data['lessons'] = Lessons::all();

        return view('page.read.create',$data);
    }

    public function postcreate(Request $request)
    {
        $arr=[
            'question' =>$request->question,
            'answer_a' =>$request->answer_a,
            'answer_b' =>$request->answer_b,
            'answer_c' =>$request->answer_c,
            'answer_d' =>$request->answer_d,
            'score' => $request->score,
            'answer_true' =>$request->answer_true,
            'note' =>$request->note,
            'lesson_id' =>$request->lesson_id,
            'created_by'=>Auth::user()->id
        ];

        if(Reads::create($arr))
        {
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Create new error'));
        }
    }

    public function edit($id)
    {
        $data['read'] = Reads::find($id);


        $data['lessons'] = Lessons::all();

        if(empty($data['read'])){
            return redirect('404');
        }

        return view('page.read.update',$data);


    }

    public function postedit($id,Request $request)
    {
        $check = Reads::find($id);

        if(empty($check))
        {
            return redirect('404');
        }

        $arr=[
            'question' =>$request->question,
            'answer_a' =>$request->answer_a,
            'answer_b' =>$request->answer_b,
            'answer_c' =>$request->answer_c,
            'answer_d' =>$request->answer_d,
            'score' => $request->score,
            'answer_true' =>$request->answer_true,
            'note' =>$request->note,
            'lesson_id' =>$request->lesson_id,
            'updated_by'=>Auth::user()->id
        ];

        if($check->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Update new error'));
        }


    }

    public function destroy($id)
    {
        $read= Reads::where('id',$id)->first();

        if (empty($read)) {
            return view('error.404');
        }else{
            if (Reads::where('id', $id)->delete()) {
                return redirect()->back()->with('success',lang_data('Delete success'));
            } else {
                return redirect()->back()->with('error',lang_data('Delete error'));
            }
        }

    }

    public function search(Request $request){

        $data['reads'] = Reads::with('lessons')
            ->where('question','like','%'.$request->search.'%')
            ->orderBy('id','desc')->paginate(Config::value('pagination'));

        return view('page.read.list',$data);

    }
}
