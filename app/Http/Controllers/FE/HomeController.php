<?php

namespace App\Http\Controllers\FE;

use App\Coures;
use App\Customer;
use App\History;
use App\History_User;
use App\Lessons;
use App\Levels;
use Session;
use App\Vocabularies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $customer = new Customer();
    }

    public function index(){
        dd(1);
        return view('frontend.layout.home.home');
    }

    public function show()
    {
        $data['coure'] =Coures::learn_menu(Coures::with('lessons')->get(),0,'');

        return view('frontend.page.cource.list',$data);

    }

    public function vocabularie(Request $request)
    {

        if($request->search)
        {
           $data['vocubal'] = Vocabularies::where('word',$request->search)->first();
        }else{
            $data['vocubal'] = [];
        }
        return view('frontend.page.vocabularie.list',$data);

    }
    public function post_vocabularie(Request $request)
    {
        $str ='';
        $vo = Vocabularies::where('word','like',$request->search.'%')->paginate(5);
        foreach ($vo as $key)
        {
            $str.='
            <div class="search-item">
                <a class="search" data-search="'.$key->word.'" href="vocabularie?search='.$key->word.'">'.$key->word.'</a>
                <a href="#" class="search-close"><i class="fas fa-times"></i></a>
            </div>
            
            ';
        }



       return $str;
    }

    public function login()
    {
       return view('frontend.login');
    }

    public function postlogin(Request $request)
    {
        $arr = array(
            'email' => $request->email,
            'password' => $request->password
        );

        if(Auth::guard('customers')->attempt($arr)){

            session::put('username_cus', Auth::guard('customers')->user()->username);

            $level =Levels::where('id',Auth::guard('customers')->user()->level_id)->first();

            session::put('level_number',$level['score']);

            return redirect('coure');
        }else{
            return redirect('FE/login')->with('error',lang_data('Login error'));
        }
    }

    public function register()
    {
        return view('frontend.register');
    }

    public function postregister(Request $request)
    {
        $check =Customer::where('email',$request->email)->first();

        if($check)
        {
            return redirect()->back()->with('error',lang_data('Email is duplicate'));
        }else{
            $arr = [
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
                'username'=>$request->frist_name.'_'.$request->last_name
            ];
            if(Customer::create($arr))
            {
                return redirect()->route('FE_login');
            }else{
                return redirect()->back()->with('error',lang_data('Register error'));
            }
        }

    }
     public function logout()
     {
         if(Auth::guard('customers')->logout()==false)
         {
             session()->forget('username_cus');

             return redirect('/');
         }

     }



     public function showdetail($coure_id,$lesson_id)
     {
         $check_coure = Coures::find($coure_id);

         $check_lesson = Lessons::with('level')->find($lesson_id);

         if(isset($check_coure)&&isset($check_lesson))
         {
             if(session('level_number')>$check_lesson['level']['score'])
             {
                 $data['i'] =1;
                 $data['test'] = Lessons::with('reads')
                     ->with('listens')
                     ->with('writes')
                     ->with('speaks')
                     ->find($lesson_id);
                 $data['lesson'] = $check_lesson;

                 History_User::history_action($lesson_id);

                 $data['title'] = $check_coure;

                 $data['lessons'] = Lessons::where('coure_id',$coure_id)->get();

                 $data['count'] = count(isset($data['test']['reads'])?$data['test']['reads']:[])+
                     count(isset($data['test']['speaks'])?$data['test']['speaks']:[])+
                     count(isset($data['test']['writes'])?$data['test']['writes']:[])+
                     count(isset($data['test']['listens'])?$data['test']['listens']:[]);

                 return view('frontend.page.lesson.detail',$data);
             }else{
                 return  redirect()->route('FE_coure')->with('error',lang_data('Your level is not enough'));
             }

         }else{
             return redirect()->route('FE_coure')->with('error',lang_data('There is no lesson'));
         }

     }


     public function notification()
     {
         $html ='';
        $notification = History::with('lesson')
             ->where('customer_id',Auth::guard('customers')->user()->id)
             ->where('status',1)->where('status_notification',0)->get();

         foreach($notification as $item)
         {
             $html.='<a href="'.route('FE_history_detail',$item->id).'" class="dropdown-item dropdown-item-unread">
                                <div class="dropdown-item-icon bg-primary text-white">
                                    <i class="fas fa-code"></i>
                                </div>
                                <div class="dropdown-item-desc">
                                   '.$item['lesson']['title'].'<span>đã được chấm !</span>
                                    <div class="time text-primary">2 Min Ago</div>
                                </div>
                            </a>';
         }

         return $html;


     }

}


