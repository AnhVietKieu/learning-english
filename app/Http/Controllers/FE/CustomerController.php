<?php

namespace App\Http\Controllers\FE;

use App\Customer;
use App\History;
use App\Lessons;
use App\Levels;
use Carbon\Carbon;
use Cassandra\Custom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    public function profile()
    {
        $data['customer'] = Customer::with('levels')->find(Auth::guard('customers')->user()->id);

        return view('frontend.page.customer.profile',$data);
    }

    public function postprofile(Request $request)
    {
        $date = new Carbon();


        if(isset($request->avatar)){
            $file = $request->avatar;
            if($file->getClientOriginalExtension()=='gif'
                || $file->getClientOriginalExtension()=='png'
                || $file->getClientOriginalExtension()=='jpg' || $file->getClientOriginalExtension()=='jpeg'){
                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error',lang_data('Image too large'));
                }

                $file_name =Auth::guard('customers')->user()->id.'_'.str_replace(array(' ',':','-'),'_',$date->toDateTimeString()).'_'.$file->getClientOriginalName();

                $file->move('public/upload/customer/', $file_name);
            }else{
                return redirect()->back()->with('error',lang_data('Image not true'));
            }
        }
        $arr = array(
            'username' =>$request->username,
            'email' =>$request->email,
            'gender' =>$request->gender,
            'address' =>$request->address,
            'phone' =>isset($request->phone)?$request->phone:'',
            'birthday' =>strtotime($request->birthday),
            'avatar' =>isset($file_name)?$file_name:Auth::guard('customers')->user()->avatar,
            'status_notification' =>isset($request->status_notification)?$request->status_notification:1
        );

        if(Customer::find(Auth::guard('customers')->user()->id)->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Change profile success'));
        }else{
            return redirect()->back()->with('error',lang_data('Change profile error'));

        }
    }

    public function password()
    {
        return view('frontend.page.customer.password');
    }
    public function postpassword(Request $request)
    {

        $arr=[
            'email' =>Auth::guard('customers')->user()->email,
            'password' => $request->password
        ];
        if(Auth::guard('customers')->attempt($arr))
        {
            Customer::find(Auth::guard('customers')->user()->id)
                ->update(['password'=>bcrypt($request->password)]);

            return lang_data('Change password success');

        }else{
            return lang_data('Incorrect password');
        }

    }

    public function level()
    {

    }
}
