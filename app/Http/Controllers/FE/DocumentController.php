<?php

namespace App\Http\Controllers\FE;

use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function index()
    {
        $data['documents'] = Document::all();
        $data['i'] =1;

        return view('frontend.page.document.list',$data);
    }
}
