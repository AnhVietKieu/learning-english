<?php

namespace App\Http\Controllers\FE;

use App\Coures;
use App\History;
use App\History_Test;
use App\History_User;
use App\Lessons;
use App\Listens;
use App\Reads;
use App\Speaks;
use App\Writes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LessonController extends Controller
{
    public function show($id)
    {
        $check = Lessons::where('coure_id',$id)->get();

        if(count($check)>=1)
        {

            $check_coure = Coures::with('level')->find($id);

            if(session('level_number')>$check_coure['level']['score'])
            {
                $data['lessons']=$check;
                $data['title'] = Coures::where('id',$id)->first();
                $data['lesson'] = Lessons::with('level')->where('coure_id',$id)->first();

                if(session('level_number')>$data['lesson']['level']['score'])
                {

                    History_User::history_action($data['lesson']['id']);

                    $data['i'] =1;
                    $data['test'] = Lessons::with('reads')->with('listens')->with('writes')->with('speaks')->where('coure_id',$id)->first();


                    $data['count'] = count(isset($data['test']['reads'])?$data['test']['reads']:[])+
                        count(isset($data['test']['speaks'])?$data['test']['speaks']:[])+
                        count(isset($data['test']['writes'])?$data['test']['writes']:[])+
                        count(isset($data['test']['listens'])?$data['test']['listens']:[]);
                    return view('frontend.page.lesson.list',$data);

                }else{
                    return redirect()->back()->with('error',lang_data('Your level is not enough'));
                }


            }else{
                return redirect()->back()->with('error',lang_data('Your level is not enough'));
            }



        }else{
            return redirect()->route('FE_coure')->with('error',lang_data('There is no lesson'));
        }
    }

    public function detail(Request $request)
    {
        $lesson = Lessons::find($request->lesson_id);

        $test = Lessons::with('reads')->with('listens')->with('writes')->with('speaks')->find($request->lesson_id);

        $html = Lessons::lesson_view($lesson->url,$lesson->time);

        $count_read = count($test->reads);

        $count_listen = $count_read+ count($test->listens);

        $count_write = $count_listen + count($test->writes);

        $count = $count_write + count($test->speaks);

        $html.='
         <div class="col-12 mb-4 display-submit">
                                        <div class="hero align-items-center text-black">
                                            <div class="hero-inner text-center">
                                                <h4>'.lang_data('Recommended test').'</h4>
                                                    <i>
                                                        ('.lang_data('The completed work can be reviewed in the history section').'<br>
                                                        '.lang_data('The system does not automatically mark arbitrary sentences').')
                                                    </i>
                                            </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        Số câu hỏi:<b>'.$count.' câu</b>
                                                    </div>
                                                    <div class="col-md-6">
                                                        Thời gian làm bài: <b>'.$lesson->time.' phút</b>
                                                    </div>
                                                </div>
                                                <div class="mt-4">
                                                    <button href="#" data-time="'.$lesson->time.'" class="btn btn-success testing" onclick="myStopFunction()"><i class="fas fa-sign-in-alt"></i> Làm bài</button>
                                                </div>

                                        </div>
                                    </div><div class="display-testing"><form method="post" id="my_form"  enctype="multipart/form-data">
            <input type="hidden" name="lesson_id" value="'.$request->lesson_id.'">';
        $html.=csrf_field() ;
           if(isset($test->reads)){
              $html.=Reads::lesson_read($test->reads);
           }
           if(isset($test->listens))
           {
               $html.=Listens::lesson_listen($test->listens,$count_read+1);
           }
           if(isset($test->writes))
           {
               $html.=Writes::lesson_write($test->writes,$count_listen+1);
           }
           if(isset($test->speaks))
           {
               $html.=Speaks::lesson_speaks($test->speaks,$count_write+1);
           }

        $html.='<div class="hero align-items-center text-black" style=" background:lawngreen;margin-top: 15px;padding: 0px;">
                                                    <div class="row">

                                                      <div class="demo'.$lesson->id.'" style="margin-right: 10px;"></div>

                                                     
                                                        <button type="button" class="btn btn-success tst">Kết thúc</button>
                                                    </div>
                                                </div>
                                          </form>
                                       
                                </div>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    '.$lesson->description.'
                                </div>
                            </div>';

       return $html;
    }
    public function mark(Request $request)
    {



//         history lesson_id, customer_id

        $arr =[
            'customer_id'=> Auth::guard('customers')->user()->id,
            'lesson_id' =>$request->lesson_id
        ];

        $lesson = Lessons::with('reads')
            ->with('writes')->with('speaks')
            ->with('listens')->find($request->lesson_id);

        $history =  History::create($arr);
//         history question type: read-listen-write-speak



        if($history)
        {
            if(isset($request->answer_read)&&count($request->answer_read)>=1)
            {
                History_Test::history_test($request->answer_read,'read',$history->id);
            }else{
                $array = [
                    'answer_customer'=>'{}',
                    'type' =>'read',
                    'history_id' =>$history->id,
                    'answer_user' =>'{}'
                ];

                $his_test =History_Test::create($array);
            }
            if(isset($request->answer_listen)&&count($request->answer_listen)>=1)
            {
                History_Test::history_test($request->answer_listen,'listen',$history->id);

            }else{
                $array = [
                    'answer_customer'=>'{}',
                    'type' =>'listen',
                    'history_id' =>$history->id,
                    'answer_user' =>'{}'
                ];

                $his_test =History_Test::create($array);
            }
            if(isset($request->answer_write)&&count($request->answer_write)>=1)
            {
                History_Test::history_test($request->answer_write,'write',$history->id);

            }else{
                $array = [
                    'answer_customer'=>'{}',
                    'type' =>'write',
                    'history_id' =>$history->id,
                    'answer_user' =>'{}'
                ];

                $his_test =History_Test::create($array);
            }
            if(isset($request->answer_speak_file)&&count($request->answer_speak_file)>=1)
            {
                History_Test::history_test($request->answer_speak_file,'speak',$history->id);

            }else{
                $array = [
                    'answer_customer'=>'{}',
                    'type' =>'speak',
                    'history_id' =>$history->id,
                    'answer_user' =>'{}'
                ];

                $his_test =History_Test::create($array);
            }

            // array[id]['status'=>'true','score' =>'']

            if(isset($lesson))
            {


                if((isset($lesson['writes'])&& count($lesson['writes'])>=1)||(isset($lesson['speaks'])&& count($lesson['speaks'])>=1))
                {
                }else{

                    if(isset($lesson['reads'])&& count($lesson['reads'])>=1)
                    {
                        $read = Lessons::markReadListen($history->id,$request->lesson_id,'read');

                        $result_read=  History_Test::history_test_update($read,'read',$history->id);

                    }

                    if(isset($lesson['listens'])&& count($lesson['listens'])>=1)
                    {
                        $listen = Lessons::markReadListen($history->id,$request->lesson_id,'listen');

                        $result_listen=  History_Test::history_test_update($listen,'listen',$history->id);
                    }

                    $score = History::total_score(isset($result_read['score_total'])?$result_read['score_total']:0,0 ,
                        isset($result_listen['score_total'])?$result_listen['score_total']:0,
                        0,'score');

                    $answer_true =History::total_score(isset($result_read['total_answer'])?$result_read['total_answer']:0,
                        0,
                        isset($result_listen['total_answer'])?$result_listen['total_answer']:0,
                        0,'score');

                    $total_question = count(isset($lesson['reads'])?$lesson['reads']:[])+
                        count(isset($lesson['listens'])?$lesson['listens']:[]);


                    History::find($history->id)->update([
                        'score'=>$score,
                        'answer_true'=>$answer_true,
                        'answer_false' =>$total_question - $answer_true,
                        'total_question' =>$total_question,
                        'status'=>1,
                        ]);



                }
            }


        }

        $html = '<div class="hero align-items-center text-black" style="background: palegreen;">
                                            <div class="hero-inner text-center">
                                                <h4>'.lang_data('Notification').'</h4>
                                                    <i>
                                                        ('.lang_data('The completed work can be reviewed in the history section').'<br>
                                                        '.lang_data('The system does not automatically mark arbitrary sentences').')
                                                    </i>
                                            </div>
                                        
                                        </div>';
        return $html;
    }
}
