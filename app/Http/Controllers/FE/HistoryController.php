<?php

namespace App\Http\Controllers\FE;

use App\Config;
use App\History;
use App\History_Test;
use App\Lessons;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    public function index()
    {

        $data['history']= History::where('customer_id', Auth::guard('customers')->user()->id)
            ->with('customer')
            ->with('history_test')
            ->with(['lesson' => function ($query) {
               $query->with('coure')->with('level');
            }])
            ->with('user')->orderBy('id','desc')->paginate(4);


        return view('frontend.page.history.list',$data);
    }

    public function show($id)
    {
        $check = History_Test::where('history_id',$id)->first();

        if($check)
        {
            $chek_id = History::find($id);
            $data['i'] = 1;
            $data['test'] = Lessons::with('reads')->with('listens')->with('writes')->with('speaks')->with('coure')->with(['history'=>function($query) use ($id){
                $query->where('id',$id)->with('history_test');
            }])->find($chek_id->lesson_id);


            foreach ($data['test']['history'] as $item)
            {
                foreach ($item['history_test'] as $item1 => $value)
                {
                    $value->answer_customer = json_decode($value->answer_customer,true);
                    $value->answer_user = json_decode($value->answer_user,true);
                }
            }

            $chek_id->update(['status_notification'=>1]);

//            dd( $data['test']->toArray());

        }else{
            return redirect('FE_404');
        }
        return view('frontend.page.history.detail',$data);
    }

}
