<?php

namespace App\Http\Controllers\FE;

use App\Config;
use App\Customer;
use App\Coures;
use App\Document;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DisplayController extends Controller
{
    public function frontend()
    {
        $data['title'] = 'Home';

        $data['coure'] = Coures::where('parent_id','!=',0)->orderBy('id','desc')->take(8)->get();

        $data['customer'] = Customer::with('levels')->take(5)->get();

        return view('frontend.home.home',$data);
    }
    public function why()
    {
        return view('frontend.home.why');
    }
    public function english()
    {
        return view('frontend.home.english');
    }
    public function target()
    {
        return view('frontend.home.target');
    }
    public function share()
    {
        $data['title'] = 'Tài liệu tham khảo';
        $data['document'] = Document::all();

        $data['i'] =1;
        return view('frontend.home.share',$data);
    }
    public function new_home()
    {
        $data['title'] = 'Khoá học';

        $data['coure'] = Coures::learn_menu(Coures::all(),0,'');

        return view('frontend.home.new',$data);
    }
    public function detail()
    {
        return view('frontend.home.detail');
    }
    public function contact()
    {
        $data['title'] = 'Liên lạc';

        return view('frontend.home.contact');
    }

    public function achievenment()
    {
        // tearcher
        $data['teacher'] = User::with(['role'=>function($query){
            $query->where('name','role_teacher');
        }])->get();

//        dd($data['teacher']->toArray());
        return view('frontend.home.achievenment',$data);
    }

    public function achievenment_student()
    {
        $data['student'] = Customer::with('levels')->orderby('score','desc')->paginate(8);

//        dd($data['student']->toArray());

        return view('frontend.home.achievement_student',$data);
    }

}
