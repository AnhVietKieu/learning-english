<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\CreateRequest;
use App\Permission;
use App\PermissionRole;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function show()
    {
        $data['roles']=Role::all();
        return view('page.role.list',$data);
    }
    public function store()
    {

       $permissions=  PermissionRole::permission_role();
       $data['permissions'] = Permission::where('parent_id',0)->get();

         $data['permission']=Role::Parent(Permission::all(),0,'');


        return view('page.role.create',$data);
    }
    public function poststore(CreateRequest $request)
    {
        $arr = array(
            'name' => $request->name,
            'display' => $request->display,
            'number_role' => $request->number_role
        );

        if ($request->number_role >= session('number_role')) {
            return redirect()->back()->with('error', lang_data('Authorization limit exceeded'));
        }
        if(empty($request->permission_id)){

            $result = Role::insert($arr);

            return redirect()->back()->with('success', 'Thêm thành công');
        }else{
            if(Role::checkpermissions($request->permission_id) === "false"){
                return redirect()->back()->with('error',lang_data('Your rights are not enough'));
            }

                $result = Role::insert($arr);
                $result1 = Role::where('name', $request->name)->first();
                    foreach ($request->permission_id as $key) {

                        PermissionRole::insert(['permission_id' => $key, 'role_id' => $result1->id]);

                    };
                return redirect()->back()->with('success', lang_data('Create new succcess'));
        }
    }

    public function destroy($id)
    {
        $Role= Role::where('id',$id)->first();
        if (empty($Role)) {
            return view('error.404');
        }
        if (session('number_role') >= $Role->number_role) {
            User::where('role_id', $id)->delete();

            // kiểm tra xem id này có trong users không;

            PermissionRole::where('role_id', $id)->delete();

            if (Role::where('id', $id)->delete()) {
                return redirect()->back()->with('success',lang_data('Delete success'));
            } else {
                return redirect()->back()->with('error',lang_data('Delete error'));
            }
        } else {
            return view('error.403');
        }

    }

    public function edit($id)
    {
        $Role =Role::where('id',$id)->first();

        if (empty($Role)) {
            return view('error.404');
        }


        $data['role'] = $Role;

        $data['permissions'] = Permission::where('parent_id',0)->get();

        $data['permission_role'] = PermissionRole::where('role_id',$id)->pluck('permission_id');

        $data['permission']=Role::Old_Parent(PermissionRole::where('role_id',$id)->pluck('permission_id'),Permission::all(),0,'');

        if (session('number_role') > $Role->number_role) {
            if (Role::find($id) == false) {
                return view('error.404');
            } else {
                return view('page.role.update', $data);
            }
        } else {
            return view('error.403');
        }
    }
    public function postedit($id, CreateRequest $request)
    {

        if ($request->number_role >= session('number_role')) {
            return redirect()->back()->with('error', lang_data('Authorization limit exceeded'));
        }

        if(isset($request->permission_id)){
            if(Role::checkpermissions($request->permission_id)=== "false"){
                return redirect()->back()->with('error',lang_data('Your rights are not enough'));
            }
            $result = Role::find($id);

            Role::where('id', $id)->update(['name' => $request->name, 'display' => $request->display, 'number_role' => $request->number_role]);

            PermissionRole::where('role_id', $result->id)->delete();

            foreach ($request->permission_id as $key) {
                PermissionRole::insert(['permission_id' => $key, 'role_id' => $id]);

            };
            return redirect()->back()->with('success', lang_data('Update new success'));
        }else{

            Role::where('id', $id)->update(['name' => $request->name, 'display' => $request->display, 'number_role' => $request->number_role]);
            PermissionRole::where('role_id',$id)->delete();

            return redirect()->back()->with('success',  lang_data('Update new success'));
        }
    }

}
