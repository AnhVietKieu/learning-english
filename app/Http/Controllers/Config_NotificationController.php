<?php

namespace App\Http\Controllers;

use App\Config_Notification;
use Illuminate\Http\Request;

class Config_NotificationController extends Controller
{
    public function show()
    {
        $data['config_notifications'] =Config_Notification::whereIn('id', [2, 4])->orderBy('id','desc')->get();

        return view('page.config_notification.list',$data);
    }

    public function create()
    {
        return view('page.config_notification.create');
    }

    public function postcreate(Request $request)
    {
        $arr = [
            'title' =>$request->title,
            'description' =>$request->description,
            'time' => $request->time
        ];
        if(Config_Notification::create($request->all()))
        {
            return redirect()->back()->with('success','Thêm thành công');
        }else{
            return redirect()->back()->with('error','Thêm thất bại');
        }
    }

    public function edit($id)
    {
        $data['config_notification'] = Config_Notification::find($id);

        if(empty($data['config_notification']))
        {
            return redirect('404');
        }else{
            return view('page.config_notification.update',$data);
        }
    }

    public function postedit($id,Request $request)
    {
        $arr = array(
            'title' =>$request->title,
            'description' =>$request->description,
            'status'=>0,
            'time' => $request->time
        );
        if (Config_Notification::where('id',$id)->update($arr))
        {
            return redirect()->back()->with('success','Sửa thành công');
        }else{
            return redirect()->back()->with('error','Sửa thất bại ');
        }
    }

    public function destroy($id)
    {
        $check = Config_Notification::find($id);
        if(empty($check))
        {
            return redirect('404');
        }else{
            $check->delete();

            return redirect()->back()->with('success','Xoá thành công');
        }
    }
}
