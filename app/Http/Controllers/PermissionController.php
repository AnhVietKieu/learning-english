<?php

namespace App\Http\Controllers;

use App\Http\Requests\Permission\PermissionRequest;
use App\Permission;
use App\PermissionRole;
use App\Permissions;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function show()
    {
        $data['permissions']=Permission::menuParent(Permission::orderBy('id','desc')->get(),0,'<span class="label label-important">Chú ý! </span> ');
        return view('page.permission.list',$data);
    }
    public function store()
    {
        $data['permission_fathers'] = Permission::where('parent_id','0')->orderBy('id','desc')->get();

        return view('page.permission.create',$data);
    }
    public function poststore(PermissionRequest $request)
    {
        if($request->name)
        {
            $text ='per_'.$request->per.'_'.strtolower($request->name);

            $checkpermission = Permission::where('name',$text)->first();
            if($checkpermission) {
                return redirect()->back()->with('error',lang_data('Middleware already exist'));
            }
        }


        if($request->per ==0 && $request->parent_id ==0 )
        {
            $arr = array(
                'display' =>$request->display,
                'parent_id' =>$request->parent_id
            );
        }else{
            $arr = array(
                'name' =>empty($request->name)?'':'per_'.$request->per.'_'.strtolower($request->name),
                'display' =>$request->display,
                'parent_id' =>$request->parent_id
            );
        }


        if(Permission::create($arr)){
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Create new error'));
        }
    }
    public function destroyall(Request $request)
    {
      for ($i=0;$i<count($request->permission_id);$i++)
      {
          $permission = Permission::select('parent_id')->where('id',$request->permission_id[$i])->first();

          $permission_id = PermissionRole::where('permission_id',$request->permission_id[$i])->get();
          if(count($permission_id)<1){
              if($permission['parent_id'] == 0){
                  $id = Permission::where('parent_id',$request->permission_id[$i])->get();
                  foreach ($id as $item){
                      Permission::destroy($item->id);
                  }
              }else{
                  Permission::destroy($request->permission_id);
              }
          }else{
              foreach ($permission_id as $item){
                  PermissionRole::where('permission_id',$item->permission_id)->delete();
              }
              if($permission['parent_id'] ==0) {
                  $id = Permission::where('parent_id', $request->permission_id[$i])->get();
                  foreach ($id as $item) {
                      Permission::destroy($item->id);
                  }
              }
          }

      }
      return redirect()->back()->with('success',lang_data('Delete success'));
    }

    public function destroy($id)
    {
        $permission = Permission::where('id',$id)->first();
        if(isset($permission))
        {
            $permission_id = PermissionRole::where('permission_id',$id)->get();
            if(empty($permission_id)){
                if($permission->parent_id ==0){
                    $id = Permission::where('parent_id',$id)->get();
                    foreach ($id as $item){
                        Permission::destroy($item->id);
                    }
                }
            }else{
                foreach ($permission_id as $item){
                    PermissionRole::where('permission_id',$item->permission_id)->delete();
                }
                if($permission->parent_id ==0) {
                    $id = Permission::where('parent_id', $id)->get();
                    foreach ($id as $item) {
                        Permission::destroy($item->id);
                    }
                }
            }
            if(Permission::where('id',$permission->id)->delete()){
                return redirect()->back()->with('success',lang_data('Delete success'));
            }else{
                return redirect()->back()->with('error',lang_data('Delete error'));
            }
        }else{
            return redirect('404');
        }
    }

    public function edit($id)
    {
        $data['permission'] = Permission::where('id',$id)->first();
        $data['permission_fathers'] = Permission::where('parent_id',0)->get();

        $result =str_replace('per_','',$data['permission']->name);
        $result1 = strstr($result,'_');

        $data['table'] = substr($result1,'1',strlen($result1));
        $data['per'] =Permission::valuepermission($data['permission']->name);

        if(isset($data)){
            return view('page.permission.edit',$data);
        }else{
            return redirect('404');
        }
    }
    public function postedit($id,PermissionRequest $request)
    {
        $text ='per_'.$request->per.'_'.strtolower($request->name);

        $result = Permission::find($id);
        $check = Permission::where('name',$text)->first();
        if(isset($check->name)){
            if($check->name !=$result->name){
                return redirect()->back()->with('error',lang_data('Middleware already exist'));
            }

        }

        $arr = array(
            'name' =>empty($request->name)?'':'per_'.$request->per.'_'.strtolower($request->name),
            'display' =>$request->display,
            'parent_id' =>$request->parent_id
        );
        if(Permission::where('id',$id)->update($arr)){
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Update new error'));
        }
    }
}
