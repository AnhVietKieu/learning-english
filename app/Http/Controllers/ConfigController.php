<?php

namespace App\Http\Controllers;

use App\Config;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
   public function show()
   {
       $data['configs'] =Config::orderBy('id','desc')->get();

       return view('page.config.list',$data);
   }

   public function create()
   {
       return view('page.config.create');
   }

   public function postcreate(Request $request)
   {
       if(Config::create($request->all()))
       {
           return redirect()->back()->with('success',lang_data('Create new success'));
       }else{
           return redirect()->back()->with('error',lang_data('Create new error'));
       }
   }

   public function edit($id)
   {
       $data['config'] = Config::find($id);

       if(empty($data['config']))
       {
           return redirect('404');
       }else{
           return view('page.config.update',$data);
       }
   }

   public function postedit($id,Request $request)
   {
       if (Config::where('id',$id)->update($request->all()))
       {
           return redirect()->back()->with('success',lang_data('Update new success'));
       }else{
           return redirect()->back()->with('error',lang_data('Update new error'));
       }
   }

   public function destroy($id)
   {
       $check = Config::find($id);
       if(empty($check))
       {
           return redirect('404');
       }else{
           $check->delete();

           return redirect()->back()->with('success',lang_data('Delete success'));
       }
   }
}
