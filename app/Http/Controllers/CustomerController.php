<?php

namespace App\Http\Controllers;

use App\Config;
use App\Customer;
use App\DeliveryNote;
use App\Http\Requests\Customer\CustomerRequest;
use App\Http\Requests\Customer\UpdateCustomerRequest;
use App\Levels;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function show()
    {
        $data['customers']= Customer::with('levels')->orderBy('customers.id','desc')->paginate(Config::value('pagination'));

        return view('page.customer.list',$data);
    }

    public function create()
    {
        $data['levels'] = Levels::all();

        return view('page.customer.create',$data);
    }

    public function postcreate(CustomerRequest $request)
    {
        $arr = array(
            'name' =>$request->name,
            'email' =>$request->email,
            'gender' =>$request->gender,
            'address' =>$request->address,
            'phone' =>isset($request->phone)?$request->phone:'',
            'birthday' =>strtotime($request->year.'/'.$request->month.'/'.$request->day)
        );
        $check = Customer::where('email',$request->email)->first();

        if(isset($check)){
            return redirect()->back()->with('error',lang_data('Email already exists'));
        }else{

                if(Customer::create($arr))
                {
                    return redirect()->back()->with('success',lang_data('Create new success'));
                }else{
                    return redirect()->back()->with('error',lang_data('Create new error'));
                }
        }
    }

    public function edit($id)
    {
        $data['customer'] = Customer::find($id);

        if(empty($data['customer'])){
            return redirect('404');
        }else{
            return view('page.customer.update',$data);
        }

    }

    public function postedit($id,UpdateCustomerRequest $request)
    {

        $arr = array(
            'name' =>$request->name,
            'gender' =>$request->gender,
            'address' =>$request->address,
            'phone' =>isset($request->phone)?$request->phone:'',
            'birthday' =>strtotime($request->birthday),
            'status' =>$request->status,

        );
        if(Customer::where('id',$id)->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Update new error'));
        }
    }
    public function destroy($id)
    {
        $check = Customer::find($id);
        if(empty($check)){
            return redirect('404');
        }else{
            $delivery = DeliveryNote::where('customer_id',$id)->first();

            if(isset($delivery)){
                if(Customer::where('id',$id)->update(['status'=>'2'])){
                    return redirect()->back()->with('success',lang_data('Delete success'));
                }
            }else{
                Customer::where('id',$id)->delete();

                return redirect()->back()->with('success',lang_data('Delete success'));
            }
            return redirect()->back()->with('error',lang_data('Delete error'));
        }
    }

    public function destroyall(Request $request)
    {
        for($i=0;$i<count($request->customer_id);$i++)
        {
            $delivery = DeliveryNote::where('customer_id',$request->customer_id[$i])->first();

            if(isset($delivery)){
                Customer::where('id',$request->customer_id[$i])->update(['status'=>'2']);
            }else{
                Customer::where('id',$request->customer_id[$i])->delete();

            }

        }
        return redirect()->back()->with('error',lang_data('Delete success'));
    }

    public function perview($id)
    {
        dd($id);
    }

    public function status_notification(Request $request)
    {
        if($request->enable==1){
            if(Customer::where('id',$request->customer_id)->update(['status_notification'=>'0']))
            {
                return $notification = lang_data('Turn on notification success');
            }
        }else{
            if(Customer::where('id',$request->customer_id)->update(['status_notification'=>'1']))
            {
                return $notification =  lang_data('Turn off notification success');
            }
        }

    }

    public function search(Request $request){

        $data['customers'] =Customer::search_not($request);

        return view('page.customer.list', $data);

    }

}
