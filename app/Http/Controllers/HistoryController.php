<?php

namespace App\Http\Controllers;

use App\Config;
use App\History;
use App\History_Test;
use App\Lessons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    public function index()
    {

        $data['history'] = History::with('customer')->with('history_test')->with('user')->with(['lesson'=>function($query){
            $query->with('coure');
        }])->orderBy('id','desc')->paginate(Config::value('pagination'));

        foreach ($data['history'] as $item)
        {
            foreach ($item['history_test'] as $item1 => $value)
            {
                $value->answer_customer = json_decode($value->answer_customer,true);
                $value->answer_user = json_decode($value->answer_user,true);
            }
        }

//        dd($data['history']->toArray());

        return view('page.history.customer.list',$data);
    }

    public function show($id)
    {
        $data['test'] = History::with('history_test')->with('customer')->with(['lesson'=>function($query){
            $query->with('reads')->with('listens')->with('writes')->with('speaks');
        }])->find($id);

        if($data['test'])
        {
            $data['i'] =1;

            foreach ($data['test']['history_test'] as $item1 => $value)
            {
                $value->answer_customer = json_decode($value->answer_customer,true);
                $value->answer_user = json_decode($value->answer_user,true);
            }

//            dd($data['test']->toArray());

            return view('page.history.customer.update',$data);
        }else{
            return redirect('404');
        }
    }

    public function markTest(Request $request,$id)
    {

//        dd($request->all());


        $chek = History_Test::where('history_id',$id)->with('history')->first();

        if($chek)
        {
            if(isset($request->answer_user_read))
            {
               $result_read =History_Test::history_test_update($request->answer_user_read,'read',$chek->history_id);

            }
            if(isset($request->answer_user_write))
            {

              $result_write = History_Test::history_test_update($request->answer_user_write,'write',$chek->history_id);

            }
            if(isset($request->answer_user_listen))
            {
                $result_listen = History_Test::history_test_update($request->answer_user_listen,'listen',$chek->history_id);

            }
            if(isset($request->answer_user_speak))
            {
              $result_speak=  History_Test::history_test_update($request->answer_user_speak,'speak',$chek->history_id);

            }

//            dd($result_read,$result_listen,$result_write,$result_speak);


            $score = History::total_score(isset($result_read['score_total'])?$result_read['score_total']:0,
                isset($result_write['score_total'])?$result_write['score_total']:0,
                isset($result_listen['score_total'])?$result_listen['score_total']:0,
                isset($result_speak['score_total'])?$result_speak['score_total']:0,'score');


            $answer_true =History::total_score(isset($result_read['total_answer'])?$result_read['total_answer']:0,
                isset($result_write['total_answer'])?$result_write['total_answer']:0,
                isset($result_listen['total_answer'])?$result_listen['total_answer']:0,
                isset($result_speak['total_answer'])?$result_speak['total_answer']:0,'score');


            $data['test'] = Lessons::with('reads')->with('listens')->with('writes')->with('speaks')->find($chek['history']['lesson_id']);


            $total_question = count(isset($data['test']['reads'])?$data['test']['reads']:[])+
                count(isset($data['test']['speaks'])?$data['test']['speaks']:[])+
                count(isset($data['test']['writes'])?$data['test']['writes']:[])+
                count(isset($data['test']['listens'])?$data['test']['listens']:[]);

            History::find($id)->update([
                'score'=>$score,
                'answer_true'=>$answer_true,
                'answer_false' =>$total_question - $answer_true,
                'total_question' =>$total_question,
                'status'=>1,
                'user_id'=>Auth::User()->id]);


            return redirect()->back()->with('success',lang_data('Mark test success'));

        }else{
            return redirect('404');
        }
    }

    public function destroy($id)
    {
        History_Test::where('history_id',$id)->delete();

        History::find($id)->delete();

        return redirect()->back()->with('success',lang_data('Delete success'));
    }

    public function search(Request $request)
    {
        if(empty($request->search))
        {
           $data['history'] = History::search_not($request);
        }else{
            $data['history'] = History::search_se($request);
        }

        foreach ($data['history'] as $item)
        {
            foreach ($item['history_test'] as $item1 => $value)
            {
                $value->answer_customer = json_decode($value->answer_customer,true);
                $value->answer_user = json_decode($value->answer_user,true);
            }
        }


//        dd($data['history']->toArray());

        return view('page.history.customer.list',$data);
    }
}
