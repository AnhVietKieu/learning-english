<?php

namespace App\Http\Controllers;

use App\Config;
use App\DeliveryNote;
use App\Http\Requests\User\CreateRequest;
use App\Receipt;
use App\Role;
use App\User;
use Carbon\Carbon;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function show()
    {
        $data['users'] = User::with('role')->paginate(Config::value('pagination'));

        return view('page.user.list',$data);
    }
    public function create()
    {
        $data['roles'] = Role::where('number_role','<',session('number_role'))->get();

        return view('page.user.create',$data);
    }
    public function postcreate(CreateRequest $request)
    {
        $date = new Carbon();

        if(isset($request->avatar)){
            $file = $request->avatar;
            if($file->getClientOriginalExtension()=='gif'
                || $file->getClientOriginalExtension()=='png'
                || $file->getClientOriginalExtension()=='jpg'){

                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error',lang_data('Image too large'));
                }
                $fil = str_replace(array(' ',':','-'),'_',$date->toDateTimeString()).'_'.$file->getClientOriginalName();

                if($file->move('public/upload/user/', $fil)==false)
                {
                    return redirect()->back()->with('error',lang_data('Download image error'));
                }
            }else{
                return redirect()->back()->with('error',lang_data('Image not true'));
            }

            $file_name = $fil;

        }

       $arr = array(
           'name' =>$request->name,
           'email' =>$request->email,
           'password' =>bcrypt($request->password),
           'gender' =>$request->gender,
           'address' =>$request->address,
           'role_id' =>$request->role_id,
           'avatar' =>isset($file_name)?$file_name:'',
           'birthday' =>strtotime($request->year.'/'.$request->month.'/'.$request->day)
       );
        $check = User::where('email',$request->email)->first();

        if(isset($check)){
            return redirect()->back()->with('error',lang_data('Email already exists'));
        }else{

            if(User::create($arr))
            {
                return redirect()->back()->with('success',lang_data('Create new success'));
            }else{
                return redirect()->back()->with('error',lang_data('Create new error'));
            }
        }
    }
    public function edit($id)
    {
        $data['user'] = User::where('id',$id)->first();
        if(empty($data['user'])){
            return redirect('404');
        }else{
           $check = User::join('roles','users.role_id','=','roles.id')->where('users.id',$id)->first();
            if($id == Auth::User()->id)
            {
                $data['roles'] = Role::where('number_role','<=',session('number_role'))->get();

                return view('page.user.update',$data);
            }
           if($check->number_role >= session('number_role')){
               return redirect('403');
           }else{
               $data['roles'] = Role::where('number_role','<',session('number_role'))->get();

               return view('page.user.update',$data);
           }

        }
    }
    public function postedit($id,Request $request)
    {
        $date = new Carbon();
        $check = User::join('roles','users.role_id','=','roles.id')->where('users.id',$id)->first();

        if($check->number_role > session('number_role')){
            return redirect('403');
        }

        if(isset($request->avatar)){
            $file = $request->avatar;
            if($file->getClientOriginalExtension()=='gif'
                || $file->getClientOriginalExtension()=='png'
                || $file->getClientOriginalExtension()=='jpg' || $file->getClientOriginalExtension()=='jpeg'){
                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error',lang_data('Image too large'));
                }
                $fil = str_replace(array(' ',':','-'),'_',$date->toDateTimeString()).'_'.$file->getClientOriginalName();
                if($check->avatar != $file->getClientOriginalName()){
                    $file->move('public/upload/user/', $fil);
                }
            }else{
                return redirect()->back()->with('error',lang_data('Image not true'));
            }
            $file_name = $fil;
        }


        $arr = array(
            'name' =>$request->name,
            'gender' =>$request->gender,
            'address' =>$request->address,
            'role_id' =>$request->role_id,
            'avatar' =>isset($file_name)?$file_name:$check->avatar,
            'birthday' =>strtotime($request->birthday),
            'phone' =>isset($request->phone)?$request->phone:'',
            'status'=>$request->status
        );


        if(User::where('id',$id)->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else
        {
            return redirect()->back()->with('error',lang_data('Update new error'));
        }

    }
    public function destroy($id)
    {
        $check = User::join('roles','users.role_id','=','roles.id')->where('users.id',$id)->first();

        if($check->number_role > session('number_role')){
            return redirect('403');
        }
        if(empty($check)){
            return redirect('404');
        }else{
            $user_delivery = DeliveryNote::where('user_id',$id)->first();
            $user_receip = Receipt::where('user_id',$id)->first();
            if(isset($user_receip)||isset($user_delivery)){
                if(User::where('id',$id)->update(['status'=>'2'])){
                    return redirect()->back()->with('success',lang_data('Delete success'));
                }else{
                    return redirect()->back()->with('error',lang_data('Delete error'));
                }
            }else{
                if(User::where('id',$id)->delete()){
                    return redirect()->back()->with('success',lang_data('Delete success'));
                }else{
                    return redirect()->back()->with('error',lang_data('Delete error'));
                }
            }

        }
    }
    public function destroyall(Request $request)
    {
        for($i=0;$i<count($request->user_id);$i++) {
            $check = User::join('roles', 'users.role_id', '=', 'roles.id')->where('users.id', $request->user_id[$i])->first();

            if ($check->number_role > session('number_role')) {
                return redirect('403');
            }
        }
       for($i=0;$i<count($request->user_id);$i++)
       {
           User::where('id',$request->user_id[$i])->update(['status'=>'2']);

       }
        return redirect()->back()->with('success',lang_data('Delete success'));
    }

    public function search(Request $request){

        $data['users'] = User::search_not($request);

        return view('page.user.list', $data);

    }

    public function changepassword()
    {
        return view('page.user.changepassword');
    }

    public function postchangepassword(Request $request)
    {
        $checkuser = User::where('email',session('email'))->first();

        $arr = array(
            'email' => session('email'),
            'password' =>$request->password
        );
        if(Auth::attempt($arr)){
            $checkuser->password = bcrypt($request->password_new);
            $checkuser->save();

            $notification =lang_data('Change password success');

        }else{
            $notification =lang_data('Old password is incorrect');
        }
        if(isset($notification)){
            return $notification;
        }
    }

    public function updateadmin()
    {
        $data['user'] = User::where('id',session('user_id'))->first();

        return view('page.user.updateadmin',$data);
    }

    public function postupdateadmin(Request $request)
    {
       $checkemail = User::where('email',$request->email)->first();

        if($request->email == session('email'))
        {
            if(User::where('id',session('user_id'))->update($request->all()))
            {
                session::put('username',$request->name);
                session::put('email',$request->name);

                return redirect()->back()->with('success',lang_data('Update new success'));
            }else{
                return redirect()->back()->with('error',lang_data('Update new error'));
            }
        }else{
            if($checkemail)
            {
                return redirect()->back()->with('error',lang_data('Email already exists'));
            }else{
                if(User::where('id',session('user_id'))->update($request->all()))
                {
                    session::put('email',$request->name);
                    session::put('username',$request->name);

                    return redirect()->back()->with('success',lang_data('Update new success'));
                }else{
                    return redirect()->back()->with('error',lang_data('Update new error'));
                }
            }
        }
    }

}
