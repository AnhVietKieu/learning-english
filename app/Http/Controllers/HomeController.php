<?php

namespace App\Http\Controllers;

use App\Config;
use App\Http\Requests\User\LoginRequest;
use App\Language;
use App\Role;
use App\User;
use App\User_Config;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        


        return view('page.dashbroad.list');
    }
    public function login()
    {
        return view('login');
    }
    public function postlogin(LoginRequest $request)
    {

         $arr = array(
                    'email' => $request->email,
                    'password' => $request->password
                );


        $secretkey = '6LdooMoUAAAAAEwfjjsq8YME1_fSrFSe8-AcPNea';
       $recap = $_POST['g-recaptcha-response'];
       if(isset($recap)){
            $url ='https://www.google.com/recaptcha/api/siteverify?secret='.urldecode($secretkey).'&response='.urldecode($recap).'';

            if (Auth::attempt($arr)) {
                    $Role_user = User::find(Auth::User()->id);

                    $Quyenhan = Role::where('id', $Role_user->role_id)->first();

                    session::put('email', Auth::User()->email);
                    session::put('number_role', $Quyenhan->number_role);
                    session::put('username', Auth::User()->name);
                    session::put('user_id', Auth::User()->id);
                    session::put('role_id', Auth::User()->role_id);

                    return redirect('admin/dashbroad');
                }else{
                    return redirect()->back()->with('error',lang_data('Error password or error email'));
                }



            $response = file_get_contents($url);
            $responseKey = json_decode($response,true);
            if($responseKey['success'] = true){


                $checkmail = User::where('email',$request->email)->first();
                if(empty($checkmail)){
                    return redirect()->back()->with('error','Sai mật khẩu hoặc tài khoản !');
                }else{
                    if($checkmail->status!=0)
                    {
                        return redirect()->back()->with('error','Tài khoản không tồn tại !');
                    }
                }
                if (Auth::attempt($arr)) {
                    $Role_user = User::find(Auth::User()->id);

                    $Quyenhan = Role::where('id', $Role_user->role_id)->first();

                    session::put('email', Auth::User()->email);
                    session::put('number_role', $Quyenhan->number_role);
                    session::put('username', Auth::User()->name);
                    session::put('user_id', Auth::User()->id);
                    session::put('role_id', Auth::User()->role_id);

                    return redirect('admin/dashbroad');
                }else{
                    return redirect()->back()->with('error','Sai mật khẩu hoặc tài khoản !');
                }
            }else{
                dd(2);
            }

       }else{
           return redirect()->back()->with('error','Sai mật khẩu hoặc tài khoản !');
       }

    }
    public function logout()
    {
        if (Auth::logout() == false) {
            session()->forget('username');
            session()->flush();

            return redirect('BE');
        }
    }

    public function resetpassword()
    {
        return view('resetpassword');
    }

    public function postresetpassword(Request $request)
    {
       $checkmail = User::where('email',$request->email)->first();
       if(empty($checkmail))
       {
           return redirect()->back()->with('error',lang_data('Email not true'));
       }else
       {
           session::put('email_ver',$request->email);

           $key = str_random('126');
           $otp = str_random('6');
           $checkmail->timeout = time();
           $checkmail->save();
            $url ='http://localhost:5000/password_new?keyreset='.$key;
           $link ='Mời bạn vào link '.$url."<br>
            Mã OTP của bạn duy trì trong 5 phút:".$otp;
           $title ='Quên mật khẩu';

            if(send_mail($request->email,$link,$title))
            {
                $checkmail->keyreset = $key;
                $checkmail->otp = $otp;
                $checkmail->save();
                return redirect()->back()->with('success',lang_data('Please check your email'));
            }else{
                return redirect()->back()->with('error',lang_data('Send email error'));
            }
       }

    }

    public function passwordnew()
    {
        return view('changepassword');
    }
    public function postpasswordnew(Request $request)
    {
        $password = bcrypt($request->password);
        $time =time()+300;

        $checkuser = User::where('keyreset',$_GET['keyreset'])->first();

        if($checkuser)
        {
            if($checkuser->timeout > $time)
            {
                return redirect()->back()->with('error',lang_data('Your otp is out of date'));
            }

            if($checkuser->otp == $request->otp)
            {
                $checkuser->password =$password;
                $checkuser->keyreset = '';
                $checkuser->timeout = '';
                $checkuser->otp = '';
                $checkuser->save();
                return redirect('');
            }else{
                return redirect()->back()->with('error',lang_data('Your OTP code is wrong'));
            }

        }else{
            return redirect()->back()->with('error',lang_data('Your key is not correct'));
        }
    }

    public function sendotp()
    {
        $otp = str_random('6');
        $body = 'Mã OTP của bạn :'.$otp;
        $email = session('email_ver');

        if(send_mail($email,$body,'Mã OTP'))
        {
            $check = User::where('email',session('email_ver'))->first();
            $check->otp = $otp;
            $check->save();

            return redirect()->back()->with('success','Mã OTP đã được gửi vào email');
        }else{
            return redirect()->back()->with('error','Gửi thất bại');
        }
    }
}
