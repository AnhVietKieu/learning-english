<?php

namespace App\Http\Controllers;

use App\Config;
use App\Coures;
use App\Lessons;
use App\Levels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LessonController extends Controller
{
    public function show()
    {
        $data['lessons'] = Lessons::with('coure')->with('level')
            ->orderBy('id','desc')->paginate(Config::value('pagination'));

        return view('page.lesson.list',$data);
    }

    public function create()
    {
        $data['coures'] = Coures::where('parent_id','!=','0')->get();

        $data['levels'] = Levels::all();

        return view('page.lesson.create',$data);
    }

    public function postcreate(Request $request)
    {
        if(isset($request->image)){
            $file = $request->image;
            if($file->getClientOriginalExtension()=='gif'
                || $file->getClientOriginalExtension()=='png'
                || $file->getClientOriginalExtension()=='jpg' || $file->getClientOriginalExtension()=='jpeg'){

                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error',lang_data('Image too large'));
                }
                if($file->move('public/upload/lesson/', $file->getClientOriginalName())==false)
                {
                    return redirect()->back()->with('error',lang_data('Download image error'));
                }
            }

            $file_name = $file->getClientOriginalName();

        }

        $arr = [
            'title' =>$request->title,
            'description' =>$request->description,
            'image' =>isset($file_name)?$file_name:'null',
            'title_video' =>$request->title_video,
            'video' => $request->video,
            'url' =>Lessons::get_url($request->video),
            'coure_id' =>$request->coure_id,
            'level_id' =>$request->level_id,
            'time' =>$request->time,
            'score'=>$request->score,
            'created_by'=>Auth::user()->id
        ];

        if(Lessons::create($arr))
        {
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
             return redirect()->back()->with('error',lang_data('Create new error'));
        }
    }

    public function edit($id)
    {
        $data['lesson'] = Lessons::find($id);
        $data['coures'] = Coures::where('parent_id','!=',0)->get();

        $data['levels'] = Levels::all();

        if(empty($data['lesson'])){
            return redirect('404');
        }

        return view('page.lesson.update',$data);


    }

    public function postedit($id,Request $request)
    {
        $check = Lessons::find($id);

        if(empty($check))
        {
            return redirect('404');
        }

        if(isset($request->image)){
            $file = $request->image;
            if($file->getClientOriginalExtension()=='gif'
                || $file->getClientOriginalExtension()=='png'
                || $file->getClientOriginalExtension()=='jpg' || $file->getClientOriginalExtension()=='jpeg'){

                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error',lang_data('Image too large'));
                }
                if($file->move('public/upload/lesson/', $file->getClientOriginalName())==false)
                {
                    return redirect()->back()->with('error',lang_data('Download image error'));
                }
            }

            $file_name = $file->getClientOriginalName();

        }

        $arr = [
            'title' =>$request->title,
            'description' =>$request->description,
            'image' =>isset($file_name)?$file_name:$check->image,
            'title_video' =>$request->title_video,
            'video' => $request->video,
            'url' =>Lessons::get_url($request->video),
            'coure_id' =>$request->coure_id,
            'level_id' =>$request->level_id,
            'time' =>$request->time,
            'score'=>$request->score,
            'updated_by'=>Auth::user()->id
        ];

        if($check->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Update new error'));
        }


    }

    public function destroy($id)
    {
        $lesson= Lessons::where('id',$id)->first();

        if (empty($lesson)) {
            return view('error.404');
        }else{

            Lessons::check_delete($id);

            if (Lessons::delete_lesson($id)) {
                return redirect()->back()->with('success',lang_data('Delete success'));
            } else {
                return redirect()->back()->with('error',lang_data('Delete error'));
            }
        }

    }

    public function search(Request $request)
    {
        $data['lessons'] = Lessons::where('title','like','%'.$request->search.'%')->with('coure')->with('level')
            ->orderBy('id','desc')->paginate(Config::value('pagination'));


        return view('page.lesson.list',$data);
    }
}
