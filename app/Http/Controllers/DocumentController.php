<?php

namespace App\Http\Controllers;

use App\Config;
use App\Document;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public function index()
    {
        $data['documents'] = Document::paginate(Config::value('pagination'));

        return view('page.document.list',$data);

    }
    public function create()
    {
        return view('page.document.create');

    }

    public function postcreate(Request $request)
    {

        if(isset($request->file)){
            $file = $request->file;
                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error', lang_data('File too large'));
                }
                if($file->move('public/upload/document/', $file->getClientOriginalName())==false)
                {
                    return redirect()->back()->with('error', lang_data('Dowload file error'));
                }

            $file_name = $file->getClientOriginalName();

        }
        $arr = [
            'title' =>$request->title,
            'file' =>isset($file_name)?$file_name:'null',
        ];

        if(Document::create($arr))
        {
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Create new error'));
        }
    }

    public function edit($id)
    {
        $data['document'] = Document::where('id',$id)->first();
        if(empty($data['document'])){
            return redirect('404');
        }else{
            return view('page.document.update',$data);
        }
    }

    public function postedit(Request $request,$id)
    {
        $check = Document::find($id);

        if($check)
        {
            if(isset($request->file)){
                $file = $request->file;
                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error', lang_data('File too large'));
                }
                if($file->move('public/upload/document/', $file->getClientOriginalName())==false)
                {
                    return redirect()->back()->with('error', lang_data('Dowload file error'));
                }

                $file_name = $file->getClientOriginalName();

            }
            $arr = [
                'title' =>$request->title,
                'file' =>isset($file_name)?$file_name:$check->file,
            ];

            if(Document::find($id)->update($arr))
            {
                return redirect()->back()->with('success',lang_data('Update new success'));
            }else{
                return redirect()->back()->with('error',lang_data('Update new error'));
            }
        }else{
            return redirect('404');
        }

    }
    public function destroy($id)
    {
        $chek = Document::find($id);

        if($chek)
        {
            $chek->delete();

            return redirect()->back()->with('success',lang_data('Delete success'));
        }else{
            return redirect('404');
        }
    }
}
