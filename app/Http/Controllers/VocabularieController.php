<?php

namespace App\Http\Controllers;

use App\Config;
use App\Vocabularies;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VocabularieController extends Controller
{
    protected $data ='';

    public function __construct()
    {
        $this->data = new Carbon();
    }

    public function index()
    {
        $data['vocabularies'] = Vocabularies::select('*')->paginate(Config::value('pagination'));

       return view('page.vocabularie.list',$data);
    }
    public function create()
    {
        return view('page.vocabularie.create');
    }
    public function postcreate(Request $request)
    {
        if(isset($request->image)){
            $file = $request->image;
            if($file->getClientOriginalExtension()=='gif'
                || $file->getClientOriginalExtension()=='png'
                || $file->getClientOriginalExtension()=='jpg'){

                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error',lang_data('Image too large'));
                }
                $fi_name =  str_replace(array(' ',':','-'),'_',$this->date->toDateTimeString()).'_'.$file->getClientOriginalName();
                if($file->move('public/upload/vocabularie/',$fi_name)==false)
                {
                    return redirect()->back()->with('error',lang_data('Download image error'));
                }
            }else{
                return redirect()->back()->with('error',lang_data('Image not true'));
            }

            $file_name = $fi_name;

        }

        $arr = [
            'word'  =>$request->word,
            'type' => $request->type,
            'pronounce'=>$request->pronounce,
            'meaning' =>$request->meaning,
            'image' =>isset($file_name)?$file_name:'',
            'description' =>$request->description
        ];

        if(Vocabularies::create($arr))
        {
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Create new error'));
        }


    }

    public function edit($id)
    {
        $check = Vocabularies::find($id);
        if($check)
        {
            $data['vocabularie'] = $check;

            return view('page.vocabularie.update',$data);
        }else{
            return redirect('404');
        }


    }

    public function postedit(Request $request,$id)
    {

        $check = Vocabularies::find($id);
        if($check)
        {
            if(isset($request->image)){
                $file = $request->image;
                if($file->getClientOriginalExtension()=='gif'
                    || $file->getClientOriginalExtension()=='png'
                    || $file->getClientOriginalExtension()=='jpg'){

                    if($file->getSize() >'2271841'){
                        return redirect()->back()->with('error',lang_data('Image too large'));
                    }
                    $fi_name =  str_replace(array(' ',':','-'),'_',$this->date->toDateTimeString()).'_'.$file->getClientOriginalName();
                    if($file->move('public/upload/vocabularie/',$fi_name)==false)
                    {
                        return redirect()->back()->with('error',lang_data('Download image error'));
                    }
                }else{
                    return redirect()->back()->with('error',lang_data('Image not true'));
                }

                $file_name = $fi_name;

            }
            $arr = [
                'word'  =>$request->word,
                'type' => $request->type,
                'pronounce'=>$request->pronounce,
                'meaning' =>$request->meaning,
                'image' =>isset($file_name)?$file_name:$check->image,
                'description' =>$request->description
            ];

            if($check->update($arr))
            {
                return redirect()->back()->with('success',lang_data('Update new success'));
            }else{
                return redirect()->back()->with('error',lang_data('Update new error'));
            }
        }else{
            return redirect('404');
        }




    }

    public function destroy($id)
    {
        $check = Vocabularies::find($id);
        if($check)
        {
            $check->delete();
            return redirect()->back()->with('success',lang_data('Delete success'));
        }else{
            return redirect('404');
        }
    }

    public function search(Request $request)
    {
        $data['vocabularies'] = Vocabularies::select('*')->where('word','like','%'.$request->search.'%')
            ->paginate(Config::value('pagination'));

        return view('page.vocabularie.list',$data);
    }
}
