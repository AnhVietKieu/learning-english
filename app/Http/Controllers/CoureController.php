<?php

namespace App\Http\Controllers;

use App\Config;
use App\Levels;
use Illuminate\Http\Request;
use App\Coures;
use App\Permission;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;

class CoureController extends Controller
{
    public function show()
    {
        $data['permissions']=Coures::menuParent(Coures::with('level')
            ->orderBy('id','desc')
            ->get(),
            0,'<span class="label label-important">Chú ý! </span> ');

        return view('page.coure.list',$data);

    }

    public function create()
    {
        $data['coures']=Coures::where('parent_id',0)->get();
        $data['levels'] = Levels::orderBy('score','desc')->get();
        return view('page.coure.create',$data);
    }

    public function postcreate(Request $request)
    {
        if(isset($request->image)){
            $file = $request->image;
            if($file->getClientOriginalExtension()=='gif'
                || $file->getClientOriginalExtension()=='png'
                || $file->getClientOriginalExtension()=='jpg' || $file->getClientOriginalExtension()=='jpeg'){

                if($file->getSize() >'2271841'){
                    return redirect()->back()->with('error',lang_data('Image too large'));
                }
                if($file->move('public/upload/coure/', $file->getClientOriginalName())==false)
                {
                    return redirect()->back()->with('error',lang_data('Dowload image error'));
                }
            }

            $file_name = $file->getClientOriginalName();

        }
        $check =  Coures::where('name',$request->name)->first();

        if(!empty($check)){
            return redirect()->back()->with('error',lang_data('Duplicate name'));
        }

        $arr = [
            'name' =>$request->name,
            'image' =>isset($file_name)?$file_name:'null',
            'description' =>$request->description,
            'level_id' =>$request->level_id,
            'parent_id' =>$request->parent_id,
            'created_by' =>Auth::User()->id
        ];

        if(Coures::create($arr))
        {
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Create new error'));
        }
    }


    public function edit($id)
    {
        $data['coure'] = Coures::where('id',$id)->first();

        $data['coures'] =Coures::where('parent_id',0)->get();

        $data['levels'] = Levels::orderBy('score','desc')->get();

        if(empty($data['coure'])){
            return redirect('404');
        }else{
            return view('page.coure.update',$data);
        }
    }
    public function postedit($id,Request $request)
    {
        $check = Coures::find($id);


        if(empty($check)){
            return redirect('404');
        }else{

            if(isset($request->image)){
                $file = $request->image;
                if($file->getClientOriginalExtension()=='gif'
                    || $file->getClientOriginalExtension()=='png'
                    || $file->getClientOriginalExtension()=='jpg' || $file->getClientOriginalExtension()=='jpeg'){
                    if($file->getSize() >'2271841'){
                        return redirect()->back()->with('error',lang_data('Image too large'));
                    }
                    if($check->image != $file->getClientOriginalName()){
                        $file->move('public/upload/coure/', $file->getClientOriginalName());
                    }
                }else{
                    return redirect()->back()->with('error',lang_data('Not a photo'));
                }
                $file_name = $file->getClientOriginalName();
            }

            $check_name = Coures::where('name',$request->name)->first();


            if($check->name == $request->name)
            {
                $arr = [
                    'image' =>isset($file_name)?$file_name:$check->image,
                    'description' =>$request->description,
                    'parent_id' =>$request->parent_id,
                    'level_id' =>$request->level_id,
                    'status' =>$request->status,
                    'updated_by' =>Auth::User()->id
                ];


            }else{
                if(isset($check_name)){
                    return redirect()->back()->with('error',lang_data('Duplicate name'));
                }else{
                    $arr = [
                        'name' =>$request->name,
                        'image' =>isset($file_name)?$file_name:$check->image,
                        'description' =>$request->description,
                        'parent_id' =>$request->parent_id,
                        'status' =>$request->status,
                        'updated_by' =>Auth::User()->id
                    ];
                }
            }
        }


        if(Coures::where('id',$id)->update($arr))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else
        {
            return redirect()->back()->with('error',lang_data('Update new error'));
        }

    }

    public function destroy($id)
    {
        $coure = Coures::where('id',$id)->first();
        if(isset($coure))
        {
            $coure_id = Coures::where('parent_id',$id)->get();

            if(empty($coure_id)){
                    if(Coures::destroy($id)) {
                        return redirect()->back()->with('success',lang_data('Delete success'));
                    }

            }else{
                foreach ($coure_id as $item){
                    Coures::where('id',$item->id)->delete();
                }
                if(Coures::destroy($id)) {
                    return redirect()->back()->with('success',lang_data('Delete success'));
                }

            }
                return redirect()->back()->with('error',lang_data('Delete error'));

        }else{
            return redirect('404');
        }

    }
}
