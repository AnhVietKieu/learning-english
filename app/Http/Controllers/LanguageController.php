<?php

namespace App\Http\Controllers;

use App\Config;
use App\Document;
use App\Language;
use App\Language_Data;
use App\User_Config;
use App\Vocabularies;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LanguageController extends Controller
{
    public function index()
    {
        $data['language'] =Language ::paginate(Config::value('pagination'));
        $meta_value =Config::where('meta_key','language_default')->first();

        $data['lan'] = Language::where('language_code',$meta_value['meta_value'])->first();

        return view('page.language.list',$data);

    }
    public function create()
    {
        return view('page.language.create');

    }

    public function postcreate(Request $request)
    {
        $arr = explode('_',$request->language);

        $date = new Carbon();


        if(isset($request->icon)){
            $file = $request->icon;
            if($file->getClientOriginalExtension()=='gif'
                || $file->getClientOriginalExtension()=='png'
                || $file->getClientOriginalExtension()=='jpg' || $file->getClientOriginalExtension()=='jpeg'){

                $file_name =Auth::guard('customers')->user()->id.'_'.str_replace(array(' ',':','-'),'_',$date->toDateTimeString()).'_'.$file->getClientOriginalName();

                $file->move('public/upload/language/', $file_name);
            }else{
                return redirect()->back()->with('error',lang_data('File not true'));
            }
        }

        $array = [
            'language' =>$arr[1],
            'language_code' =>$arr[0],
            'status' =>$request->status,
            'icon' =>isset($file_name)?$file_name:''
        ];

        if(Language::create($array))
        {
            return redirect()->back()->with('success',lang_data('Add language success'));
        }else{
            return redirect()->back()->with('error',lang_data('Add language error'));

        }
    }
    public function edit($id)
    {
        $data['lan'] = Language::find($id);
        if(empty($data['lan'])){
            return redirect('404');
        }else{
            return view('page.language.update',$data);
        }
    }

    public function postedit(Request $request,$id)
    {

        $check= Language::find($id);

        if($check)
        {
            $arr = explode('_',$request->language);

            $date = new Carbon();


            if(isset($request->icon)){
                $file = $request->icon;
                if($file->getClientOriginalExtension()=='gif'
                    || $file->getClientOriginalExtension()=='png'
                    || $file->getClientOriginalExtension()=='jpg' || $file->getClientOriginalExtension()=='jpeg'){

                    $file_name =Auth::guard('customers')->user()->id.'_'.str_replace(array(' ',':','-'),'_',$date->toDateTimeString()).'_'.$file->getClientOriginalName();

                    $file->move('public/upload/language/', $file_name);
                }else{
                    return redirect()->back()->with('error',lang_data('File not true'));
                }
            }

            $array = [
                'language' =>$arr[1],
                'language_code' =>$arr[0],
                'status' =>$request->status,
                'icon' =>isset($file_name)?$file_name:$check->icon
            ];

            if($check->update($array))
            {
                return redirect()->back()->with('success',lang_data('Update language success'));
            }else{
                return redirect()->back()->with('error',lang_data('Update language error'));

            }
        }else{
            return redirect('404');
        }


    }
    public function destroy($id)
    {
        $chek = Language::find($id);

        if($chek)
        {
            $chek_language = Language_Data::where('lan_id',$id)->get();

            if($chek_language)
            {
                foreach ($chek_language as $item) {
                    Language_Data::where('lan_id', $item['id'])->delete();
                }
            }
            $chek->delete();

            return redirect()->back()->with('success',lang_data('Delete success'));
        }else{
            return redirect('404');
        }
    }

    public function setting($id)
    {
        $check = Language::find($id);

        if($check)
        {
            $user_config = User_Config::where('user_id',Auth::User()->id)->where('meta_key','language_default')->first();
            if(empty($user_config))
            {
               User_Config::create(['meta_key'=>'language_default','meta_value' =>$check->language_code,'user_id'=>Auth::User()->id]);

               return redirect()->back()->with('success',lang_data('Change language success'));
            }else{
                User_Config::where('user_id',Auth::User()->id)->update(['meta_key'=>'language_default','meta_value' =>$check->language_code]);

                return redirect()->back()->with('success',lang_data('Change language success'));
            }
        }else{
            return redirect('404');
        }

    }

    public function create_lan()
    {
        $data['language_data'] = Language_Data::where('lan_id',0)->orderBy('id','desc')->paginate(Config::value('pagination'));
        return view('page.language.create',$data);
    }

    public function lan_data(Request $request)
    {

        $check = Language_Data::where('lan_id',0)->where('lan_data',$request->lan_data)->get();

        if(isset($check)&&count($check)>=1)
        {
            return redirect()->back()->with('error',lang_data('Duplicate name'));
        }else{

            $arr= [
                'lan_id'=>0,
                'lan_data' =>ucfirst(strtolower($request->lan_data))
            ];
            if(Language_Data::create($arr)){
                return redirect()->back()->with('success',lang_data('Create new success'));
            }else{
                return redirect()->back()->with('error',lang_data('Create new error'));
            }
        }



    }

    public function setting_lan($id)
    {

        $data['language_data'] = Language_Data::where('lan_id',0)->orderBy('id','desc')->paginate(Config::value('pagination'));

        $data['lan'] = Language::find($id);

        return view('page.language.list_create',$data);

    }

    public function postsetting_lan(Request $request)
    {

        foreach($request->lan_value as $key =>$value)
        {
            $check = Language_Data::where('lan_id',$request->lan_id)->where('lan_data',$key)->get();

            if(isset($check)&&count($check)>=1)
            {
                $chek =Language_Data::where('lan_id',$request->lan_id)->where('lan_data',$key)->first();

               $chek->update([
                   'lan_id' => $request->lan_id,
                   'lan_data' =>$key,
                   'lan_value' =>ucfirst(strtolower($value))
               ]);
            }else{
                Language_Data::create([
                    'lan_id' => $request->lan_id,
                    'lan_data' =>$key,
                    'lan_value' =>ucfirst(strtolower($value))
                ]);
            }

        }

        return redirect()->back()->with('success',lang_data('Change success'));
    }
    public function lan_search(Request $request)
    {

        $search = ucfirst(strtolower($request->lan_data));
        $data['language_data'] = Language_Data::where('lan_id',0)->where('lan_data','like','%'.$search.'%')->orderBy('id','desc')->get();

        $data['lan'] = Language::find($request->lan_id);

        return view('page.language.search',$data);


    }
}
