<?php

namespace App\Http\Controllers;

use App\Config_Notification;
use App\Customer;
use Illuminate\Http\Request;

class Config_Notification_CustomerController extends Controller
{
    public function show()
    {
        $data['config_notifications'] =Config_Notification::orderBy('id','desc')->get();

        return view('page.config_notification_customer.list',$data);
    }

    public function create()
    {

        return view('page.config_notification_customer.create');
    }

    public function postcreate(Request $request)
    {
        if(Config_Notification::create($request->all()))
        {
            return redirect()->back()->with('success',lang_data('Create new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Create new error'));
        }
    }

    public function edit($id)
    {
        $data['config_notification'] = Config_Notification::find($id);

        if(empty($data['config_notification']))
        {
            return redirect('404');
        }else{
            return view('page.config_notification_customer.update',$data);
        }
    }

    public function postedit($id,Request $request)
    {
        if (Config_Notification::where('id',$id)->update($request->all()))
        {
            return redirect()->back()->with('success',lang_data('Update new success'));
        }else{
            return redirect()->back()->with('error',lang_data('Update new error'));
        }
    }

    public function destroy($id)
    {
        $check = Config_Notification::find($id);
        if(empty($check))
        {
            return redirect('404');
        }else{
            $check->delete();

            return redirect()->back()->with('success',lang_data('Delete success'));
        }
    }

    public function send($id)
    {
        $check = Config_Notification::where('id',$id)->first();

        if(empty($check))
        {
            return redirect('404');
        }else{
            $all_customer = Customer::where('status_notification','0')->get();


            if(isset($all_customer)){
                foreach ($all_customer as $item)
                {
                    send_mail($item->email,$check->description,$check->title);

                }

            }
            Config_Notification::where('id',$id)->update(['status'=>1]);

            return redirect()->back()->with('success',lang_data('Send success'));
        }
    }
}
