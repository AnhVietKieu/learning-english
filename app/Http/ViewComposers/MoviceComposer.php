<?php
namespace App\Http\ViewComposers;

use App\Customer;
use App\History;
use App\Language;
use App\User_Config;
use Illuminate\View\View;

class MoviceComposer
{
    public $movieList = [];
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->movieList =Language::where('status','1')->get();


    }
    public function compose(View $view)
    {
        $view->with('language', end($this->movieList));
    }
}
