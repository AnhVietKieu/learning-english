<?php
namespace App\Http\ViewComposers;

use App\Config;
use App\Customer;
use App\History;
use App\Language;
use App\User_Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class LanguageConposer
{

    public $user =[] ;
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {

        $meta_value = User_Config::where('user_id',Auth::User()->id)->where('meta_key','language_default')->first();



        if($meta_value)
        {
            $icon=  Language::where('language_code',$meta_value['meta_value'])->pluck('icon');
        }else{
           $lang_code = Config::where('meta_key','language_default')->first();

            $icon=  Language::where('language_code',$lang_code['meta_value'])->pluck('icon');

        }


        $this->user = $icon;
    }
    public function compose(View $view)
    {
        $view->with('user_config',end($this->user));

    }
}
