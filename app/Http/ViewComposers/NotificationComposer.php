<?php
namespace App\Http\ViewComposers;

use App\History;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class NotificationComposer
{
    protected $notification = [];

    public function __construct()
    {
        $this->notification = History::with('lesson')
            ->where('customer_id',Auth::guard('customers')->user()->id)
            ->where('status',1)->where('status_notification',0)->get();
    }

    public function compose(View $view)
    {
        $view->with('notification', end($this->notification));
    }
}
