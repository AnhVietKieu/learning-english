<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class Role extends Model
{
    use Notifiable;
    public static $html ='';
    protected $hidden = [];
    protected $fillable = [
        'name',
        'display'
    ];
    protected $casts = [
        'created_at' => 'date',
        'update_at' => 'date'
    ];

    static function Parent($data,$parent,$str)
    {
        $html='<div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="grid simple ">
                                    <table class="table table-bordered no-more-tables">
                                        <thead class="cf">
                                            <tr>
                                                <th class="text-center" colspan="2">Ten parner id 0</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                            <tr>
                                                <td><input type="checkbox" name="permission_id[]" value=""></td>
                                                <td>Ten </td>
                                            </tr>
                                            
                                            
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>';

        foreach($data as $val){
                if($val->parent_id == $parent){


                    if($val->parent_id==0)
                    {
                        static::$html.='
                        <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="grid simple ">
                                    <table class="table table-bordered no-more-tables">
                                        <thead class="cf">
                                        <tr>
                                            <th class="text-center" colspan="2">'.$val->display.'</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                    ';

                    }else{
                        static::$html.='<tr>
                                            <td style="width: 20px"><input type="checkbox" name="permission_id[]" value="'.$val->id.'"></td>
                                            <td>'.$val->display.'</td>
                                       </tr>
                                       ';
                    }


                    Role::Parent($data,$val->id,'');




                }

        }
        return static::$html;
    }
    static function Old_Parent($data_old,$data_new,$parent,$str)
    {
       if(count($data_old)<1){
           foreach($data_new as $val){
               if($val->parent_id == $parent){
                   static::$html.="<ul class='row' style='display:inline-block; list-style-type: none;'>";
                   if($val->parent_id==0)
                   {
                       static::$html.="<h4 style='text-decoration: underline'>".$val->display."</h4>";
                   }else
                   {
                       static::$html.=" <li><h5><input type='checkbox' name='permission_id[]' value='$val->id'> ".$str.$val->display."</h5></li>";
                   }
                   Role::Old_Parent($data_old,$data_new,$val->id,'');
                   static::$html.= "</ul></li>";
               }
           }
       }else{
           foreach($data_new as $val){
                   if($val->parent_id == $parent){
                       static::$html.="<ul class='row' style='display:inline-block; list-style-type: none;'>";
                       if($val->parent_id==0)
                       {
                           static::$html.="<h4 style='text-decoration: underline'>".$val->display."</h4>";
                       }else{
                           static::$html.="<li><h5><input type='checkbox' name='permission_id[]' value='$val->id'";
                           if($data_old->contains($val->id)){
                               static::$html.='checked';

                           }
                           static::$html.=">".$str.$val->display."</h5></li>";
                       }

                       Role::Old_Parent($data_old,$data_new,$val->id,' ');
                       static::$html.= "</ul></li>";
                   }
           }
       }
        return static::$html;
    }

    static  function checkpermissions($role){
        $test ='';
        $ListRoleOfUser =User::where('id',Auth::User()->id)->select('role_id')->first();

        $Role_id =Role::select('permission_roles.permission_id')
            ->join('permission_roles','roles.id','=','permission_roles.role_id')
            ->join('permissions','permissions.id','=','permission_roles.permission_id')
            ->where('roles.id',$ListRoleOfUser->role_id)->pluck('permission_id')->unique();

        for($i=0;$i<count($role);$i++){
            if($Role_id->contains($role[$i])) {
                $test ='true';
            }else{
                $test ='false';
            }
        }
        if(isset($test)){
            return $test;
        }else{
            return false;
        }

    }

}
