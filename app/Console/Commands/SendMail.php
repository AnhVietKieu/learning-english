<?php

namespace App\Console\Commands;

use App\Config_Notification;
use App\Customer;
use Illuminate\Console\Command;

class SendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $config = Config_Notification::where('status',0)->get();

       if($config){
           foreach ($config as $item)
           {
               if(strtotime($item['time'])<= time())
               {
                   $all_customer = Customer::where('status_notification','0')->get();

                   foreach ($all_customer as $customer)
                   {
                       send_mail($customer->email,$item->description,$item->title);
                   }

                   Config_Notification::where('id',$item->id)->update(['status'=>1]);

               }
           }
       }


    }
}
