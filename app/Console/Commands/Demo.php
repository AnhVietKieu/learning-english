<?php

namespace App\Console\Commands;

use App\Coures;
use App\Lessons;
use Illuminate\Console\Command;

class Demo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'test thanhf cong !';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $lesson = Lessons::with('reads')->with('listens')->with('writes')->with('speaks')->get();

        if(isset($lesson))
        {
            foreach( $lesson as $item)
            {
                $score =0;

                foreach($item['reads'] as $read)
                {
                    $score +=$read['score'];
                }

                foreach($item['listens'] as $listen)
                {
                    $score +=$listen['score'];
                }
                foreach($item['writes'] as $write)
                {
                    $score +=$write['score'];
                }
                foreach($item['speaks'] as $speak)
                {
                    $score +=$speak['score'];
                }

                Lessons::find($item['id'])->update(['score'=>$score]);
            }
        }

    }
}
