<?php

namespace App\Console\Commands;

use App\Customer;
use App\Levels;
use Illuminate\Console\Command;

class UpdateLevel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'level:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $level =Levels::orderby('score')->get();

        $customer = Customer::with(['history' => function ($query) {
            $query->where('status',1);
        }])->get();

        if(isset($customer))
        {
            foreach ($customer as $item) {
                $score = 0;

                if (count($item['history']) >= 1) {
                    foreach ($item['history'] as $item1) {
                        $score += $item1['score'];
                    }

                    Customer::find($item->id)->update(['score' => $score]);
                }
            }


            foreach ($customer as $item) {

                foreach ($level as $item2)
                {
                    if($item['score']-$item2['score']>=0)
                    {
                        Customer::find($item->id)->update(['level_id'=>$item2['id']]);
                    }elseif($item['score']>=$item2['score'])
                    {
                        Customer::find($item->id)->update(['level_id'=>$item2['id']]);
                    }

                }
            }

        }
    }
}
