<?php

use App\Language;
use App\Permission;
use App\Role;
use App\User;
use App\History_User;
use App\User_Config;
use Illuminate\Support\Facades\Auth;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Database\Eloquent\Model;
use App\Config;
use Symfony\Component\HttpFoundation\Session;
use App\Language_Data;
use App\PermissionRole;
use App\Config_Notification;

    function send_mail($email,$body,$title)
    {
        $username = Config::where('meta_key', 'email_send')->first();
        $password = Config::where('meta_key', 'email_send_password')->first();

        $mail = new PHPMailer(true);
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = $username->meta_value;                 // SMTP username
            $mail->Password = $password->meta_value;                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;
            $mail->CharSet = 'UTF-8';
            //Recipients
            $mail->setfrom($username->meta_value, $title);
            $mail->addAddress($email);  // Add a recipient
            $mail->addreplyto('info@example.com', 'Information');

            $mail->isHTML(true);
            $mail->Subject = $title;
            $mail->Body = $body;

            $mail->send();


        } catch (phpmailerException $e) {
            dd($e);
        } catch (Exception $e) {
            dd($e);
        }
        if($mail){
            return true;
        }else{
            return false;
        }
    }

    function utf8convert($str) {

        if(!$str) return false;

        $utf8 = array(

            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'd'=>'đ|Đ',

            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',

            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach($utf8 as $ascii=>$uni)
            $str = preg_replace("/($uni)/i",$ascii,$str);

        return $str;

    }

    function convert_slug($text){
        $text = strtolower(utf8convert($text));
        $text = str_replace( "ß", "ss", $text);
        $text = str_replace( "%", "", $text);
        $text = preg_replace("/[^_a-zA-Z0-9 -] /", "",$text);
        $text = str_replace(array('%20', ' '), '-', $text);
        $text = str_replace("----","-",$text);
        $text = str_replace("---","-",$text);
        $text = str_replace("--","-",$text);
        return $text.'.html';
    }

    function menu_permission($role)
    {
        $ListRoleOfUser =User::where('id',Auth::User()->id)->select('role_id')->first();

        $Role_id =Role::select('permission_roles.permission_id')
            ->join('permission_roles','roles.id','=','permission_roles.role_id')
            ->join('permissions','permissions.id','=','permission_roles.permission_id')
            ->where('roles.id',$ListRoleOfUser->role_id)->pluck('permission_id')->unique();

        $checkPermission = Permission::where('name',$role)->value('id');

        if($Role_id->contains($checkPermission))
        {
            return true;

        }else{
            return false;
        }
    }

    function lang_data($var)
    {

       if(isset(Auth::User()->id))
       {
           $meta_value = User_Config::where('user_id',Auth::User()->id)->where('meta_key','language_default')->first();
       }
        if(isset($meta_value))
        {
            $id=  Language::where('language_code',$meta_value['meta_value'])->pluck('id');
        }else{
            $language_code =Config::where('meta_key','language_default')->first();

            $id= Language::where('language_code',$language_code['meta_value'])->pluck('id');

        }
        $data_value = Language_Data::where('lan_id',$id)->where('lan_data',ucfirst(strtolower($var)))->first();


        if($data_value['lan_value'])
        {
            return $data_value['lan_value'];;
        }else{
            return "Unknown";
        }

    }

    function lan_id()
    {
        $meta_value = Config::where('meta_key','language_default')->first();

        if($meta_value)
        {
           $lan = Language::where('language_code',$meta_value['meta_value'])->first();
            return $lan;
        }else{
            return false;
        }
    }

    function meta_value_language($lan_data,$lan_id=0)
    {
       $lan_value = Language_Data::where('lan_id',$lan_id)->where('lan_data',$lan_data)->first();

       if($lan_value)
       {
           return $lan_value['lan_value'];
       }else{
           return '';
       }


    }

    function action_history($lesson_id)
    {
        $check =History_User::where('customer_id',Auth::guard('customers')->user()->id)
            ->where('lesson_id',$lesson_id)->first();

        if(isset($check))
        {
            return true;
        }else{
            return false;
        }
    }

    function permission_parent($parent_id)
    {
       $permission= PermissionRole::select('permissions.id','permissions.name','permissions.display')
           ->join('permissions','permissions.id','=','permission_roles.permission_id')
           ->where('role_id',session('role_id'))
           ->where('parent_id',$parent_id)->get();
//       Permission::where('parent_id',$parent_id)->get();

//        dd($permission->toArray());

       if($permission)
       {
           return $permission->toArray();
       }else{
           return [];
       }

    }

?>
