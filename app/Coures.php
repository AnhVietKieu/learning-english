<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Coures extends Model
{
    protected $fillable = [
        'id',
        'name',
        'image',
        'level_id',
        'description',
        'parent_id',
        'status',
        'created_by',
        'updated_by'
    ];
    public static $html = '';

    public function lessons()
    {
        return $this->belongsTo(Lessons::class, 'id', 'coure_id');
    }

    public function level()
    {
        return $this->belongsTo(Levels::class, 'level_id', 'id');
    }


    static function lesson_count($coure_id)
    {
        return Lessons::where('coure_id',$coure_id)->count();
    }

    static function menuParent($data, $parent, $str)
    {
        foreach ($data as $val) {
            if ($val->parent_id == $parent) {
                static::$html .= "
                    <tr>
                       <td class='v-align-middle'>
                        <input type='checkbox' name='permission_id[]' value='$val->id' class='checkbox delete-user'>
                    </td>
                     
                     <td>" . $str . $val->name . "</td>
                     <td><img src=".asset('public/upload/coure/'.$val->image)." width='50px' height='50px'></td>
                    <td ><div style='overflow: auto;height:200px;width: 200px;'>" .$val->description . "</div></td>
                    <td style='width: 200px; text-align: center'>";

                        if($val->status ==0){
                           static::$html.="<p class='alert alert-success'><span>Hoạt động</span></p>";
                        }else{
                           static::$html.="<p class='alert alert-danger'><span>Không hoạt động</span></p>";
                        }
                    static::$html.="</td>
                            <td><img src=".asset('public/upload/level/'.$val['level']['image'])." width='50px' height='50px'></td>
                    <td><span>".User::find_user($val->created_by)."</span></td>
                    <td><span>".User::find_user($val->updated_by)."</span></td>
                    <td class='center'>";
                if(menu_permission('per_edit_coure')==true){
                    static::$html.="<a  href='coure/edit/" . $val->id . "'>
                    <button class='btn btn-success' type='button'><i class='glyphicon glyphicon-edit'></i></button>
                    </a>";
                }
                if(menu_permission('per_delete_coure')==true){
                    static::$html.=
                    "<a  href='coure/delete/" . $val->id . "'>
                       <button class='btn btn-danger' onclick=\"return confirm('Bạn có chắc chắn muốn xóa??')\" type='button'><i class='glyphicon glyphicon-trash'></i></button>
                    </a>";
                }
                static::$html.="
                     </td>
                    </tr>";

                Coures::menuParent($data, $val->id, '---- ');
            }
        }

        return static::$html;
    }

    static function learn_menu($data,$parent,$str)
    {



        foreach($data as $key)
        {
            if($key->parent_id==$parent)
            {
                if($key->parent_id!=0)
                {
                    $str = '<a href="'.route('FE_lesson',$key->id).'" style="text-decoration: none;">
                <div class="col-lg-6 col-md-4 col-sm-12" id="hover">
                    <div class="card author-box card-success">
                    <div >
                        <div class="author-box-left" style="padding-right:10px; padding-left: 0px;">
                          <img alt="image" width="160px" height="180px" src='.asset('public/upload/coure/'.$key->image).'>
                          <div class="clearfix"></div>
                      </div>
                    <div class="author-box-details" style="box-sizing: border-box;">
                      <div class="author-box-job" style="padding-top:5px;"><h4>'.$key->name.'</h4></div>
                      <div class="author-box-description" style="overflow: hidden;height: 100px;color: black;">
                        <p>'.$key->description.'</p>
                      </div>
                      <div  style="color: black;"> <u> Số bài học: </u><b style="color: red;">'.static::lesson_count($key->id).'</b></div>
             
                    </div>
                  </div>
                </div>
             
                </a>
              
                </div>';
                }

                if($key->parent_id==0){
                static::$html.='
                <div class="section-header" style="border: 1px solid #D9D9D9">
                <h1>'.$key->name;
                static::$html.='</h1></div><div class="row">';
                }elseif($key->parent_id!=0){
                    static::$html.=$str;

                }

                Coures::learn_menu($data, $key->id, $str);
                if($key->parent_id==0)
                {
                    static::$html.='</div>';
                }

            }


        }


        return static::$html;
    }

}
