<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Config extends Model
{
    use Notifiable;
    protected $hidden = [];
    protected $fillable = [
        'meta_value',
        'meta_key',
        '_token'
    ];
    protected $casts = [
        'created_at' => 'date',
        'update_at' => 'date'
    ];
    static function value($meta_key){
        $value=Config::where('meta_key',$meta_key)->first();

        return $value->meta_value;
    }
}
