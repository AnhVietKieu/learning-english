<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language_Data extends Model
{
    protected $table='language_data';

    protected $fillable = [
        'lan_id',
        'lan_data',
        'lan_value',
        'created_by',
        'updated_by'
    ];
}
