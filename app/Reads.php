<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reads extends Model
{
    protected $fillable = [
        'id',
        'name',
        'question',
        'answer_a',
        'answer_b',
        'answer_c',
        'answer_d',
        'answer_true',
        'lesson_id',
        'status',
        'score',
        'note',
        'created_by',
        'updated_by'
    ];

    public function lessons()
    {
        return $this->belongsTo(Lessons::class,'lesson_id','id');
    }
    static function lesson_read($reads)
    {
        $html='';
        $i=1;
        foreach ($reads as $read){
            $html.='<label class="form-label">Câu hỏi '.$i++.': '.$read->question.'</label>
                                  
                                    <div class="grid-body no-border">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Đáp án A:</label>
                                                    <span class="help"><input type="radio" value="answer_a" name="answer_read['.$read->id.']"></span>
                                                    <div class="controls">
                                                        <input type="text" value="'.$read->answer_a.'" disabled class="form-control" name="answer_a" id="phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Đáp án C:</label>
                                                    <span class="help"><input type="radio" value="answer_c" name="answer_read['.$read->id.']"></span>
                                                    <div class="controls">
                                                        <input type="text" value="'.$read->answer_c.'" disabled class="form-control" name="answer_c" id="tin">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Đáp án B:</label>
                                                    <span class="help"><input type="radio" value="answer_b" name="answer_read['.$read->id.']"></span>
                                                    <div class="controls">
                                                        <input type="text" value="'.$read->answer_b.'" disabled class="form-control auto" name="answer_b" data-a-sign="$ ">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Đáp án D:</label>
                                                    <span class="help"><input type="radio" value="answer_d" name="answer_read['.$read->id.']"></span>
                                                    <div class="controls">
                                                        <input type="text" value="'.$read->answer_d.'" disabled class="form-control auto" name="answer_d" data-v-max="9999" data-v-min="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>';

        }

        return $html;
    }

}
