<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'address',
        'phone',
        'birthday',
        'gender',
        'avatar',
        'role_id',
        'status',
        'key'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birthday' => 'date',
        'created_at' => 'date',
        'updated_at' => 'date'
    ];
    static function find_user($id)
    {
        $user = User::find($id);
        if(isset($user)){
            return $user->name;
        }
    }

    public function role()
    {
        return $this->belongsTo(Role::class,'role_id','id');
    }

    static function search_not($request)
    {
        $sql = User::with('role');

        switch ($request->filter){
            case 'phone':
                $user = $sql->where('phone',trim($request->search))->paginate(Config::value('pagination'));
                break;
            case 'email':
                $search=str_replace("%40",'@',$request->search);
                $user = $sql->where('email','like','%'.$search.'%')->paginate(Config::value('pagination'));
                break;
            case 'name':
                $user = $sql->where('name','like','%'.trim($request->search).'%')
                    ->paginate(Config::value('pagination'));
            default:
                $user = $sql->paginate(Config::value('pagination'));
                break;
        }

        return $user;


    }
}
