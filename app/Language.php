<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table='language';

    protected $fillable = [
        'language',
        'language_code',
        'icon',
        'status',
        'created_by',
        'updated_by'
    ];
}
