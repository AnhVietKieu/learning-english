-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 17, 2020 lúc 07:48 AM
-- Phiên bản máy phục vụ: 10.4.8-MariaDB
-- Phiên bản PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `newshop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(1) DEFAULT 0 COMMENT '0-Hoạt động 1- Không hoạt động',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Man', 0, 0, '2019-12-28 00:01:35', '2020-01-01 03:29:08'),
(12, 'Hunter', 1, 0, '2020-01-01 03:28:57', '2020-01-01 03:28:57'),
(13, 'Bitit', 2, 0, '2020-01-02 02:16:56', '2020-01-02 02:16:56'),
(19, 'Keagan Gislason', 0, 1, '2020-01-07 17:38:15', '2020-01-09 20:19:07'),
(23, 'Miss Keara Stracke', 0, 1, '2020-01-07 17:38:15', '2020-01-09 20:16:39'),
(25, 'Content', 16, 0, '2020-01-07 17:40:23', '2020-01-07 17:40:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `colors`
--

CREATE TABLE `colors` (
  `id` int(11) UNSIGNED NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(1) DEFAULT 0 COMMENT '0 - Hoạt động 1 Khoá',
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `colors`
--

INSERT INTO `colors` (`id`, `color`, `created_at`, `updated_at`, `status`, `_token`) VALUES
(7, 'LightSteelBlue', '2020-01-01 17:14:35', '2020-01-01 17:14:35', 0, NULL),
(11, 'SteelBlue', '2020-01-07 17:37:54', '2020-01-14 18:54:23', 0, '6PFDbotEKOtylSyHK0bRXdEzYRKiLh3bp4XTH06W'),
(12, 'LightBlue', '2020-01-14 18:46:45', '2020-01-14 18:46:45', 0, NULL),
(13, 'pink', '2020-01-14 19:01:52', '2020-01-14 19:01:52', 0, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `configs`
--

CREATE TABLE `configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `configs`
--

INSERT INTO `configs` (`id`, `meta_key`, `meta_value`, `created_at`, `updated_at`, `_token`) VALUES
(11, 'email_ssl', 'kieuvietanh8597@gmail.com', NULL, NULL, ''),
(12, 'email_cmc', 'kieuvietanh85@gmail.com', NULL, NULL, '0aEOYM5ITRrBIcPRu1DwjHDIki9dKuN6kMRPUZQe'),
(13, 'email_send', 'norepmoninet@gmail.com', NULL, NULL, ''),
(14, 'email_send_password', 'iaegzlcvfkqvmxyp', NULL, NULL, ''),
(15, 'email_send_title', 'Moninet', NULL, NULL, ''),
(16, 'price_scan', '150000', NULL, NULL, ''),
(17, 'supplier', 'CMC Global', NULL, NULL, ''),
(18, 'pagination', '12', NULL, '2020-01-09 03:03:13', 'z2epR1eJtmvZrdcEVVhgvJDdyu0mXAKd23KNLJCC'),
(19, 'pagination_mini', '1', NULL, '2020-01-08 04:47:49', 'VzQRbsTdfLnYSOmQ0BHotkl7QZKWnZFMfmAwlHFz'),
(20, 'subject_ssl', 'Đăng ký ssl', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `config_notifications`
--

CREATE TABLE `config_notifications` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '0: chưa gửi 1 đã gửi',
  `_token` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `config_notifications`
--

INSERT INTO `config_notifications` (`id`, `title`, `description`, `status`, `_token`, `updated_at`, `created_at`) VALUES
(2, 'title_invoice', '<div class=\"row\">\r\n<div class=\"col-md-11\">\r\n<div class=\"grid simple\">\r\n<div class=\"grid-body invoice-body no-border\">&nbsp;\r\n<div class=\"pull-left\">\r\n<address><strong>Twitter, Inc.</strong><br />\r\n795 Folsom Ave, Suite 600<br />\r\nSan Francisco, CA 94107<br />\r\nP: (123) 456-7890</address>\r\n</div>\r\n\r\n<div class=\"pull-right\">\r\n<h2>H&Oacute;A ĐƠN</h2>\r\n</div>\r\n\r\n<div class=\"clearfix\">&nbsp;</div>\r\n<br />\r\n<br />\r\n&nbsp;\r\n<div class=\"row\">\r\n<div class=\"col-md-9\">\r\n<h4>Daren forthway</h4>\r\n\r\n<address><strong>Webarch Holdings.</strong><br />\r\n795 Folsom Ave, Suite 600<br />\r\nSan Francisco, CA 94107<br />\r\nP: (123) 456-7890</address>\r\n</div>\r\n\r\n<div class=\"col-md-3\">&nbsp;\r\n<div>\r\n<div class=\"pull-left\">INVOICE NO :</div>\r\n\r\n<div class=\"pull-right\">000985</div>\r\n\r\n<div class=\"clearfix\">&nbsp;</div>\r\n</div>\r\n\r\n<div>\r\n<div class=\"pull-left\">DATE :</div>\r\n\r\n<div class=\"pull-right\">15/02/13</div>\r\n\r\n<div class=\"clearfix\">&nbsp;</div>\r\n</div>\r\n&nbsp;\r\n\r\n<div>\r\n<div class=\"pull-left\">Customer No.</div>\r\n\r\n<div style=\"text-decoration:underline\">84,000 USD</div>\r\n\r\n<div class=\"clearfix\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 0, 'uriV5POZA3RnVLtRyq5NF3IFMVDqX6h5puCoiP2d', '2020-01-02 01:51:39', '2020-01-02 01:37:53'),
(4, 'body_invoice', '<table class=\"table\">\r\n	<thead>\r\n		<tr>\r\n			<th style=\"width:60px\">QTY</th>\r\n			<th style=\"width:100px\">ITEM ID</th>\r\n			<th>DESCRIPTION</th>\r\n			<th style=\"width:140px\">UNIT PRICE</th>\r\n			<th style=\"width:90px\">AMOUNT</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>1</td>\r\n			<td>ID 1</td>\r\n			<td>iPhone 5 32GB White &amp; Silver (GSM) Unlocked</td>\r\n			<td>$749.00</td>\r\n			<td>$749.00</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"3\" rowspan=\"4\">&nbsp;</td>\r\n			<td><strong>TOTAL DUE</strong></td>\r\n			<td>$1607.00</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h5>Designer identity</h5>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h5>John Smith</h5>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', 0, 'uriV5POZA3RnVLtRyq5NF3IFMVDqX6h5puCoiP2d', '2020-01-02 01:52:25', '2020-01-02 01:52:25'),
(5, 'Thông báo sản phẩm mới', 'Sản phẩm mới : {product}', 1, 'uriV5POZA3RnVLtRyq5NF3IFMVDqX6h5puCoiP2d', '2020-01-07 03:04:55', '2020-01-02 02:37:31'),
(6, 'Thông báo', '<p>-H&agrave;m n&agrave;y cũng c&oacute; t&aacute;c dụng tạo ta c&aacute;c th&ocirc;ng b&aacute;o tr&ecirc;n m&agrave;n h&igrave;nh như h&agrave;m&nbsp;<code>alert()</code>&nbsp;v&agrave; đồng thời k&egrave;m theo 2 button OK, hủy(t&ugrave;y theo ng&ocirc;n ngữ của tr&igrave;nh duyệt c&oacute; thể sẽ kh&aacute;c). Khi của sổ hiện n&ecirc;n nếu như người d&ugrave;ng chọn OK th&igrave; n&oacute; sẽ trả về TRUE v&agrave; ngược lại th&igrave; n&oacute; sẽ trả về FALSE.</p>\r\n\r\n<p><img alt=\"confirm trong javascript\" src=\"https://toidicode.com/upload/images/javascript/confirm-trong-javascript.jpg\" /></p>\r\n\r\n<p><strong>C&uacute; ph&aacute;p</strong>:</p>\r\n\r\n<pre>\r\n<code>confirm(content);</code></pre>', 0, 'KGSe8csNwqK1zojehgXDsotp4arKHRFLT3PwlxbR', '2020-01-13 19:00:53', '2020-01-13 18:45:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_notification` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0-có 1-không',
  `customer_type` int(1) DEFAULT 0 COMMENT '0-bình thương 1 - vip',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-Hoạt động 1- Bị khoá 2 Không hoạt động',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `name`, `username`, `password`, `email`, `address`, `phone`, `birthday`, `gender`, `status_notification`, `customer_type`, `status`, `created_at`, `updated_at`, `_token`) VALUES
(3, 'Marlene Fisher', 'sylvan.pagac', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kieuvietanh851997@gmail.com', '16116 Ronny AlleyXavierborough, WI 28097-6169', '0976820967', '852076800', '0', '0', 1, 0, '2019-12-28 00:01:35', '2020-01-01 18:00:10', ''),
(4, 'Cayla Paucek Jr.', 'sonia.cassin', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kieuvietanh8597@gmail.com', '39166 Jannie Cape Apt. 101Cruickshankbury, IA 84067', '0976820967', '0', '1', '0', 0, 0, '2019-12-28 00:01:35', '2020-01-01 04:04:19', ''),
(6, 'Thuaan', NULL, NULL, 'ducthuan181295@gmail.com', 'HN', '', '-2208988800', '1', '0', 0, 0, '2020-01-13 18:50:15', '2020-01-15 02:02:21', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `delivery_notes`
--

CREATE TABLE `delivery_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_of_delivery_note` date NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `delivery_price` int(11) NOT NULL DEFAULT 0,
  `status_pay` int(1) DEFAULT 1 COMMENT '0-Thanh toán 1-Chưa thanh toán',
  `delivery_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1 COMMENT '0-Giao hàng 1- Chưa giao hàng',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `delivery_notes`
--

INSERT INTO `delivery_notes` (`id`, `date_of_delivery_note`, `customer_id`, `user_id`, `total`, `delivery_price`, `status_pay`, `delivery_date`, `status`, `created_at`, `updated_at`) VALUES
(1, '1994-07-08', 3, 1, 34921, 5, 1, NULL, 0, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(2, '1983-02-08', 4, 1, 46930, 84, 1, NULL, 2, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(3, '1994-08-03', 4, 1, 39472, 3, 1, NULL, 1, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(4, '1980-08-03', 4, 1, 74099, 13, 1, NULL, 0, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(5, '2005-09-22', 3, 1, 97246, 67, 0, NULL, 2, '2020-01-07 18:07:33', '2020-01-15 16:48:13'),
(6, '2020-01-16', 3, 1, 1, 0, 1, '2018/1/1', 0, '2020-01-15 19:19:53', '2020-01-15 19:19:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `delivery_note_details`
--

CREATE TABLE `delivery_note_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `delivery_note_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT 0,
  `tax` int(11) DEFAULT NULL,
  `color_id` int(12) DEFAULT NULL,
  `size_id` int(12) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `delivery_note_details`
--

INSERT INTO `delivery_note_details` (`id`, `delivery_note_id`, `product_id`, `quantity`, `price`, `tax`, `color_id`, `size_id`, `created_at`, `updated_at`) VALUES
(1, 5, 2, 89, 96622, 2, NULL, NULL, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(3, 1, 5, 53, 83357, 21, NULL, NULL, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(4, 6, 7, 1, 1, NULL, 11, 7, '2020-01-15 19:19:53', '2020-01-15 19:19:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `headers`
--

CREATE TABLE `headers` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_12_24_074901_create_categories_table', 1),
(4, '2019_12_24_075110_create_products_table', 1),
(5, '2019_12_24_095058_create_roles_table', 1),
(6, '2019_12_25_023512_create_permissions_table', 1),
(7, '2019_12_25_023619_create_permission_roles_table', 1),
(8, '2019_12_25_023719_create_customers_table', 1),
(9, '2019_12_25_024311_create_size_products_table', 1),
(10, '2019_12_25_024439_create_sizes_table', 1),
(11, '2019_12_25_024530_create_photos_table', 1),
(12, '2019_12_25_024546_create_photo_products_table', 1),
(13, '2019_12_25_024558_create_colors_table', 1),
(14, '2019_12_25_024612_create_color_products_table', 1),
(15, '2019_12_25_024627_create_receipts_table', 1),
(16, '2019_12_25_024640_create_receipt_details_table', 1),
(17, '2019_12_25_024653_create_suppliers_table', 1),
(18, '2019_12_25_024722_create_delivery_notes_table', 1),
(19, '2019_12_25_024738_create_delivery_note_details_table', 1),
(20, '2019_12_25_024750_create_configs_table', 1),
(21, '2019_12_25_024802_create_headers_table', 1),
(22, '2019_12_25_024822_create_revenues_table', 1),
(23, '2019_12_25_030120_add_users_to_all_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display`, `parent_id`, `created_at`, `updated_at`, `_token`) VALUES
(1, '', 'Cấp quyền', 0, '2019-12-28 00:01:35', '2019-12-30 21:10:28', NULL),
(3, 'per_delete_role', 'Xoá role', 1, '2019-12-28 00:01:35', '2019-12-29 21:18:20', NULL),
(4, 'per_edit_role', 'Sửa role', 1, '2019-12-28 00:01:35', '2019-12-28 00:01:35', NULL),
(6, '', 'Cấp quyền hạn', 0, '2019-12-29 03:21:35', '2019-12-29 03:21:35', NULL),
(7, 'per_create_permission', 'Thêm quyền', 6, '2019-12-29 03:21:35', '2019-12-29 03:21:35', NULL),
(8, 'per_delete_permission', 'Xoá quyền', 6, '2019-12-29 03:21:35', '2019-12-29 03:21:35', NULL),
(9, 'per_edit_permission', 'Sửa quyền', 6, '2019-12-29 03:21:35', '2019-12-29 03:21:35', NULL),
(11, 'per_list_role', 'Danh sách quyền', 1, NULL, NULL, NULL),
(12, 'per_list_permission', 'Danh sách quyền hạn', 6, NULL, NULL, NULL),
(98, '', 'Chức năng khách hàng', 0, '2019-12-30 03:59:26', '2019-12-30 03:59:26', NULL),
(99, 'per_list_customer', 'Danh sách khách hàng', 98, '2019-12-30 03:59:51', '2019-12-30 04:00:19', NULL),
(100, 'per_create_customer', 'Thêm khách hàng', 98, '2019-12-30 04:01:24', '2019-12-30 04:01:24', NULL),
(101, 'per_edit_customer', 'Sửa thông tin khách hàng', 98, '2020-01-16 12:42:17', '2020-01-16 12:42:17', NULL),
(102, 'per_delete_customer', 'Xoá khách hàng', 98, '2020-01-16 12:42:36', '2020-01-16 12:42:36', NULL),
(106, NULL, 'Chức năng nhân viên', 0, '2020-01-16 12:49:49', '2020-01-16 12:49:49', NULL),
(107, 'per_list_employess', 'Danh sách nhân viên', 106, '2020-01-16 12:50:33', '2020-01-16 12:51:03', NULL),
(108, 'per_create_employess', 'Thêm nhân viên', 106, '2020-01-16 12:51:39', '2020-01-16 12:53:24', NULL),
(109, 'per_edit_employess', 'Sửa nhân viên', 106, '2020-01-16 12:52:58', '2020-01-16 12:53:43', NULL),
(110, 'per_delete_employess', 'Xoá nhân viên', 106, '2020-01-16 12:58:30', '2020-01-16 12:58:30', NULL),
(111, NULL, 'Chức năng sản phẩm', 0, '2020-01-16 12:59:12', '2020-01-16 12:59:12', NULL),
(112, 'per_list_product', 'Danh sách sản phẩm', 111, '2020-01-16 12:59:38', '2020-01-16 12:59:38', NULL),
(113, 'per_create_product', 'Thêm sản phẩm', 111, '2020-01-16 12:59:55', '2020-01-16 12:59:55', NULL),
(114, 'per_edit_product', 'Sửa sản phẩm', 111, '2020-01-16 13:00:09', '2020-01-16 13:00:09', NULL),
(115, 'per_delete_product', 'Xoá sản phẩm', 111, '2020-01-16 13:00:21', '2020-01-16 13:00:21', NULL),
(116, NULL, 'Chức năng của màu sắc', 0, '2020-01-16 13:00:53', '2020-01-16 13:00:53', NULL),
(117, 'per_list_color', 'Danh sách màu sắc', 116, '2020-01-16 13:01:08', '2020-01-16 13:01:08', NULL),
(118, 'per_create_color', 'Thêm màu', 116, '2020-01-16 13:01:25', '2020-01-16 13:01:25', NULL),
(119, 'per_edit_color', 'Sửa màu', 116, '2020-01-16 13:01:36', '2020-01-16 13:01:36', NULL),
(120, 'per_delete_color', 'Xoá màu', 116, '2020-01-16 13:01:47', '2020-01-16 13:01:47', NULL),
(121, NULL, 'Chức năng kích thước', 0, '2020-01-16 13:02:11', '2020-01-16 13:02:11', NULL),
(122, 'per_list_size', 'Danh sách kích thước', 121, '2020-01-16 13:02:41', '2020-01-16 13:02:41', NULL),
(123, 'per_create_size', 'Thêm kích thước', 121, '2020-01-16 13:03:04', '2020-01-16 13:03:04', NULL),
(124, 'per_edit_size', 'Sửa kích thước', 121, '2020-01-16 13:03:18', '2020-01-16 13:03:18', NULL),
(125, 'per_delete_size', 'Xoá kích thước', 121, '2020-01-16 13:03:35', '2020-01-16 13:03:35', NULL),
(126, NULL, 'Chức năng danh mục', 0, '2020-01-16 13:03:56', '2020-01-16 13:03:56', NULL),
(127, 'per_list_category', 'Danh sách danh mục', 126, '2020-01-16 13:04:19', '2020-01-16 13:04:19', NULL),
(128, 'per_create_category', 'Thêm danh mục', 126, '2020-01-16 13:04:34', '2020-01-16 13:04:34', NULL),
(129, 'per_edit_category', 'Sửa danh mục', 126, '2020-01-16 13:04:48', '2020-01-16 13:04:48', NULL),
(130, 'per_delete_category', 'Xoá danh mục', 126, '2020-01-16 13:05:25', '2020-01-16 13:05:25', NULL),
(131, NULL, 'Chức năng hoá đơn', 0, '2020-01-16 13:21:47', '2020-01-16 13:21:47', NULL),
(132, 'per_list_receipt', 'Danh sách hoá đơn', 131, '2020-01-16 13:22:46', '2020-01-16 13:22:46', NULL),
(133, 'per_create_receipt', 'Thêm hoá đơn', 131, '2020-01-16 13:23:00', '2020-01-16 13:23:00', NULL),
(134, 'per_edit_receipt', 'Sửa hoá đơn', 131, '2020-01-16 13:23:13', '2020-01-16 13:23:13', NULL),
(135, 'per_delete_receipt', 'Xoá hoá đơn', 131, '2020-01-16 13:23:23', '2020-01-16 13:23:23', NULL),
(136, NULL, 'Chức năng phiếu giao dịch', 0, '2020-01-16 13:23:54', '2020-01-16 13:23:54', NULL),
(137, 'per_list_delivery', 'Danh sách phiếu giao dịch', 136, '2020-01-16 13:24:15', '2020-01-16 13:24:15', NULL),
(138, 'per_create_delivery', 'Thêm phiếu giao dịch', 136, '2020-01-16 13:24:31', '2020-01-16 13:24:31', NULL),
(139, 'per_edit_delivery', 'Sửa phiếu giao dịch', 136, '2020-01-16 13:24:55', '2020-01-16 13:24:55', NULL),
(140, 'per_delete_delivery', 'Xoá phiếu giao dịch', 136, '2020-01-16 13:25:12', '2020-01-16 13:25:12', NULL),
(141, NULL, 'Chức năng cài đặt', 0, '2020-01-16 13:25:28', '2020-01-16 13:25:28', NULL),
(142, 'per_list_config', 'Danh sách cấu hình', 141, '2020-01-16 13:25:54', '2020-01-16 13:25:54', NULL),
(143, 'per_create_config', 'Thêm cài đặt', 141, '2020-01-16 13:26:13', '2020-01-16 13:26:13', NULL),
(144, 'per_edit_config', 'Sửa cài đặt', 141, '2020-01-16 13:26:33', '2020-01-16 13:26:33', NULL),
(145, 'per_delete_config', 'Xoá cài đặt', 141, '2020-01-16 13:26:58', '2020-01-16 13:26:58', NULL),
(146, NULL, 'Chức năng thông báo', 0, '2020-01-16 13:28:33', '2020-01-16 13:28:33', NULL),
(147, 'per_list_config_notification', 'Danh sách thông báo', 146, '2020-01-16 13:29:02', '2020-01-16 13:29:02', NULL),
(148, 'per_create_config_notification', 'Thêm thông báo', 146, '2020-01-16 13:29:23', '2020-01-16 13:29:23', NULL),
(149, 'per_edit_config_notification', 'Sửa thông báo', 146, '2020-01-16 13:29:35', '2020-01-16 13:29:35', NULL),
(150, 'per_delete_config_notification', 'Xoá thông báo', 146, '2020-01-16 13:29:47', '2020-01-16 13:29:47', NULL),
(151, 'per_list_config_notification_send', 'Gửi thông báo', 146, '2020-01-16 13:30:09', '2020-01-16 13:30:09', NULL),
(152, NULL, 'Chức năng nhà cung cấp', 0, '2020-01-16 14:24:59', '2020-01-16 14:24:59', NULL),
(153, 'per_list_supplier', 'Danh sách nhà cung cấp', 152, '2020-01-16 14:25:22', '2020-01-16 14:25:22', NULL),
(154, 'per_create_supplier', 'Thêm nhà cung cấp', 152, '2020-01-16 14:25:35', '2020-01-16 14:25:35', NULL),
(155, 'per_edit_supplier', 'Sửa nhà cung cấp', 152, '2020-01-16 14:25:54', '2020-01-16 14:25:54', NULL),
(156, 'per_delete_supplier', 'Xoá nhà cung cấp', 152, '2020-01-16 14:26:08', '2020-01-16 14:26:08', NULL),
(157, NULL, 'Nhà kho', 0, '2020-01-16 14:38:48', '2020-01-16 14:38:48', NULL),
(158, 'per_list_storehouse', 'Danh sách kho', 157, '2020-01-16 14:39:08', '2020-01-16 14:39:08', NULL),
(159, 'per_list_clean', 'Danh sách thanh lọc', 157, '2020-01-16 14:39:39', '2020-01-16 14:39:39', NULL),
(160, 'per_delete_clean', 'Xoá dữ liệu', 157, '2020-01-16 14:39:57', '2020-01-16 14:39:57', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_roles`
--

CREATE TABLE `permission_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permission_roles`
--

INSERT INTO `permission_roles` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(361, 3, 2, NULL, NULL),
(362, 4, 2, NULL, NULL),
(363, 11, 2, NULL, NULL),
(364, 7, 2, NULL, NULL),
(365, 8, 2, NULL, NULL),
(366, 9, 2, NULL, NULL),
(367, 12, 2, NULL, NULL),
(368, 99, 2, NULL, NULL),
(369, 100, 2, NULL, NULL),
(370, 101, 2, NULL, NULL),
(371, 102, 2, NULL, NULL),
(372, 107, 2, NULL, NULL),
(373, 108, 2, NULL, NULL),
(374, 109, 2, NULL, NULL),
(375, 110, 2, NULL, NULL),
(376, 112, 2, NULL, NULL),
(377, 113, 2, NULL, NULL),
(378, 114, 2, NULL, NULL),
(379, 115, 2, NULL, NULL),
(380, 117, 2, NULL, NULL),
(381, 118, 2, NULL, NULL),
(382, 119, 2, NULL, NULL),
(383, 120, 2, NULL, NULL),
(384, 122, 2, NULL, NULL),
(385, 123, 2, NULL, NULL),
(386, 124, 2, NULL, NULL),
(387, 125, 2, NULL, NULL),
(388, 127, 2, NULL, NULL),
(389, 128, 2, NULL, NULL),
(390, 129, 2, NULL, NULL),
(391, 130, 2, NULL, NULL),
(392, 132, 2, NULL, NULL),
(393, 133, 2, NULL, NULL),
(394, 134, 2, NULL, NULL),
(395, 135, 2, NULL, NULL),
(396, 137, 2, NULL, NULL),
(397, 138, 2, NULL, NULL),
(398, 139, 2, NULL, NULL),
(399, 140, 2, NULL, NULL),
(400, 142, 2, NULL, NULL),
(401, 143, 2, NULL, NULL),
(402, 144, 2, NULL, NULL),
(403, 145, 2, NULL, NULL),
(404, 147, 2, NULL, NULL),
(405, 148, 2, NULL, NULL),
(406, 149, 2, NULL, NULL),
(407, 150, 2, NULL, NULL),
(408, 151, 2, NULL, NULL),
(479, 152, 2, '2020-01-16 14:45:42', '2020-01-16 14:45:42'),
(480, 153, 2, '2020-01-16 14:45:42', '2020-01-16 14:45:42'),
(481, 154, 2, '2020-01-16 14:45:42', '2020-01-16 14:45:42'),
(482, 155, 2, '2020-01-16 14:45:42', '2020-01-16 14:45:42'),
(483, 156, 2, '2020-01-16 14:45:42', '2020-01-16 14:45:42'),
(484, 157, 2, '2020-01-16 14:45:42', '2020-01-16 14:45:42'),
(485, 158, 2, '2020-01-16 14:45:43', '2020-01-16 14:45:43'),
(486, 159, 2, '2020-01-16 14:45:43', '2020-01-16 14:45:43'),
(487, 160, 2, '2020-01-16 14:45:43', '2020-01-16 14:45:43');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '0-Hoạt động'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `photos`
--

INSERT INTO `photos` (`id`, `photo`, `created_at`, `updated_at`, `status`) VALUES
(1, 'shoe7.jpg', '2019-12-28 00:01:35', '2019-12-28 00:01:35', 0),
(2, 'shoe9.jpg', '2019-12-28 00:01:35', '2019-12-28 00:01:35', 0),
(3, 'shoe10.jpg', '2019-12-28 00:01:35', '2019-12-28 00:01:35', 0),
(4, 'shoe10.jpg', '2019-12-28 00:01:35', '2019-12-28 00:01:35', 0),
(5, 'shoe8.jpg', '2019-12-28 00:01:35', '2019-12-28 00:01:35', 0),
(6, 'shoe5.jpg', '2020-01-07 17:37:54', '2020-01-07 17:37:54', 0),
(7, 'shoe7.jpg', '2020-01-07 17:37:54', '2020-01-07 17:37:54', 0),
(8, 'shoe7.jpg', '2020-01-07 17:37:54', '2020-01-07 17:37:54', 0),
(9, 'shoe8.jpg', '2020-01-07 17:37:54', '2020-01-07 17:37:54', 0),
(10, 'shoe9.jpg', '2020-01-07 17:37:54', '2020-01-07 17:37:54', 0),
(11, 'shoe10.jpg', '2020-01-07 17:38:15', '2020-01-07 17:38:15', 0),
(13, 'shoe8.jpg', '2020-01-07 17:38:15', '2020-01-07 17:38:15', 0),
(14, 'shoe7.jpg', '2020-01-07 17:38:15', '2020-01-07 17:38:15', 0),
(15, 'shoe9.jpg', '2020-01-07 17:38:15', '2020-01-07 17:38:15', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `photo_product`
--

CREATE TABLE `photo_product` (
  `id` int(12) NOT NULL,
  `color_id` int(12) UNSIGNED NOT NULL,
  `product_id` int(12) UNSIGNED NOT NULL,
  `photo_id` int(12) UNSIGNED NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `update_at` varchar(255) NOT NULL,
  `_token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL,
  `sale_off` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) DEFAULT 0 COMMENT '0 hoạt động 1 không hoạt động',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `title`, `slug`, `description`, `category_id`, `price`, `sale_off`, `quantity`, `user_id`, `status`, `created_at`, `updated_at`, `_token`) VALUES
(2, 'Dr.', 'nihil-nostrum-at-tenetur-consequatur', 'Non distinctio consectetur animi aliquam. Omnis quod et eos odio rerum qui. Dolorem ut id ut nostrum suscipit. Asperiores sunt debitis est facere saepe fuga.', 23, 40000, NULL, 6, 1, 0, '2020-01-07 17:45:49', '2020-01-07 17:45:49', NULL),
(5, 'Dr.', 'repellat-odit-repellat-modi-sit-excepturi', 'Ab distinctio quo dolorum porro natus. Voluptas incidunt odio quia vel et. Incidunt dolores esse sed placeat nulla fugit assumenda.', 25, 30000, NULL, 1, 1, 0, '2020-01-07 17:45:49', '2020-01-07 17:45:49', NULL),
(7, 'Miss', 'laudantium-perspiciatis-consectetur-placeat-voluptate', 'Laborum reiciendis tempore amet animi asperiores. Laudantium et alias voluptas explicabo ut error ut. Dolorem id hic dolore culpa blanditiis. Qui rerum consectetur est eum rerum necessitatibus.', 23, 900000, NULL, 1, 1, 0, '2020-01-07 17:45:49', '2020-01-07 17:45:49', NULL),
(8, 'Dr.', 'minima-ab-velit-aut-nulla-explicabo-odit-minus', 'Itaque natus dolorum nostrum odio voluptate esse. Deleniti rerum animi optio sed. Cum nihil harum rerum sit et dolore. Culpa itaque ratione nemo culpa ut.', 19, 1000000, NULL, 0, 1, 0, '2020-01-07 17:45:49', '2020-01-07 17:45:49', NULL),
(40, 'KACY Black Patent', 'kacy-black-patent-40.html', 'Product Details\r\n\r\nYou can always rely on these black patent chunky lace up boots. They look great with denim to dresses. Upper: 100% Polyurethane. Specialist Clean Only.\r\nColour: BLACK\r\nProduct Code: 42K01SBLA', 12, 2000000, NULL, NULL, 1, 0, '2020-01-09 20:20:39', '2020-01-09 20:20:39', NULL),
(44, 'Mr.', 'tenetur-quibusdam-quia-hic-possimus-non-aut-unde', 'Culpa cupiditate deserunt consequuntur libero. Iste autem fugiat nostrum dolor ex voluptas eum. Numquam veritatis provident ratione.', 19, 1, NULL, 4, 1, 0, '2020-01-14 18:09:21', '2020-01-14 18:09:21', NULL),
(45, 'Mrs.', 'dolorem-rem-saepe-qui-dicta-libero', 'Aliquid omnis vitae quibusdam ab. Nam consequatur quo doloremque ratione odit sint aut. Nihil sit architecto ut quos. Est omnis magnam iure ipsam. Voluptatem alias est cupiditate ut.', 1, 6, NULL, 7, 1, 0, '2020-01-14 18:09:21', '2020-01-14 18:09:21', NULL),
(46, 'Miss', 'dolorem-repellat-ut-iusto-eum-perferendis-repellendus-magni', 'Quod est ut ut qui quae. Itaque rem nulla sed. Provident at eos possimus sint nihil veniam.', 13, 2, NULL, 8, 1, 0, '2020-01-14 18:09:21', '2020-01-14 18:09:21', NULL),
(47, 'Mr.', 'temporibus-dolorem-nisi-corrupti-quo-neque-dolorem-accusantium', 'Laudantium quis placeat error non tenetur placeat. Blanditiis ad labore et et deserunt sed. Qui sunt praesentium quia ad distinctio magnam neque.', 19, 3, NULL, 7, 1, 0, '2020-01-14 18:09:21', '2020-01-14 18:09:21', NULL),
(48, 'Dr.', 'et-tenetur-dolorem-minima-eaque-debitis-cumque', 'Tempora inventore sed quis. Quia tempora illo quia debitis. Et iusto dolorum officiis ut. Totam aliquam non quaerat alias.', 1, 9, NULL, 3, 1, 0, '2020-01-14 18:09:21', '2020-01-14 18:09:21', NULL),
(49, 'Prof.', 'esse-voluptatem-nesciunt-aperiam', 'Totam neque quos accusamus numquam. Sed eaque labore fuga. Dolore atque et iusto error consequatur sed quod.', 19, 8, NULL, 3, 1, 0, '2020-01-14 18:09:21', '2020-01-14 18:09:21', NULL),
(50, 'vitttt', 'vitttt-50.html', '<p>Quia ullam nesciunt suscipit repellendus necessitatibus. Qui et non non.</p>', 12, 8, NULL, 1, 1, 0, '2020-01-14 18:09:21', '2020-01-14 18:33:56', NULL),
(53, 'vittt', 'vittt-53.html', '<p>Veritatis aperiam rem iste qui porro. Est maiores aut ut et deserunt. Eos occaecati et provident eos.</p>', 12, 5, NULL, 6, 1, 0, '2020-01-14 18:09:21', '2020-01-14 18:29:08', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `receipts`
--

CREATE TABLE `receipts` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_of_receipt` date NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `total` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0- Hoạt đông 1 Không hoạt động',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `receipts`
--

INSERT INTO `receipts` (`id`, `date_of_receipt`, `supplier_id`, `user_id`, `total`, `status`, `created_at`, `updated_at`) VALUES
(1, '1973-09-20', 5, 1, NULL, 2, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(2, '1987-08-08', 8, 1, NULL, 2, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(3, '1995-05-28', 2, 1, NULL, 2, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(4, '2002-01-08', 7, 1, NULL, 2, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(5, '1993-02-03', 4, 1, NULL, 2, '2020-01-07 18:07:33', '2020-01-07 18:07:33'),
(6, '1992-11-08', 9, 1, NULL, 2, '2020-01-07 18:07:33', '2020-01-07 18:07:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `receipt_details`
--

CREATE TABLE `receipt_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `receipt_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT 0,
  `tax` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `color_id` int(12) DEFAULT NULL,
  `size_id` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `receipt_details`
--

INSERT INTO `receipt_details` (`id`, `receipt_id`, `product_id`, `quantity`, `price`, `tax`, `created_at`, `updated_at`, `color_id`, `size_id`) VALUES
(3, 4, 2, 6, 0, 0, '2020-01-07 18:07:33', '2020-01-07 18:07:33', 12, 5),
(4, 6, 5, 1, 2, 1, '2020-01-07 18:07:33', '2020-01-07 18:07:33', 7, 6),
(5, 5, 2, 3, 6, 6, '2020-01-07 18:07:33', '2020-01-07 18:07:33', 11, 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `revenues`
--

CREATE TABLE `revenues` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number_role` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `display`, `number_role`, `created_at`, `updated_at`) VALUES
(1, 'role_member', 'Member', 3, '2019-12-28 00:01:35', '2020-01-16 14:20:06'),
(2, 'role_admin', 'Admin', 4, '2019-12-28 00:01:35', '2020-01-16 14:14:09'),
(3, 'role_superadmin', 'SuperAdmin', 5, '2019-12-28 00:01:35', '2019-12-28 00:01:35'),
(4, 'role_test', 'Test', 2, '2019-12-28 00:01:35', '2020-01-02 13:44:37'),
(8, '1', '1', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sizes`
--

INSERT INTO `sizes` (`id`, `size`, `status`, `created_at`, `updated_at`, `_token`) VALUES
(3, '41', 0, '2019-12-28 00:01:35', '2020-01-09 14:39:39', '4ziRVvgnJiZTpacKcnjcyiIEeNo2iXO2WFv4L9Hk'),
(4, '27', 0, '2019-12-28 00:01:35', '2019-12-28 00:01:35', NULL),
(5, '28', 0, '2019-12-28 00:01:35', '2020-01-01 17:54:24', 'LpbRm0SEEAFJe9RFMvjmyF8AivSa0vkJtdWdOJA3'),
(6, '31', 0, '2020-01-07 17:37:54', '2020-01-07 17:37:54', NULL),
(7, '50', 0, '2020-01-14 18:47:07', '2020-01-14 18:47:07', NULL),
(8, '43', 0, '2020-01-14 18:47:19', '2020-01-14 18:47:19', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `style_products`
--

CREATE TABLE `style_products` (
  `id` int(12) UNSIGNED NOT NULL,
  `color_id` int(12) UNSIGNED DEFAULT NULL,
  `product_id` int(12) UNSIGNED DEFAULT NULL,
  `size_id` int(12) UNSIGNED DEFAULT NULL,
  `photo_id` int(12) UNSIGNED DEFAULT NULL,
  `quantity` int(12) DEFAULT NULL,
  `sort_order` int(2) DEFAULT NULL,
  `status` int(1) DEFAULT 0 COMMENT '0-Hoạt động 1 Khoá',
  `updated_at` varchar(20) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `style_products`
--

INSERT INTO `style_products` (`id`, `color_id`, `product_id`, `size_id`, `photo_id`, `quantity`, `sort_order`, `status`, `updated_at`, `created_at`) VALUES
(1, 11, 7, 5, 11, 2, 1, 1, '2020-01-08 16:28:44', '2020-01-08 16:28:44'),
(2, 11, 2, 3, 11, 8, 10, 1, '2020-01-08 16:28:44', '2020-01-08 16:28:44'),
(4, 11, 2, 4, 4, 10, 9, 1, '2020-01-08 16:28:44', '2020-01-08 16:28:44'),
(7, 11, 7, 6, 15, 5, 5, 2, '2020-01-08 16:29:02', '2020-01-08 16:29:02'),
(8, 7, 8, 5, 3, 3, 1, 2, '2020-01-08 16:29:02', '2020-01-08 16:29:02'),
(13, 11, 8, 3, 4, 4, 8, 1, '2020-01-08 16:29:20', '2020-01-08 16:29:20'),
(147, 7, 5, 5, NULL, 2, NULL, 0, '2020-01-15 08:07:57', '2020-01-15 08:07:57'),
(148, 11, 2, 5, 1, 8, 7, 1, '2020-01-15 08:08:48', '2020-01-15 08:08:48'),
(149, 11, 7, 6, 5, 3, 5, 1, '2020-01-15 08:08:48', '2020-01-15 08:08:48'),
(150, 11, 5, 6, 5, 9, 7, 1, '2020-01-15 08:08:48', '2020-01-15 08:08:48'),
(151, 7, 40, 4, 2, 3, 2, 1, '2020-01-15 08:08:48', '2020-01-15 08:08:48'),
(152, 7, 7, 4, 8, 6, 8, 2, '2020-01-15 08:08:48', '2020-01-15 08:08:48'),
(153, 7, 7, 6, 14, 9, 6, 1, '2020-01-15 08:08:52', '2020-01-15 08:08:52'),
(154, 11, 40, 5, 3, 2, 3, 1, '2020-01-15 08:08:52', '2020-01-15 08:08:52'),
(155, 7, 40, 6, 2, 1, 6, 1, '2020-01-15 08:08:52', '2020-01-15 08:08:52'),
(156, 7, 40, 4, 10, 7, 4, 1, '2020-01-15 08:08:52', '2020-01-15 08:08:52'),
(157, 11, 40, 3, 2, 9, 9, 2, '2020-01-15 08:08:52', '2020-01-15 08:08:52'),
(158, 11, 2, 3, 8, 1, 9, 2, '2020-01-15 08:09:03', '2020-01-15 08:09:03'),
(159, 7, 5, 5, 10, 1, 4, 2, '2020-01-15 08:09:03', '2020-01-15 08:09:03'),
(160, 11, 7, 3, 5, 1, 6, 1, '2020-01-15 08:09:03', '2020-01-15 08:09:03'),
(161, 7, 2, 6, 6, 8, 3, 2, '2020-01-15 08:09:03', '2020-01-15 08:09:03'),
(162, 7, 7, 5, 11, 4, 5, 2, '2020-01-15 08:09:03', '2020-01-15 08:09:03'),
(163, 11, 45, 6, 9, 4, 9, 2, '2020-01-15 08:09:21', '2020-01-15 08:09:21'),
(164, 7, 5, 4, 13, 10, 10, 1, '2020-01-15 08:09:21', '2020-01-15 08:09:21'),
(165, 11, 8, 5, 1, 7, 1, 2, '2020-01-15 08:09:21', '2020-01-15 08:09:21'),
(166, 11, 8, 6, 9, 4, 3, 2, '2020-01-15 08:09:21', '2020-01-15 08:09:21'),
(168, 7, 7, 6, 11, 1, 10, 2, '2020-01-15 08:09:29', '2020-01-15 08:09:29'),
(169, 11, 45, 4, 1, 7, 7, 2, '2020-01-15 08:09:29', '2020-01-15 08:09:29'),
(170, 7, 47, 3, 2, 5, 5, 1, '2020-01-15 08:09:29', '2020-01-15 08:09:29'),
(171, 11, 47, 3, 2, 9, 6, 2, '2020-01-15 08:09:30', '2020-01-15 08:09:30'),
(172, 11, 7, 4, 5, 5, 9, 1, '2020-01-15 08:09:30', '2020-01-15 08:09:30'),
(174, 7, 49, 6, 14, 8, 5, 2, '2020-01-15 08:09:35', '2020-01-15 08:09:35'),
(175, 11, 47, 6, 10, 5, 9, 2, '2020-01-15 08:09:35', '2020-01-15 08:09:35'),
(176, 7, 48, 3, 7, 3, 6, 2, '2020-01-15 08:09:35', '2020-01-15 08:09:35'),
(177, 7, 49, 3, 6, 8, 4, 2, '2020-01-15 08:09:35', '2020-01-15 08:09:35'),
(178, 7, 48, 4, 14, 9, 5, 1, '2020-01-15 08:09:36', '2020-01-15 08:09:36'),
(179, 11, 44, 6, 5, 4, 3, 1, '2020-01-15 08:09:36', '2020-01-15 08:09:36'),
(180, 7, 8, 4, 11, 3, 3, 1, '2020-01-15 08:09:36', '2020-01-15 08:09:36'),
(181, 7, 7, 5, 9, 3, 2, 1, '2020-01-15 08:09:36', '2020-01-15 08:09:36'),
(182, 7, 45, 6, 6, 5, 3, 2, '2020-01-15 08:09:36', '2020-01-15 08:09:36'),
(183, 7, 7, 3, 9, 8, 7, 2, '2020-01-15 08:09:41', '2020-01-15 08:09:41'),
(184, 7, 5, 4, 5, 5, 8, 2, '2020-01-15 08:09:41', '2020-01-15 08:09:41'),
(186, 7, 46, 3, 11, 2, 1, 2, '2020-01-15 08:09:41', '2020-01-15 08:09:41'),
(187, 7, 49, 4, 9, 2, 2, 1, '2020-01-15 08:09:41', '2020-01-15 08:09:41'),
(189, 11, 45, 5, 14, 4, 2, 2, '2020-01-15 08:09:42', '2020-01-15 08:09:42'),
(190, 11, 48, 6, 4, 10, 3, 1, '2020-01-15 08:09:42', '2020-01-15 08:09:42'),
(191, 11, 44, 6, 11, 4, 4, 2, '2020-01-15 08:09:42', '2020-01-15 08:09:42'),
(192, 7, 48, 6, 6, 7, 9, 2, '2020-01-15 08:09:42', '2020-01-15 08:09:42'),
(193, 7, 7, 4, 14, 6, 1, 1, '2020-01-15 08:09:46', '2020-01-15 08:09:46'),
(194, 11, 46, 6, 11, 9, 7, 1, '2020-01-15 08:09:46', '2020-01-15 08:09:46'),
(195, 11, 8, 6, 5, 6, 9, 2, '2020-01-15 08:09:46', '2020-01-15 08:09:46'),
(196, 7, 7, 5, 15, 9, 8, 2, '2020-01-15 08:09:46', '2020-01-15 08:09:46'),
(197, 7, 49, 4, 5, 8, 7, 1, '2020-01-15 08:09:46', '2020-01-15 08:09:46'),
(198, 11, 5, 3, 7, 6, 9, 1, '2020-01-15 08:09:47', '2020-01-15 08:09:47'),
(199, 7, 49, 5, 11, 2, 5, 1, '2020-01-15 08:09:47', '2020-01-15 08:09:47'),
(200, 11, 45, 5, 14, 9, 8, 1, '2020-01-15 08:09:47', '2020-01-15 08:09:47'),
(202, 7, 49, 5, 1, 8, 10, 2, '2020-01-15 08:09:47', '2020-01-15 08:09:47'),
(204, 11, 2, 3, 10, 9, 9, 2, '2020-01-15 08:09:50', '2020-01-15 08:09:50'),
(205, 11, 49, 5, 5, 7, 9, 2, '2020-01-15 08:09:50', '2020-01-15 08:09:50'),
(207, 11, 49, 4, 10, 7, 4, 1, '2020-01-15 08:09:50', '2020-01-15 08:09:50'),
(208, 11, 49, 6, 9, 1, 10, 2, '2020-01-15 08:09:54', '2020-01-15 08:09:54'),
(209, 7, 47, 3, 13, 1, 1, 1, '2020-01-15 08:09:54', '2020-01-15 08:09:54'),
(210, 7, 44, 5, 9, 8, 1, 2, '2020-01-15 08:09:54', '2020-01-15 08:09:54'),
(211, 11, 45, 5, 15, 7, 3, 1, '2020-01-15 08:09:54', '2020-01-15 08:09:54'),
(212, 11, 49, 3, 1, 2, 3, 2, '2020-01-15 08:09:54', '2020-01-15 08:09:54'),
(213, 11, 2, 6, 10, 8, 3, 2, '2020-01-15 08:09:55', '2020-01-15 08:09:55'),
(214, 7, 47, 6, 5, 9, 8, 1, '2020-01-15 08:09:55', '2020-01-15 08:09:55'),
(215, 11, 44, 3, 2, 7, 7, 1, '2020-01-15 08:09:55', '2020-01-15 08:09:55'),
(216, 11, 5, 3, 3, 10, 1, 2, '2020-01-15 08:09:55', '2020-01-15 08:09:55'),
(292, 7, 53, 3, NULL, 1, NULL, 0, '2020-01-15 08:37:31', '2020-01-15 08:37:31'),
(293, 7, 53, 4, NULL, 2, NULL, 0, '2020-01-15 08:37:31', '2020-01-15 08:37:31'),
(294, 7, 50, 3, NULL, 1, NULL, 0, '2020-01-15 15:39:38', '2020-01-15 15:39:38'),
(295, 13, 50, 4, NULL, 1, NULL, 0, '2020-01-15 15:39:38', '2020-01-15 15:39:38');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-hoạt động 1- không hoạt động',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `country`, `address`, `phone`, `email`, `description`, `status`, `created_at`, `updated_at`, `_token`) VALUES
(1, 'Aurelie Feeney', 'Cambodia', '826 Tomasa Pine\nKundetown, WI 62863-1701', '+1 (998) 990-4907', 'ruecker.wilton@hotmail.com', 'Et dolores veritatis et eum harum atque tempore. Quo est quibusdam labore et quis maxime laudantium autem. Voluptatem blanditiis magnam exercitationem vitae fuga voluptas. Aliquid expedita doloribus et asperiores.', 0, '2019-12-28 00:01:35', '2019-12-28 00:01:35', NULL),
(2, 'Cornell Wolff', 'Romania', '825 Powlowski Forks\nCollinstown, TN 47405', '(804) 577-7916 x39418', 'smoore@runte.biz', 'Provident quae culpa consequatur laudantium modi quia reprehenderit autem. Facilis ut nisi quibusdam animi nihil illo architecto corporis. Deleniti consequuntur ut blanditiis non expedita vitae. Quia voluptas atque velit delectus vel porro consequatur.', 0, '2019-12-28 00:01:35', '2019-12-28 00:01:35', NULL),
(4, 'Antonina Vandervort', 'Heard Island and McDonald Islands', '9836 Jermain Corner\nSouth Jaquelinburgh, WA 56848', '1-495-324-8333 x66977', 'dgleason@monahan.com', 'Similique asperiores omnis iure nemo. Iusto aliquam in repellat placeat. Omnis ea vel possimus.', 0, '2019-12-28 00:01:35', '2019-12-28 00:01:35', NULL),
(5, 'Aurelio Stiedemann II', 'Malaysia', '90219 Carter Corner\nPort Keven, WY 43415', '(916) 735-5858 x717', 'hahn.dedrick@hotmail.com', 'Ut enim aliquid dolores fugit voluptas omnis sequi consequatur. Ratione sint vel itaque ea voluptatem perspiciatis dolores. Rerum voluptas quia consectetur aut.', 0, '2019-12-28 00:01:35', '2019-12-28 00:01:35', NULL),
(7, 'Leora Robel PhD', 'Mauritania', '51254 Eliza Ferry\nWest Alecland, NC 82224-2599', '635.380.5381', 'hudson32@heidenreich.com', 'Accusamus tempora repudiandae molestiae praesentium cum eveniet illo. Voluptate nostrum consectetur vel totam voluptatem. Tempora dolor temporibus qui est.', 0, '2019-12-31 15:01:00', '2019-12-31 15:01:00', NULL),
(8, 'Mrs. Nannie Dickens', 'Sao Tome and Principe', '387 Hammes Pines Suite 541\nNorth Rowlandtown, KY 63614', '261.724.2207 x0994', 'qkessler@hackett.com', 'Libero voluptatibus reiciendis consequatur asperiores explicabo iste consequatur. In ut aut est ut fugiat molestias voluptatum. Velit delectus libero nihil temporibus.', 0, '2019-12-31 15:01:00', '2019-12-31 15:01:00', NULL),
(9, 'Katrine Bernhard', 'Gibraltar', '62348 Jaclyn Harbor Apt. 199North Felicity, WV 32533', '0976820967', 'twalter@baumbach.info', '<p>Laborum qui deleniti illum corporis qui. Quasi enim soluta aut eos. Rerum maiores asperiores quia et.</p>', 0, '2019-12-31 15:01:00', '2020-01-16 14:48:25', '28B3amcNDhkm937y8YR5hJ3GZb3YplCQtDUtBulJ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0: hoạt đông 1- khoá 2- không hoạt động',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyreset` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timeout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `address`, `phone`, `birthday`, `gender`, `avatar`, `role_id`, `status`, `remember_token`, `created_at`, `updated_at`, `_token`, `keyreset`, `timeout`, `otp`) VALUES
(1, 'Viet Anh', 'lwilderman', 'kieuvietanh8597@gmail.com', '2020-01-07 17:34:19', '$2y$10$LT5zAkVdrryvMzzDjXK9sOfXT9DGHilGBmU9HBOY4JQ.yBn8mCZ7C', '2464 Altenwerth Plain Suite 173East Sammie, IN 72230', '0976820967', '761616000', '1', '64322314_674567853008530_3719096736627556352_n.png', 2, 0, 'W7tXrZTBS4yA977cUlCS1lL8HmLgkMAzkwWY63tK06HBu8oeH3staYXBrxNE', '2020-01-07 17:34:19', '2020-01-15 01:39:15', '3KYwGKAq1D6LA3K1DST3nwd28CNOyOIbBLzHUZeE', '', '', ''),
(2, 'test', NULL, 'test@gmail.com', NULL, '$2y$10$4AVNJmxYGMCgvTs/KAsTeeYZ2uEF8Ho2foIID7FkTPbryBWV5/Kmq', 'HN', NULL, '1900-01-01 00:00:00', '1', 'backgroud_login.jpg', 4, 0, NULL, '2020-01-15 01:44:53', '2020-01-15 01:44:53', NULL, NULL, NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `config_notifications`
--
ALTER TABLE `config_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Chỉ mục cho bảng `delivery_notes`
--
ALTER TABLE `delivery_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_notes_customer_id_foreign` (`customer_id`),
  ADD KEY `delivery_notes_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `delivery_note_details`
--
ALTER TABLE `delivery_note_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_note_details_delivery_note_id_foreign` (`delivery_note_id`),
  ADD KEY `delivery_note_details_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `headers`
--
ALTER TABLE `headers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `permission_roles`
--
ALTER TABLE `permission_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_roles_role_id_foreign` (`role_id`),
  ADD KEY `permission_roles_permission_id_foreign` (`permission_id`);

--
-- Chỉ mục cho bảng `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `photo_product`
--
ALTER TABLE `photo_product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receipts_supplier_id_foreign` (`supplier_id`),
  ADD KEY `receipts_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receipt_details_receipt_id_foreign` (`receipt_id`),
  ADD KEY `receipt_details_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `revenues`
--
ALTER TABLE `revenues`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `style_products`
--
ALTER TABLE `style_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `color_id` (`color_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `photo_id` (`photo_id`),
  ADD KEY `size_id` (`size_id`);

--
-- Chỉ mục cho bảng `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `suppliers_email_unique` (`email`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT cho bảng `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `config_notifications`
--
ALTER TABLE `config_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `delivery_notes`
--
ALTER TABLE `delivery_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `delivery_note_details`
--
ALTER TABLE `delivery_note_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `headers`
--
ALTER TABLE `headers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT cho bảng `permission_roles`
--
ALTER TABLE `permission_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=488;

--
-- AUTO_INCREMENT cho bảng `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `photo_product`
--
ALTER TABLE `photo_product`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT cho bảng `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `receipt_details`
--
ALTER TABLE `receipt_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `revenues`
--
ALTER TABLE `revenues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `style_products`
--
ALTER TABLE `style_products`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;

--
-- AUTO_INCREMENT cho bảng `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `delivery_notes`
--
ALTER TABLE `delivery_notes`
  ADD CONSTRAINT `delivery_notes_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `delivery_notes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `delivery_note_details`
--
ALTER TABLE `delivery_note_details`
  ADD CONSTRAINT `delivery_note_details_delivery_note_id_foreign` FOREIGN KEY (`delivery_note_id`) REFERENCES `delivery_notes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `delivery_note_details_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `permission_roles`
--
ALTER TABLE `permission_roles`
  ADD CONSTRAINT `permission_roles_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `receipts`
--
ALTER TABLE `receipts`
  ADD CONSTRAINT `receipts_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `receipts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD CONSTRAINT `receipt_details_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `receipt_details_receipt_id_foreign` FOREIGN KEY (`receipt_id`) REFERENCES `receipts` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `style_products`
--
ALTER TABLE `style_products`
  ADD CONSTRAINT `style_products_ibfk_1` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`),
  ADD CONSTRAINT `style_products_ibfk_2` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`),
  ADD CONSTRAINT `style_products_ibfk_3` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`),
  ADD CONSTRAINT `style_products_ibfk_4` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
