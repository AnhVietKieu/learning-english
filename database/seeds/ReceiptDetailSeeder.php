<?php

use Illuminate\Database\Seeder;

class ReceiptDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\ReceiptDetail::class,5)->create();
    }
}
