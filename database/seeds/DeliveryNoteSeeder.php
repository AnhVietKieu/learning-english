<?php

use Illuminate\Database\Seeder;

class DeliveryNoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\DeliveryNote::class,5)->create();
    }
}
