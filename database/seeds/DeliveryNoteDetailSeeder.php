<?php

use Illuminate\Database\Seeder;

class DeliveryNoteDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\DeliveryNoteDetail::class,5)->create();
    }
}
