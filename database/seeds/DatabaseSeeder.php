<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
//        $this->call(RoleSeeder::class);
//        $this->call(UserSeeder::class);
//        $this->call(CategorySeeder::class);
//        $this->call(ColorSeeder::class);
//        $this->call(PhotoSeeder::class);
//        $this->call(SizeSeeder::class);
//        $this->call(ProductSeeder::class);
//        $this->call(ColorProductSeeder::class);
//        $this->call(PhotoProductSeeder::class);
        $this->call(SizeProductSeeder::class);
//        $this->call(CustomerSeeder::class);
//        $this->call(DeliveryNoteSeeder::class);
//        $this->call(DeliveryNoteDetailSeeder::class);
//        $this->call(PermissionSeeder::class);
//        $this->call(PermissionRoleSeeder::class);
//        $this->call(SupplierSeeder::class);
//        $this->call(ReceiptSeeder::class);
//        $this->call(ReceiptDetailSeeder::class);
    }
}
