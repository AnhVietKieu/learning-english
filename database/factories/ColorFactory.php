<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Color;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Color::class, function (Faker $faker) {
    return [
        'color' => $faker->colorName
    ];
});
