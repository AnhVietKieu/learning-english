<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use App\DeliveryNote;
use App\Model;
use App\User;
use Faker\Generator as Faker;

$factory->define(DeliveryNote::class, function (Faker $faker) {
    return [
        'date_of_delivery_note' => $faker->date(),
        'customer_id' => Customer::all(['id'])->random(),
        'user_id' => User::all(['id'])->random(),
        'total' => $faker->randomNumber(5),
        'delivery_price' => $faker->randomNumber(2),
        'status' => $faker->numberBetween(0,2)
    ];
});
