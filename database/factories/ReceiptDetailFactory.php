<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Product;
use App\Receipt;
use App\ReceiptDetail;
use Faker\Generator as Faker;

$factory->define(ReceiptDetail::class, function (Faker $faker) {
    return [
        'receipt_id' => Receipt::all(['id'])->random(),
        'product_id' => Product::all(['id'])->random(),
        'quantity' => $faker->randomDigit,
        'price' => $faker->randomDigit,
        'tax' => $faker->randomDigit
    ];
});
