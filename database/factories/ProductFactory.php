<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Color;
use App\Model;
use App\Photo;
use App\Product;
use App\Size;
use App\User;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'slug' => $faker->slug,
        'description' => $faker->text,
        'category_id' => Category::all(['id'])->random(),
        'price' => $faker->randomDigit,
        'quantity' => $faker->randomDigit,
        'user_id' => User::all(['id'])->random()
    ];
});
