<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->userName,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',  //password
        'email' => $faker->email,
        'address' => $faker->address,
        'birthday' => $faker->date(),
        'gender' => $faker->randomDigit,
        'status_notification' => $faker->randomDigit,
        'customer_type' => $faker->numberBetween(0,5),
        'status' => $faker->randomDigit
    ];
});
