<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Photo;
use App\PhotoProduct;
use App\Product;
use App\Color;
use Faker\Generator as Faker;

$factory->define(PhotoProduct::class, function (Faker $faker) {
    return [
        'photo_id' => Photo::all(['id'])->random(),
        'product_id' => Product::all(['id'])->random(),
        'color_id' => Color::all(['id'])->random(),
        'sort_order' => $faker->numberBetween(1,10),
        'status' => $faker->numberBetween(1,2)
    ];
});
