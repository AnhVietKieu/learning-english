<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Permission;
use App\PermissionRole;
use App\Role;
use Faker\Generator as Faker;

$factory->define(PermissionRole::class, function (Faker $faker) {
    return [
        'permission_id' => Permission::all(['id'])->random(),
        'role_id' => Role::all(['id'])->random()
    ];
});
