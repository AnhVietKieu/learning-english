<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DeliveryNote;
use App\DeliveryNoteDetail;
use App\Model;
use App\Product;
use Faker\Generator as Faker;

$factory->define(DeliveryNoteDetail::class, function (Faker $faker) {
    return [
        'delivery_note_id' => DeliveryNote::all(['id'])->random(),
        'product_id' => Product::all(['id'])->random(),
        'quantity' => $faker->randomNumber(2),
        'price' => $faker->randomNumber(5),
        'tax' => $faker->randomNumber(2),
    ];
});
