<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Receipt;
use Faker\Generator as Faker;

$factory->define(Receipt::class, function (Faker $faker) {
    return [
        'date_of_receipt' => $faker->date(),
        'supplier_id' => \App\Supplier::all(['id'])->random(),
        'user_id' => \App\User::all(['id'])->random(),
        'status' => $faker->numberBetween(1,2)
    ];
});
