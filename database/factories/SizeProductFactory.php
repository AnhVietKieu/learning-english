<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Product;
use App\Size;
use App\SizeProduct;
use App\Color;
use App\Photo;
use Faker\Generator as Faker;

$factory->define(\App\StyleProducts::class, function (Faker $faker) {
    return [
        'size_id' => Size::all(['id'])->random(),
        'product_id' => Product::all(['id'])->random(),
        'color_id' =>Color::all(['id'])->random(),
        'photo_id' =>Photo::all(['id'])->random(),
        'quantity' =>$faker->numberBetween(1,10),
        'sort_order' => $faker->numberBetween(1,10),
        'status' => $faker->numberBetween(1,2)
    ];
});
