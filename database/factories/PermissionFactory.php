<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Permission;
use Faker\Generator as Faker;

$factory->define(Permission::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'display' => '1',
        'parent_id' => '1'
    ];
});
