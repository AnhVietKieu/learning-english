<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Role;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('123456'), // password
        'remember_token' => Str::random(10),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'birthday' => $faker->date(),
        'gender' => $faker->numberBetween(0,1),
        'avatar' => $faker->imageUrl(),
        'role_id' => Role::all(['id'])->random(),
        'status' => $faker->numberBetween($min = 0, $max = 3),
    ];
});
