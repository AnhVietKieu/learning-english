<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersToAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
        Schema::table('permission_roles', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
        });
        // Schema::table('products', function (Blueprint $table) {
        //     $table->foreign('size_id')->references('id')->on('sizes')->onDelete('cascade');
        //     $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade');
        //     $table->foreign('photo_id')->references('id')->on('photos')->onDelete('cascade');
        //     $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        //     $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        // });
        // Schema::table('size_products', function (Blueprint $table) {
        //     $table->foreign('size_id')->references('id')->on('sizes')->onDelete('cascade');
        //     $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        // });
        // Schema::table('photo_products', function (Blueprint $table) {
        //     $table->foreign('photo_id')->references('id')->on('photos')->onDelete('cascade');
        //     $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        // });
        // Schema::table('color_products', function (Blueprint $table) {
        //     $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade');
        //     $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        // });
        // Schema::table('receipts', function (Blueprint $table) {
        //     $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
        //     $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        // });
        // Schema::table('receipt_details', function (Blueprint $table) {
        //     $table->foreign('receipt_id')->references('id')->on('receipts')->onDelete('cascade');
        //     $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        // });
        // Schema::table('delivery_notes', function (Blueprint $table) {
        //     $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        //     $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        // });
        // Schema::table('delivery_note_details', function (Blueprint $table) {
        //     $table->foreign('delivery_note_id')->references('id')->on('delivery_notes')->onDelete('cascade');
        //     $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
